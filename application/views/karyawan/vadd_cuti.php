<div class="row animated fadeInRight">
    <div class="col-md-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Pengajuan Cuti</h5>
            </div>
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Jenis Cuti <b style="color: red">*</b></label>
                            <div class="col-sm-9">
                                <select name="jenis_leave" class="form-control" id="colorselector">
                                    <?php foreach ($jeniscuti as $row) { ?>
                                        <option value="<?php echo $row->id; ?>">
                                            <?php echo $row->name; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Nama</label>
                            <div class="col-sm-9">
                                <input type="text" readonly="" value="<?php echo $this->session->userdata('nama'); ?>" name="nama_lengkap"
                                class="form-control">
                            </div>
                        </div>
                        <form  autocomplete="off" id="cuti" method="post" action="<?php echo base_url(); ?>karyawan/simpan_cuti" class="form-horizontal">
                            <div id="1" class="colors">
                                <input type="hidden" value="1" name="jenis_leave">
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <input type="hidden" value="<?php echo $this->session->userdata('nama'); ?>" name="nama_lengkap">
                                <?php foreach ($sisa as $rows) { ?>
                                    <div class="form-group row">
                                        <label class="col-sm-3 control-label">Sisa Kuota</label>
                                        <div class="col-sm-9">
                                            <input type="text" readonly="" value="<?php echo $rows->sisa_cutitahunan; ?>" class="form-control">
                                            <h5 style="color: red">*Input sesuai sisa cuti yang ada</h5>
                                        </div>
                                    </div>
                                <?php }?>
                                <div class="form-group row" autocomplete="off" id="data_2">
                                    <label class="col-sm-3 control-label">Mulai Cuti <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="start_date" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="sdate_cuti" required="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" autocomplete="off" id="">
                                    <label class="col-sm-3 control-label">Selesai Cuti <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="end_date" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="edate_cuti" required="" />
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($sisa as $rows) { ?>
                                    <?php if($rows->sisa_cutitahunan=='0'){?>
                                        <div class="form-group row" id="data_2">
                                            <label class="col-sm-3 control-label">Jumlah Cuti 1</label>
                                            <div class="col-sm-9">
                                                <input type='text' name="jumlah_leave" value="Kuota tidak mencukupi, silahkan menunggu kontrak baru" class="form-control"
                                                data-date-format="YYYY-MM-DD" readonly />
                                            </div>
                                        </div>                 
                                    <?php }else{ ?>
                                        <div class="form-group row" id="data_2">
                                            <label class="col-sm-3 control-label">Jumlah Cuti 1</label>
                                            <div class="col-sm-9">
                                                <input type='text' name="jumlah_leave" id="jml" class="form-control"
                                                data-date-format="YYYY-MM-DD" readonly />
                                            </div>
                                        </div> 
                                    <?php }?>
                                <?php }?>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Alamat <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="alamat_cuti" placeholder="Alamat selama cuti" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Keterangan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="keterangan" class="form-control" required="">
                                    </div>
                                </div>
                                <?php foreach ($sisa as $rows) { ?>
                                    <?php if($rows->sisa_cutitahunan=='0'){?>
                                        <div class="form-group">
                                            <div class="col-sm-12 col-sm-offset-3">
                                                <p class="btn btn-primary">kuota Habis</p>
                                            </div>
                                        </div>            
                                    <?php }else{ ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 col-sm-offset-3">
                                                <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                            </div>
                                        </div>
                                    <?php }?>
                                <?php }?>
                            </div>
                        </form>
                        <form  autocomplete="off" id="cuti" method="post" action="<?php echo base_url(); ?>karyawan/simpan_cuti" class="form-horizontal">
                            <div id="2" class="colors" style="display:none">
                                <input type="hidden" value="2" name="jenis_leave">
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <input type="hidden" value="<?php echo $this->session->userdata('nama'); ?>" name="nama_lengkap">
                                
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Total Kuota diambil</label>
                                    <div class="col-sm-9">
                                        <input type="text" readonly="" value="<?php echo $melahirkan[0]->jml_melahirkan ?>" class="form-control">
                                        <h5 style="color: red">*Cuti melahirkan hanya 1x pengajuan selama /kontrak</h5>
                                    </div>
                                </div>
                                <div class="form-group row" autocomplete="off" id="data_2">
                                    <label class="col-sm-3 control-label">Mulai Cuti <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="start_date" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="sdate_cutii" required="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" autocomplete="off" id="">
                                    <label class="col-sm-3 control-label">Selesai Cuti <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="end_date" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="edate_cutii" required="" />
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($melahirkan as $rows) { ?>
                                    <?php if($rows->jml_melahirkan>='0'){?>
                                        <div class="form-group row" id="data_2">
                                            <label class="col-sm-3 control-label">Jumlah Cuti 2</label>
                                            <div class="col-sm-9">
                                                <input type='text' name="jumlah_leave" value="Cuti melahirkan hanya dapat dilakukan 1x pengajuan /kontrak" class="form-control"
                                                data-date-format="YYYY-MM-DD" readonly />
                                            </div>
                                        </div>                 
                                    <?php }elseif($rows->jml_melahirkan<='0'){ ?>
                                        <div class="form-group row" id="data_2">
                                            <label class="col-sm-3 control-label">Jumlah Cuti 2</label>
                                            <div class="col-sm-9">
                                                <input type='text' name="jumlah_leave" id="jmll" class="form-control"
                                                data-date-format="YYYY-MM-DD" readonly />
                                            </div>
                                        </div> 
                                    <?php }?>
                                <?php }?>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Alamat <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="alamat_cuti" placeholder="Alamat selama cuti" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Keterangan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="keterangan" class="form-control" required="">
                                    </div>
                                </div>
                                <?php foreach ($melahirkan as $rows) { ?>
                                    <?php if($rows->jml_melahirkan>='0'){?>
                                        <div class="form-group">
                                            <div class="col-sm-12 col-sm-offset-3">
                                                <p class="btn btn-primary">kuota Habis</p>
                                            </div>
                                        </div>            
                                    <?php }elseif($rows->jml_melahirkan<='0'){ ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 col-sm-offset-3">
                                                <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                            </div>
                                        </div>
                                    <?php }?>
                                <?php }?>
                            </div>
                        </form>

                        <form  autocomplete="off" id="cuti" method="post" action="<?php echo base_url(); ?>karyawan/simpan_cuti" class="form-horizontal">
                            <div id="3" class="colors" style="display:none">
                                <input type="hidden" value="3" name="jenis_leave">
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <input type="hidden" value="<?php echo $this->session->userdata('nama'); ?>" name="nama_lengkap">
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Total Kuota diambil</label>
                                    <div class="col-sm-9">
                                        <input type="text" readonly="" value="<?php echo $ibadah[0]->jml_ibadah ?>" class="form-control">
                                        <h5 style="color: red">*Cuti ibadah hanya 1x pengajuan selama /kontrak</h5>
                                    </div>
                                </div>
                                <div class="form-group row" autocomplete="off" id="data_2">
                                    <label class="col-sm-3 control-label">Mulai Cuti <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="start_date" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="sdate_cutiii" required="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" autocomplete="off" id="">
                                    <label class="col-sm-3 control-label">Selesai Cuti <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="end_date" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="edate_cutiii" required="" />
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($ibadah as $rows) { ?>
                                    <?php if($rows->jml_ibadah>='0'){?>
                                        <div class="form-group row" id="data_2">
                                            <label class="col-sm-3 control-label">Jumlah Cuti 3</label>
                                            <div class="col-sm-9">
                                                <input type='text' name="jumlah_leave" value="Kuota tidak mencukupi, silahkan menunggu kontrak baru" class="form-control"
                                                data-date-format="YYYY-MM-DD" readonly />
                                            </div>
                                        </div>                 
                                    <?php }elseif($rows->jml_ibadah<='0'){ ?>
                                        <div class="form-group row" id="data_2">
                                            <label class="col-sm-3 control-label">Jumlah Cuti 3</label>
                                            <div class="col-sm-9">
                                                <input type='text' name="jumlah_leave" id="jmlll" class="form-control"
                                                data-date-format="YYYY-MM-DD" readonly />
                                            </div>
                                        </div> 
                                    <?php }?>
                                <?php }?>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Alamat <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="alamat_cuti" placeholder="Alamat selama cuti" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Keterangan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="keterangan" class="form-control" required="">
                                    </div>
                                </div>
                                <?php foreach ($ibadah as $rows) { ?>
                                    <?php if($rows->jml_ibadah>='0'){?>
                                        <div class="form-group">
                                            <div class="col-sm-12 col-sm-offset-3">
                                                <p class="btn btn-primary">kuota Habis</p>
                                            </div>
                                        </div>            
                                    <?php }elseif($rows->jml_ibadah<='0'){ ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 col-sm-offset-3">
                                                <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                            </div>
                                        </div>
                                    <?php }?>
                                <?php }?>
                            </div>
                        </form>
                        <form  autocomplete="off" id="cuti" method="post" action="<?php echo base_url(); ?>karyawan/simpan_cuti" class="form-horizontal">
                            <div id="12" class="colors" style="display:none">
                                <input type="hidden" value="12" name="jenis_leave">
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <input type="hidden" value="<?php echo $this->session->userdata('nama'); ?>" name="nama_lengkap">
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Jenis Cuti Khusus <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <select name="jenis_cutikhusus" class="form-control">
                                            <?php foreach ($jeniscutikhusus as $row) { ?>
                                                <option value="<?php echo $row->name; ?>">
                                                    <?php echo $row->name; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Total Kuota diambil</label>
                                    <div class="col-sm-9">
                                        <input type="text" readonly="" value="<?php echo $sakit[0]->jml_sakit ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row" autocomplete="off" id="data_2">
                                    <label class="col-sm-3 control-label">Mulai Cuti <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="start_date" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="sdate_cutiiii" required="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" autocomplete="off" id="">
                                    <label class="col-sm-3 control-label">Selesai Cuti <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="end_date" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="edate_cutiiii" required="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" id="data_2">
                                    <label class="col-sm-3 control-label">Jumlah Cuti 4</label>
                                    <div class="col-sm-9">
                                        <input type='text' name="jumlah_leave" id="jmllll" class="form-control"
                                        data-date-format="YYYY-MM-DD" readonly />
                                    </div>
                                </div> 
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Alamat <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="alamat_cuti" placeholder="Alamat selama cuti" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Keterangan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="keterangan" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-3">
                                        <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#sdate_cuti').change(function () {
            $("#edate_cuti").val($("#sdate_cuti").val());
            $("#jml").val(1);
        })
        $("#edate_cuti").change(function () {
            var sdate = new Date($("#sdate_cuti").val());
            var edate = new Date($("#edate_cuti").val());
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            if (diffDays > 3 || sdate > edate) {
                alert('Selesai cuti tidak boleh lebih dulu atau batas maksimal pengajuan hanya 3 hari, periksa kembali pengajuan anda');
                $("#edate_cuti").val($("#sdate_cuti").val());
                return false;
            }
            $("#jml").val(diffDays);
        });
    })
</script>

<script>
    $(document).ready(function () {
        $('#sdate_cutii').change(function () {
            $("#edate_cutii").val($("#sdate_cutii").val());
            $("#jmll").val(1);
        })
        $("#edate_cutii").change(function () {
            var sdate = new Date($("#sdate_cutii").val());
            var edate = new Date($("#edate_cutii").val());
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            if (diffDays > 92 || sdate > edate) {
                alert('Selesai cuti tidak boleh lebih dulu atau batas maksimal pengajuan hanya 3 bulan, periksa kembali pengajuan anda');
                $("#edate_cutii").val($("#sdate_cutii").val());
                return false;
            }
            $("#jmll").val(diffDays);
        });
    })
</script>

<script>
    $(document).ready(function () {
        $('#sdate_cutiii').change(function () {
            $("#edate_cutiii").val($("#sdate_cutiii").val());
            $("#jmlll").val(1);
        })
        $("#edate_cutiii").change(function () {
            var sdate = new Date($("#sdate_cutiii").val());
            var edate = new Date($("#edate_cutiii").val());
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            if (diffDays > 40 || sdate > edate) {
                alert('Selesai cuti tidak boleh lebih dulu atau batas maksimal pengajuan hanya 40 hari, periksa kembali pengajuan anda');
                $("#edate_cutiii").val($("#sdate_cutiii").val());
                return false;
            }
            $("#jmlll").val(diffDays);
        });
    })
</script>

<script>
    $(document).ready(function () {
        $('#sdate_cutiiii').change(function () {
            $("#edate_cutiiii").val($("#sdate_cutiiii").val());
            $("#jmllll").val(1);
        })
        $("#edate_cutiiii").change(function () {
            var sdate = new Date($("#sdate_cutiiii").val());
            var edate = new Date($("#edate_cutiiii").val());
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            if (sdate > edate) {
                alert('periksa kembali pengajuan anda');
                $("#edate_cutiiii").val($("#sdate_cutiiii").val());
                return false;
            }
            $("#jmllll").val(diffDays);
        });
    })
</script>

<script>  
    $(function() {
        $('#colorselector').change(function(){
            $('.colors').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>