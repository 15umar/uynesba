<div class="row animated fadeInRight">
    <div class="col-md-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Perjalanan Dinas</h5>
            </div>
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="tab-content">

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Nama</label>

                            <div class="col-sm-9">
                                <input type="text" readonly="" value="<?php echo $this->session->userdata('nama'); ?>" name="nama"
                                class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Status <b style="color: red">*</b></label>
                            <div class="col-sm-9">
                                <select name="pengajuan" class="form-control" id="colorselector">
                                    <option value="baru">Baru</option>
                                    <option value="perpanjangan">Perpanjangan</option>
                                </select>
                            </div>
                        </div>
                        <form  method="post" action="<?php echo base_url(); ?>karyawan/simpan_dinas" class="form-horizontal" enctype="multipart/form-data">
                            <div id="perpanjangan" class="colors" style="display:none">
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <input type="hidden" value="perpanjangan" name="pengajuan">

                                <input type="hidden" value="6" name="transportasi">
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Referensi ID</label>
                                    <div class="col-sm-9">
                                        <select name="referensi" class="form-control">

                                            <?php foreach ($data_report as $row) { ?>
                                                <option value="<?php echo $row->id_dinas; ?>">
                                                    <?php echo $row->id_dinas; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" autocomplete="off" id="data_2">
                                    <label class="col-sm-3 control-label">Tgl Pergi <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="tanggal_pergi" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="sdate_sakitt" required="" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" autocomplete="off" id="data_2">
                                    <label class="col-sm-3 control-label">Tgl Pulang <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="tanggal_pulang" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="edate_sakitt" required="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" id="data_2">
                                    <label class="col-sm-3 control-label">Jumlah Hari</label>
                                    <div class="col-sm-9">

                                        <input type='text' name="jumlah" id="jml_sakitt" class="form-control"
                                        data-date-format="YYYY-MM-DD" readonly />

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Kota Asal <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="kota_asal" placeholder="Jakarta" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Kota Tujuan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="tujuan" placeholder="Padang" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Pekerjaan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="pekerjaan" placeholder="Cek Kabel" class="form-control" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-3">
                                        <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form  method="post" action="<?php echo base_url(); ?>karyawan/simpan_dinas" class="form-horizontal" enctype="multipart/form-data">
                            <div id="baru" class="colors">
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <input type="hidden" value="1" name="referensi">
                                <input type="hidden" value="baru" name="pengajuan">
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Transportasi</label>
                                    <div class="col-sm-9">
                                        <select name="transportasi" class="form-control" id="leavetype">
                                            <option value="5">Kendaraan Perusahaan</option>
                                            <option value="1">Pesawat Udara</option>
                                            <option value="4">Kapal Laut</option>
                                            <option value="2">Kereta Api</option>
                                            <option value="3">Bus</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" autocomplete="off" id="data_2">
                                    <label class="col-sm-3 control-label">Tgl Pergi <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="tanggal_pergi" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="sdate_sakit" required="" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" autocomplete="off" id="data_2">
                                    <label class="col-sm-3 control-label">Tgl Pulang <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="tanggal_pulang" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Year-Month-Date" id="edate_sakit" required="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" id="data_2">
                                    <label class="col-sm-3 control-label">Jumlah Hari</label>
                                    <div class="col-sm-9">

                                        <input type='text' name="jumlah" id="jml_sakit" class="form-control"
                                        data-date-format="YYYY-MM-DD" readonly />

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Kota Asal <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="kota_asal" placeholder="Jakarta" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Kota Tujuan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="tujuan" placeholder="Padang" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Pekerjaan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="pekerjaan" placeholder="Cek Kabel" class="form-control" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-3">
                                        <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('#sdate_cuti').change(function () {
            $("#edate_cuti").val($("#sdate_cuti").val());
            $("#jml").val(1);
        })
        $("#edate_cuti").change(function () {
            var sdate = new Date($("#sdate_cuti").val());
            var edate = new Date($("#edate_cuti").val());
            if (sdate > edate) {
                alert('End Date tidak boleh lebih dulu');
                $("#edate_cuti").val($("#sdate_cuti").val());
                return false;
            }
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            $("#jml").val(diffDays);

        });
        $('#sdate_sakit').change(function () {
            $("#edate_sakit").val($("#sdate_sakit").val());
            $("#jml_sakit").val(1);
        })
        $("#edate_sakit").change(function () {
            var sdate = new Date($("#sdate_sakit").val());
            var edate = new Date($("#edate_sakit").val());
            if (sdate > edate) {
                alert('Choose start date first');
                $("#edate_sakit").val($("#sdate_sakit").val());
                return false;
            }
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            $("#jml_sakit").val(diffDays);

        });

         $('#sdate_sakitt').change(function () {
            $("#edate_sakitt").val($("#sdate_sakitt").val());
            $("#jml_sakitt").val(1);
        })
        $("#edate_sakitt").change(function () {
            var sdate = new Date($("#sdate_sakitt").val());
            var edate = new Date($("#edate_sakitt").val());
            if (sdate > edate) {
                alert('Choose start date first');
                $("#edate_sakitt").val($("#sdate_sakitt").val());
                return false;
            }
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            $("#jml_sakitt").val(diffDays);

        });

        function showhide()
        {
            var type=$('#leavetype').val();
            if(type ==1){
                $("#cuti").show();
                $("#sakit").hide();
            }else{
                $("#cuti").hide();
                $("#sakit").show();
            }
        }

        $('#leavetype').change(function(){
            showhide();
        });

    })


</script>

<script>  
    $(function() {
        $('#colorselector').change(function(){
            $('.colors').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>