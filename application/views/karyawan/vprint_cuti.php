<!DOCTYPE html>
<html>
<head>
  <title>print</title>
  <style type="text/css">
    .style1 {font-size: 20px}
    .style2 {font-size: 36px; }
  </style>
  <style type="text/css">
    .watermark {
      opacity: 0.5;
      color: BLACK;
      position: fixed;
      top: 25px;
      right: 1%;
    }
  </style>
</head>
<body onLoad="window.print()">
  <!-- <div class="watermark">
    <img src="<?php echo base_url(); ?>uploads/avatar/must_logo.png">
  </div> -->
  <br>
  <?php $no = 0;
  foreach ($cuti as $rows){      
  ?>
  <center>
    <h1 class="style2">SURAT PERMOHONAN CUTI (SPC) </h1>
  </center>
  <table width="1200" border="0" cellpadding="0" cellspacing="7" class="style1">
    <tr>
      <td width="164"><strong>BIDANG</strong></td>
      <td width="452">: <?php echo $rows->bidang; ?></td>
    </tr>
    <tr>
      <td><strong>SUB BIDANG </strong></td>
      <td>: <?php echo $rows->subbid; ?></td>
    </tr>
    <tr>
      <td><strong>SUPERVISOR</strong></td>
      <td>: <?php echo $rows->spv; ?></td>
    </tr>
    <tr>
      <td><strong>BULAN</strong></td>
      <td>: <?php echo  date("F Y", strtotime($rows->start_date)); ?></td>
    </tr>
  </table>
  <p class="style1">Dengan ini mengajukan permohonan cuti sebagai berikut:</p>
  <table width="auto" border="0" cellpadding="0" cellspacing="7" class="style1">
    <tr>
      <td width="12">1.</td>
      <td width="204">Nama Karyawan </td>
      <td width="3">:</td>
      <td width="446"><?php echo $rows->nama_lengkap; ?></td>
    </tr>
    <tr>
      <td>2.</td>
      <td>Nomor Induk </td>
      <td>:</td>
      <td><?php echo $rows->no_induk; ?></td>
    </tr>
    <tr>
      <td>3.</td>
      <td>Fungsi Pekerjaan </td>
      <td>:</td>
      <td><?php echo $rows->jabatan; ?></td>
    </tr>
    <tr>
      <td>4.</td>
      <td>Tgl Pengajuan Cuti </td>
      <td>:</td>
      <td><?php echo date("d-m-Y", strtotime($rows->tanggal_input)); ?></td>
    </tr>
    <tr>
      <td>5.</td>
      <td>Jumlah Cuti Diambil </td>
      <td>:</td>
      <td><?php echo $rows->jumlah_leave; ?> Hari</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="3"><table width="1000" border="1" cellpadding="0" cellspacing="0" class="style1">
        <tr>
          <td colspan="2"><div align="center"><strong>MULAI CUTI </strong></div></td>
          <td colspan="2"><div align="center"><strong>SELESAI CUTI </strong></div></td>
          <td colspan="2"><div align="center"><strong>JUMLAH CUTI DIAMBIL </strong></div></td>
        </tr>
        <tr>
          <td width="90"><div align="center">TANGGAL</div></td>
          <td width="90"><div align="center">HARI</div></td>
          <td width="90"><div align="center">TANGGAL</div></td>
          <td width="90"><div align="center">HARI</div></td>
          <td width="150"><div align="center">TOTAL HARI </div></td>
          <td width="150"><div align="center">JENIS CUTI </div></td>
        </tr>
        <tr>
          <td><div align="center"><?php echo  date("d-m-Y", strtotime($rows->start_date)); ?></div></td>
          <td><div align="center"><?php echo date("l", strtotime($rows->start_date)); ?></div></td>
          <td><div align="center"><?php echo date("d-m-Y", strtotime($rows->end_date)); ?></div></td>
          <td><div align="center"><?php echo date("l", strtotime($rows->end_date)); ?></div></td>
          <td><div align="center"><?php echo $rows->jumlah_leave; ?></div></td>
          <td><div align="center"><?php echo $rows->jeniscuti; ?></div></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>6.</td>
      <td>Alamat Selama Cuti </td>
      <td>:</td>
      <td><?php echo $rows->alamat_cuti; ?></td>
    </tr>
    <tr>
      <td>7.</td>
      <td>Keterangan Cuti </td>
      <td>:</td>
      <td><?php echo $rows->keterangan; ?></td>
    </tr>
  </table>
  <?php } ?> 
  <br> <br>
  <p class="style1">Demikian Surat Permohonan Cuti (SPC) ini dibuat, terima kasih.</p>
  <table width="1200" border="0" cellpadding="0" cellspacing="0" class="style1">
    <tr>
      <td width="600">&nbsp;</td>
      <td width="600"><div align="center">Jakarta, <?php echo date("d-m-Y", strtotime($rows->tanggal_input)); ?></div></td>
    </tr>
    <tr>
      <td>
        <p align="center">Menyetujui,</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center"><b><?php echo $rows->spv; ?></b><br />SUPERVISOR</p>
      </td>
      <td>
        <p align="center">Yang Mengajukan,</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center"><b><?php echo $rows->nama_lengkap; ?></b><br /><?php echo $rows->name; ?></p>
      </td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <hr />
  <?php $no = 0;
  foreach($tot as $total){      
  ?>
  <?php } ?>  
  <?php $no = 0;
  foreach($sisa as $cut){      
  ?>
  <?php } ?>  
  <?php $no = 0;
  foreach($sisa as $cut){      
  ?>
  <b><p class="style1">Catatan Cuti</p></b>
  <table width="1000" height="200" border="0" cellpadding="0" cellspacing="0" class="style1">
    <tr>
      <td width="26">1</td>
      <td width="216">Jumlah Cuti yang telah diambil </td>
      <td width="4">:</td>
      <td width="56"><?php echo $total->total; ?></td>
      <td width="225">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>- Ijin tanpa keterangan </td>
      <td>:</td>
      <td>&nbsp;</td>
      <td>hari</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>- Sakit tanpa keterangan </td>
      <td>:</td>
      <td>&nbsp;</td>
      <td>hari</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>- Lain-lain </td>
      <td>:</td>
      <td>&nbsp;</td>
      <td>hari</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><div align="right">Jumlah</div></td>
      <td>:</td>
      <td><?php echo $total->total; ?></td>
      <td>hari</td>
    </tr>
    <tr>
      <td>2</td>
      <td colspan="4">Sisa Cuti periode Oktober 2019 - September 2020 :  </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><?php echo $cut->total; ?> hari</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <?php } ?>  
</body>
</html>