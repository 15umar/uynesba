<!DOCTYPE html>
<html>
<head>
  <title>print</title>
  <style type="text/css">
    .style1 {font-size: 20px}
    .style2 {font-size: 30px; }
  </style>
  <style type="text/css">
    .watermark {
      opacity: 0.5;
      color: BLACK;
      position: fixed;
      top: 25px;
      right: 1%;
    }
  </style>
</head>
<body onLoad="window.print()">
 <!--  <div class="watermark">
    <img src="<?php echo base_url(); ?>uploads/avatar/must_logo.png">
  </div> -->
  <br>
  <?php $no = 0;
  foreach ($lembur as $rows){      
    ?>
    <center>
      <h1 class="style2">SURAT PERINTAH PERJALANAN BISNIS (SPPB) </h1>
    </center>
    <table width="100%" border="1" cellpadding="0" cellspacing="0" class="style1">
      <tr>
        <td>Nomor :</td>
        <td>Tanggal : <?php echo $rows->insert; ?></td>
      </tr>
    </table>
    <br>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="style1">

      <tr>
        <td colspan="3">Dengan ini menugaskan kepada : </td>
      </tr>
      <tr>
        <td><strong>Nama</strong></td>
        <td>:</td>
        <td rowspan="9"><table width="100%" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;<?php echo $rows->nama_lengkap; ?></td>
          </tr>
          <tr>
            <td>&nbsp;<?php echo $rows->jabatan; ?></td>
          </tr>
          <tr>
            <td>&nbsp;<?php echo $rows->pekerjaan; ?></td>
          </tr>
          <tr>
            <td>&nbsp;<?php echo $rows->pekerjaan; ?></td>
          </tr>
          <tr>
            <td>&nbsp;<?php echo $rows->pekerjaan; ?></td>
          </tr>
          <tr>
            <td>&nbsp;<?php echo $rows->pekerjaan; ?></td>
          </tr>
          <tr>
            <td>&nbsp;<?php echo $rows->tujuan; ?></td>
          </tr>
          <tr>
            <td>&nbsp;<?php echo $rows->tanggal_pergi; ?> - <?php echo $rows->tanggal_pulang; ?></td>
          </tr>
          <tr>
            <td>&nbsp;<?php echo $rows->jumlah; ?> Hari</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><strong>Bidang Kerja </strong></td>
        <td>:</td>
      </tr>
      <tr>
        <td valign="top"><strong>Maksud Perjalanan </strong></td>
        <td valign="top">:</td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td><strong>Sifat Penugasan </strong></td>
        <td>:</td>
      </tr>
      <tr>
        <td><strong>Tempat Keberangkatan </strong></td>
        <td>:</td>
      </tr>
      <tr>
        <td valign="top"><div align="left"><strong>Lokasi Tujuan </strong></div></td>
        <td valign="top"><div align="left">:</div></td>
      </tr>
      <tr>
        <td><strong>Tanggal Perjalanan</strong></td>
        <td>:</td>
      </tr>
      <tr>
        <td>Jumlah</td>
        <td>:</td>
      </tr>
    </table>
    <br>
    <table width="100%" border="1" cellpadding="0" cellspacing="0">
      <tr>
        <td>Alat Transportasi </td>
        <td>No. Kendaraan </td>
      </tr>
      <tr>
        <td><?php echo $rows->nama_transportasi; ?></td>
        <td>&nbsp;</td>
      </tr>
    </table>

    <br><br>
    Diisi oleh Tenaga Kerja Yang Bertugas:
    <table width="100%" border="1">
      <tr>
        <td colspan="2"><div align="center"><strong>URAIAN</strong></div></td>
        <td width="17%"><div align="center"><strong>TARIF</strong></div></td>
        <td width="14%"><div align="center"><strong>QTY</strong></div></td>
        <td width="17%"><div align="center"><strong>JUMLAH</strong></div></td>
        <td width="16%"><div align="center"><strong>KETERANGAN</strong></div></td>
      </tr>
      <tr>
        <td colspan="2">Biaya Harian Perjalanan Bisnis </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="3%"><div align="center">a</div></td>
        <td width="33%">Penginapan &amp; Cuci </td>
        <td><?php echo $rows->penginapan; ?></td>
        <td><?php echo $rows->jumlah; ?></td>
        <td>
          <?php if($rows->pengajuan=='baru'){?>
            <?php echo $rows->totalpenginapan; ?>                 
          <?php }elseif($rows->pengajuan=='perpanjangan'){ ?>
            <?php echo $rows->totalpenginapann; ?> 

          <?php }?>
        </td>      
        <!-- <td><?php echo $rows->totalpenginapan; ?></td> -->
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center">b</div></td>
        <td>Biaya Makan </td>
        <td><?php echo $rows->biaya_makan; ?></td>
        <td><?php echo $rows->jumlah; ?></td>
        <td><?php echo $rows->makan; ?></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center">c</div></td>
        <td>Uang Saku </td>
        <td><?php echo $rows->uang_saku; ?></td>
        <td><?php echo $rows->jumlah; ?></td>
        <td><?php echo $rows->uangsaku; ?></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center">d</div></td>
        <td>Angkutan Setempat </td>
        <td><?php echo $rows->angkutan_setempat; ?></td>
        <td><?php echo $rows->jumlah; ?></td>
        <td><?php echo $rows->angkutan; ?></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center">e</div></td>
        <td>PP Kendaraan Umum </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center">f</div></td>
        <td>Transport ke Bandara / Stasiun</td>

        <?php if($rows->transportasi=='1' or $rows->transportasi=='4'){?>
          <td><?php echo $rows->ke_bandara; ?></td>
        <?php } elseif($rows->transportasi=='3' or $rows->transportasi=='2'){?>
          <td><?php echo $rows->ke_terminal; ?></td>
        <?php } elseif($rows->transportasi=='6' or $rows->transportasi=='5'){?>
          <td>&nbsp;</td>
        <?php } ?>

        <?php if($rows->transportasi=='1' or $rows->transportasi=='4' or $rows->transportasi=='2' or $rows->transportasi=='3'){?>
          <td><?php echo $rows->jumlah; ?></td>
        <?php } elseif($rows->transportasi=='6' or $rows->transportasi=='5'){?>
          <td>&nbsp;</td>
        <?php } ?>

        <!-- <td><?php echo $rows->jumlah; ?></td> -->


        <?php if($rows->transportasi=='1' or $rows->transportasi=='4'){?>
          <td><?php echo $rows->tobandara; ?></td>
        <?php } elseif($rows->transportasi=='3' or $rows->transportasi=='2'){?>
          <td><?php echo $rows->toterminal; ?></td>
        <?php } elseif($rows->transportasi=='6' or $rows->transportasi=='5'){?>
          <td>&nbsp;</td>
        <?php } ?>
        <td>&nbsp;</td>
      </tr>
      <?php if($rows->transportasi=='1'){?>
        <tr>
          <td><div align="center">g</div></td>
          <td>Tiket Pesawat Udara </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><?php echo $rows->harga_tiket; ?></td>
          <td>&nbsp;</td>
        </tr>
      <?php }elseif($rows->transportasi!='1'){ ?>
        <tr>
          <td><div align="center">g</div></td>
          <td>Tiket Pesawat Udara </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      <?php } ?>

      <?php if($rows->transportasi=='4'){?>
        <tr>
          <td><div align="center">h</div></td>
          <td>Tiket Kapal Laut </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><?php echo $rows->harga_tiket; ?></td>
          <td>&nbsp;</td>
        </tr>
      <?php }elseif($rows->transportasi!='4'){ ?>
        <tr>
          <td><div align="center">h</div></td>
          <td>Tiket Kapal Laut </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      <?php } ?>
      
      <?php if($rows->transportasi=='2'){?>
        <tr>
          <td><div align="center">i</div></td>
          <td>Tiket Kereta Api </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><?php echo $rows->harga_tiket; ?></td>
          <td>&nbsp;</td>
        </tr>
      <?php }elseif($rows->transportasi!='2'){ ?>
        <tr>
          <td><div align="center">g</div></td>
          <td>Tiket Kereta Api </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      <?php } ?>
      <?php if($rows->transportasi=='3'){?>
        <tr>
          <td><div align="center">j</div></td>
          <td>Tiket Bus </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><?php echo $rows->harga_tiket; ?></td>
          <td>&nbsp;</td>
        </tr>
      <?php }elseif($rows->transportasi!='5'){ ?>
        <tr>
          <td><div align="center">g</div></td>
          <td>Tiket Bus </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      <?php } ?>
      <tr>
        <td colspan="2"><div align="center"><strong>TOTAL</strong></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><?php echo $rows->totalsppd; ?></td>
        <td>&nbsp;</td>
      </tr>
    </table>
  <?php } ?> 
  <br>
  <table width="1100px" height="177" border="1" cellpadding="0" cellspacing="0">
    <tr>
      <td width="37%" rowspan="2"><p align="center">Mengetahui<br />
      Perusahaan Penyedia Jasa </p>    </td>
      <td colspan="2"><div align="center"><strong>PT. Indonesia Comnets Plus </strong></div></td>
    </tr>
    <tr>
      <td width="32%"><div align="center">Yang Menugaskan </div></td>
      <td width="31%"><div align="center">Menyetujui</div></td>
    </tr>
    <tr>
      <td height="96">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><div align="center"><strong>PT. Mitra Utama Solusi Telematika </strong></div></td>
      <td><div align="center"><strong>Supervisor</strong></div></td>
      <td><div align="center"><strong>Manajer</strong></div></td>
    </tr>
  </table>
  <br> <br>
  <p class="style1">* Khusus Untuk Pengemudi</p>



</body>
</html>
