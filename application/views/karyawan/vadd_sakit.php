<div class="row animated fadeInRight">
    <div class="col-md-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Pengajuan Sakit</h5>
            </div>
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="tab-content">
                        <form method="post"  action="<?php echo base_url(); ?>karyawan/simpan_sakit" onsubmit="return validasi_input(this)"  class="form-horizontal" enctype="multipart/form-data">

                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Total Sakit</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly="" value="<?php echo $sakit[0]->jml_sakit ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Nama</label>
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <div class="col-sm-9">
                                    <input type="text" readonly="" value="<?php echo $this->session->userdata('nama'); ?>" name="nama_lengkap"
                                    class="form-control">
                                </div>
                            </div>
                            <div class="form-group row" autocomplete="off" id="data_2">
                                <label class="col-sm-3 control-label">Mulai Sakit <b style="color: red">*</b></label>
                                <div class="col-sm-9">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type='text' name="start_date" class="form-control" data-date-format="YYYY-MM-DD"
                                        placeholder="Year-Month-Date" id="sdate_sakit" required="" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" autocomplete="off" id="data_2">
                                <label class="col-sm-3 control-label">Selesai Sakit <b style="color: red">*</b></label>
                                <div class="col-sm-9">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type='text' name="end_date" class="form-control" data-date-format="YYYY-MM-DD"
                                        placeholder="Year-Month-Date" id="edate_sakit" required="" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" id="data_2">
                                <label class="col-sm-3 control-label">Jumlah Sakit</label>
                                <div class="col-sm-9">

                                    <input type='text' name="jumlah_leave" id="jml_sakit" class="form-control"
                                    data-date-format="YYYY-MM-DD" readonly />

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Keterangan <b style="color: red">*</b></label>
                                <div class="col-sm-9">
                                    <input type="text" name="keterangan" id="keterangan" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Attach <b style="color: red">*</b></label>
                                <div class="col-sm-9">
                                    <input type="file" name="userfile" class="form-control" required="">
                                    <p>Format dalam bentuk .jpg</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-sm-offset-3">
                                    <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('#sdate_sakit').change(function () {
            $("#edate_sakit").val($("#sdate_sakit").val());
            $("#jml_sakit").val(1);
        })
        $("#edate_sakit").change(function () {
            var sdate = new Date($("#sdate_sakit").val());
            var edate = new Date($("#edate_sakit").val());
            if (sdate > edate) {
                alert('Choose start date first');
                $("#edate_sakit").val($("#sdate_sakit").val());
                return false;
            }
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            $("#jml_sakit").val(diffDays);

        });
    })
</script>