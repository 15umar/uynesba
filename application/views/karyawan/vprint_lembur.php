<!DOCTYPE html>
<html>
<head>
  <title>print</title>
  <style type="text/css">
    .style1 {font-size: 20px}
    .style2 {font-size: 36px; }
  </style>
  <style type="text/css">
    .watermark {
      opacity: 0.5;
      color: BLACK;
      position: fixed;
      top: 25px;
      right: 1%;
    }
  </style>
</head>
<body onLoad="window.print()">
  <!-- <div class="watermark">
    <img src="<?php echo base_url(); ?>uploads/avatar/must_logo.png">
  </div> -->
  <br>
  <?php $no = 0;
  foreach ($lembur as $rows){      
    ?>
    <center>
      <h1 class="style2">SURAT PENUGASAN KERJA LEMBUR (SPKL) </h1>
    </center>
    <table width="1200" border="0" cellpadding="0" cellspacing="7" class="style1">
      <tr>
        <td width="164"><strong>BIDANG</strong></td>
        <td width="452">: <?php echo $rows->bidang; ?></td>
      </tr>
      <tr>
        <td><strong>SUB BIDANG </strong></td>
        <td>: <?php echo $rows->subbid; ?></td>
      </tr>
      <tr>
        <td><strong>SUPERVISOR</strong></td>
        <td>: <?php echo $rows->spv; ?></td>
      </tr>
      <tr>
        <td><strong>BULAN</strong></td>
        <td>: <?php echo  date("F Y", strtotime($rows->start_datetime)); ?></td>
      </tr>
    </table>
    <p class="style1">Dengan ini menugaskan kepada:</p>

    <table width="1200" border="0" cellpadding="0" cellspacing="7" class="style1">
      <tr>
        <td width="12">1.</td>
        <td width="204">Nama Karyawan </td>
        <td width="3">:</td>
        <td width="446"><?php echo $rows->nama_lengkap; ?></td>
      </tr>
      <tr>
        <td>2.</td>
        <td>Nomor Induk </td>
        <td>:</td>
        <td><?php echo $rows->no_induk; ?></td>
      </tr>
      <tr>
        <td>3.</td>
        <td>Fungsi Pekerjaan </td>
        <td>:</td>
        <td><?php echo $rows->jabatan; ?></td>
      </tr>
      <tr>
        <td>4.</td>
        <td>Waktu Kerja Lembur </td>
        <td>:</td>
        <td><?php echo date("d-m-Y", strtotime($rows->start_datetime)); ?></td>
      </tr>

      <tr>
        <td>&nbsp;</td>
        <td colspan="3"><table width="1000" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td width="200"><div align="center"><strong>TANGGAL</strong></div></td>
            <td width="200"><div align="center"><strong>HARI</strong></div></td>
            <td width="200"><div align="center"><strong>MULAI</strong></div></td>
            <td width="200"><div align="center"><strong>AKHIR</strong></div></td>
            <td width="200"><div align="center"><strong>J.REAL</strong></div></td>
            <td width="200"><div align="center"><strong>HK/HL/HR</strong></div></td>
          </tr>
          <tr>
            <td><div align="center"><?php echo date("d-m-Y", strtotime($rows->start_datetime)); ?></div></td>
            <td><div align="center"><?php echo date("l", strtotime($rows->start_datetime)); ?></div></td>
            <td><div align="center"><?php echo date("H:i", strtotime($rows->start_datetime)); ?></div></td>
            <td><div align="center"><?php echo date("H:i", strtotime($rows->end_datetime)); ?></div></td>
            <td><div align="center"><?php
            $s_date = new DateTime($rows->start_datetime);
            $e_date = new DateTime($rows->end_datetime);
            $diff = date_diff($s_date, $e_date); 
            echo ($diff->format('%h:%i')).'';
            ?></div></td>
            <td><div align="center">HK</div></td>
          </tr>
          <tr>
            <td colspan="6"><div align="center">T O T A L   J A M   L E M B U R   : <?php
            $s_date = new DateTime($rows->start_datetime);
            $e_date = new DateTime($rows->end_datetime);
            $diff = date_diff($s_date, $e_date); 
            echo ($diff->format('%h,%i')).'';
            ?></div></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><b>K E T E R A N G A N :</b><br />
          HK : Hari Kerja <br />
          HL : Hari Libur <br />
        HR : Hari Raya </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>5.</td>
        <td>Tugas Kerja Lembur </td>
        <td>:</td>
        <td><?php echo $rows->alasan; ?></td>
      </tr>
    </table>
  <?php } ?> 

  <br> <br>
  <p class="style1">Demikian Surat Penugasan Kerja Lembur (SPKL) ini dibuat, untuk dapat dilaksanakan sebaik-baiknya dengan penuh tanggung jawab.</p>

  <table width="1200" border="0" cellpadding="0" cellspacing="0" class="style1">
    <tr>
      <td>
        <div align="center">
          <p><br />
          Pelaksana Tugas Lembur,</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p><?php echo $rows->nama_lengkap; ?><br /><?php echo $rows->name; ?></p>
        </div>
      </td>
      <td>
        <div align="center">
          <p>Jakarta, <?php echo date("d-m-Y", strtotime($rows->insert)); ?><br />
          Pemberi Tugas Lembur,</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p><?php echo $rows->spv; ?><br />
          SUPERVISOR</p>
        </div>
      </td>
    </tr>
    <tr>
      <td colspan="3"><p align="center">Mengetahui,</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center"><?php echo $rows->manajer; ?><br />
        MANAJER</p>
      </td>
    </tr>
  </table>

</body>
</html>
