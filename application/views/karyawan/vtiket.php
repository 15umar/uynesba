<div class="row animated fadeInRight">
  <div class="col-md-9">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Edit</h5>
      </div>
      <div class="ibox-content">
        <div class="panel-body">
          <div class="tab-content">
            <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>karyawan/update_tiket" enctype="multipart/form-data">


              <?php foreach ($cuti as $rows) { } ?>
              <input type="hidden" name="id_dinas" value="<?php echo $rows->id_dinas; ?>">
              <div class="form-group">
                <label class="col-sm-3 control-label">Nama</label>
                <div class="col-sm-9">
                  <input type="text" readonly="" value="<?php echo $rows->nama_lengkap; ?>"  class="form-control">
                </div>
              </div>
              <div class="form-group" id="data_2">
                <label class="col-sm-3 control-label">Tanggal Pergi</label>
                <div class="col-sm-9">
                  <input type="text" readonly="" value="<?php echo $rows->tanggal_pergi; ?>" name="tanggal_pergi" class="form-control">
                </div>
              </div>
              <div class="form-group" id="data_2">
                <label class="col-sm-3 control-label">Tanggal Pulang</label>
                <div class="col-sm-9">
                  <input type="text" readonly="" value="<?php echo $rows->tanggal_pulang; ?>" name="tanggal_pulang" class="form-control">
                </div>
              </div>
              <div class="form-group" id="data_2">
                <label class="col-sm-3 control-label">Alat Transportasi</label>
                <div class="col-sm-9">
                  <input type="text" readonly="" value="<?php echo $rows->nama_transportasi; ?>" name="nama_transportasi" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Harga Tiket</label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $rows->harga_tiket; ?>" name="harga_tikett" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Attach <b style="color: red">*</b></label>
                <div class="col-sm-9">
                  <input type="file" value="<?php echo $rows->nama_file; ?>" name="userfile" class="form-control" required="">
                  <p style="color: red">Format upload yang dibolehkan dalam bentuk .jpg dan .pdf</p>
                  <p style="color: red">Maksimal file 2MB</p>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 col-sm-offset-3">
                  <button class="btn btn-primary submit-update" type="submit">Simpan</button>
                </div>
              </div>
              
              

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>