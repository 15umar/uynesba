<div class="row">

    <div class="col-lg-3">
        <div class="widget style1 navy-bg">
            <div class="row">
                <div class="col-4">
                    <i class="fa fa-users fa-4x"></i>
                </div>
                <div class="col-8 text-right">
                    <span> Data Karyawan </span>
                    <h2 class="font-bold"><?php echo $member; ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget style1 blue-bg">
            <div class="row">
                <div class="col-4">
                    <i class="fa fa-bell-slash fa-4x"></i>
                </div>
                <div class="col-8 text-right">
                    <span> Data Cuti | <?php echo $jmlcutiapp; ?></span>
                    <h2 class="font-bold"><?php echo $jmlcuti; ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="widget style1 red-bg">
            <div class="row">
                <div class="col-3">
                    <i class="fa fa-plus-square-o fa-2x"></i>
                </div>
                <div class="col-9 text-right">
                    <span> Sakit | <?php echo $jmlsakitapp; ?></span>
                    <h2 class="font-bold"><?php echo $jmlsakit; ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="widget style1 lazur-bg">
            <div class="row">
                <div class="col-3">
                    <i class="fa fa-file-pdf-o fa-2x"></i>
                </div>
                <div class="col-9 text-right">
                    <span> Lembur | <?php echo $jmllemburapp; ?></span>
                    <h2 class="font-bold"><?php echo $jmllembur; ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="widget style1 yellow-bg">
            <div class="row">
                <div class="col-3">
                    <i class="fa fa-bus fa-2x"></i>
                </div>
                <div class="col-9 text-right">
                    <span> SPPD | 0</span>
                    <h2 class="font-bold">0</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="card">
                    <form method="post" action="<?php echo base_url(); ?>karyawan/absen_masuk" class="form-horizontal">
                        <div class="card-body">
                            <h4 class="card-title">Silahkan absen</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="my_camera"></div>
                                    <span onClick="take_snapshot()" id="myCheck"></span>
                                    <input type="hidden" name="image" class="image-tag" id="results">
                                </div>
                                <?php
                                if ($this->agent->is_browser()) {
                                    $agent = $this->agent->browser() . ' ' . $this->agent->version();
                                } elseif ($this->agent->is_mobile()) {
                                    $agent = $this->agent->mobile();
                                } else {
                                    $agent = 'Data user gagal di dapatkan';
                                }
                                "Browser = " . $agent . "<br/>";
                                "Sistem Operasi = " . $this->agent->platform() . "<br/>";
                                "IP = " . $this->input->ip_address();
                                ?>
                            </div>
                            <div class="row">
                                <span onclick="getLocation()" id="klik"></span>
                                <input type="hidden" name="lang" id="demo">
                                <div id="error">&nbsp;</div>
                            </div>
                        </div>
                        <div class="border-top">

                            <div class="card-body">
                                <div class="form-group row">
                            <label class="col-sm-3 control-label">Realisasi Kehadiran <b style="color: red">*</b></label>
                            <div class="col-sm-9">
                                <select name="realisasi" class="form-control" id="colorselector">
                                    <option value="P">Hadir</option>
                                    <option value="P">WFO Pagi</option>
                                    <option value="S">WFO Sore</option>
                                    <option value="M">WFO Malam</option>
                                    <option value="WFH1">WFH Pagi</option>
                                    <option value="WFH2">WFH Sore</option>
                                    <option value="WFH3">WFH Malam</option>
                                    <option value="L">Libur</option>
                                </select>
                            </div>
                            <label class="col-sm-12 control-label">Perhatikan dan pilih Realisasi Kehadiran masuk sesuai dengan jam kerja dan abaikan Realisasi Kehadiran pada absen pulang. </br> <span style="font-weight: bold">*Hadir untuk yang Non Shift</span></label>
                        </div>
                                <button type="submit" onmouseover="myFunction()" class="btn btn-success btn-lg"><i class="mdi mdi-airplane-landing"></i> Absen Masuk</button>
                                <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal"><i class="mdi mdi-airplane-takeoff"></i> Absen Pulang</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div id="container" style="min-width: 150px; height: 150px; max-width: 800px; margin: 0 auto"></div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <?php foreach ($jadwal as $rows) { ?>
                    <p>Hari ini Anda <b><?php echo $rows->note; ?></b></p>
                <?php } ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th width="10px">No.</th>
                            <th>Nama</th>
                            <th>Shift</th>
                            <th>Hadir</th>
                            <th>Jam</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $perpage = $this->uri->segment(4);
                        $no = 1 + $perpage;
                        foreach ($umarkoto as $rows) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $rows->nama_lengkap; ?></td>
                                <td><?php echo $rows->realisasi; ?></td>
                                <!-- <td><?php echo $rows->id_shifting; ?></td> -->
                                <td>
                                    <?php if ($rows->id_shifting >= '0') { ?>
                                        <p><b><?php echo $rows->id_shifting; ?></b></p>
                                    <?php } else { ?>
                                        <p><b>Non Shift</b></p>
                                    <?php } ?>

                                </td>
                                <td>
                                    <?php if ($rows->kode_abs == '1') { ?>
                                        <p><b><?php echo $rows->masuk; ?></b></p>
                                    <?php } else { ?>
                                        <p><b><?php echo $rows->keluar; ?></b></p>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if ($rows->kode_abs == '1') { ?>
                                        <p><b>Masuk</b></p>
                                    <?php } else { ?>
                                        <p><b>Pulang</b></p>
                                    <?php } ?>
                                </td>

                            </tr>
                        <?php $no++;
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">

</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Absen Pulang</h4>
            </div>
            <form method="post" action="<?php echo base_url(); ?>karyawan/absen_pulang" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="my_camera"></div>
                            <span onClick="take_snapshot()" id="myCheck">
                                <input type="hidden" name="image" class="image-tag" id="results">
                        </div>
                        <?php
                        if ($this->agent->is_browser()) {
                            $agent = $this->agent->browser() . ' ' . $this->agent->version();
                        } elseif ($this->agent->is_mobile()) {
                            $agent = $this->agent->mobile();
                        } else {
                            $agent = 'Data user gagal di dapatkan';
                        }
                        "Browser = " . $agent . "<br/>";
                        "Sistem Operasi = " . $this->agent->platform() . "<br/>";
                        "IP = " . $this->input->ip_address();
                        ?>
                    </div>
                    <div class="row">
                        <span onclick="getLocation()" id="klik"></span>
                        <input type="hidden" name="lang" id="demo">
                        <div id="error">&nbsp;</div>
                    </div>
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Pekerjaan</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="pekerjaan" placeholder="Masukkan list pekerjaan hari ini"></textarea>
                        </div>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <button type="submit" onmouseover="myFunction()" class="btn btn-primary">Pulang</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalberhalangan" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Berhalangan Hadir</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>Klik tombol berhalangan hadir dibawah ini, silahkan isi formnya.</p>
                <a href="<?php echo base_url(); ?>karyawan/sakit" class="btn btn-danger">Sakit</a>
                <!-- <a href="<?php echo base_url(); ?>karyawan/izin/" class="btn btn-warning">Izin</a> -->
                <a href="<?php echo base_url(); ?>karyawan/cuti" class="btn btn-info">Cuti</a>
            </div>
        </div>
    </div>
</div>
<script language="JavaScript">
    Webcam.set({
        width: 442,
        height: 310,
        image_format: 'jpeg',
        jpeg_quality: 10
    });
    Webcam.attach('#my_camera');

    function take_snapshot() {
        Webcam.snap(function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="' + data_uri + '"/>';
        });
    }
</script>
<script>
    var x = document.getElementById("demo");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(showPosition);
            $("#demo").val();
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        $("#demo").val(position.coords.latitude +
            "," + position.coords.longitude);
    }
</script>
<script>
    function aclick() {
        document.getElementById("klik").click();
    }
</script>

<script>
    function myFunction() {
        document.getElementById("myCheck").click();
    }
</script>

<?php foreach ($absen as $rows) {
} ?>
<script type="text/javascript">
    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Count Absensi'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Masuk',
                y: <?php echo $rows->masuk; ?>,
                sliced: true,
                selected: true
            }, {
                name: 'Pulang',
                y: <?php echo $rows->pulang; ?>
            }]
        }]
    });
</script>