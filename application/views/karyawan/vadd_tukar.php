    <div class="row animated fadeInRight">
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tukar Jadwal</h5>
                </div>
                <div class="ibox-content">
                    <div class="panel-body">
                        <div class="tab-content">
                            <form autocomplete="off" method="post" action="<?php echo base_url(); ?>karyawan/simpan_tukar" class="form-horizontal" enctype="multipart/form-data">

                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Nama</label>
                                    <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                    <input type="hidden" value="" name="id_shifting" id="id_shifting">
                                    <input type="hidden" value="" name="shift_pengganti" id="shift_pengganti">
                                    <div class="col-sm-9">
                                        <input type="text" readonly="" value="<?php echo $this->session->userdata('nama'); ?>" name="nama"
                                        class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row" id="shift_anda">
                                    <label class="col-sm-3 control-label">Jadwal Asli</label>
                                    <div class="col-sm-9">
                                        <input type="text" readonly name="id_shifting" id="id_shifting" value="" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row" id="data_2">
                                    <label class="col-sm-3 control-label">Tanggal <b style="color: red">*</b></label>
                                    <div class="col-sm-5">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="tanggal" class="form-control" data-date-format="YYYY-MM-DD"
                                            placeholder="Tahun-Bulan-Tanggal" id="tanggal" required="" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="pull-right"><button type="button" id="btn-search" class="btn btn-success">Lihat Jadwal</button></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 control-label" id="label_jadwal" >Tukar Dengan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <table class="table table-striped table-bordered table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Kode Shift</th>
                                                    <!-- <th>Pilih</th> -->
                                                </tr>
                                            </thead>
                                            <tbody id="target">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group row" id="alasan" >
                                    <label class="col-sm-3 control-label">Alasan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="alasan" placeholder="Alasan" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-3">
                                        <button class="btn btn-primary submit-update" id="submit" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <script>
    $("#btn-search").click(function(){ // Ketika user mengklik tombol Cari  
        var tanggal=$("[name = 'tanggal']").val();
        var id_user=$("[name = 'id_user']").val();

        $.ajax({
            type:'POST',
            data:'tanggal='+tanggal,
            url: 'karyawan/ambiljadwal',
            dataType: 'JSON',
            success: function(data)
            {
                $("[name = 'id_shifting']").val(data[0].id_shifting);
                var jdwl = data[0].id_shifting;
                console.log(jdwl);
                if(jdwl=="L"){
                    alert("Jadwal kamu libur, silahkan pilih hari lain!!");
                    $('#submit').hide();
                    $('#example').hide();
                    $('#label_jadwal').hide();
                    $('#alasan').hide();
                    $('#shift_anda').hide();
                }else{
                    $.ajax({
                        type:'POST',
                        data:'tanggal='+tanggal+'&jdwl='+jdwl,
                        url: 'karyawan/get_available',
                        dataType: 'JSON',
                        success: function(hasil){   
                            if(hasil==0){
                                alert("Jadwal tanggal tersebut belum diupload");
                            }else{
                                $('#example').show();
                                $('#label_jadwal').show();
                                $('#alasan').show();
                                $('#shift_anda').show();
                                var baris='';
                                for(var i=0;i<hasil.length;i++){
                                    baris += '<tr>'+
                                    '<td><input type="radio"  class="radio" id="check" name="id_pengganti" value="'+hasil[i].nip+','+hasil[i].id_shifting+'">'+hasil[i].nama_lengkap+'</td>'+
                                    '<td>'+hasil[i].id_shifting+'</td>'+

                                    '</tr>';
                                }
                                $('#target').html(baris);
                                    // ambiljadwal();
                                }
                            }
                        });
                    $('#submit').show();
                }
            }
        });
        



    });    
</script>