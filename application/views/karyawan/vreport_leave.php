<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Data Cuti</h5>
      </div>
      <div class="ibox-content">
        <!-- <div class="row"> -->



          <table id="r_overtime" class="table table-striped table-bordered table-hover dataTables-example">
            <thead>
              <tr>
                <th width="10px">No.</th>
                <th>Nama</th>
                <th>Jenis Cuti</th>
                <th>Tanggal</th>
                <th>Jumlah</th>
                <th>Sisa Kuota</th>
                <th>Tanggal Input</th>
                <th>Status</th>
                <th>Print</th>
              </tr>
            </thead>
            <tbody id="baris">

              <?php 
              $perpage = $this->uri->segment(4);
              $no=1+$perpage;  
              foreach ($data_report as $rows) { ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $rows->nama_lengkap; ?></td>
                  <td>
                    <?php if($rows->jenis_leave=='12'){?>
                      <p><b><?php echo $rows->jenis_cuti; ?></b> (<?php echo $rows->jenis_cutikhusus; ?>)</p>                  
                    <?php }else{ ?>
                      <p><b><?php echo $rows->jenis_cuti; ?></b></p>   
                    <?php }?>
                  </td>
                  <!-- <td><?php echo $rows->jenis_cuti; ?></td> -->
                  <td><?php echo date('d/m/Y', strtotime($rows->start_date)); ?> - <?php echo date('d/m/Y', strtotime($rows->end_date)); ?></td>
                  <td><?php echo $rows->jumlah_leave; 
                  ?></td>
                  <td><?php echo $rows->sisa_cuti; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($rows->tanggal_input)); ?></td> 

                  <td>
                    <?php if($rows->status=='1'){?>
                      <p><b>Menunggu</b></p>                  
                    <?php }elseif($rows->status=='4'){ ?>
                      <p><b>Ditolak</b></p>  
                    <?php }elseif($rows->status=='3'){ ?>
                      <p><b>Diterima</b></p>  
                    <?php }?>
                  </td>  
                  <td>
                    <?php
                    if(($rows->status=='1')){?>
                      <a href="#modal-hapus<?php echo $rows->id_user; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger"><i class="fa fa-close"></i> Batal</a>
                      <?php
                    }elseif(($rows->status=='2')){
                      ?>
                      <p>Surat Anda Ditolak</p>
                      <?php
                    }elseif(($rows->status=='3')){ 
                      ?>
                      <a href="<?php echo base_url()."karyawan/print_cuti/".$rows->id_user?>" target="_blank" class="btn btn-sm btn-outline btn-info"><i class="fa  fa-print">  Print</i></a>
                    <?php }?>
                  </td>                   
                </tr>
                <?php $no++; } ?>
              </tbody>
            </table>

            <hr>
            <!-- </div> -->
          </div>
        </div>
      </div>
    </div>

    <?php foreach ($data_report as $rows) { ?>
      <div class="modal inmodal fade" id="modal-hapus<?php echo $rows->id_user; ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content animated flipInY">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <b>Konfirmasi</b>
            </div>
            <div class="modal-body">
              <p>Apakah Anda yakin membatalkan cuti anda <strong><?php echo $rows->nama_lengkap; ?></strong> ini?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal">Tidak</button>
              <a href="<?php echo base_url(); ?>karyawan/delete_cuti/<?php echo $rows->id_user; ?>" class="btn btn-primary">Ya</a>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>

    <script>
      $(document).ready(function () {

        $('#sdate_cuti').change(function () {
          $("#edate_cuti").val($("#sdate_cuti").val());
          $("#jml").val(1);
        })
        $("#edate_cuti").change(function () {
          var sdate = new Date($("#sdate_cuti").val());
          var edate = new Date($("#edate_cuti").val());
          if (sdate > edate) {
            alert('End Date tidak boleh lebih dulu');
            $("#edate_cuti").val($("#sdate_cuti").val());
            return false;
          }
          var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
          var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
          $("#jml").val(diffDays);

        });
        $('#sdate_sakit').change(function () {
          $("#edate_sakit").val($("#sdate_sakit").val());
          $("#jml_sakit").val(1);
        })
        $("#edate_sakit").change(function () {
          var sdate = new Date($("#sdate_sakit").val());
          var edate = new Date($("#edate_sakit").val());
          if (sdate > edate) {
            alert('Choose start date first');
            $("#edate_sakit").val($("#sdate_sakit").val());
            return false;
          }
          var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
          var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
          $("#jml_sakit").val(diffDays);

        });



      })


    </script>
    <script>
$("#btnSearch").click(function(){ // Ketika user mengklik tombol Cari 
  var tglawal=$("#tglawal").val();
  var tglakhir=$("#tglakhir").val();
  $("#loading").show();
  $.ajax({
    type:'POST',
    data:'tglawal='+tglawal+'&tglakhir='+tglakhir,
    url: 'report/get_leave',
    dataType: 'text',
    success: function(data){
      $("#loading").hide();
      var html=JSON.parse(data);
      console.log(html);
      console.log(tglakhir);

      var table =$("#r_overtime").dataTable();
      table.fnClearTable();
      table.fnDraw();
      table.fnDestroy();
      var i=0;
      $.each(html, function (index, val) {
        var status = val.status; 
        if (status == 1) {
          var stat = '<p class="btn btn-sm btn-info" disabled><b>Waiting</b></p>';
        }else if (status == 2){
          var stat = '<p class="btn btn-sm btn-success" disabled><b>Reviewed</b></p>';
          var print = '<p><i class="fa  fa-print">  Print</i></p>';
          var rejected_reason = '-';
        }else if(status == 3){
          var stat = '<p class="btn btn-sm btn-primary" disabled><b>Approved</b></p>';
          var print = '<a href="<?php echo base_url()."report/print/".$rows->id_om?>" target="_blank" class="btn btn-sm btn-outline btn-info"><i class="fa  fa-print">  Print</i></a>';
          var rejected_reason = '-';
        }else if(status ==4){
          var stat = '<p class="btn btn-sm btn-danger" disabled><b>Rejected</b></p>';
          var print = '<p>Surat Anda Ditolak</p>';
          var rejected_reason = val.rejected_reason;
        }
        i++;

        var row = '<tr>'+
        '<td>'+i+'</td>'+
        '<td>'+val.nama_lengkap+'</td>'+
        '<td>'+val.jenis_cuti+'</td>'+
        '<td>'+val.start_date+'-'+val.end_date+'</td>'+
        '<td>'+val.jumlah_leave+'</td>'+
        '<td>'+val.sisa_cuti+'</td>'+
        '<td>'+val.insert+'</td>'+
        '<td>'+rejected_reason+'</td>'+
        '<td>'+stat+'</td>'+
        '<td>'+print+'</td>'+
        '</tr>';

        $("#r_overtime > tbody").append(row);
      });
      $('#r_overtime').DataTable();
    },
    error: function (xhr,status,error) {
      console.log('error');
    },
    statusCode: {
      404: function() {
        alert("page not found");
      }
    }

  });
});
</script>