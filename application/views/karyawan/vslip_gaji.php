<div class="row">
	<div class="col-lg-6">  
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Slip Gaji</h5>
			</div>
			<div class="ibox-content">
				<div class="row">
					
					<div class="row">
						<div class="col-lg-12">
							<table id="r_overtime" class="table table-striped table-responsive table-bordered data-table table-hover dataTables-example">
								<thead>
									<tr>
										<th width="10px">No.</th>
										<th>Periode</th>
										<th>Print</th>
									</tr>
								</thead>
								<tbody id="baris">
									<?php 
									$perpage = $this->uri->segment(4);
									$no=1+$perpage;  
									foreach ($data_report as $rows) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $rows->bulan; ?> <?php echo $rows->tahun; ?></td>
											<td>
												
												<a href="<?php echo base_url()."karyawan/print_slip/".$rows->id?>" target="_blank" class="btn btn-sm btn-outline btn-info"><i class="fa  fa-print">  Print</i></a>
											</td>   
										</tr>
										<?php $no++; } ?>
									</tbody>
								</table>

							</div>	
						</div>
						<hr>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php foreach ($data_report as $rows) { ?>	
		<div class="modal inmodal" id="modal-realization<?php echo $rows->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content animated flipInY">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">CLOSE</span></button>
						<h2>Realization Time</h2>
					</div>
					<form autocomplete="off" method="post" action="<?php echo base_url(); ?>report/input_realization/" class="form-horizontal">
						<div class="modal-body" style="padding:50">
							<div class="form-group">
								<input type="hidden" value="<?php echo $rows->id; ?>" name="id">  
								<div class="col-sm-3">					
									<label class="col-sm-12 control-label">Start Time</label>
								</div>
								<div class="col-sm-9">						
									<div class="input-group clockpicker" data-autoclose="true">
										<input type="text" class="form-control" name="start_time" placeholder="08:00">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-3">					
									<label class="col-sm-12 control-label">End Time</label>
								</div>
								<div class="col-sm-9">
									<div class="input-group clockpicker" data-autoclose="true">
										<input type="text" class="form-control" name="end_time" placeholder="16:00">
									</div>
								</div>
							</div>	
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-white pull-left" data-dismiss="modal">NO</button>
							<button type="submit" class="btn btn-primary">YES</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php } ?>
	<script type="text/javascript">
		$('.clockpicker').clockpicker();

$("#btnSearch").click(function(){ // Ketika user mengklik tombol Cari 
	var tglawal=$("#tglawal").val();
	var tglakhir=$("#tglakhir").val();
	$("#loading").show();
	$.ajax({
		type:'POST',
		data:'tglawal='+tglawal+'&tglakhir='+tglakhir,
		url: 'report/get_overtime',
		dataType: 'text',
		success: function(data){
			$("#loading").hide();
			var html=JSON.parse(data);
			console.log(html);

			var table =$("#r_overtime").dataTable();
			table.fnClearTable();
			table.fnDraw();
			table.fnDestroy();
			var i=0;
			$.each(html, function (index, val) {
				var status = val.status; 
				if (status == 1) {
					var stat = '<p class="btn btn-sm btn-info" disabled><b>Waiting</b></p>';
					var action ='';
				}else if (status == 2){
					var stat = '<p class="btn btn-sm btn-success" disabled><b>Reviewed</b></p>';
					var action ='';
				}else if(status == 3){
					var stat = '<p class="btn btn-sm btn-primary" disabled><b>Approved</b></p>';
					var action = '<a href="#modal-realization<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-info" title="Input Realization"><i class="fa fa-edit"></i></a>';
				}else if(status ==4){
					var stat = '<p class="btn btn-sm btn-danger" disabled><b>Rejected</b></p>';
					var action ='';
				}
				i++;
				var row = '<tr>'+
				'<td>'+i+'</td>'+
				'<td>'+val.nama_lengkap+'</td>'+
				'<td>'+val.jenis_layanan+'</td>'+
				'<td>'+val.tanggal+'</td>'+
				'<td>'+val.start_time+'-'+val.end_time+'</td>'+
				'<td>'+val.jama+'</td>'+
				'<td>'+val.alasan+'</td>'+
				'<td>'+val.real_starttime+'-'+val.real_endtime+'</td>'+
				'<td>'+val.jamb+'</td>'+
				'<td>'+stat+'</td>'+
				'<td>'+action+'</td>'+
				'</tr>';

				$("#r_overtime > tbody").append(row);
			});
			$('#r_overtime').DataTable();
		},
		error: function (xhr,status,error) {
			console.log('error');
		},
		statusCode: {
			404: function() {
				alert("page not found");
			}
		}

	});
});
</script>