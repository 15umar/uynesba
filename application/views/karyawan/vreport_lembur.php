<div class="row">
	<div class="col-lg-12">  
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Lembur</h5>
			</div>
			<div class="ibox-content">
				<table id="r_overtime" class="table table-striped table-bordered table-hover dataTables-example">
					<thead>
						<tr>
							<th width="10px">No.</th>
							<th>Nama</th>
							<th>Jenis Lembur</th>
							<th>Tanggal</th>
							<th>Selesai</th>
							<th>Jam</th>
							<th>Keterangan</th>
							<th>Status</th>
							<th>Print</th>
						</tr>
					</thead>
					<tbody id="baris">
						<?php 
						$perpage = $this->uri->segment(4);
						$no=1+$perpage;  
						foreach ($data_report as $rows) { ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $rows->nama_lengkap; ?></td>
								<td><?php echo $rows->jenis_lembur; ?></td>
								<td><?php echo date('d/m/Y H:i', strtotime($rows->start_datetime)); ?></td>
								<td><?php echo date('d/m/Y H:i', strtotime($rows->end_datetime)); ?></td>
								<td><?php
								$s_date = new DateTime($rows->start_datetime);
								$e_date = new DateTime($rows->end_datetime);
								$diff = date_diff($s_date, $e_date); 
								echo ($diff->format('%h')).' Hours';
								?></td>
								<td><?php echo $rows->alasan; ?></td>  
								<td>
									<?php if($rows->status=='1'){?>
										<p><b>Menunggu</b></p>			
									<?php }elseif($rows->status=='2'){ ?>
										<p><b>Diterima SPV</b></p>	
									<?php }elseif($rows->status=='3'){ ?>
										<p><b>Diterima SPV & Manager</b></p>
									<?php }elseif($rows->status=='4'){ ?>
										<p><b>Ditolak</b></p>		
									<?php }?>
								</td>    
								<td>
									<?php
									if(($rows->status=='1')){?>
										<a href="#modal-hapus<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger"><i class="fa fa-close"></i> Batal</a>
										<?php
									}elseif(($rows->status=='2')){
										?>
										<p>Surat Diterima SPV</p>
										<?php
									}elseif(($rows->status=='4')){
										?>
										<p>Surat Ditolak</p>
										<?php
									}elseif(($rows->status=='3')){ 
										?>
										<a href="<?php echo base_url()."karyawan/print_lembur/".$rows->id?>" target="_blank" class="btn btn-sm btn-outline btn-info"><i class="fa  fa-print">  Print</i></a>
									<?php }?>
								</td>   
							</tr>
							<?php $no++; } ?>
						</tbody>
					</table>

					<hr>
				</div>
			</div>
		</div>
	</div>
	<?php foreach ($data_report as $rows) { ?>	
		<div class="modal inmodal fade" id="modal-hapus<?php echo $rows->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content animated flipInY">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<b>Konfirmasi</b>
					</div>
					<div class="modal-body">
						<p>Apakah Anda yakin membatalkan lembur anda <strong><?php echo $rows->nama_lengkap; ?></strong> ini?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">Tidak</button>
						<a href="<?php echo base_url(); ?>karyawan/delete_lembur/<?php echo $rows->id; ?>" class="btn btn-primary">Ya</a>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<script type="text/javascript">
		$('.clockpicker').clockpicker();

$("#btnSearch").click(function(){ // Ketika user mengklik tombol Cari 
	var tglawal=$("#tglawal").val();
	var tglakhir=$("#tglakhir").val();
	$("#loading").show();
	$.ajax({
		type:'POST',
		data:'tglawal='+tglawal+'&tglakhir='+tglakhir,
		url: 'report/get_overtime',
		dataType: 'text',
		success: function(data){
			$("#loading").hide();
			var html=JSON.parse(data);
			console.log(html);

			var table =$("#r_overtime").dataTable();
			table.fnClearTable();
			table.fnDraw();
			table.fnDestroy();
			var i=0;
			$.each(html, function (index, val) {
				var status = val.status; 
				if (status == 1) {
					var stat = '<p class="btn btn-sm btn-info" disabled><b>Waiting</b></p>';
					var action ='';
				}else if (status == 2){
					var stat = '<p class="btn btn-sm btn-success" disabled><b>Reviewed</b></p>';
					var action ='';
				}else if(status == 3){
					var stat = '<p class="btn btn-sm btn-primary" disabled><b>Approved</b></p>';
					var action = '<a href="#modal-realization<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-info" title="Input Realization"><i class="fa fa-edit"></i></a>';
				}else if(status ==4){
					var stat = '<p class="btn btn-sm btn-danger" disabled><b>Rejected</b></p>';
					var action ='';
				}
				i++;
				var row = '<tr>'+
				'<td>'+i+'</td>'+
				'<td>'+val.nama_lengkap+'</td>'+
				'<td>'+val.jenis_layanan+'</td>'+
				'<td>'+val.tanggal+'</td>'+
				'<td>'+val.start_time+'-'+val.end_time+'</td>'+
				'<td>'+val.jama+'</td>'+
				'<td>'+val.alasan+'</td>'+
				'<td>'+val.real_starttime+'-'+val.real_endtime+'</td>'+
				'<td>'+val.jamb+'</td>'+
				'<td>'+stat+'</td>'+
				'<td>'+action+'</td>'+
				'</tr>';

				$("#r_overtime > tbody").append(row);
			});
			$('#r_overtime').DataTable();
		},
		error: function (xhr,status,error) {
			console.log('error');
		},
		statusCode: {
			404: function() {
				alert("page not found");
			}
		}

	});
});
</script>