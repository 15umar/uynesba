<div class="row">
	<div class="col-lg-12">  
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Perjalanan Dinas</h5>
			</div>
			<div class="ibox-content">
				<table class="table table-striped table-bordered table-hover dataTables-example">
					<thead>
						<tr>
							<th width="10px">No.</th>
							<th>ID</th>
							<th>Nama</th>
							<th>Tanggal</th>
							<th>Jumlah</th>
							<th>Tujuan</th>
							<th>Nominal</th>
							<th>Status</th>
							<th>Tiket</th>
							<th>Print</th>
							<th>Pengajuan</th>
						</tr>
					</thead>
					<tbody id="baris">

						<?php 
						$perpage = $this->uri->segment(4);
						$no=1+$perpage;  
						foreach ($data_report as $rows) { ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $rows->id_dinas; ?></td>
								<td><?php echo $rows->nama_lengkap; ?></td>
								
								<td><?php echo date('d/m/Y', strtotime($rows->tanggal_pergi)); ?> - <?php echo date('d/m/Y', strtotime($rows->tanggal_pulang)); ?></td>
								<td><?php echo $rows->jumlah; ?> Hari</td>
								<td><?php echo $rows->tujuan; ?></td>   
								<td><?php echo $rows->totalsppd; ?></td>
								<!-- <td>
									<?php if($rows->pengajuan=='baru'){?>
										<p class="btn btn-sm btn-info" disabled><?php echo $rows->total; ?></p>                  
									<?php }elseif($rows->pengajuan=='perpanjangan'){ ?>
										<p class="btn btn-sm btn-success" disabled><?php echo $rows->totall; ?></p>  

									<?php }?>
								</td>  -->                   
								<td>
									<?php if($rows->status=='1'){?>
										<p><b>Menunggu</b></p>                  
									<?php }elseif($rows->status=='2'){ ?>
										<p><b>Diterima SPV</b></p>  
									<?php }elseif($rows->status=='3'){ ?>
										<p><b>Diterima SPV & Manager</b></p>  
									<?php }elseif($rows->status=='4'){ ?>
										<p><b>Ditolak</b></p>  
									<?php }?>
								</td>  
								<td>
									<a href="<?php echo base_url()."karyawan/tiket/".$rows->id_dinas?>" class="btn btn-sm btn-outline btn-info"><i class="fa  fa-plane" title="Insert Tiket"></i></a>
								</td>         
								<td>
									<?php
									if(($rows->status=='1')){?>
										<a href="#modal-hapus<?php echo $rows->id_dinas; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger"><i class="fa fa-close" title="batal"></i></a>
										<?php
									}elseif(($rows->status=='2')){
										?>
										<p>Menunggu Aprov Manager</p>
										<?php
									}elseif(($rows->status=='3')){ 
										?>
										<a href="<?php echo base_url()."karyawan/print_dinas/".$rows->id_dinas?>" target="_blank" class="btn btn-sm btn-outline btn-info"><i class="fa  fa-print"></i></a>
									<?php }?>
								</td>  
								<td>
									<?php if($rows->pengajuan=='baru'){?>
										<p class="btn btn-sm btn-danger" disabled><b>Baru</b></p>                  
									<?php }elseif($rows->pengajuan=='perpanjangan'){ ?>
										<p class="btn btn-sm btn-success" disabled><b>Perpanjangan</b></p>  
									<?php }elseif($rows->status=='3'){ ?>
										<p class="btn btn-sm btn-primary" disabled><b>Diterima SPV & Manajer</b></p>  
									<?php }elseif($rows->status=='4'){ ?>
										<p class="btn btn-sm btn-danger" disabled><b>Ditolak</b></p>  
									<?php }?>
								</td>                   
							</tr>
							<?php $no++; } ?>
						</tbody>
					</table>

					<hr>
				</div>
			</div>
		</div>
	</div>
	<?php foreach ($data_report as $rows) { ?>	
		<div class="modal inmodal fade" id="modal-hapus<?php echo $rows->id_dinas; ?>" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content animated flipInY">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<b>Konfirmasi</b>
					</div>
					<div class="modal-body">
						<p>Apakah Anda yakin membatalkan perjalanan dinas anda <strong><?php echo $rows->nama_lengkap; ?></strong> ini?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">Tidak</button>
						<a href="<?php echo base_url(); ?>karyawan/delete_dinas/<?php echo $rows->id_dinas; ?>" class="btn btn-primary">Ya</a>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<script>
$("#btnSearch").click(function(){ // Ketika user mengklik tombol Cari 
	var tglawal=$("#tglawal").val();
	var tglakhir=$("#tglakhir").val();
	$("#loading").show();
	$.ajax({
		type:'POST',
		data:'tglawal='+tglawal+'&tglakhir='+tglakhir,
		url: 'report/get_leave',
		dataType: 'text',
		success: function(data){
			$("#loading").hide();
			var html=JSON.parse(data);
			console.log(html);
			console.log(tglakhir);

			var table =$("#r_overtime").dataTable();
			table.fnClearTable();
			table.fnDraw();
			table.fnDestroy();
			var i=0;
			$.each(html, function (index, val) {
				var status = val.status; 
				if (status == 1) {
					var stat = '<p class="btn btn-sm btn-info" disabled><b>Waiting</b></p>';
				}else if (status == 2){
					var stat = '<p class="btn btn-sm btn-success" disabled><b>Reviewed</b></p>';
					var print = '<p><i class="fa  fa-print">  Print</i></p>';
					var rejected_reason = '-';
				}else if(status == 3){
					var stat = '<p class="btn btn-sm btn-primary" disabled><b>Approved</b></p>';
					var print = '<a href="<?php echo base_url()."report/print/".$rows->id_om?>" target="_blank" class="btn btn-sm btn-outline btn-info"><i class="fa  fa-print">  Print</i></a>';
					var rejected_reason = '-';
				}else if(status ==4){
					var stat = '<p class="btn btn-sm btn-danger" disabled><b>Rejected</b></p>';
					var print = '<p>Surat Anda Ditolak</p>';
					var rejected_reason = val.rejected_reason;
				}
				i++;

				var row = '<tr>'+
				'<td>'+i+'</td>'+
				'<td>'+val.nama_lengkap+'</td>'+
				'<td>'+val.start_date+'-'+val.end_date+'</td>'+
				'<td>'+val.jumlah_leave+'</td>'+
				'<td>'+val.sisa_cuti+'</td>'+
				'<td>'+val.insert+'</td>'+
				'<td>'+rejected_reason+'</td>'+
				'<td>'+stat+'</td>'+
				'<td>'+print+'</td>'+
				'</tr>';

				$("#r_overtime > tbody").append(row);
			});
			$('#r_overtime').DataTable();
		},
		error: function (xhr,status,error) {
			console.log('error');
		},
		statusCode: {
			404: function() {
				alert("page not found");
			}
		}

	});
});
</script>