<div class="row animated fadeInRight">
    <div class="col-md-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Pengajuan Lembur</h5>
            </div>
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="tab-content">

                        <?php foreach ($om as $rows) {} ?>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Nama</label>    
                            <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">                            
                            <div class="col-sm-9">
                                <input type="text" readonly="" value="<?php echo $this->session->userdata('nama'); ?>" name="nama" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">Jenis Lembur</label>
                            <div class="col-sm-9">
                                <select name="jenis_lembur" class="form-control" id="colorselector">

                                    <?php foreach ($jenislembur as $row) { ?>
                                        <option value="<?php echo $row->id; ?>">
                                            <?php echo $row->name; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <form  autocomplete="off" id="cuti" method="post" action="<?php echo base_url(); ?>karyawan/simpan_lembur" class="form-horizontal">
                            <div id="1" class="colors">
                                <input type="hidden" value="1" name="jenis_lembur">
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Lembur <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <select name="type_lembur" class="form-control" id="colorselector">
                                            <option value="Awal Waktu">Awal Waktu</option>
                                            <option value="Tengah Waktu">Tengah Waktu</option>
                                            <option value="Akhir Waktu">Akhir Waktu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" id="data_2">
                                    <label class="col-sm-3 control-label">Mulai Lembur <b style="color: red">*</b></label>
                                    <div class="col-sm-5">

                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="start_date" class="form-control" data-date-format="YYYY-MM-DD" placeholder="Year-Month-Date" required="" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                     <div class="input-group clockpicker" data-autoclose="true">
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        <input type="text" class="form-control" name="start_time" required=""  >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Selesai Lembur <b style="color: red">*</b></label>
                                <div class="col-sm-5">
                                    <!-- <div class="col-sm-5" style="padding-left:0px;"> -->
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="end_date" class="form-control" data-date-format="YYYY-MM-DD" placeholder="Year-Month-Date" required="" />
                                        </div>
                                        <!-- </div> -->

                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group clockpicker" data-autoclose="true">
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                            <input type="text" class="form-control" name="end_time" required="" >
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Keterangan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="alasan" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-3">
                                        <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form  autocomplete="off" id="cuti" method="post" action="<?php echo base_url(); ?>karyawan/simpan_lembur" class="form-horizontal">
                            <div id="2" class="colors" style="display:none">
                                <input type="hidden" value="2" name="jenis_lembur">
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Lembur <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <select name="type_lembur" class="form-control" id="colorselector">
                                            <option value="Libur Kerja">Libur Kerja</option>
                                            <option value="Libur Pemerintah">Libur Pemerintah</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row" id="data_2">
                                    <label class="col-sm-3 control-label">Mulai Lembur <b style="color: red">*</b></label>
                                    <div class="col-sm-5">

                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="start_date" class="form-control" data-date-format="YYYY-MM-DD" placeholder="Year-Month-Date" required="" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                     <div class="input-group clockpicker" data-autoclose="true">
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        <input type="text" class="form-control" name="start_time" required=""  >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Selesai Lembur <b style="color: red">*</b></label>
                                <div class="col-sm-5">
                                    <!-- <div class="col-sm-5" style="padding-left:0px;"> -->
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="end_date" class="form-control" data-date-format="YYYY-MM-DD" placeholder="Year-Month-Date" required="" />
                                        </div>
                                        <!-- </div> -->

                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group clockpicker" data-autoclose="true">
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                            <input type="text" class="form-control" name="end_time" required="" >
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Keterangan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="alasan" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-3">
                                        <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form  autocomplete="off" id="cuti" method="post" action="<?php echo base_url(); ?>karyawan/simpan_lembur" class="form-horizontal">
                            <div id="3" class="colors" style="display:none">
                                <input type="hidden" value="3" name="jenis_lembur">
                                <input type="hidden" value="Hari Keagamaan" name="type_lembur">
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <div class="form-group row" id="data_2">
                                    <label class="col-sm-3 control-label">Mulai Lembur <b style="color: red">*</b></label>
                                    <div class="col-sm-5">

                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="start_date" class="form-control" data-date-format="YYYY-MM-DD" placeholder="Year-Month-Date" required="" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                     <div class="input-group clockpicker" data-autoclose="true">
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                        <input type="text" class="form-control" name="start_time" required=""  >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Selesai Lembur <b style="color: red">*</b></label>
                                <div class="col-sm-5">
                                    <!-- <div class="col-sm-5" style="padding-left:0px;"> -->
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="end_date" class="form-control" data-date-format="YYYY-MM-DD" placeholder="Year-Month-Date" required="" />
                                        </div>
                                        <!-- </div> -->

                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group clockpicker" data-autoclose="true">
                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                            <input type="text" class="form-control" name="end_time" required="" >
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-3 control-label">Keterangan <b style="color: red">*</b></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="alasan" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-3">
                                        <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Keterangan</h5>
            </div>
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="tab-content">
                        <p>Pengajuan lembur tenaga kerja untuk meminimalisir kesalahan penginputan “Referensi ID”: Mohon diperhatikan</br>
                        • Hari Kerja Awal Waktu, Tengah Waktu atau Akhir Waktu</br>
                    • Hari Libur /</br>
                • Hari Libur Pemerintah </br>
            • Hari Raya Keagamaan (ditulis hari raya keagamaan yang diakomodir dalam perjanjian) </br>
        • Cuti Bersama</p>
    </div>
</div>
</div>
</div>
</div>


</div>
<!-- </div> -->
<script type="text/javascript">
    $('.clockpicker').clockpicker();
</script>

<script>  
    $(function() {
        $('#colorselector').change(function(){
            $('.colors').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>