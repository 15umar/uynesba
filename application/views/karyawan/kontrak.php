<div class="row">
  <div class="col-lg-12">  
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Data Kontrak</h5>
      </div>
      <div class="ibox-content">
        <div class="row">
          <div class="row">
            <div class="col-lg-12">
              <table id="r_overtime" class="table table-striped table-responsive table-bordered data-table table-hover dataTables-example">
                <thead>
                  <tr>
                    <th width="10px">No.</th>
                    <th>Nama</th>
                    <th>Awal Kontrak</th>
                    <th>Tanggal Berakhir</th>
                  </tr>
                </thead>
                <tbody id="baris">

                  <?php 
                  $perpage = $this->uri->segment(4);
                  $no=1+$perpage;  
                  foreach ($statuskontrak as $rows) { ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $rows->nama_lengkap; ?></td>   
                      <td><?php echo $rows->awal_kontrak; ?></td>
                      <td><?php echo $rows->akhir_kontrak; ?></td>               
                    </tr>
                    <?php $no++; } ?>
                  </tbody>
                </table>

              </div>  
            </div>
            <hr>
          </div>
        </div>
      </div>
    </div>
  </div>