<div class="row">
  <div class="col-lg-12">  
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Reminder Pengajuan Lembur</h5>
      </div>
      <div class="ibox-content">
        <div class="row">
          <div class="row">
            <div class="col-lg-12">
              <table id="r_overtime" class="table table-striped table-responsive table-bordered data-table table-hover dataTables-example">
                <thead>
                  <tr>
                    <th width="10px">No.</th>
                    <th>Nama</th>
                    <th>Tanggal Pengajuan</th>
                    <th>Lewat Dari Tanggal Pengajuan</th>
                  </tr>
                </thead>
                <tbody id="baris">

                  <?php 
                  $perpage = $this->uri->segment(4);
                  $no=1+$perpage;  
                  foreach ($reminder_lembur as $rows) { ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $rows->nama_lengkap; ?></td>   
                      <td><?php echo $rows->umar; ?></td>
                      <td><?php echo $rows->due_date; ?></td>               
                    </tr>
                    <?php $no++; } ?>
                  </tbody>
                </table>
                <p>Jika pengajuan sesuai mohon untuk dilakukan aproval dan jika tidak silahkan reject pada pengajuan tsb</p>
              </div>  
            </div>
            <hr>
          </div>
        </div>
      </div>
    </div>
  </div>