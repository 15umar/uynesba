<div class="row animated fadeInRight">
    <div class="col-md-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Pengajuan Izin</h5>
            </div>
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="tab-content">
                        <form  autocomplete="off" id="cuti" method="post" action="<?php echo base_url(); ?>karyawan/simpan_izin" class="form-horizontal">
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Total Izin</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly="" value="<?php echo $sakit[0]->jml_sakit ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Jenis Izin</label>
                                <div class="col-sm-9">
                                    <select name="jenis_leave" class="form-control">

                                        <?php foreach ($jeniscuti as $row) { ?>
                                            <option value="<?php echo $row->id; ?>">
                                                <?php echo $row->name; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Nama</label>
                                <input type="hidden" value="<?php echo $this->session->userdata('id_user'); ?>" name="id_user">
                                <div class="col-sm-9">
                                    <input type="text" readonly="" value="<?php echo $this->session->userdata('nama'); ?>" name="nama_lengkap"
                                    class="form-control">
                                </div>
                            </div>
                            <div class="form-group row" autocomplete="off" id="data_2">
                                <label class="col-sm-3 control-label">Mulai Cuti <b style="color: red">*</b></label>
                                <div class="col-sm-9">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type='text' name="start_date" class="form-control" data-date-format="YYYY-MM-DD"
                                        placeholder="Year-Month-Date" id="sdate_cuti"  required="" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" autocomplete="off" id="">
                                <label class="col-sm-3 control-label">Selesai Cuti <b style="color: red">*</b></label>
                                <div class="col-sm-9">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type='text' name="end_date" class="form-control" data-date-format="YYYY-MM-DD"
                                        placeholder="Year-Month-Date" id="edate_cuti" required=""  />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" id="data_2">
                                <label class="col-sm-3 control-label">Jumlah Cuti</label>
                                <div class="col-sm-9">
                                    <input type='text' name="jumlah_leave" id="jml" class="form-control"
                                    data-date-format="YYYY-MM-DD" readonly />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label">Keterangan <b style="color: red">*</b></label>
                                <div class="col-sm-9">
                                    <input type="text" name="keterangan" class="form-control" required="" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-sm-offset-3">
                                    <button class="btn btn-primary submit-update" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#sdate_cuti').change(function () {
            $("#edate_cuti").val($("#sdate_cuti").val());
            $("#jml").val(1);
        })
        $("#edate_cuti").change(function () {
            var sdate = new Date($("#sdate_cuti").val());
            var edate = new Date($("#edate_cuti").val());
            if (sdate > edate) {
                alert('End Date tidak boleh lebih dulu');
                $("#edate_cuti").val($("#sdate_cuti").val());
                return false;
            }
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            $("#jml").val(diffDays);
        });
        $('#sdate_sakit').change(function () {
            $("#edate_sakit").val($("#sdate_sakit").val());
            $("#jml_sakit").val(1);
        })
        $("#edate_sakit").change(function () {
            var sdate = new Date($("#sdate_sakit").val());
            var edate = new Date($("#edate_sakit").val());
            if (sdate > edate) {
                alert('Choose start date first');
                $("#edate_sakit").val($("#sdate_sakit").val());
                return false;
            }
            var timeDiff = Math.abs(edate.getTime() - sdate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24) + 1);
            $("#jml_sakit").val(diffDays);
        });
    })
</script>