<!DOCTYPE html>
<html>
<head>
  <title>print</title>
  <style type="text/css">
    .style1 {font-family: "Courier New"; font-size: 18px;}
    .style2 {font-size: 36px; }
  </style>
  <style type="text/css">
    .watermark {
      opacity: 0.5;
      color: BLACK;
      position: fixed;
      top: 25px;
      right: 1%;
    }
  </style>
</head>
<body onLoad="window.print()">
  <!-- <div class="watermark">
    <img src="<?php echo base_url(); ?>uploads/avatar/logo.JPG">
  </div> -->
  <br>
  <center>
    <img src="<?php echo base_url(); ?>uploads/avatar/logo.JPG" width='1200'>
  </center>
  <!-- <table width="1200" border="0" cellpadding="0" cellspacing="7" class="style1"> -->
    <?php $no = 0;
    foreach ($lembur as $rows){      
    ?>
    <table width="auto" border="0" cellpadding="0" cellspacing="7" class="style1">
      <tr>
        <td width="250">PERIODE</td>
        <td width="300">: <?php echo $rows->bulan; ?> <?php echo $rows->tahun; ?></td>
        <td width="250">JABATAN</td>
        <td width="400">: <?php echo $rows->jabatan; ?></td>
      </tr>
      <tr>
        <td>NIK</td>
        <td>: <?php echo $rows->nik; ?></td>
        <td>LOKASI</td>
        <td>: <?php echo $rows->lokasi; ?></td>
      </tr>
      <tr>
        <td>NAMA</td>
        <td>: <?php echo $rows->nama_lengkap; ?></td>
        <td>UNIT KERJA </td>
        <td>: <?php echo $rows->unit_kerja; ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Bank</td>
        <td colspan="3"><?php echo $rows->bank; ?> / <?php echo $rows->no_rek; ?></td>
      </tr>
    </table>
    <br />
    <table width="auto" border="0" cellpadding="0" cellspacing="7" class="style1">
      <tr>
        <td width="400"><strong>PENDAPATAN:</strong></td>
        <td width="500">&nbsp;</td>
        <td width="200">&nbsp;</td>
        <td width="100">&nbsp;</td>
      </tr>
      <tr>
        <td>Gaji pokok </td>
        <td>&nbsp;</td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->gapok,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>Tunjangan</td>
        <td>&nbsp;</td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->tunjangan,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>Uang Shift </td>
        <td>&nbsp;</td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->u_shift,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>Uang Lembur </td>
        <td>&nbsp;</td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->u_lembur,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>Uang THR </td>
        <td>&nbsp;</td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->thr,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>Uang Cuti </td>
        <td>&nbsp;</td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->u_cuti,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>Reward performance </td>
        <td>&nbsp;</td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->reward,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><strong>PENYESUAIAN:</strong></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Koreksi Periode Sebelumnya </td>
        <td>&nbsp;</td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->koreksi,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><strong>Total Gaji (Gross) </strong></td>
        <td><strong>= RP. </strong></td>
        <td><div align="right"><strong><?php echo number_format($rows->total1,0,',','.'); ?></strong></div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><strong>POTONGAN:</strong></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>BPJS TK 2% </td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->bpjs_tk,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>BPJS Kes 1% </td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->bpjs_kes,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>BPJS Pensiun </td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->jp,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>PPh 21 </td>
        <td>= RP. </td>
        <td><div align="right"><?php echo number_format($rows->pph21,0,',','.'); ?></div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><strong>Total Gaji (Net) </strong></td>
        <td><strong>= RP. </strong></td>
        <td><div align="right"><strong><?php echo number_format($rows->total2,0,',','.'); ?></strong></div></td>
      </tr>
    </table>
    <?php } ?>

  </body>
  </html>