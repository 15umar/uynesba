<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Sub Bidang</button>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID Sub Bidang</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($subbid as $rows) { ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->id_subbid; ?></td>
                                        <td><?php echo $rows->nama; ?></td>  
                                        <td><?php echo $rows->status; ?></td>                        
                                        <td>
                                            <a href="<?php echo base_url(); ?>superadmin/review_approval/<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-info" title="Approve"><i class="fa fa-pencil"></i></a>
                                            <a href="#modal-reject<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject"><i class="fa fa-trash-o"></i></a>
                                        </td> 
                                    </tr>          
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h2>Tambah Sub Bidang</h2>
            </div>
            <form method="post" action="<?php echo base_url(); ?>superadmin/simpan_subbidang/" class="form-horizontal">
                <div class="modal-body" style="padding:50">
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">ID Sub Bidang</label>
                        <div class="col-sm-9">
                            <input type="text" name="id_subbid" placeholder="SUB001" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama" placeholder="INFRASTRUKTUR CONTACT CENTER" class="form-control">
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>