<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah SPPD</button>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID Transportasi</th>
                                    <th>Nama Transportasi</th>
                                    <th>Penginapan</th>
                                    <th>Biaya Makan</th>
                                    <th>Uang Saku</th>
                                    <th>Angkutan Setempat</th>
                                    <th>Ke_Bandara</th>
                                    <th width="100px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($sppd as $rows) { ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->id_transportasi; ?></td>
                                        <td><?php echo $rows->nama_transportasi; ?></td>
                                        <td><?php echo $rows->penginapan; ?></td>
                                        <td><?php echo $rows->biaya_makan; ?></td>
                                        <td><?php echo $rows->uang_saku; ?></td>
                                        <td><?php echo $rows->angkutan_setempat; ?></td>      
                                        <td><?php echo $rows->ke_bandara; ?></td>                                
                                        <td>
                                            <a href="<?php echo base_url(); ?>admin/review_approval/<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-info" title="Approve"><i class="fa fa-pencil"></i></a>
                                            <a href="#modal-reject<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject"><i class="fa fa-trash-o"></i></a>
                                        </td> 
                                    </tr>          
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h2>Tambah SPPD</h2>
            </div>
            <form method="post" action="<?php echo base_url(); ?>superadmin/simpan_sppd/" class="form-horizontal">
                <div class="modal-body" style="padding:50">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">ID Transportasi</label>
                        <div class="col-sm-9">
                            <input type="text" name="id_transportasi" placeholder="1" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama Transportasi</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_transportasi" placeholder="Pesawat Udara" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Penginapan</label>
                        <div class="col-sm-9">
                            <input type="text" name="penginapan" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Biaya Makan</label>
                        <div class="col-sm-9">
                            <input type="text" name="biaya_makan" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Uang Saku</label>
                        <div class="col-sm-9">
                            <input type="text" name="uang_saku" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Angkutan Setempat</label>
                        <div class="col-sm-9">
                            <input type="text" name="angkutan_setempat" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ke - Bandara</label>
                        <div class="col-sm-9">
                            <input type="text" name="ke_bandara" class="form-control">
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>