

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add Data Organitazion</h5>
                <span class="hide" id="page">4.organisasi</span>
                <!-- <a href="#" class="btn btn-primary fa fa-plus pull-right"> <i class="fa fa-database"></i></a> -->
            </div>
            <div class="ibox-content">

                <form action="" method="post">
                    <div class="form-group  row "><label class="col-sm-2 col-form-label" for="id">Division ID</label>
                        <div class="col-md-1"><input type="text" class="form-control" id="id" name="id"></div>
                        <span class="form-text m-b-none text-danger"><?= form_error('id');?></span>
                    </div>
                    <div class="form-group  row "><label class="col-sm-2 col-form-label" for="divisi">Name of Division</label>
                        <div class="col-md-3"><input type="text" class="form-control" id="divisi" name="divisi"></div>
                        <span class="form-text m-b-none text-danger"><?= form_error('divisi');?></span>
                    </div>
                    <div class="form-group  row "><label class="col-sm-2 col-form-label" for="bidang">Name of Sub Division</label>
                        <div class="col-md-3"><input type="text" class="form-control" id="bidang" name="bidang"></div>
                        <span class="form-text m-b-none text-danger"><?= form_error('bidang');?></span>
                    </div>
                    <div class="form-group row"><label class="col-sm-2 col-form-label" for="mother">Mother Of Division</label>
                        <div class="col-md-3"><input type="text" class="form-control" id="mother" name="mother">
                        </div>
                        <span class="form-text m-b-none text-success">*Allowed to be empty</span>
                    </div>
                    <div class="form-group row"><label class="col-sm-2 col-form-label" for="child">Child Of Division</label>
                        <div class="col-md-3"><input type="text" class="form-control" id="child" name="child">
                        </div>
                        <span class="form-text m-b-none text-success">*Allowed to be empty</span>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label" for="status">Status</label>
                        <div class="col-md-2">
                          <select class="form-control m-b" id="status" name="status">
                            <option value="1" active>Active</option>
                            <option value="0">Disable</option>
                        </select>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <button class="btn btn-white btn-sm" type="button" ><a href="<?= base_url() ?>config/organisasi">Cancel</a></button>
                        <button class="btn btn-primary btn-sm" type="submit">Save</button>
                    </div>
                </div>
            </form>
                    <!-- <table id="tab_leave" class="table table-striped table-responsive table-bordered data-table table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th class="no">No.</th>
                                <th>Name</th>
                                <th>Quota</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

						            </tbody>
                              </table> -->
                          </div>
                      </div>
                  </div>
              </div>