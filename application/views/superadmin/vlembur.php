<?php
if($this->session->flashdata('sukses')) {
	echo '<div class="alert alert-success"><i class="fa fa-check"></i> ';
	echo $this->session->flashdata('sukses');
	echo '</div>';
}else if($this->session->flashdata('gagal')){
	echo '<div class="alert alert-danger"><i class="fa fa-close"></i> ';
	echo $this->session->flashdata('gagal');
	echo '</div>';	
}
$user_level= $this->session->userdata('level');
?>
<script src="<?php echo base_url(); ?>assets/css/plugins/datapicker/clockpicker.css"></script>
<div class="row">
	<div class="col-lg-12">  
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Approval Overtime</h5>
			</div>
			<div class="ibox-content">
				<div class="row">
					<div class="col-lg-12">
						<form name="form" action=<?php echo base_url('approval/reviewa_all') ?> method="post" >
							<input type="hidden" class="form-control" id="user_level" name="user_level" value="<?php echo $user_level ?>" >
							<table class="table table-striped table-bordered table-hover dataTables-example">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Start - End</th>
										<th>Jam</th>
										<th>Keterangan</th>
										<th>Status</th>
										<th>Tgl Aprov</th>
									</tr>
								</thead>
								<tbody>
									<?php
						/* if($data_lembur==null){?>
                        <tr>
                            <td colspan="9">No Data Available</td>
						</tr>
					<?php } */
					$perpage = $this->uri->segment(4);
					$no=1+$perpage;  
					foreach ($data_lembur as $rows) { ?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $rows->nama_lengkap; ?></td>
							<td><?php echo $rows->start_datetime; ?> - <b><?php echo $rows->end_datetime; ?></b></td>
							<td><?php
							$s_date = new DateTime($rows->start_datetime);
							$e_date = new DateTime($rows->end_datetime);
							$diff = date_diff($s_date, $e_date); 
							echo ($diff->format('%h')).' Jam';
							?></td>
							<td><?php echo $rows->alasan; ?></td>  
							<td>
								<?php if($rows->status=='1'){?>
									<p class="btn btn-sm btn-info" disabled><b>Menunggu</b></p>			
								<?php }elseif($rows->status=='2'){ ?>
									<p class="btn btn-sm btn-primary" disabled><b>Diterima SPV</b></p>	
								<?php }elseif($rows->status=='3'){ ?>
									<p class="btn btn-sm btn-success" disabled><b>Diterima SPV & Manager</b></p>
								<?php }elseif($rows->status=='4'){ ?>
									<p class="btn btn-sm btn-primary" disabled><b>Ditolak</b></p>		
								<?php }?>
							</td>
							<td><?php echo $rows->reviewed_date; ?></td>  

						</tr>
						<?php $no++; } ?>
					</tbody>
				</table>		
			</form>					
		</div>	
	</div>
	<hr>
</div>
</div>
</div>
</div>
