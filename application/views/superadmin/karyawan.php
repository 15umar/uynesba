<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Karyawan</button>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-responsive table-bordered data-table table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No Induk</th>
                                    <th>Nama</th>
                                    <th style="display:none;">TTL</th>
                                    <th style="display:none;">Jenis Kelamin</th>
                                    <th style="display:none;">Agama</th>
                                    <th style="display:none;">No Telepon</th>
                                    <th style="display:none;">Status</th>
                                    <th style="display:none;">NIK</th>
                                    <th style="display:none;">Pendidikan Terakhir</th>
                                    <th style="display:none;">Jurusan</th>
                                    <th style="display:none;">NPWP</th>
                                    <th style="display:none;">BPJSTK</th>
                                    <th style="display:none;">BPJS</th>
                                    <th style="display:none;">No Rekening</th>
                                    <th style="display:none;">Bank</th>
                                    <th style="display:none;">Nama</th>
                                    <th style="display:none;">Gaji Pokok</th>
                                    <th style="display:none;">Uang Makan</th>
                                    <th style="display:none;">Uang Pulsa</th>
                                    <th>Email</th>
                                    <th>Client</th>
                                    <th>Jabatan</th>
                                    <th>Supervisor</th>
                                    <th style="display:none;">Manager</th>
                                    <th style="display:none;">Kontrak</th>
                                    <th>Level</th>
                                    <th width="70px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($karyawan as $rows) { ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->no_induk; ?></td> 
                                        <td><?php echo $rows->nama_lengkap; ?></td>  
                                        <td style="display:none;"><?php echo $rows->tempat_lahir; ?>, <?php echo $rows->tgl_lahir; ?></td>
                                        <td style="display:none;"><?php echo $rows->jenis_kelamin; ?></td>
                                        <td style="display:none;"><?php echo $rows->agama; ?></td>
                                        <td style="display:none;"><?php echo $rows->no_telepon; ?></td>
                                        <td style="display:none;"><?php echo $rows->status_perkawinan; ?></td>
                                        <td style="display:none;"><?php echo $rows->no_ktp; ?></td>
                                        <td style="display:none;"><?php echo $rows->pendidikan_terakhir; ?></td>
                                        <td style="display:none;"><?php echo $rows->jurusan; ?></td>
                                        <td style="display:none;"><?php echo $rows->no_npwp; ?></td>
                                        <td style="display:none;"><?php echo $rows->no_bpjs_ketenagakerjaan; ?></td>
                                        <td style="display:none;"><?php echo $rows->no_bpjs_kesehatan; ?></td>
                                        <td style="display:none;"><?php echo $rows->no_rek; ?></td>
                                        <td style="display:none;"><?php echo $rows->bank; ?></td>
                                        <td style="display:none;"><?php echo $rows->nama_sesuai_rekening; ?></td>
                                        <td style="display:none;"><?php echo $rows->gapok; ?></td>
                                        <td style="display:none;"><?php echo $rows->uang_makan; ?></td>
                                        <td style="display:none;"><?php echo $rows->uang_pulsa; ?></td>
                                        <td><?php echo $rows->email; ?></td>
                                        <td><?php echo $rows->client; ?></td>
                                        <td><?php echo $rows->jabatan; ?></td> 
                                        <td><?php echo $rows->spv; ?></td> 
                                        <td style="display:none;"><?php echo $rows->nama; ?></td>
                                        <td style="display:none;"><?php echo $rows->awal_kontrak; ?> s/d <?php echo $rows->akhir_kontrak; ?></td>
                                        <td><?php echo $rows->name; ?></td>                       
                                        <td>
                                            <a href="#modal-edit<?php echo $rows->no_induk; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-info" title="Approve"><i class="fa fa-pencil"></i></a>
                                            <a href="#modal-reset<?php echo $rows->no_induk; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-warning" title="Approve"><i class="fa fa-gear"></i></a>
                                            <a href="#modal-reject<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject"><i class="fa fa-trash-o"></i></a>
                                        </td> 
                                    </tr>          
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h2>Tambah Karyawan</h2>
            </div>
            <form method="post" action="<?php echo base_url(); ?>superadmin/simpan_karyawan/" class="form-horizontal">
                <div class="modal-body" style="padding:50">
                    <input type="hidden" name="password" value="12345">
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">No Induk</label>
                        <div class="col-sm-9">
                            <input type="text" name="no_induk" placeholder="123" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_lengkap" onkeyup="this.value = this.value.toUpperCase()" placeholder="Umar" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <input type="text" name="email" placeholder="email@gmail.com" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Tempat Lahir</label>
                        <div class="col-sm-9">
                            <input type="text" name="tempat_lahir" placeholder="Medan" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row" autocomplete="off" id="data_2">
                        <label class="col-sm-3 control-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type='text' name="tgl_lahir" class="form-control" data-date-format="YYYY-MM-DD"
                                placeholder="Year-Month-Date"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Level</label>
                        <div class="col-sm-9">
                            <select name="level" class="form-control">
                                <?php foreach ($level as $row) { ?>
                                    <option value="<?php echo $row->id; ?>">
                                        <?php echo $row->name; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Jabatan</label>
                        <div class="col-sm-9">
                            <input type="text" name="jabatan" placeholder="Jabatan" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Organisasi</label>
                        <div class="col-sm-9">
                            <select name="organisasi" class="form-control">
                                <?php foreach ($organisasi as $row) { ?>
                                    <option value="<?php echo $row->id_personal; ?>">
                                        <?php echo $row->spv; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" autocomplete="off" id="data_2">
                        <label class="col-sm-3 control-label">Awal Kontrak</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type='text' name="awal_kontrak" class="form-control" data-date-format="YYYY-MM-DD"
                                placeholder="Year-Month-Date"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" autocomplete="off" id="data_2">
                        <label class="col-sm-3 control-label">Akhir Kontrak</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type='text' name="akhir_kontrak" class="form-control" data-date-format="YYYY-MM-DD"
                                placeholder="Year-Month-Date"/>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php foreach ($karyawan as $rows) { ?>
<div class="modal fade" id="modal-edit<?php echo $rows->no_induk; ?>" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h2>Edit Karyawan</h2>
            </div>
            <form method="post" action="<?php echo base_url(); ?>superadmin/update_karyawan/" class="form-horizontal">
                <div class="modal-body" style="padding:50">
                    <!-- <input type="hidden" name="password" value="12345"> -->
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">No Induk</label>
                        <div class="col-sm-9">
                            <input type="text" name="no_induk" value="<?php echo $rows->no_induk; ?>" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_lengkap" onkeyup="this.value = this.value.toUpperCase()" value="<?php echo $rows->nama_lengkap; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" value="<?php echo $rows->email; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Jabatan</label>
                        <div class="col-sm-9">
                            <input type="text" name="jabatan" value="<?php echo $rows->jabatan; ?>" class="form-control">
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label class="col-sm-3 control-label">Organisasi</label>
                        <div class="col-sm-9">
                            <select name="organisasi" class="form-control">
                                <?php foreach ($organisasi as $row) { ?>
                                    <option value="<?php echo $row->id_personal; ?>">
                                        <?php echo $row->spv; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        
                        </div>
                    </div> -->
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>


<?php foreach ($karyawan as $rows) { ?>
<div class="modal fade" id="modal-reset<?php echo $rows->no_induk; ?>" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h2>Reset Password <?php echo $rows->nama_lengkap; ?></h2>
            </div>
            <form method="post" action="<?php echo base_url(); ?>superadmin/reset_karyawan/" class="form-horizontal">
                <div class="modal-body" style="padding:50">
                    <input type="hidden" name="no_induk" value="<?php echo $rows->no_induk; ?>">
                    <input type="hidden" name="password" value="12345">
                    
                    <div class="form-group row">
                        <h2>Silahkan klik reset untuk melakukan reset password default yaitu 12345</h2>
                    </div> 
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>