<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Organisasi</button>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID Organisasi</th>
                                    <th>Client</th>
                                    <th>Bidang</th>
                                    <th>Sub Bidang</th>
                                    <th>Supervisor</th>
                                    <th>Manager</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($organisasi as $rows) { ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->id_personal; ?></td>
                                        <td><?php echo $rows->client; ?></td>  
                                        <td><?php echo $rows->bidang; ?></td> 
                                        <td><?php echo $rows->subbid; ?></td>
                                        <td><?php echo $rows->spv; ?></td> 
                                        <td><?php echo $rows->manajer; ?></td>                         
                                        <td>
                                            <a href="<?php echo base_url(); ?>superadmin/review_approval/<?php echo $rows->id_p; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-info" title="Approve"><i class="fa fa-pencil"></i></a>
                                            <a href="#modal-reject<?php echo $rows->id_p; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject"><i class="fa fa-trash-o"></i></a>
                                        </td> 
                                    </tr>          
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h2>Tambah Organisasi</h2>
            </div>
            <form method="post" action="<?php echo base_url(); ?>superadmin/simpan_organisasi/" class="form-horizontal">
                <div class="modal-body" style="padding:50">
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">ID Organisasi</label>
                        <div class="col-sm-9">
                            <input type="text" name="id_personal" placeholder="ICON001" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Client</label>
                        <div class="col-sm-9">
                            <select name="client" class="form-control">
                                <?php foreach ($client as $row) { ?>
                                    <option value="<?php echo $row->nama; ?>">
                                        <?php echo $row->nama; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Bidang</label>
                        <div class="col-sm-9">
                            <select name="bidang" class="form-control">
                                <?php foreach ($bidang as $row) { ?>
                                    <option value="<?php echo $row->nama; ?>">
                                        <?php echo $row->nama; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Sub Bidang</label>
                        <div class="col-sm-9">
                            <select name="subbid" class="form-control">
                                <?php foreach ($subbid as $row) { ?>
                                    <option value="<?php echo $row->nama; ?>">
                                        <?php echo $row->nama; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Supervisor</label>
                        <div class="col-sm-9">
                            <select name="spv" class="form-control">
                                <?php foreach ($supervisor as $row) { ?>
                                    <option value="<?php echo $row->nama; ?>">
                                        <?php echo $row->nama; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 control-label">Manager</label>
                        <div class="col-sm-9">
                            <select name="manajer" class="form-control">
                                <?php foreach ($manager as $row) { ?>
                                    <option value="<?php echo $row->nama; ?>">
                                        <?php echo $row->nama; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>