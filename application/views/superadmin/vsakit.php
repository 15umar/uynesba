<?php
if($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success"><i class="fa fa-check"></i> ';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}else if($this->session->flashdata('gagal')){
    echo '<div class="alert alert-danger"><i class="fa fa-close"></i> ';
    echo $this->session->flashdata('gagal');
    echo '</div>';  
}
$user_level= $this->session->userdata('level');
?>
<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Approval Leave</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <input type="hidden" class="form-control" id="user_level" name="user_level" value="<?php echo $user_level ?>" >
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Jumlah Cuti</th>
                                    <th>Sisa Cuti</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($data_cuti as $rows) { ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->nama_lengkap; ?></td>
                                        <td><?php echo $rows->start_date.' - '.$rows->end_date; ?></td>
                                        <td><?php echo $rows->jumlah_leave; ?></td>
                                        <td><?php echo $rows->sisa_cuti; ?></td>                                
                                        <td><?php echo $rows->keterangan; ?></td>    
                                        <td>
                                            <?php if($rows->status=='1'){?>
                                                <p class="btn btn-sm btn-info" disabled><b>Menunggu</b></p>                                  
                                            <?php }elseif($rows->status=='4'){ ?>
                                                <p class="btn btn-sm btn-danger" disabled><b>Ditolak</b></p>  
                                            <?php }elseif($rows->status=='3'){ ?>
                                                <p class="btn btn-sm btn-success" disabled><b>Diterima</b></p>   
                                            <?php }?>
                                        </td>   
                                        <td>
                                            <?php 
                                                if($this->session->userdata('level') == '1'){
                                                    if($rows->status=='1'){?> 
                                                        <a href="<?php echo base_url(); ?>admin/review_approval/<?php echo '3/'.$rows->id_user; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-info" title="Approve"><i class="fa fa-check"></i></a>
                                                        <a href="#modal-reject<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject"><i class="fa fa-ban"></i></a>
                                                    <?php }
                                                }
                                                ?>
                                        </td>
                                    </tr> 
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
