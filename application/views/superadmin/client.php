<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Client</button>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-responsive table-bordered data-table table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID Client</th>
                                    <th>Client</th>
                                    <th>Bidang</th>
                                    <th>Sub Bidang</th>
                                    <th>SPV</th>
                                    <th>Manajer</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($client as $rows) { ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->id_personal; ?></td>
                                        <td><?php echo $rows->client; ?></td>
                                        <td><?php echo $rows->bidang; ?></td>
                                        <td><?php echo $rows->subbid; ?></td>
                                        <td><?php echo $rows->spv; ?></td>      
                                        <td><?php echo $rows->manajer; ?></td>                                
                                        <td>
                                            <a href="<?php echo base_url(); ?>admin/review_approval/<?php echo $rows->id_p; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-info" title="Approve"><i class="fa fa-check"></i></a>
                                            <a href="#modal-reject<?php echo $rows->id_p; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject"><i class="fa fa-ban"></i></a>
                                        </td> 
                                    </tr>          
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h2>Tambah Client</h2>
            </div>
            <form method="post" action="<?php echo base_url(); ?>superadmin/simpan_client/" class="form-horizontal">
                <div class="modal-body" style="padding:50">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">ID Clien</label>
                        <div class="col-sm-9">
                            <input type="text" name="id_personal" placeholder="MUST001" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Client</label>
                        <div class="col-sm-9">
                            <input type="text" name="client" placeholder="PT. MUST" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Bidang</label>
                        <div class="col-sm-9">
                            <input type="text" name="bidang" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sub Bidang</label>
                        <div class="col-sm-9">
                            <input type="text" name="subbid" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Supervisor</label>
                        <div class="col-sm-9">
                            <input type="text" name="spv" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Manajer</label>
                        <div class="col-sm-9">
                            <input type="text" name="manajer" class="form-control">
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>