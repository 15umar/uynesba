<?php
if($this->session->flashdata('sukses')) {
	echo '<div class="alert alert-success"><i class="fa fa-check"></i> ';
	echo $this->session->flashdata('sukses');
	echo '</div>';
}else if($this->session->flashdata('gagal')){
	echo '<div class="alert alert-danger"><i class="fa fa-close"></i> ';
	echo $this->session->flashdata('gagal');
	echo '</div>';	
}
$user_level= $this->session->userdata('level');
?>
<script src="<?php echo base_url(); ?>assets/css/plugins/datapicker/clockpicker.css"></script>
<div class="row">
	<div class="col-lg-12">  
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Pengajuan Perjalanan Dinas</h5>
			</div>
			<div class="ibox-content">
				<div class="row">
					<div class="col-lg-12">
						<form name="form" method="post" action="<?php echo base_url('approval/reviewa_all') ?>"  >
							<input type="hidden" class="form-control" id="user_level" name="user_level" value="<?php echo $user_level ?>" >
							<table class="table table-striped table-bordered table-hover dataTables-example">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Tanggal</th>
										<th>Jumlah</th>
										<th>Tujuan</th>
										<th>Nominal</th>
										<th>Status</th>
										<th>Tgl Aprov</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$perpage = $this->uri->segment(4);
									$no=1+$perpage;  
									foreach ($data_perjalanan_dinas as $rows) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $rows->nama_lengkap; ?></td>
											<td><?php echo $rows->tanggal_pergi.' - '.$rows->tanggal_pulang; ?></td>
											<td><?php echo $rows->jumlah; ?></td>
											<td><?php echo $rows->tujuan; ?></td> 
											<td><?php echo $rows->total; ?></td>    

											<td>
												<?php if($rows->status=='1'){?>
													<p class="btn btn-sm btn-info" disabled><b>Menunggu</b></p>									
												<?php }elseif($rows->status=='2'){ ?>
													<p class="btn btn-sm btn-success" disabled><b>Menunggu Aprov Manager</b></p>	
												<?php }elseif($rows->status=='3'){ ?>
													<p class="btn btn-sm btn-primary" disabled><b>Diterima</b></p>	
												<?php }elseif($rows->status=='4'){ ?>
													<p class="btn btn-sm btn-danger" disabled><b>Ditolak</b></p>		
												<?php }?>
											</td>   
											<td><?php echo $rows->reviewed_date; ?></td>  
										</tr>
										<?php $no++; } ?>
									</tbody>
								</table>		
							</form>					
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php foreach ($data_perjalanan_dinas as $rows) { ?>  
		<div class="modal inmodal" id="modal-reject<?php echo $rows->id_dinas;?>" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content animated flipInY">
					<div class="modal-header">
						<h2>Apakah anda yakin?</h2>
					</div>
					<form method="post" action="<?php echo base_url(); ?>spv/reject_approval/" class="form-horizontal">
						<div class="modal-body" style="padding:50">
							<div class="form-group">
								<input type="hidden" value="2" name="kode">
								<input type="hidden" value="<?php echo $rows->id_dinas; ?>" name="id"> 
							</div>  
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">YES</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	<?php } ?>
