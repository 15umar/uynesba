<?php
$id_user  = $this->session->userdata('id_user'); 
$pengguna = $this->model_login->getBiodata($id_user);
foreach ($pengguna as $bio) {
    $namalengkap = $bio->nama_lengkap;
    $avatar      = $bio->foto;
    $level       = $bio->level;
    $alamat      = $bio->alamat;
}
?>
<div class="row animated fadeInRight">
    <div class="col-lg-4">
        <div class="widget-head-color-box navy-bg p-lg text-center">
            <div class="m-b-md">
                <h2 class="font-bold no-margins">
                    <?php echo $this->session->userdata('nama'); ?>
                </h2>
            </div>
            <img src="<?php echo base_url(); ?>uploads/avatar/<?php if ($avatar!=null) {echo $avatar; } else { echo 'pic_ava.jpg'; } ?>" class="img-circle circle-border m-b-md img-responsive"  alt="image">
        </div>
        <div class="widget-text-box">
            <p>Halo <strong><?php echo $this->session->userdata('nama'); ?></strong> anda adalah salah satu Karyawan ICON+. Alamat anda <a class="badge badge-xs badge-primary"><i class="fa fa-map-marker"></i> <?php echo $alamat; ?></a></p>
        </div>
    </div>
    <div class="col-md-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Profil Lengkap</h5>
            </div>
            <div class="ibox-content">
                <div class="panel-heading">
                    <div class="panel-options">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1">Info Personal</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2">Upload Avatar</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3">Ganti Password</a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <strong>Informasi Personal</strong>
                            <div class="hr-line-dashed"></div>
                            <form method="post" action="<?php echo base_url(); ?>superadmin/update_profile" class="form-horizontal form-update">
                                <div class="result-update"></div>
                                <?php foreach ($om as $rows) {} ?>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">No Induk</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="<?php echo $rows->no_induk; ?>" name="no_induk" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama OM</label>
                                    <input type="hidden" value="<?php echo $id_user ?>" name="id_user">
                                    <div class="col-sm-9">
                                        <input type="text" value="<?php echo $rows->nama_lengkap; ?>" name="nama" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Jabatan</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="<?php echo $rows->jabatan; ?>" id="jabatan" name="jabatan" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="<?php echo $rows->alamat; ?>" name="alamat" class="form-control">
                                    </div>
                                </div>                                       
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tempat Lahir</label>
                                    <div class="col-sm-9">
                                        <input type="text" value="<?php echo $rows->tempat_lahir; ?>" name="tempat_lahir" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group" id="data_2">
                                    <label class="col-sm-3 control-label">Tanggal Lahir</label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type='text' name="tgl_lahir" value="<?php echo $rows->tgl_lahir; ?>" class="form-control" name="tgl_lahir" data-date-format="YYYY-MM-DD" placeholder="Tahun-Bulan-Tanggal"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-9"> 
                                        <select name="jenis_kelamin" class="form-control">
                                            <option value="">Pilih Jenis Kelamin...</option>
                                            <option value="Laki-laki" <?php if($rows->jenis_kelamin=="Laki-laki") { echo "selected"; } ?>>Laki-laki</option>
                                            <option value="Perempuan" <?php if($rows->jenis_kelamin=="Perempuan") { echo "selected"; } ?>>Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Telepon / HP</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="telepon" value="<?php echo $rows->no_telepon; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="email" value="<?php echo $rows->email; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-3">
                                        <button class="btn btn-primary submit-update" type="submit">Simpan Perubahan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <strong>Upload Avatar</strong>
                            <div class="hr-line-dashed"></div>
                            <form method="post" action="superadmin/changeavatar" class="form-horizontal " enctype="multipart/form-data">
                                <input type="hidden" value="<?php echo $id_user ?>" name="id_user" >

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div data-provides="fileinput" class="fileinput fileinput-new">
                                            <div data-trigger="fileinput" style="width: 150px; height: 150px;" class="fileinput-preview thumbnail-circle">
                                                <?php if($rows->foto!="") { ?>
                                                    <img src="uploads/avatar/<?php echo $rows->foto; ?>" class="img-circle" alt="..."/>
                                                <?php } else { ?>
                                                    <img src="uploads/avatar/user.jpg" class="img-circle" alt="..."/>
                                                <?php } ?>
                                            </div>
                                            <div>
                                                <span class="btn btn-primary btn-file">
                                                    <span class="fileinput-new"><i class="fa fa-camera"></i> Choose Photo</span>
                                                    <span class="fileinput-exists">Ganti</span>
                                                    <input type="file" name="foto"/>
                                                </span>
                                                &nbsp;
                                                <a href="#" data-dismiss="fileinput" class="btn btn-danger fileinput-exists">Remove</a>
                                                <input type="hidden" name="avatar_lama" value="<?php echo $rows->foto; ?>">
                                                <p>Max. 1MB</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <button type="submit" class="btn btn-primary submit-reset">Upload Photo</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <strong>Ganti Password</strong>
                            <div class="hr-line-dashed"></div>
                            <form method="post" action="superadmin/update_password" class="form-horizontal form-reset">
                                <div class="result-reset"></div>
                                <input type="hidden" value="<?php echo $id_user ?>" name="id_user" >
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Password Sekarang</label>
                                    <div class="col-sm-6">
                                        <input type="password" name="password_lama" class="form-control input-dark">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Password Baru</label>
                                    <div class="col-sm-6">
                                        <input type="password" name="password_baru" class="form-control input-dark">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Masukan Lagi Password Baru</label>
                                    <div class="col-sm-6">
                                        <input type="password" name="repassword_baru" class="form-control input-dark">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 col-sm-offset-3">
                                        <button class="btn btn-primary submit-reset" type="submit">Ganti Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>