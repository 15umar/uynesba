   
<div class="row">
	<div class="col-lg-12">  
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5><a href="spv/create">Tambah Jadwal</a></h5>
				
			</div>
			<div class="ibox-content">
				<div class="row">
					<div class="col-lg-2">
						<div class="input-group date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type='text' id="start" autocomplete="off" name="start" placeholder="start" class="form-control"/>
						</div>
					</div>
					<div class="col-lg-2"> 
						<div class="input-group date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type='text' id="end" autocomplete="off" name="end" placeholder="end" class="form-control"/>
						</div>
					</div>
					<div class="col-lg-2"> 
						<button class="btn btn-success" type="button" id="btnshow"> <i class="fa fa-search"></i> Show</button>
					</div>
				</div>
				<hr>
				<div id="scheduleList" style="overflow-y: auto;"></div>
			</div>
		</div>
	</div>
</div>


<script src="<?php echo base_url()?>assets/js/apps/admin/schedule.js"></script>