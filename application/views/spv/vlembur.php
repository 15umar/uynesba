<?php
if($this->session->flashdata('sukses')) {
	echo '<div class="alert alert-success"><i class="fa fa-check"></i> ';
	echo $this->session->flashdata('sukses');
	echo '</div>';
}else if($this->session->flashdata('gagal')){
	echo '<div class="alert alert-danger"><i class="fa fa-close"></i> ';
	echo $this->session->flashdata('gagal');
	echo '</div>';	
}
$user_level= $this->session->userdata('level');
?>

<!-- <?php
// header("Content-type: application/vnd-ms-excel");
// header("Content-Disposition: attachment; filename=Data Pegawai.xls");
?> -->
<script src="<?php echo base_url(); ?>assets/css/plugins/datapicker/clockpicker.css"></script>
<div class="row">
	<div class="col-lg-12">  
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Pengajuan Lembur</h5>
			</div>
			<div class="ibox-content">
				<table id="r_overtime" class="table table-striped table-bordered table-hover dataTables-example">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Jenis Lembur</th>
							<th>Lembur</th>
							<th>Start</th>
							<th>End</th>
							<th>Jumlah Jam</th>
							<th>Keterangan</th>
							<th>Status</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody  id="baris">
						<?php
						$perpage = $this->uri->segment(4);
						$no=1+$perpage;  
						foreach ($data_lembur as $rows) { ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $rows->nama_lengkap; ?></td>
								<td><?php echo $rows->jenis_lembur; ?></td>
								<td><?php echo $rows->type_lembur; ?></td>
								<td><?php echo $rows->start_datetime; ?></td>
								<td><?php echo $rows->end_datetime; ?></td>
								<td><?php
								$s_date = new DateTime($rows->start_datetime);
								$e_date = new DateTime($rows->end_datetime);
								$diff = date_diff($s_date, $e_date); 
								echo ($diff->format('%h')).' Hours';
								?></td>
								<td><?php echo $rows->alasan; ?></td>  

								<td>
									<?php if($rows->status=='1'){?>
										<p class="btn btn-sm btn-info" disabled><b>Menunggu</b></p>									
									<?php }elseif($rows->status=='2'){ ?>
										<p class="btn btn-sm btn-primary" disabled><b>Diterima SPV</b></p>	
									<?php }elseif($rows->status=='3'){ ?>
										<p class="btn btn-sm btn-success" disabled><b>Diterima SPV dan Manager</b></p>
									<?php }elseif($rows->status=='4'){ ?>
										<p class="btn btn-sm btn-danger" disabled><b>Ditolak</b></p>		
									<?php }?>
								</td>   
								<td>								

									<?php 
									if($this->session->userdata('level') == '2'){
										if($rows->status=='1'){?> 
											<a href="<?php echo base_url(); ?>spv/review_approval/<?php echo '1/'.$rows->id; ?>" class="btn btn-sm btn-outline btn-info" title="Approve"><i class="fa fa-check"></i></a>
											<a href="#modal-reject<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject"><i class="fa fa-ban"></i></a>
										<?php }
									}
									?>

								</td>                      
							</tr>
							<?php $no++; } ?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</div>

	<?php foreach ($data_lembur as $rows) { ?>  
		<div class="modal inmodal fade" id="modal-reject<?php echo $rows->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content animated flipInY">
					<div class="modal-header">
						<h2>Apakah anda yakin?</h2>
					</div>
					<form method="post" action="<?php echo base_url(); ?>spv/reject_approval/" class="form-horizontal">
						<div class="modal-body" style="padding:50">
							<div class="form-group">
								<input type="hidden" value="1" name="kode">
								<input type="hidden" value="<?php echo $rows->id; ?>" name="id"> 
							</div>  
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">YES</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	<?php } ?>

	<script type="text/javascript">
		$('.clockpicker').clockpicker();

$("#btnSearch").click(function(){ // Ketika user mengklik tombol Cari 
	var tglawal=$("#tglawal").val();
	var tglakhir=$("#tglakhir").val();
	$("#loading").show();
	$.ajax({
		type:'POST',
		data:'tglawal='+tglawal+'&tglakhir='+tglakhir,
		url: 'admin/get_overtime',
		dataType: 'text',
		success: function(data){
			$("#loading").hide();
			var html=JSON.parse(data);
			console.log(html);

			var table =$("#r_overtime").dataTable();
			table.fnClearTable();
			table.fnDraw();
			table.fnDestroy();
			var i=0;
			$.each(html, function (index, val) {
				var status = val.status; 
				if (status == 1) {
					var stat = '<p class="btn btn-sm btn-info" disabled><b>Waiting</b></p>';
					var action ='';
				}else if (status == 2){
					var stat = '<p class="btn btn-sm btn-success" disabled><b>Reviewed</b></p>';
					var action ='';
				}else if(status == 3){
					var stat = '<p class="btn btn-sm btn-primary" disabled><b>Diterima</b></p>';
					var action = '<a href="#modal-realization<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-info" title="Input Realization"><i class="fa fa-edit"></i></a>';
				}else if(status ==4){
					var stat = '<p class="btn btn-sm btn-danger" disabled><b>Rejected</b></p>';
					var action ='';
				}
				i++;
				var row = '<tr>'+
				'<td>'+i+'</td>'+
				'<td>'+val.nama_lengkap+'</td>'+
				'<td>'+val.start_datetime+'</td>'+
				'<td>'+val.end_datetime+'</td>'+
				'<td>'+-+'</td>'+
				'<td>'+val.alasan+'</td>'+

				'<td>'+stat+'</td>'+
				'<td>'+action+'</td>'+
				'</tr>';

				$("#r_overtime > tbody").append(row);
			});
			$('#r_overtime').DataTable();
		},
		error: function (xhr,status,error) {
			console.log('error');
		},
		statusCode: {
			404: function() {
				alert("page not found");
			}
		}

	});
});
</script>