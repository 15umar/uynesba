
<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Pengajuan Sakit</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Jumlah Sakit</th>
                                    <th>Total Sakit</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($data_cuti as $rows) { ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->nama_lengkap; ?></td>
                                        <td><?php echo $rows->start_date.' - '.$rows->end_date; ?></td>
                                        <td><?php echo $rows->jumlah_leave; ?></td>
                                        <td><?php echo $rows->sisa_cuti; ?></td>                               
                                        <td><?php echo $rows->keterangan; ?></td>    
                                        <td>
                                            <?php if($rows->status=='1'){?>
                                                <p class="btn btn-sm btn-info" disabled><b>Menunggu</b></p>                                  
                                            <?php }elseif($rows->status=='4'){ ?>
                                                <p class="btn btn-sm btn-danger" disabled><b>Ditolak</b></p>  
                                            <?php }elseif($rows->status=='3'){ ?>
                                                <p class="btn btn-sm btn-success" disabled><b>Diterima</b></p>   
                                            <?php }?>
                                        </td>   
                                        <td>
                                            <?php 
                                            if($this->session->userdata('level') == '2'){
                                                if($rows->status=='1'){?> 
                                                    <a href="<?php echo base_url(); ?>spv/review_approval/<?php echo '3/'.$rows->id; ?>" class="btn btn-sm btn-outline btn-info" title="Approve"><i class="fa fa-check"></i></a>
                                                    <a href="#modal-reject<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject"><i class="fa fa-ban"></i></a>
                                                <?php }
                                            }
                                            ?>
                                        </td>
                                    </tr> 
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php foreach ($data_cuti as $rows) { ?>  
        <div class="modal inmodal fade" id="modal-reject<?php echo $rows->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <h2>Apakah anda yakin?</h2>
                    </div>
                    <form method="post" action="<?php echo base_url(); ?>spv/reject_approval/" class="form-horizontal">
                        <div class="modal-body" style="padding:50">
                            <div class="form-group">
                                <input type="hidden" value="3" name="kode">
                                <input type="hidden" value="<?php echo $rows->id; ?>" name="id"> 
                            </div>  
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">YES</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <?php } ?>
    