<link href="assets/css/multiple-select.css" rel="stylesheet">
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Atur Jadwal</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
				<div class="well" style="height: 75px; display: block;">
					<div class="row">

						<div class="col-lg-3">
							<select id="employee" class="form-control dinput" style="width: 225px !important; min-width: 200px; max-width: 250px;"></select>
						</div>
						<div class="col-lg-2">
							<div class="input-group date">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input type='text' id="start" autocomplete="off" name="start" placeholder="start" class="form-control"/>
							</div>
						</div>
						<div class="col-lg-2"> 
							<div class="input-group date">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								<input type='text' id="end" autocomplete="off" name="end" placeholder="end" class="form-control"/>
							</div>
						</div>
						<div class="col-lg-3">
							<textarea id="shiftdata" class="form-control" name="shiftdata" rows="1" cols="20" placeholder="Paste from excel"></textarea>
						</div>
						<div class="col-lg-1">
							<button class="btn btn-success" type="button" id="btnreport" > <i class="fa fa-search"></i> Search</button>
						</div>

					</div>
				</div>
				<div id="resultS" style="overflow-y: auto;"></div>
				
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url()?>assets/js/apps/admin/schedule.js"></script>