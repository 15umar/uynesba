<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Data Karyawan</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No Induk</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Jabatan</th>
                                        <th>SPV</th>
                                        <th>Status</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $perpage = $this->uri->segment(4);
                                    $no=1+$perpage;  
                                    foreach ($data_karyawan as $rows) { ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $rows->no_induk; ?></td>
                                            <td><?php echo $rows->nama_lengkap; ?></td>
                                            <td><?php echo $rows->email; ?></td>
                                            <td><?php echo $rows->jabatan; ?></td>     
                                            <td><?php echo $rows->spv; ?></td>     
                                            <td>
                                                <?php if($rows->active=='1'){?>
                                                    <p class="btn btn-sm btn-info" disabled><b>Aktif</b></p>                                  
                                                <?php }else{ ?>
                                                    <p class="btn btn-sm btn-danger" disabled><b>Non Aktif</b></p>    
                                                <?php }?>
                                            </td>       
                                            <!-- <td>
                                                <a href="#modal-reject<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject">Edit</a>
                                                    
                                            </td> -->
                                        </tr> 
                                        <?php $no++; } ?>
                                    </tbody>
                                </table>
                            </form>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>