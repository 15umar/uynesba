<div class="row">
	<div class="col-lg-3">
		<div class="col-lg-12">
			<div class="widget style1 navy-bg">
				<div class="row">
					<div class="col-4">
						<i class="fa fa-users fa-5x"></i>
					</div>
					<div class="col-8 text-right">
						<span> Data Karyawan </span>
						<h2 class="font-bold"><?php echo $member; ?></h2>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="widget style1 blue-bg">
				<div class="row">
					<div class="col-4">
						<i class="fa fa-bell-slash fa-5x"></i>
					</div>
					<div class="col-8 text-right">
						<span> Data Cuti | <?php echo $jmlcutiapp; ?></span>
						<h2 class="font-bold"><?php echo $jmlcuti; ?></h2>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="widget style1 red-bg">
				<div class="row">
					<div class="col-4">
						<i class="fa fa-plus-square-o fa-5x"></i>
					</div>
					<div class="col-8 text-right">
						<span> Data Sakit | <?php echo $jmlsakitapp; ?></span>
						<h2 class="font-bold"><?php echo $jmlsakit; ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="col-lg-12">
			<div class="widget style1 white-bg">
				<div class="row">
					<div class="col-4">
						<i class="fa fa-sliders fa-5x"></i>
					</div>
					<div class="col-8 text-right">
						<span> Tukar Jadwal | <?php echo $tukarjadwal; ?></span>
						<h2 class="font-bold">0</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="widget style1 lazur-bg">
				<div class="row">
					<div class="col-4">
						<i class="fa fa-file-pdf-o fa-5x"></i>
					</div>
					<div class="col-8 text-right">
						<span> Data Lembur | <?php echo $jmllemburapp; ?></span>
						<h2 class="font-bold"><?php echo $jmllembur; ?></h2>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="widget style1 yellow-bg">
				<div class="row">
					<div class="col-4">
						<i class="fa fa-bus fa-5x"></i>
					</div>
					<div class="col-8 text-right">
						<span> Data SPPD | 0</span>
						<h2 class="font-bold">0</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div id="container" style="min-width: 300px; height: 280px; max-width: 800px; margin: 0 auto"></div>
			</div>
		</div>
	</div>
</div>

<?php foreach($absen as $rows){} ?>
<script type="text/javascript">
	Highcharts.chart('container', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title: {
			text: 'Count Absensi'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %'
				}
			}
		},
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [{
				name: 'Masuk',
				y: <?php echo $rows->masuk; ?>,
				sliced: true,
				selected: true
			}, {
				name: 'Pulang',
				y: <?php echo $rows->pulang; ?>
			}]
		}]
	});
</script>