<style type="text/css">
	#table-wrapper {
		position: relative;
	}

	#table-scroll {
		height: 600px;
		overflow: auto;
		margin-top: 20px;
	}

	#table-wrapper table {
		width: 100%;

	}

	#table-wrapper table * {
		color: black;
	}

	#table-wrapper table thead th .text {
		position: absolute;
		top: -20px;
		z-index: 2;
		height: 20px;
		width: 35%;
		border: 1px solid red;
	}
</style>

<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Bauk, Biaya Non PO</h5>
			</div>
			<div class="ibox-content">
				<div id="table-scroll">
					<form method="get" action="<?php echo base_url("sdm/pencarianpremi/") ?>">
						<div class="row">
							<div class="col-lg-3" id="tgl1" style="padding:0%;">
								<div class="input-group date">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input autocomplete="off" type='text' name="tglawal" class="form-control" data-date-format="YYYY-MM-DD" autocomplete="false" placeholder="Year-Month-Date" id="tglawal" />
								</div>

							</div>

							<div class="col-lg-3" id="tgl2">
								<div class="input-group date">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input autocomplete="off" type='text' name="tglakhir" class="form-control" data-date-format="YYYY-MM-DD" autocomplete="false" placeholder="Year-Month-Date" id="tglakhir" />
								</div>

							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="submit" class="btn btn-primary" value="Cari">
								</div>
							</div>
						</div>
					</form>
					<table class="table table-striped table-bordered table-hover dataTables-example">
						<thead>
							<tr>
								<th width="10px" rowspan="2">No.</th>
								<th rowspan="2">NIK</th>
								<th rowspan="2">Nama</th>
								<th rowspan="2">Shift</th>
								<th rowspan="2">Bidang</th>
								<th rowspan="2">Subbidang</th>
								<th rowspan="2">Jabatan</th>
								<th rowspan="2">1</th>
								<th rowspan="2">2</th>
								<th rowspan="2">3</th>
								<th rowspan="2">4</th>
								<th rowspan="2">5</th>
								<th rowspan="2">6</th>
								<th rowspan="2">7</th>
								<th rowspan="2">8</th>
								<th rowspan="2">9</th>
								<th rowspan="2">10</th>
								<th rowspan="2">11</th>
								<th rowspan="2">12</th>
								<th rowspan="2">13</th>
								<th rowspan="2">14</th>
								<th rowspan="2">15</th>
								<th rowspan="2">16</th>
								<th rowspan="2">17</th>
								<th rowspan="2">18</th>
								<th rowspan="2">19</th>
								<th rowspan="2">20</th>
								<th rowspan="2">21</th>
								<th rowspan="2">22</th>
								<th rowspan="2">23</th>
								<th rowspan="2">24</th>
								<th rowspan="2">25</th>
								<th rowspan="2">26</th>
								<th rowspan="2">27</th>
								<th rowspan="2">28</th>
								<th rowspan="2">29</th>
								<th rowspan="2">30</th>
								<th rowspan="2">31</th>
								<th colspan="3">Premi Shift Hari Biasa</th>
								<th rowspan="2">Nominal Premi</th>
							</tr>
							<tr>
								<th>Pagi <p>(17.000)</p>
								</th>
								<th>Siang <p>(20.000)</p>
								</th>
								<th>Malam <p>(23.000)</p>
								</th>


							</tr>
						</thead>
						<tbody id="baris">
							<?php
							$perpage = $this->uri->segment(4);
							$no = 1 + $perpage;
							foreach ($nonpo as $rows) { ?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $rows->no_induk; ?></td>
									<td><?php echo $rows->nama_lengkap; ?></td>
									<td><?php echo $rows->nama; ?></td>
									<td><?php echo $rows->bidang; ?></td>
									<td><?php echo $rows->subbid; ?></td>
									<td><?php echo $rows->jabatan; ?></td>
									<td><?php echo $rows->day1; ?></td>
									<td><?php echo $rows->day2; ?></td>
									<td><?php echo $rows->day3; ?></td>
									<td><?php echo $rows->day4; ?></td>
									<td><?php echo $rows->day5; ?></td>
									<td><?php echo $rows->day6; ?></td>
									<td><?php echo $rows->day7; ?></td>
									<td><?php echo $rows->day8; ?></td>
									<td><?php echo $rows->day9; ?></td>
									<td><?php echo $rows->day10; ?></td>
									<td><?php echo $rows->day11; ?></td>
									<td><?php echo $rows->day12; ?></td>

									<td><?php echo $rows->day13; ?></td>
									<td><?php echo $rows->day14; ?></td>
									<td><?php echo $rows->day15; ?></td>
									<td><?php echo $rows->day16; ?></td>
									<td><?php echo $rows->day17; ?></td>
									<td><?php echo $rows->day18; ?></td>
									<td><?php echo $rows->day19; ?></td>
									<td><?php echo $rows->day20; ?></td>
									<td><?php echo $rows->day21; ?></td>
									<td><?php echo $rows->day22; ?></td>
									<td><?php echo $rows->day23; ?></td>
									<td><?php echo $rows->day24; ?></td>
									<td><?php echo $rows->day25; ?></td>
									<td><?php echo $rows->day26; ?></td>
									<td><?php echo $rows->day27; ?></td>
									<td><?php echo $rows->day28; ?></td>
									<td><?php echo $rows->day29; ?></td>
									<td><?php echo $rows->day30; ?></td>
									<td><?php echo $rows->day31; ?></td>
									<td><?php echo $rows->p; ?></td>
									<td><?php echo $rows->s; ?></td>
									<td><?php echo $rows->m; ?></td>
									<td><?php echo $rows->premishift; ?></td>

								</tr>
							<?php $no++;
							} ?>
						</tbody>
					</table>

				</div>
				<hr>
			</div>
		</div>
	</div>
</div>