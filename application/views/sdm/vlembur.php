<script src="<?php echo base_url(); ?>assets/css/plugins/datapicker/clockpicker.css"></script>
<div class="row">
	<div class="col-lg-12">  
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Pengajuan Lembur</h5>
			</div>
			<div class="ibox-content">
				<div class="row">
					<div class="col-lg-12">
						<form name="form" method="post" action="<?php echo base_url('approval/reviewa_all') ?>"  >
							<table id="r_overtime" class="table table-striped table-bordered data-table table-hover dataTables-example">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Jenis Lembur</th>
										<th>Start - End</th>
										<!-- <th>End</th> -->
										<th>Jam</th>
										<th>Keterangan</th>
										<th>Status</th>
										<th>Tgl Aprov/reject SPV</th>
										<th>Tgl Aprov/reject Manager</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody  id="baris">
									<?php
									$perpage = $this->uri->segment(4);
									$no=1+$perpage;  
									foreach ($data_lembur as $rows) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $rows->nama_lengkap; ?></td>
											<td><?php echo $rows->jenis_lembur; ?></td>
											<td><?php echo $rows->start_datetime; ?> - <b><?php echo $rows->end_datetime; ?></b></td>
											<!-- <td><?php echo $rows->end_datetime; ?></td> -->
											<td><?php
											$s_date = new DateTime($rows->start_datetime);
											$e_date = new DateTime($rows->end_datetime);
											$diff = date_diff($s_date, $e_date); 
											echo ($diff->format('%h')).' Jam';
											?></td>
											<td><?php echo $rows->alasan; ?></td>  

											<td>
												<?php if($rows->status=='1'){?>
													<p class="btn btn-sm btn-info" disabled><b>Menunggu</b></p>									
												<?php }elseif($rows->status=='2'){ ?>
													<p class="btn btn-sm btn-primary" disabled><b>Diterima SPV</b></p>	
												<?php }elseif($rows->status=='3'){ ?>
													<p class="btn btn-sm btn-success" disabled><b>Diterima SPV & Manager</b></p>
												<?php }elseif($rows->status=='4'){ ?>
													<p class="btn btn-sm btn-danger" disabled><b>Ditolak</b></p>		
												<?php }?>
											</td>
											<td><?php echo $rows->umar; ?></td>  
											<td><?php echo $rows->approved_date; ?></td>  
											<td>								
												<a href="#modal-reject<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Batal Pengajuan"><i class="fa fa-ban"></i></a>
											</td>                    
										</tr>
										<?php $no++; } ?>
									</tbody>
								</table>
							</form>					
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<?php foreach ($data_lembur as $rows) { ?>  
		<div class="modal inmodal fade" id="modal-reject<?php echo $rows->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content animated flipInY">
					<div class="modal-header">
						<h2>Apakah anda yakin?</h2>
					</div>
					<form method="post" action="<?php echo base_url(); ?>manager/reject_approval/" class="form-horizontal">
						<div class="modal-body" style="padding:50">
							<div class="form-group">
								<input type="hidden" value="1" name="kode">
								<input type="hidden" value="<?php echo $rows->id; ?>" name="id"> 
							</div>  
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">YES</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	<?php } ?>

	