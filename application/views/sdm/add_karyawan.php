<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Data Karyawan</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered data-table table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No Induk</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Jabatan</th>
                                    <th>SPV</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($data_karyawan as $rows) { ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->no_induk; ?></td>
                                        <td><?php echo $rows->nama_lengkap; ?></td>
                                        <td><?php echo $rows->email; ?></td>
                                        <td><?php echo $rows->jabatan; ?></td>     
                                        <td><?php echo $rows->spv; ?></td>     
                                        <td>
                                            <?php if($rows->active=='1'){?>
                                                <p class="btn btn-sm btn-info" disabled><b>Aktif</b></p>                                  
                                            <?php }elseif($rows->active=='0'){ ?>
                                                <p class="btn btn-sm btn-danger" disabled><b>Non Aktif</b></p>    
                                            <?php }?>
                                        </td>     
                                        <td>
                                            <a href="#modal-reject<?php echo $rows->id; ?>" data-toggle="modal" class="btn btn-sm btn-outline btn-danger" title="Reject"><i class="fa fa-users"> Detail</i></a>
                                        </td>       
                                    </tr> 
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                        </form>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach ($data_karyawan as $rows) { ?>  
    <div class="modal inmodal fade" id="modal-reject<?php echo $rows->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated flipInY">
                <div class="modal-header">
                    <h2>Detail Karyawan</h2>
                    <img alt="image" class="rounded-circle" src="<?php echo base_url(); ?>uploads/avatar/<?php if ($rows->foto!=null) {echo $rows->foto; } else { echo 'um.png'; } ?>" width="100px" height="100px">
                </div>
                <form method="post" action="<?php echo base_url(); ?>manager/reject_approval/" class="form-horizontal">
                    <!-- <div class="modal-body" style="padding:50"> -->
                        <div class="panel-body">
                            <div class="tabs-container">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home<?php echo $rows->id;?>" role="tab" aria-controls="home" aria-selected="true">1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile<?php echo $rows->id;?>" role="tab" aria-controls="profile" aria-selected="false">2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact<?php echo $rows->id;?>" role="tab" aria-controls="contact" aria-selected="false">3</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home<?php echo $rows->id;?>" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">NIK</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->no_induk; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Nama Lengkap</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->nama_lengkap; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">TTL</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->tempat_lahir; ?>, <?php echo $rows->tgl_lahir; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Agama</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->agama; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Email</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->email; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Pendidikan Terakhir</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->pendidikan_terakhir; ?>" readonly="">
                                                    </div>
                                                </div>



                                            </div> 
                                            <div class="col-lg-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">No KTP</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->no_ktp; ?>" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Jenis Kelamin</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->jenis_kelamin; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Status</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->status_perkawinan; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">No Telp</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->no_telepon; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Jabatan</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->jabatan; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Jurusan</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->jurusan; ?>" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile<?php echo $rows->id;?>" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Awal Kontrak</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->awal_kontrak; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Supervisor</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->spv; ?>" readonly="">
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="col-lg-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Akhir Kontrak</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->akhir_kontrak; ?>" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Manager</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->manajer; ?>" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact<?php echo $rows->id;?>" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Gaji Pokok</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->gapok; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Uang Makan</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->uang_makan; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 control-label">Uang Pulsa</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" value="<?php echo $rows->uang_pulsa; ?>" readonly="">
                                                    </div>
                                                </div>

                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- </div> -->
                </form>
            </div>
        </div>
    </div>

    <?php } ?>