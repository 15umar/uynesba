<style type="text/css">
	#table-wrapper {
		position:relative;
	}
	#table-scroll {
		height:700px;
		overflow:auto;  
		margin-top:20px;
	}
	#table-wrapper table {
		width:100%;

	}
	#table-wrapper table * {
		color:black;
	}
	#table-wrapper table thead th .text {
		position:absolute;   
		top:-20px;
		z-index:2;
		height:20px;
		width:35%;
		border:1px solid red;
	}
</style>

<div class="row">
	<div class="col-lg-12">  
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Bauk, Biaya Non PO</h5>
			</div>
			<div class="ibox-content">
				<div id="table-scroll">
					<form method="get" action="<?php echo base_url("sdm/pencarianbauk/")?>">
						<div class="row">
							<div class="col-lg-3" id="tgl1" style="padding:0%;">
								<div class="input-group date">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input autocomplete="off" type='text' name="tglawal" class="form-control" data-date-format="YYYY-MM-DD"
									autocomplete="false" placeholder="Year-Month-Date" id="tglawal" />
								</div>

							</div>

							<div class="col-lg-3" id="tgl2">
								<div class="input-group date">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input autocomplete="off" type='text' name="tglakhir" class="form-control" data-date-format="YYYY-MM-DD"
									autocomplete="false" placeholder="Year-Month-Date" id="tglakhir" />
								</div>

							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="submit" class="btn btn-primary" value="Cari">   
								</div>
							</div>
						</div>
					</form>
					<table class="table table-striped table-bordered table-hover dataTables-example">
						<thead>
							<tr>
								<th width="10px" rowspan="2">No.</th>
								<th rowspan="2">Nama</th>
								<th rowspan="2">Bidang</th>
								<th rowspan="2">Subbidang</th>
								<th rowspan="2">Jabatan</th>
								<th colspan="3">Jumlah dan Jam Lembur</th>
								<th rowspan="2">Total Nominal Lembur</th>
								<th colspan="3">Premi Shift Hari Biasa</th>
								<th rowspan="2">Premi Shift</th>
								<th colspan="6">Biaya SPPD</th>
								<th rowspan="2">Tiket</th>
								<th rowspan="2">Total SPPD</th>
							</tr>
							<tr>
								<th>Hari Kerja</th>
								<th>Hari Libur</th>
								<th>Hari Besar</th>
								<th>I</th>
								<th>II</th>
								<th>III</th>
								<th>Penginapan</th>
								<th>Biaya Makan</th>
								<th>Uang Saku</th>
								<th>Angkutan</th>
								<th>Bandara</th>
								<th>Terminal</th>

							</tr>
						</thead>
						<tbody id="baris">
							<?php 
							$perpage = $this->uri->segment(4);
							$no=1+$perpage;  
							foreach ($nonpo as $rows) { ?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $rows->nama_lengkap; ?></td>
									<td><?php echo $rows->bidang; ?></td>
									<td><?php echo $rows->subbid; ?></td>
									<td><?php echo $rows->jabatan; ?></td>
									<td><?php echo $rows->harikerja; ?></td>
									<td><?php echo $rows->harilibur; ?></td>
									<td><?php echo $rows->haribesar; ?></td>
									<td><?php echo $rows->totallembur; ?></td>
									<td><?php echo $rows->p; ?></td>
									<td><?php echo $rows->s; ?></td>
									<td><?php echo $rows->m; ?></td>
									<td><?php echo $rows->premishift; ?></td>
									<td><?php echo $rows->bpenginapan; ?></td>
									<td><?php echo $rows->uangmakan; ?></td>
									<td><?php echo $rows->uangsaku; ?></td>
									<td><?php echo $rows->angkutan; ?></td>
									<td><?php echo $rows->bandara; ?></td>
									<td><?php echo $rows->terminal; ?></td>
									<td><?php echo $rows->tiket; ?></td>
									<td><?php echo $rows->totalsppd; ?></td>

								</tr>
								<?php $no++; } ?>
							</tbody>
						</table>
						
					</div>
					<hr>
				</div>
			</div>
		</div>
	</div>