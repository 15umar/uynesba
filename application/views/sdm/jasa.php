<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">

                <div class="table-responsive">
                    <form method="get" action="<?php echo base_url("sdm/pencarianjasa/")?>">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <select class="form-control" name="organisasi">
                                        <?php foreach ($ogjasa as $row) { ?>
                                            <option value="<?php echo $row->organisasi; ?>"><?php echo $row->bidang; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Cari">   
                                </div>
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Bidang</th>
                                <th>Jabatan</th>
                                <th>QTY PO</th>
                                <th>QTY Real</th>
                                <th>Jml HK PO</th>
                                <th>Jml HK Real</th>
                                <th>Harga perorang</th>
                                <th>Management Fee</th>
                                <th>Biaya Satuan Jasa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $perpage = $this->uri->segment(4);
                            $no=1+$perpage;  
                            foreach ($jasa as $rows) { ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $rows->no_induk; ?></td>
                                    <td><?php echo $rows->nama_lengkap; ?></td>
                                    <td><?php echo $rows->bidang; ?></td>
                                    <td><?php echo $rows->jabatan; ?></td>
                                    <td><?php echo $rows->qty_po; ?></td>
                                    <td><?php echo $rows->qty_real; ?></td>
                                    <td><?php echo $rows->hk_po; ?></td>
                                    <td><?php echo $rows->hk_real; ?></td>
                                    <!-- <td><?php echo $rows->hargaorang; ?></td> -->
                                    <td><?php echo "Rp " . number_format($rows->hargaorang, 0, ",", "."); ?></td>
                                    <td><?php echo "Rp " . number_format($rows->fee, 0, ",", "."); ?></td> 
                                    <td><?php echo "Rp " . number_format($rows->satuanjasa, 0, ",", "."); ?></td>                      

                                </tr>          
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Team</th>
                                <th>QTY PO</th>
                                <th>QTY Real</th>
                                <th>Jml HK PO</th>
                                <th>Jml HK Real</th>
                                <th>Harga perorang</th>
                                <th>Management Fee</th>
                                <th>Biaya Satuan Jasa</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>