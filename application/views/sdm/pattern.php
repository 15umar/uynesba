<?php
$emp=str_replace("\\'", '', $emp);
$array = explode(',', $emp); 
$a=0;
echo "<span id='datashift' class='hide'>$input</span><span id='emp' class='hide'>$emp</span><span id='date' class='hide'>$date</span>";
echo "<table class=\"table table-striped table-bordered\" id=\"pattern\" style='width:100%'>
<thead>
<tr><th>NAMA</th>";
foreach ($range as $value){   
	$day=date('D', strtotime($value));
	$css=(($day=='Sun') or ($day=='Sat'))?"class='danger text-center'":"class='text-center'";
	echo "<th $css>".str_ireplace('-','',$value)."<br> (".$day.")</th>";
}  
echo "</tr></thead>";

foreach ($array as $value){ 
	$a++;

	$nip=substr(trim($value), strrpos(trim($value), '|') + 1);
	$niparr[]="'".$nip."'";
	echo "<tr><td>".current(explode("|", str_ireplace('_',' ',trim($value))))."</td>";
	$i=0;
	foreach ($range as $tgl){   
		$i++;
		echo "<td class='text-center'><input type=\"text\"  align='middle' class='pattern r_".$a."_$i' id='id_".str_ireplace('/','',$tgl)."_".$nip."' size=\"3\" maxlength=\"3\"></td>";
	}
	echo "</tr>";
}  
echo "</tbody></table>";
echo "<button class=\"btn btn-info\" type=\"button\" id=\"btn-generate\"> <i class=\"fa fa-save\"></i> Save to DB</button>";
?>
<script>
	$(function() {

		var data = $('#datashift').text();
		var rows = data.split("\n");
		for(var y in rows) {
			var one=(parseInt(y)+1);
			var cells = rows[y].split("\t");
			for(var x in cells) {
				var two=(parseInt(x)+1);
				$(".r_"+one+"_"+two).val(cells[x]);
			}
		}

	});

	var selected = new Array();
	$('#btn-generate').click(function(){
		var gen=0;
		$('.pattern').each(function() {
			if(!$(this).val()){
				$(this).css("background-color", "yellow");
            //alert('Some fields are empty');
            while(selected.length > 0) {
            	selected.pop();
            }
            gen=0;
            return false;
        }else{gen=1}
    });

		if(gen==1){
			$('.pattern').each(function() {
				$(this).css("background-color", "");
				selected.push($(this).attr('id')+'_'+$(this).val());
				gen=1;
			});

			if(confirm("Are you sure to proccess this ?")){
				$('#loadlist').show();
				var dataString = 'date='+$('#date').text()+'&emp='+$('#emp').text()+'&input='+selected;

				$.ajax({
					url: 'spv/save',
					data: dataString,
					type: 'POST',
					cache: false,
					success: function (data){
						console.log(data);
					},
					complete: function () 
					{
						$('#loadlist').hide();
					}
				}); 
			}
			else{
				return false;
			}
			while(selected.length > 0) {
				selected.pop();
			}
		}
	})
</script>