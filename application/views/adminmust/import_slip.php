<div class="row animated fadeInRight">
    <div class="col-md-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Import Slip Gaji</h5>
            </div>
            <div class="ibox-content">
                <div class="panel-body">
                    <div class="tab-content">
                        <form autocomplete="off" method="post" action="<?php echo base_url(); ?>adminmust/createdata" class="form-horizontal" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Pilih File</label>
                                <div class="col-sm-9">
                                    <input type="file" name="userfile" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-sm-offset-3">
                                    <button class="btn btn-primary submit-update" type="submit">Import</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Format Import</h5>
                
            </div>
            <div class="ibox-content ibox-heading">
                <h3>Peraturan Import Excel</h3>
                <small>
                    <i class="fa fa-stack-exchange"></i> Silahkan download contoh File import user <a href="<?php echo base_url(); ?>uploads/daftar_new.xls"> ini.</a>  </br>
                    <i class="fa fa-stack-exchange"></i> Lakukan pengisian data sesuai dengan format yang telah disediakan.
                </small>
            </div>
            <div class="ibox-content">
                <div>

                    <div class="pull-right text-right">


                        <br/>
                    </div>
                    <h4>Penting
                        <br/>
                        <small class="m-r"><a href="graph_flot.html"> Dilarang melakukan perubahan format yang telah disediakan</a> </small>
                    </h4>
                </div>
            </div>
        </div>

    </div>
    
</div>