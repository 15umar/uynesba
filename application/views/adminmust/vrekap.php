<?php
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success"><i class="fa fa-check"></i> ';
    echo $this->session->flashdata('sukses');
    echo '</div>';
} else if ($this->session->flashdata('gagal')) {
    echo '<div class="alert alert-danger"><i class="fa fa-close"></i> ';
    echo $this->session->flashdata('gagal');
    echo '</div>';
}
$user_level = $this->session->userdata('level');
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Pengajuan Sakit</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form name="form" method="post" action="<?php echo base_url('approval/review_all') ?>">
                            <input type="hidden" class="form-control" id="user_level" name="user_level" value="<?php echo $user_level ?>">
                            <table class="table table-striped table-responsive table-bordered data-table table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>
                                        <th>6</th>
                                        <th>7</th>
                                        <th>8</th>
                                        <th>9</th>
                                        <th>10</th>
                                        <th>11</th>
                                        <th>12</th>
                                        <th>13</th>
                                        <th>14</th>
                                        <th>15</th>
                                        <th>16</th>
                                        <th>17</th>
                                        <th>18</th>
                                        <th>19</th>
                                        <th>20</th>
                                        <th>21</th>
                                        <th>22</th>
                                        <th>23</th>
                                        <th>24</th>
                                        <th>25</th>
                                        <th>26</th>
                                        <th>27</th>
                                        <th>28</th>
                                        <th>29</th>
                                        <th>30</th>
                                        <th>31</th>

                                        <th>H</th>
                                        <th>P</th>
                                        <th>S</th>
                                        <th>M</th>
                                        <th>L</th>
                                        <th>I</th>
                                        <th>SK</th>
                                        <th>C</th>
                                        <th>A</th>
                                        <th>T</th>

                                        <th>TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $perpage = $this->uri->segment(4);
                                    $no = 1 + $perpage;
                                    foreach ($data_cuti as $rows) { ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $rows->nama_lengkap; ?></td>
                                            <td><?php echo $rows->day1; ?></td>
                                            <td><?php echo $rows->day2; ?></td>
                                            <td><?php echo $rows->day3; ?></td>
                                            <td><?php echo $rows->day4; ?></td>
                                            <td><?php echo $rows->day5; ?></td>
                                            <td><?php echo $rows->day6; ?></td>
                                            <td><?php echo $rows->day7; ?></td>
                                            <td><?php echo $rows->day8; ?></td>
                                            <td><?php echo $rows->day9; ?></td>
                                            <td><?php echo $rows->day10; ?></td>
                                            <td><?php echo $rows->day11; ?></td>
                                            <td><?php echo $rows->day12; ?></td>

                                            <td><?php echo $rows->day13; ?></td>
                                            <td><?php echo $rows->day14; ?></td>
                                            <td><?php echo $rows->day15; ?></td>
                                            <td><?php echo $rows->day16; ?></td>
                                            <td><?php echo $rows->day17; ?></td>
                                            <td><?php echo $rows->day18; ?></td>
                                            <td><?php echo $rows->day19; ?></td>
                                            <td><?php echo $rows->day20; ?></td>
                                            <td><?php echo $rows->day21; ?></td>
                                            <td><?php echo $rows->day22; ?></td>
                                            <td><?php echo $rows->day23; ?></td>
                                            <td><?php echo $rows->day24; ?></td>
                                            <td><?php echo $rows->day25; ?></td>
                                            <td><?php echo $rows->day26; ?></td>
                                            <td><?php echo $rows->day27; ?></td>
                                            <td><?php echo $rows->day28; ?></td>
                                            <td><?php echo $rows->day29; ?></td>
                                            <td><?php echo $rows->day30; ?></td>
                                            <td><?php echo $rows->day31; ?></td>

                                            <td><?php echo $rows->H; ?></td>
                                            <td><?php echo $rows->P; ?></td>
                                            <td><?php echo $rows->S; ?></td>
                                            <td><?php echo $rows->M; ?></td>
                                            <td><?php echo $rows->L; ?></td>
                                            <td><?php echo $rows->I; ?></td>
                                            <td><?php echo $rows->SK; ?></td>
                                            <td><?php echo $rows->C; ?></td>
                                            <td><?php echo $rows->A; ?></td>
                                            <td><?php echo $rows->T; ?></td>

                                            <td><?php echo $rows->jumlah; ?></td>
                                        </tr>
                                    <?php $no++;
                                    } ?>
                                </tbody>
                            </table>
                            <h5><a href="adminmust/export_excel">Download Rekap</a></h5>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>