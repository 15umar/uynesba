<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Jabatan</th>
                                    <th>Team</th>
                                    <th>QTY PO</th>
                                    <th>QTY Real</th>
                                    <th>Jml HK PO</th>
                                    <th>Jml HK Real</th>
                                    <th>Harga Satuan</th>
                                    <th>Jumlah Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($jasa as $rows) { ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->nama_lengkap; ?></td>
                                        <td><?php echo $rows->jabatan; ?></td>
                                        <td><?php echo $rows->spv; ?></td>
                                        <td><?php echo $rows->qty_po; ?></td>
                                        <td><?php echo $rows->qty_real; ?></td>
                                        <td><?php echo $rows->hk_po; ?></td>
                                        <td><?php echo $rows->hk_real; ?></td>
                                        <td><?php echo "Rp " . number_format($rows->harga_satuan, 0, ",", "."); ?></td> 
                                        <td><?php echo "Rp " . number_format($rows->jumlah_harga, 0, ",", "."); ?></td>                      

                                    </tr>          
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h2>Tambah Jasa</h2>
            </div>
            <form method="post" action="<?php echo base_url(); ?>adminmust/simpan_jasa/" class="form-horizontal">
                <div class="modal-body" style="padding:50">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-9">
                            <select name="id_user" class="form-control">
                                <?php foreach ($user as $row) { ?>
                                    <option value="<?php echo $row->id; ?>">
                                        <?php echo $row->nama_lengkap; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">QTY PO</label>
                        <div class="col-sm-9">
                            <input type="text" name="qty_po" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">QTY REAL</label>
                        <div class="col-sm-9">
                            <input type="text" name="qty_real" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">JML HK PO</label>
                        <div class="col-sm-9">
                            <input type="text" name="hk_po" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">JML HK REAL</label>
                        <div class="col-sm-9">
                            <input type="text" name="hk_real" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Harga Satuan</label>
                        <div class="col-sm-9">
                            <input type="text" name="harga_satuan" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jumlah Harga</label>
                        <div class="col-sm-9">
                            <input type="text" name="jumlah_harga" class="form-control">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>