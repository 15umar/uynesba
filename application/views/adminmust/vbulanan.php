<?php
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success"><i class="fa fa-check"></i> ';
    echo $this->session->flashdata('sukses');
    echo '</div>';
} else if ($this->session->flashdata('gagal')) {
    echo '<div class="alert alert-danger"><i class="fa fa-close"></i> ';
    echo $this->session->flashdata('gagal') . "asd " . validation_errors();
    echo '</div>';
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Data Bulanan</h5>
            </div>


            <div class="ibox-content">
                <form method="get" action="<?php echo base_url("adminmust/pencarianbulanan/") ?>">
                    <div class="row">
                        <div class="col-lg-3" id="tgl1" style="padding:0%;">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input autocomplete="off" type='text' name="tglawal" class="form-control" data-date-format="YYYY-MM-DD" autocomplete="false" placeholder="Year-Month-Date" id="tglawal" />
                            </div>

                        </div>

                        <div class="col-lg-3" id="tgl2">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input autocomplete="off" type='text' name="tglakhir" class="form-control" data-date-format="YYYY-MM-DD" autocomplete="false" placeholder="Year-Month-Date" id="tglakhir" />
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Cari">
                            </div>
                        </div>
                    </div>
                </form>
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>
                            <th class="no">No.</th>
                            <th>Nama</th>
                            <th>Tanggal</th>
                            <th>Jam Masuk</th>
                            <th>Jam Pulang</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $perpage = $this->uri->segment(4);
                        $no = 1 + $perpage;
                        foreach ($koreksi as $rows) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $rows->nama_lengkap; ?></td>
                                <td><?php echo $rows->tanggal; ?></td>
                                <td><?php echo $rows->jam_masuk; ?></td>

                                
                                <td><?php echo $rows->jam_pulang; ?></td>
                                
                            </tr>
                            <?php $no++;
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- </div> -->
<script type="text/javascript">
    $('.clockpicker').clockpicker({
        timeFormat: 'h:mm:ss p'
    });
</script>