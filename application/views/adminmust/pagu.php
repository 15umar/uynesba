<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Pagu</button>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-responsive table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Bidang</th>
                                    <th>Biaya Jasa</th>
                                    <th>Biaya Non PO</th>
                                    <th>Sisa Biaya Jasa</th>
                                    <th>Sisa Biaya Non PO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $perpage = $this->uri->segment(4);
                                $no=1+$perpage;  
                                foreach ($pagu as $rows) { 
                                    $a = $rows->premishift;
                                    $b = $rows->lembur;
                                    $c = $rows->sppd;
                                    $d = $rows->baukjasa;
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $rows->bidang; ?></td>
                                        <td><?php echo "Rp " . number_format($rows->biaya_jasa, 0, ",", "."); ?></td>  
                                        <td><?php echo "Rp " . number_format($rows->biaya_non_po, 0, ",", "."); ?></td>
                                        <td class=" btn-danger btn-sm"><?php echo "Rp " . number_format($rows->biaya_jasa - $d, 0, ",", "."); ?></td> 
                                        <td class=" btn-danger btn-sm"><?php echo "Rp " . number_format($rows->biaya_non_po - ($a+$b+$c), 0, ",", "."); ?></td>                      

                                    </tr>          
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h2>Tambah Pagu</h2>
            </div>
            <form method="post" action="<?php echo base_url(); ?>adminmust/simpan_pagu/" class="form-horizontal">
                <div class="modal-body" style="padding:50">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Bidang</label>
                        <div class="col-sm-9">
                            <select name="id_organisasi" class="form-control">
                                <?php foreach ($bidang as $row) { ?>
                                    <option value="<?php echo $row->id_bidang; ?>">
                                        <?php echo $row->nama; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Biaya Jasa</label>
                        <div class="col-sm-9">
                            <input type="text" name="biaya_jasa" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Biaya Non PO</label>
                        <div class="col-sm-9">
                            <input type="text" name="biaya_non_po" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" autocomplete="off" id="data_2">
                        <label class="col-sm-3 control-label">Awal Kontrak</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type='text' name="awal_kontrak" class="form-control" data-date-format="YYYY-MM-DD"
                                placeholder="Year-Month-Date"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" autocomplete="off" id="data_2">
                        <label class="col-sm-3 control-label">Akhir Kontrak</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type='text' name="akhir_kontrak" class="form-control" data-date-format="YYYY-MM-DD"
                                placeholder="Year-Month-Date"/>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>