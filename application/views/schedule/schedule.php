<?php
error_reporting(E_ALL & ~E_NOTICE);
$arr = json_decode($schedule, true);

$empall = array();
$schedu = array();
foreach ($arr as $r) {
    $empall[$r['nip']] = $r['nama_lengkap'];
    $schedu[$r['nip'] . str_ireplace('-', '', $r['tanggal'])] = $r['id_shifting'];
}

$array = (array_unique($empall));

echo "<table class=\"table table-striped table-bordered table-hover dataTables-example\" id=\"pattern\" style='width:100%'>
<thead>
	<tr><th>NAMA</th>";
foreach ($range as $value) {
    $day = date('D', strtotime($value));
    $css = (($day == 'Sun') or ($day == 'Sat')) ? "class='danger text-center'" : "class='text-center'";
    $ddd = ($sdate == '1') ? substr(str_ireplace('/', '', $value), 6, 2) : substr(str_ireplace('/', '', $value), 4, 4);
    echo "<th $css>" . $ddd . "<br> (" . $day . ")</th>";
}
echo "</tr></thead>";

foreach ($array as $k => $value) {

    // echo "<tr><td id=id_".$k.">".explode(' ',$value)[0]."</td>";
    echo "<tr><td>" . current(explode("|", str_ireplace('_', ' ', trim($value)))) . "</td>";
    foreach ($range as $tgl) {
        $index = $k . str_ireplace("/", "", $tgl);
        $x = (trim($schedu[$index]) == 'L') ? "danger success" : "";
        echo "<td class='text-center pattern r_" . $k . " " . $x . "' id='id_" . str_ireplace('/', '', $tgl) . "_" . $k . "'>" . $schedu[$index] . "</td>";
    }
    echo "</tr>";
}
echo "</tbody></table>";
?>
<script>
    $(function() {

        var data = $('#datashift').text();
        var rows = data.split("\n");
        for (var y in rows) {
            var one = (parseInt(y) + 1);
            var cells = rows[y].split("\t");
            for (var x in cells) {
                var two = (parseInt(x) + 1);
                $(".r_" + one + "_" + two).val(cells[x]);
            }
        }
        var fullDate = new Date()

        //convert month to 2 digits
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
        var twoDigitDate = ((fullDate.getDate().length) === 1) ? (fullDate.getDate()) : '0' + (fullDate.getDate());

        var currentDate = fullDate.getFullYear() + twoDigitMonth + twoDigitDate;
        console.log(currentDate);
        $('.pattern').each(function() {
            if (currentDate == $(this).attr('id').split('_')[1]) {
                $(this).addClass("success");
            }
        });

    });
</script>


<script src="assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="assets/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('.dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'ExampleFile'
                },
                {
                    extend: 'pdf',
                    title: 'ExampleFile'
                },

                {
                    extend: 'print',
                    customize: function(win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });

    });
</script>