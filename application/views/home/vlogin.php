<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Uynesba V.2.0</title>
    <link rel="icon" type="image/png" href="uploads/avatar/must_icon.png">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>
                <img src="<?php echo base_url(); ?>uploads/avatar/must_logo.png" > 
            </div>
            <h3>Welcome to Uynesba</h3>
            <p>Login in. To see it in action.</p>
            <?php echo form_open("login/auth"); ?>
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="NIK / Email" required="">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            <!-- <a href="#"><small>Forgot password?</small></a> -->
            <?php echo form_close(); ?>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>