<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>">
</base>
<link rel="icon" type="image/png" href="uploads/avatar/must_icon.png">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/summernote/summernote.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/tabel.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/plugins/jquery-file-input/file-input.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/clockpicker/dist/bootstrap-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/js/plugins/nprogress/nprogress.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/nprogress/nprogress.js"></script>
<link href="<?php echo base_url(); ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/file-input/css/file-input.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/file-input/css/selectize-custom.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/webcamjs/webcam.js"></script>
<link href="<?php echo base_url(); ?>assets/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/vue/vue.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/vue/axios.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>


<style>
    .act {
        font-weight: bold;
    }
</style>
</head>

<body oncontextmenu='return false;' onLoad="aclick();">
    <?php
    $id_user  = $this->session->userdata('id_user');
    $pengguna = $this->model_login->getBiodata($id_user);
    foreach ($pengguna as $bio) {
        $namalengkap = $bio->nama_lengkap;
        $avatar      = $bio->foto;
        $level       = $bio->name;
        $client       = $bio->client;
    }

    $notiflembur = $this->Model_spv->getnotiflembur();
    $notifjadwal = $this->Model_spv->getnotifjadwal();
    $notifsakit = $this->Model_spv->getnotifsakit();
    $notifcuti = $this->Model_spv->getnotifcuti();
    $notifdinas = $this->Model_spv->getnotifdinas();
    function tgl_indo($tgl)
    {
        $tanggal = substr($tgl, 8, 2);
        $bulan   = substr($tgl, 5, 2);
        $tahun   = substr($tgl, 0, 4);
        return $tanggal . '/' . $bulan . '/' . $tahun;
    }
    ?>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" class="rounded-circle" src="<?php echo base_url(); ?>uploads/avatar/<?php if ($avatar != null) {
                                echo $avatar;
                                } else {
                                    echo 'um.png';
                                } ?>" width="46px" height="46px">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="block m-t-xs font-bold"><?php echo $namalengkap; ?></span>
                                    <span class="text-muted text-xs block"><?php echo ucwords($level); ?> <b class="caret"></b></span>
                                </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>home/profile">Profil</a></li>
                                    <li class="dropdown-divider"></li>
                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>logout">Keluar</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                UY
                            </div>
                        </li>
                        <?php if ($this->session->userdata('level') == "1") { ?>
                            <li <?php if ($this->uri->segment(1) == 'superadmin' and $this->uri->segment(2) == '') {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?php echo base_url(); ?>superadmin"><i class="fa fa-th-large"></i> <span class="nav-label">Beranda</span></a>
                        </li>
                        <li <?php if ($this->uri->segment(2) == 'klien' or $this->uri->segment(2) == 'bidang' or $this->uri->segment(2) == 'subbidang' or $this->uri->segment(2) == 'supervisor' or $this->uri->segment(2) == 'manager' or $this->uri->segment(2) == 'organisasi') {
                            echo 'class="active"';
                        } ?>>
                        <a href="#"><i class="fa fa-sliders"></i> <span class="nav-label">Data Master</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li <?php echo (($this->uri->segment(2) == 'klien') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/klien">Client</a></li>
                            <li <?php echo (($this->uri->segment(2) == 'bidang') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/bidang">Bidang</a></li>
                            <li <?php echo (($this->uri->segment(2) == 'subbidang') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/subbidang">Sub Bidang</a></li>
                            <li <?php echo (($this->uri->segment(2) == 'supervisor') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/supervisor">Supervisor</a></li>
                            <li <?php echo (($this->uri->segment(2) == 'manager') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/manager">Manager</a></li>
                            <li <?php echo (($this->uri->segment(2) == 'organisasi') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/organisasi">Organisasi</a></li>
                        </ul>
                    </li>
                    <li <?php if ($this->uri->segment(2) == 'cuti' or $this->uri->segment(2) == 'sakit' or $this->uri->segment(2) == 'izin' or $this->uri->segment(2) == 'lembur' or $this->uri->segment(2) == 'perjalanan_dinas' or $this->uri->segment(2) == 'tukar') {
                        echo 'class="active"';
                    } ?>>
                    <a href="#"><i class="fa fa-list-ol"></i> <span class="nav-label">Pengajuan</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li <?php echo (($this->uri->segment(2) == 'cuti') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/cuti">Cuti</a></li>
                        <li <?php echo (($this->uri->segment(2) == 'sakit') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/sakit">Sakit</a></li>
                        <!-- <li <?php echo (($this->uri->segment(2) == 'izin') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/izin">Izin</a></li> -->
                        <li <?php echo (($this->uri->segment(2) == 'lembur') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/lembur">Lembur</a></li>
                        <li <?php echo (($this->uri->segment(2) == 'perjalanan_dinas') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/perjalanan_dinas">Perjalanan Dinas</a></li>
                        <!-- <li <?php echo (($this->uri->segment(2) == 'tukar') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/tukar">Tukar Jadwal</a></li> -->
                    </ul>
                </li>
                <!-- <li <?php echo (($this->uri->segment(1) == 'schedule') ? 'class="active"' : '');  ?>>
                    <a href="<?php echo base_url(); ?>schedule/"><i class="fa fa-calendar"></i> <span class="nav-label">Jadwal Kerja</span></a>
                </li> -->
                <li <?php echo (($this->uri->segment(2) == 'premi' or $this->uri->segment(2) == 'sppd') ? 'class="active"' : ''); ?>>
                    <a href="#"><i class="fa fa-dollar"></i> <span class="nav-label">Nominal</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li <?php echo (($this->uri->segment(2) == 'premi') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/premi">Premi</a></li>
                        <li <?php echo (($this->uri->segment(2) == 'sppd') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/sppd">SPPD</a></li>
                    </ul>
                </li>
                <li <?php echo (($this->uri->segment(2) == 'karyawan' or $this->uri->segment(2) == 'import_karyawan') ? 'class="active"' : ''); ?>>
                    <a href="#"><i class="fa fa-users"></i> <span class="nav-label">User</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li <?php echo (($this->uri->segment(2) == 'karyawan') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/karyawan">Karyawan</a></li>
                        <li <?php echo (($this->uri->segment(2) == 'import_karyawan') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/import_karyawan">Import Karyawan</a></li>
                    </ul>
                </li>
                <!-- supervisor -->
            <?php } elseif ($this->session->userdata('level') == "2") { ?>
                <li <?php if ($this->uri->segment(1) == 'spv' and $this->uri->segment(2) == '') {
                    echo 'class="active"';
                } ?>>
                <a href="<?php echo base_url(); ?>spv"><i class="fa fa-th-large"></i> <span class="nav-label">Beranda</span></a>
            </li>
            <li <?php if ($this->uri->segment(2) == 'cuti' or $this->uri->segment(2) == 'sakit' or $this->uri->segment(2) == 'izin' or $this->uri->segment(2) == 'lembur' or $this->uri->segment(2) == 'perjalanan_dinas' or $this->uri->segment(2) == 'tukar') {
                echo 'class="active"';
            } ?>>
            <a href="#"><i class="fa fa-ioxhost"></i> <span class="nav-label">Pengajuan</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li <?php echo (($this->uri->segment(2) == 'cuti') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>spv/cuti">Cuti<span class="label label-danger float-right"><?php echo $notifcuti; ?></span></a></li>
                <li <?php echo (($this->uri->segment(2) == 'sakit') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>spv/sakit">Sakit<span class="label label-danger float-right"><?php echo $notifsakit; ?></span></a></li>
                <!-- <li <?php echo (($this->uri->segment(2) == 'izin') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>spv/izin">Izin</a></li> -->
                <li <?php echo (($this->uri->segment(2) == 'lembur') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>spv/lembur">Lembur<span class="label label-danger float-right"><?php echo $notiflembur; ?></span></a></li>
                <li <?php echo (($this->uri->segment(2) == 'perjalanan_dinas') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>spv/perjalanan_dinas">Perjalanan Dinas<span class="label label-danger float-right">0</span></a></li>
                <!-- <li <?php echo (($this->uri->segment(2) == 'tukar') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>spv/tukar">Tukar Jadwal<span class="label label-danger float-right"><?php echo $notifjadwal; ?></a></li> -->
                </ul>
            </li>
            <!-- <li <?php echo (($this->uri->segment(2) == 'schedule') ? 'class="active"' : '');  ?>>
                <a href="<?php echo base_url(); ?>spv/schedule"><i class="fa fa-calendar"></i> <span class="nav-label">Jadwal Kerja</span></a>
            </li> -->
            <li <?php echo (($this->uri->segment(2) == 'laporanabsensi') ? 'class="active"' : '');  ?>>
                <a href="<?php echo base_url(); ?>spv/laporanabsensi"><i class="fa fa-file-movie-o"></i> <span class="nav-label">Laporan Absensi</span></a>
            </li>
            <li <?php echo (($this->uri->segment(2) == 'add_karyawan') ? 'class="active"' : '');  ?>>
                <a href="<?php echo base_url(); ?>spv/add_karyawan"><i class="fa fa-user"></i> <span class="nav-label">Data Karyawan</span></a>
            </li>
            <!-- manager -->
        <?php } elseif ($this->session->userdata('level') == "6") { ?>
            <li <?php if ($this->uri->segment(1) == 'manager' and $this->uri->segment(2) == '') {
                echo 'class="active"';
            } ?>>
            <a href="<?php echo base_url(); ?>manager"><i class="fa fa-th-large"></i> <span class="nav-label">Beranda</span></a>
        </li>
        <li <?php if ($this->uri->segment(2) == 'cuti' or $this->uri->segment(2) == 'sakit' or $this->uri->segment(2) == 'izin' or $this->uri->segment(2) == 'lembur' or $this->uri->segment(2) == 'perjalanan_dinas' or $this->uri->segment(2) == 'tukar') {
            echo 'class="active"';
        } ?>>
        <a href="#"><i class="fa fa-ioxhost"></i> <span class="nav-label">Pengajuan</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li <?php echo (($this->uri->segment(2) == 'cuti') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>manager/cuti">Cuti</a></li>
            <li <?php echo (($this->uri->segment(2) == 'sakit') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>manager/sakit">Sakit</a></li>
            <!-- <li <?php echo (($this->uri->segment(2) == 'izin') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>manager/izin">Izin</a></li> -->
            <li <?php echo (($this->uri->segment(2) == 'lembur') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>manager/lembur">Lembur</a></li>
            <li <?php echo (($this->uri->segment(2) == 'perjalanan_dinas') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>manager/perjalanan_dinas">Perjalanan Dinas</a></li>
            <!-- <li <?php echo (($this->uri->segment(2) == 'tukar') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>manager/tukar">Tukar Jadwal</a></li> -->
        </ul>
    </li>
    <!-- <li <?php echo (($this->uri->segment(2) == 'schedule') ? 'class="active"' : '');  ?>>
        <a href="<?php echo base_url(); ?>spv/schedule"><i class="fa fa-calendar"></i> <span class="nav-label">Jadwal Kerja</span></a>
    </li> -->
    <li <?php echo (($this->uri->segment(2) == 'add_karyawan') ? 'class="active"' : '');  ?>>
        <a href="<?php echo base_url(); ?>manager/add_karyawan"><i class="fa fa-user"></i> <span class="nav-label">Data Karyawan</span></a>
    </li>
    <!-- adminmust -->
<?php } elseif ($this->session->userdata('level') == "4") { ?>
    <li <?php if ($this->uri->segment(1) == 'adminmust' and $this->uri->segment(2) == '') {
        echo 'class="active"';
    } ?>>
    <a href="<?php echo base_url(); ?>adminmust"><i class="fa fa-th-large"></i> <span class="nav-label">Beranda</span></a>
</li>
<li <?php if ($this->uri->segment(2) == 'harian' or $this->uri->segment(2) == 'bulanan') {
    echo 'class="active"';
} ?>>
<a href="#"><i class="fa fa-file-movie-o"></i> <span class="nav-label">Laporan Absensi</span><span class="fa arrow"></span></a>
<ul class="nav nav-second-level collapse">
    <li <?php echo (($this->uri->segment(2) == 'bulanan') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/bulanan">Bulanan</a></li>
</ul>
</li>
<li <?php if ($this->uri->segment(2) == 'cuti' or $this->uri->segment(2) == 'sakit' or $this->uri->segment(2) == 'izin' or $this->uri->segment(2) == 'lembur' or $this->uri->segment(2) == 'perjalanan_dinas' or $this->uri->segment(2) == 'tukar') {
    echo 'class="active"';
} ?>>
<a href="#"><i class="fa fa-ioxhost"></i> <span class="nav-label">Pengajuan</span><span class="fa arrow"></span></a>
<ul class="nav nav-second-level collapse">
    <li <?php echo (($this->uri->segment(2) == 'cuti') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/cuti">Cuti</a></li>
    <li <?php echo (($this->uri->segment(2) == 'sakit') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/sakit">Sakit</a></li>
    <!-- <li <?php echo (($this->uri->segment(2) == 'izin') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/izin">Izin</a></li> -->
    <li <?php echo (($this->uri->segment(2) == 'lembur') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/lembur">Lembur</a></li>
    <li <?php echo (($this->uri->segment(2) == 'perjalanan_dinas') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/perjalanan_dinas">Perjalanan Dinas</a></li>
    <!-- <li <?php echo (($this->uri->segment(2) == 'tukar') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/tukar">Tukar Jadwal</a></li> -->
</ul>
</li>
<!-- <li <?php echo (($this->uri->segment(1) == 'schedule') ? 'class="active"' : '');  ?>>
    <a href="<?php echo base_url(); ?>schedule"><i class="fa fa-calendar"></i> <span class="nav-label">Jadwal Kerja</span></a>
</li> -->
                        <!-- <li <?php echo (($this->uri->segment(1) == 'slip') ? 'class="active"' : '');  ?>>
                            <a href="<?php echo base_url(); ?>adminmust/slip"><i class="fa fa-money"></i> <span class="nav-label">Input Slip Gaji</span></a>
                        </li> -->
                        <li <?php if ($this->uri->segment(2) == 'karyawan' or $this->uri->segment(2) == 'import_karyawan') {
                            echo 'class="active"';
                        } ?>>
                        <a href="#"><i class="fa fa-align-left"></i> <span class="nav-label">Pengaturan</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li <?php echo (($this->uri->segment(2) == 'karyawan') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/karyawan">Karyawan</a></li>
                            <li <?php echo (($this->uri->segment(2) == 'import_karyawan') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>superadmin/import_karyawan">Import Karyawan</a></li>
                        </ul>
                    </li>
                    <li <?php if ($this->uri->segment(2) == 'baukpremi' or $this->uri->segment(2) == 'bauklembur' or $this->uri->segment(2) == 'bauksppd') {
                        echo 'class="active"';
                    } ?>>
                    <a href="#"><i class="fa fa-align-left"></i> <span class="nav-label">BAUK NON PO</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li <?php echo (($this->uri->segment(2) == 'baukpremi') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/baukpremi">Premi Shift</a></li>
                        <li <?php echo (($this->uri->segment(2) == 'bauklembur') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/bauklembur">Lembur</a></li>
                        <li <?php echo (($this->uri->segment(2) == 'bauksppd') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/bauksppd">Perjalanan Dinas</a></li>
                    </ul>
                </li>
                        <!-- <li <?php if ($this->uri->segment(2) == 'jasa' or $this->uri->segment(2) == 'nonpo') {
                                        echo 'class="active"';
                                    } ?>>
                            <a href="#"><i class="fa fa-align-left"></i> <span class="nav-label">BAUK</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li <?php echo (($this->uri->segment(2) == 'jasa') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/jasa">Biaya Jasa</a></li>
                                <li <?php echo (($this->uri->segment(2) == 'nonpo') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>adminmust/nonpo">Biaya Non PO</a></li>
                            </ul>
                        </li>
                        <li <?php echo (($this->uri->segment(2) == 'pagu') ? 'class="active"' : '');  ?>>
                            <a href="<?php echo base_url(); ?>adminmust/pagu"><i class="fa fa-area-chart"></i> <span class="nav-label">Pagu</span></a>
                        </li> -->
                        <!-- <li <?php echo (($this->uri->segment(1) == 'rekap') ? 'class="active"' : '');  ?>>
                            <a href="<?php echo base_url(); ?>adminmust/rekap"><i class="fa fa-calendar"></i> <span class="nav-label">Rekap Harian dan Lembur</span></a>
                        </li> -->
                        <!-- sdm -->
                    <?php } elseif ($this->session->userdata('level') == "5") { ?>
                        <li <?php if ($this->uri->segment(1) == 'sdm' and $this->uri->segment(2) == '') {
                            echo 'class="active"';
                        } ?>>
                        <a href="<?php echo base_url(); ?>sdm"><i class="fa fa-th-large"></i> <span class="nav-label">Beranda</span></a>
                    </li>
                    <li <?php if ($this->uri->segment(2) == 'harian' or $this->uri->segment(2) == 'bulanan') {
                        echo 'class="active"';
                    } ?>>
                    <a href="#"><i class="fa fa-file-movie-o"></i> <span class="nav-label">Laporan Absensi</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li <?php echo (($this->uri->segment(2) == 'bulanan') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/bulanan">Bulanan</a></li>
                    </ul>
                </li>
                <li <?php if ($this->uri->segment(2) == 'cuti' or $this->uri->segment(2) == 'sakit' or $this->uri->segment(2) == 'izin' or $this->uri->segment(2) == 'lembur' or $this->uri->segment(2) == 'perjalanan_dinas' or $this->uri->segment(2) == 'tukar') {
                    echo 'class="active"';
                } ?>>
                <a href="#"><i class="fa fa-ioxhost"></i> <span class="nav-label">Pengajuan</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li <?php echo (($this->uri->segment(2) == 'cuti') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/cuti">Cuti</a></li>
                    <li <?php echo (($this->uri->segment(2) == 'sakit') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/sakit">Sakit</a></li>
                    <!-- <li <?php echo (($this->uri->segment(2) == 'izin') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/izin">Izin</a></li> -->
                    <li <?php echo (($this->uri->segment(2) == 'lembur') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/lembur">Lembur</a></li>
                    <li <?php echo (($this->uri->segment(2) == 'perjalanan_dinas') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/perjalanan_dinas">Perjalanan Dinas</a></li>
                    <!-- <li <?php echo (($this->uri->segment(2) == 'tukar') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/tukar">Tukar Jadwal</a></li> -->
                </ul>
            </li>

            <!-- <li <?php echo (($this->uri->segment(2) == 'schedule') ? 'class="active"' : '');  ?>>
                <a href="<?php echo base_url(); ?>sdm/schedule"><i class="fa fa-calendar"></i> <span class="nav-label">Jadwal Kerja</span></a>
            </li> -->
            <li <?php echo (($this->uri->segment(2) == 'add_karyawan') ? 'class="active"' : '');  ?>>
                <a href="<?php echo base_url(); ?>sdm/add_karyawan"><i class="fa fa-user"></i> <span class="nav-label">Data Karyawan</span></a>
            </li>
                        <!--  <li <?php if ($this->uri->segment(2) == 'jasa' or $this->uri->segment(2) == 'nonpo') {
                                        echo 'class="active"';
                                    } ?>>
                            <a href="#"><i class="fa fa-align-left"></i> <span class="nav-label">BAUK</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li <?php echo (($this->uri->segment(2) == 'jasa') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/jasa">Biaya Jasa</a></li>
                                <li <?php echo (($this->uri->segment(2) == 'nonpo') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/nonpo">Biaya Non PO</a></li>
                            </ul>
                        </li> -->
                        <li <?php echo (($this->uri->segment(2) == 'baukpo') ? 'class="active"' : '');  ?>>
                            <a href="<?php echo base_url(); ?>sdm/baukpo"><i class="fa fa-user"></i> <span class="nav-label">BAUK PO</span></a>
                        </li>
                        <li <?php echo (($this->uri->segment(2) == 'bauknonpo') ? 'class="active"' : '');  ?>>
                            <a href="<?php echo base_url(); ?>sdm/bauknonpo"><i class="fa fa-user"></i> <span class="nav-label">BAUK NON PO</span></a>
                        </li>

                        <!-- <li <?php if ($this->uri->segment(2) == 'baukpremi' or $this->uri->segment(2) == 'bauklembur' or $this->uri->segment(2) == 'bauksppd') {
                            echo 'class="active"';
                        } ?>>
                        <a href="#"><i class="fa fa-align-left"></i> <span class="nav-label">BAUK NON PO</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li <?php echo (($this->uri->segment(2) == 'baukpremi') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/baukpremi">Premi Shift</a></li>
                            <li <?php echo (($this->uri->segment(2) == 'bauklembur') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/bauklembur">Lembur</a></li>
                            <li <?php echo (($this->uri->segment(2) == 'bauksppd') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>sdm/bauksppd">Perjalanan Dinas</a></li>
                        </ul>
                    </li> -->

                        <!--  <li <?php echo (($this->uri->segment(2) == 'pagu') ? 'class="active"' : '');  ?>>
                            <a href="<?php echo base_url(); ?>sdm/pagu"><i class="fa fa-area-chart"></i> <span class="nav-label">Pagu</span></a>
                        </li> -->
                        <!-- karyawan -->
                    <?php } else { ?>
                        <li <?php if ($this->uri->segment(1) == 'karyawan' and $this->uri->segment(2) == '') {
                            echo 'class="active"';
                        } ?>>
                        <a href="<?php echo base_url(); ?>karyawan"><i class="fa fa-th-large"></i> <span class="nav-label">Beranda</span></a>
                    </li>
                    <li <?php if ($this->uri->segment(2) == 'harian' or $this->uri->segment(2) == 'bulanan') {
                        echo 'class="active"';
                    } ?>>
                    <a href="#"><i class="fa fa-file-movie-o"></i> <span class="nav-label">Laporan Absensi</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li <?php echo (($this->uri->segment(2) == 'bulanan') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/bulanan">Bulanan</a></li>
                    </ul>
                </li>
                <!-- <li <?php echo (($this->uri->segment(2) == 'schedule') ? 'class="active"' : '');  ?>>
                    <a href="<?php echo base_url(); ?>karyawan/schedule"><i class="fa fa-calendar"></i> <span class="nav-label">Jadwal Kerja</span></a>
                </li> -->
                <li <?php if ($this->uri->segment(2) == 'cuti' or $this->uri->segment(2) == 'sakit' or $this->uri->segment(2) == 'izin' or $this->uri->segment(2) == 'lembur' or $this->uri->segment(2) == 'perjalanan_dinas' or $this->uri->segment(2) == 'tukar') {
                    echo 'class="active"';
                } ?>>
                <a href="#"><i class="fa fa-paper-plane"></i> <span class="nav-label">Pengajuan</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li <?php echo (($this->uri->segment(2) == 'cuti') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/cuti">Cuti</a></li>
                    <li <?php echo (($this->uri->segment(2) == 'sakit') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/sakit">Sakit</a></li>
                    <!-- <li <?php echo (($this->uri->segment(2) == 'izin') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/izin">Izin</a></li> -->
                    <li <?php echo (($this->uri->segment(2) == 'lembur') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/lembur">Lembur</a></li>
                    <li <?php echo (($this->uri->segment(2) == 'perjalanan_dinas') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/perjalanan_dinas">Perjalanan Dinas</a></li>
                    <!-- <li <?php echo (($this->uri->segment(2) == 'tukar') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/tukar">Tukar Jadwal</a></li> -->
                </ul>
            </li>
            <li <?php if ($this->uri->segment(2) == 'report_cuti' or $this->uri->segment(2) == 'report_sakit' or $this->uri->segment(2) == 'report_izin' or $this->uri->segment(2) == 'report_lembur' or $this->uri->segment(2) == 'report_perjalanan_dinas' or $this->uri->segment(2) == 'report_tukar') {
                echo 'class="active"';
            } ?>>
            <a href="#"><i class="fa fa-file-pdf-o"></i> <span class="nav-label">Data Pengajuan</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li <?php echo (($this->uri->segment(2) == 'report_cuti') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/report_cuti">Cuti</a></li>
                <li <?php echo (($this->uri->segment(2) == 'report_sakit') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/report_sakit">Sakit</a></li>
                <!-- <li <?php echo (($this->uri->segment(2) == 'report_izin') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/report_izin">Izin</a></li> -->
                <li <?php echo (($this->uri->segment(2) == 'report_lembur') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/report_lembur">Lembur</a></li>
                <li <?php echo (($this->uri->segment(2) == 'report_perjalanan_dinas') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/report_perjalanan_dinas">Perjalanan Dinas</a></li>

                <!-- <li <?php echo (($this->uri->segment(2) == 'report_tukar') ? 'class="active"' : ''); ?>><a href="<?php echo base_url(); ?>karyawan/report_tukar">Tukar Jadwal</a></li> -->
            </ul>
        </li>
    <?php } ?>
</ul>

</div>
</nav>

<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">
                        Halo <strong><?php echo $namalengkap; ?></strong> Selamat Datang
                    </span>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-sign-out"></i> Keluar
                    </a>
                </li>

            </ul>
        </nav>
    </div>


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><?php echo $page; ?></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Beranda</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong><?php echo $page; ?></strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">