<?php
$id_user  = $this->session->userdata('id_user'); 
$pengguna = $this->model_login->getBiodata($id_user);
foreach ($pengguna as $bio) {
    $namalengkap = $bio->nama_lengkap;
    $avatar      = $bio->foto;
    $level       = $bio->level;
    $alamat      = $bio->alamat;
}
?>
<div class="row animated fadeInRight">
    <div class="col-md-3">
        <div class="widget-head-color-box navy-bg p-lg text-center">
            <div class="m-b-md">
                <h2 class="font-bold no-margins">
                    <?php echo $this->session->userdata('nama'); ?>
                </h2>
            </div>
            <img src="<?php echo base_url(); ?>uploads/avatar/<?php if ($avatar!=null) {echo $avatar; } else { echo 'um.png'; } ?>" class="img-circle circle-border m-b-md img-responsive"  alt="image" width="160px" height="160px">
        </div>
        <div class="widget-text-box">
            <p>Halo <strong><?php echo $this->session->userdata('nama'); ?></strong> anda adalah salah satu Karyawan ICON+. Alamat anda <a class="badge badge-xs badge-primary"><i class="fa fa-map-marker"></i> <?php echo $alamat; ?></a></p>
        </div>
    </div>
    <div class="col-md-9">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Profil Lengkap</h5>
            </div>
            <div class="ibox-content">
                <div class="panel-heading">
                    <div class="panel-options">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1">Info Personal</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2">Upload Avatar</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3">Ganti Password</a></li>
                        </ul>
                    </div>
                </div>
                <?php if ($this->session->userdata('level') == "2") { ?>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <strong>Informasi Personal</strong>
                                <div class="hr-line-dashed"></div>
                                <form method="post" action="<?php echo base_url(); ?>home/update_profile" class="form-horizontal form-update">
                                    <div class="result-update"></div>
                                    <?php foreach ($om as $rows) {} ?>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Nama</label>
                                                <input type="hidden" value="<?php echo $id_user ?>" name="id_user">
                                                <div class="col-sm-9">
                                                    <input type="text" readonly="" value="<?php echo $rows->nama_lengkap; ?>" name="nama" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Sub Bidang</label>
                                                <div class="col-sm-9">
                                                    <input type="text" readonly="" value="<?php echo $rows->subbid; ?>" id="jabatan" name="jabatan" class="form-control">
                                                </div>
                                            </div>  
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Bidang</label>
                                                <div class="col-sm-9">
                                                    <input type="text" readonly="" value="<?php echo $rows->bidang; ?>" id="jabatan" name="jabatan" class="form-control">
                                                </div>
                                            </div>  

                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Email</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="email" value="<?php echo $rows->email; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div class="col-sm-12 col-sm-offset-3">
                                                    <button class="btn btn-primary submit-update" type="submit">Simpan Perubahan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <strong>Upload Avatar</strong>
                                <div class="hr-line-dashed"></div>
                                <form method="post" action="home/changeavatar" class="form-horizontal " enctype="multipart/form-data">
                                    <input type="hidden" value="<?php echo $id_user ?>" name="id_user" >

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div data-provides="fileinput" class="fileinput fileinput-new">
                                                <div data-trigger="fileinput" style="width: 150px; height: 150px;" class="fileinput-preview thumbnail-circle">
                                                    <?php if($rows->foto!="") { ?>
                                                        <img src="uploads/avatar/<?php echo $rows->foto; ?>" class="img-circle" alt="..."/>
                                                    <?php } else { ?>
                                                        <img src="uploads/avatar/um.png" class="img-circle" alt="..."/>
                                                    <?php } ?>
                                                </div>
                                                <div>
                                                    <span class="btn btn-primary btn-file">
                                                        <span class="fileinput-new"><i class="fa fa-camera"></i> Choose Photo</span>
                                                        <span class="fileinput-exists">Ganti</span>
                                                        <input type="file" name="foto"/>
                                                    </span>
                                                    &nbsp;
                                                    <a href="#" data-dismiss="fileinput" class="btn btn-danger fileinput-exists">Remove</a>
                                                    <input type="hidden" name="avatar_lama" value="<?php echo $rows->foto; ?>">
                                                    <p>Max. 1MB</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <button type="submit" class="btn btn-primary submit-reset">Upload Photo</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <strong>Ganti Password</strong>
                                <div class="hr-line-dashed"></div>
                                <form method="post" action="home/update_password" class="form-horizontal form-reset">
                                    <div class="result-reset"></div>
                                    <input type="hidden" value="<?php echo $id_user ?>" name="id_user" >
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Password Sekarang</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password_lama" class="form-control input-dark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Password Baru</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password_baru" class="form-control input-dark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Masukan Lagi Password Baru</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="repassword_baru" class="form-control input-dark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-sm-offset-3">
                                            <button class="btn btn-primary submit-reset" type="submit">Ganti Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php } elseif ($this->session->userdata('level') == "6") { ?>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <strong>Informasi Personal</strong>
                                <div class="hr-line-dashed"></div>
                                <form method="post" action="<?php echo base_url(); ?>home/update_profile" class="form-horizontal form-update">
                                    <div class="result-update"></div>
                                    <?php foreach ($om as $rows) {} ?>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Nama</label>
                                                <input type="hidden" value="<?php echo $id_user ?>" name="id_user">
                                                <div class="col-sm-9">
                                                    <input type="text" readonly="" value="<?php echo $rows->nama_lengkap; ?>" name="nama" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Sub Bidang</label>
                                                <div class="col-sm-9">
                                                    <input type="text" readonly="" value="<?php echo $rows->subbid; ?>" id="jabatan" name="jabatan" class="form-control">
                                                </div>
                                            </div>  
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Bidang</label>
                                                <div class="col-sm-9">
                                                    <input type="text" readonly="" value="<?php echo $rows->bidang; ?>" id="jabatan" name="jabatan" class="form-control">
                                                </div>
                                            </div>  

                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Email</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="email" value="<?php echo $rows->email; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div class="col-sm-12 col-sm-offset-3">
                                                    <button class="btn btn-primary submit-update" type="submit">Simpan Perubahan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <strong>Upload Avatar</strong>
                                <div class="hr-line-dashed"></div>
                                <form method="post" action="home/changeavatar" class="form-horizontal " enctype="multipart/form-data">
                                    <input type="hidden" value="<?php echo $id_user ?>" name="id_user" >

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div data-provides="fileinput" class="fileinput fileinput-new">
                                                <div data-trigger="fileinput" style="width: 150px; height: 150px;" class="fileinput-preview thumbnail-circle">
                                                    <?php if($rows->foto!="") { ?>
                                                        <img src="uploads/avatar/<?php echo $rows->foto; ?>" class="img-circle" alt="..."/>
                                                    <?php } else { ?>
                                                        <img src="uploads/avatar/um.png" class="img-circle" alt="..."/>
                                                    <?php } ?>
                                                </div>
                                                <div>
                                                    <span class="btn btn-primary btn-file">
                                                        <span class="fileinput-new"><i class="fa fa-camera"></i> Choose Photo</span>
                                                        <span class="fileinput-exists">Ganti</span>
                                                        <input type="file" name="foto"/>
                                                    </span>
                                                    &nbsp;
                                                    <a href="#" data-dismiss="fileinput" class="btn btn-danger fileinput-exists">Remove</a>
                                                    <input type="hidden" name="avatar_lama" value="<?php echo $rows->foto; ?>">
                                                    <p>Max. 1MB</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <button type="submit" class="btn btn-primary submit-reset">Upload Photo</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <strong>Ganti Password</strong>
                                <div class="hr-line-dashed"></div>
                                <form method="post" action="home/update_password" class="form-horizontal form-reset">
                                    <div class="result-reset"></div>
                                    <input type="hidden" value="<?php echo $id_user ?>" name="id_user" >
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Password Sekarang</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password_lama" class="form-control input-dark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Password Baru</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password_baru" class="form-control input-dark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Masukan Lagi Password Baru</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="repassword_baru" class="form-control input-dark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-sm-offset-3">
                                            <button class="btn btn-primary submit-reset" type="submit">Ganti Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>

                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <strong>Informasi Personal</strong>
                                <div class="hr-line-dashed"></div>
                                <form method="post" action="<?php echo base_url(); ?>home/update_profile" class="form-horizontal form-update">
                                    <div class="result-update"></div>
                                    <?php foreach ($om as $rows) {} ?>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">No Induk</label>
                                                <div class="col-sm-9">
                                                    <input type="text" value="<?php echo $rows->no_induk; ?>" name="no_induk" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Jabatan</label>
                                                <div class="col-sm-9">
                                                    <input type="text" readonly="" value="<?php echo $rows->jabatan; ?>" id="jabatan" name="jabatan" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Alamat</label>
                                                <div class="col-sm-9">
                                                    <input type="text" value="<?php echo $rows->alamat; ?>" name="alamat" class="form-control">
                                                </div>
                                            </div>   
                                            <div class="form-group row" id="data_2">
                                                <label class="col-sm-3 control-label">Tanggal Lahir</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type='text' name="tgl_lahir" value="<?php echo $rows->tgl_lahir; ?>" class="form-control" name="tgl_lahir" data-date-format="YYYY-MM-DD" placeholder="Tahun-Bulan-Tanggal"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">HP</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="telepon" value="<?php echo $rows->no_telepon; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Status</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="status_perkawinan" value="<?php echo $rows->status_perkawinan; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Pendidikan Terakhir</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="pendidikan_terakhir" value="<?php echo $rows->pendidikan_terakhir; ?>" class="form-control">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Nama</label>
                                                <input type="hidden" value="<?php echo $id_user ?>" name="id_user">
                                                <div class="col-sm-9">
                                                    <input type="text" readonly="" value="<?php echo $rows->nama_lengkap; ?>" name="nama" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Agama</label>
                                                <div class="col-sm-9">
                                                    <input type="text" value="<?php echo $rows->agama; ?>" id="agama" name="agama" class="form-control">
                                                </div>
                                            </div>                                 
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Tempat Lahir</label>
                                                <div class="col-sm-9">
                                                    <input type="text" value="<?php echo $rows->tempat_lahir; ?>" name="tempat_lahir" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Jenis Kelamin</label>
                                                <div class="col-sm-9"> 
                                                    <select name="jenis_kelamin" class="form-control">
                                                        <option value="">Pilih Jenis Kelamin...</option>
                                                        <option value="Laki-laki" <?php if($rows->jenis_kelamin=="Laki-laki") { echo "selected"; } ?>>Laki-laki</option>
                                                        <option value="Perempuan" <?php if($rows->jenis_kelamin=="Perempuan") { echo "selected"; } ?>>Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Email</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="email" value="<?php echo $rows->email; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">NIK</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="no_ktp" value="<?php echo $rows->no_ktp; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Jurusan</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="jurusan" value="<?php echo $rows->jurusan; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">NPWP</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="no_npwp" value="<?php echo $rows->no_npwp; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">No Rekening</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="no_rek" value="<?php echo $rows->no_rek; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Gaji Pokok</label>
                                                <div class="col-sm-8">
                                                    <input type="text" readonly="" name="gapok" value="<?php echo $rows->gapok; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">BPJS TK</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="no_bpjs_ketenagakerjaan" value="<?php echo $rows->no_bpjs_ketenagakerjaan; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Bank</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="bank" value="<?php echo $rows->bank; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Uang Makan</label>
                                                <div class="col-sm-8">
                                                    <input type="text" readonly="" name="uang_makan" value="<?php echo $rows->uang_makan; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">BPJS</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="no_bpjs_kesehatan" value="<?php echo $rows->no_bpjs_kesehatan; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Nama</label>
                                                <div class="col-sm-8">
                                                    <input type="text"  name="nama_sesuai_rekening" value="<?php echo $rows->nama_sesuai_rekening; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Uang Pulsa</label>
                                                <div class="col-sm-8">
                                                    <input type="text" readonly="" name="uang_pulsa" value="<?php echo $rows->uang_pulsa; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Awal Kontrak</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="awal_kontrak" readonly="" value="<?php echo $rows->awal_kontrak; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 control-label">Akhir Kontrak</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="akhir_kontrak" readonly="" value="<?php echo $rows->akhir_kontrak; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <div class="col-sm-12 col-sm-offset-3">
                                                    <button class="btn btn-primary submit-update" type="submit">Simpan Perubahan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <strong>Upload Avatar</strong>
                                <div class="hr-line-dashed"></div>
                                <form method="post" action="home/changeavatar" class="form-horizontal " enctype="multipart/form-data">
                                    <input type="hidden" value="<?php echo $id_user ?>" name="id_user" >

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div data-provides="fileinput" class="fileinput fileinput-new">
                                                <div data-trigger="fileinput" style="width: 150px; height: 150px;" class="fileinput-preview thumbnail-circle">
                                                    <?php if($rows->foto!="") { ?>
                                                        <img src="uploads/avatar/<?php echo $rows->foto; ?>" class="img-circle" alt="..."/>
                                                    <?php } else { ?>
                                                        <img src="uploads/avatar/um.png" class="img-circle" alt="..."/>
                                                    <?php } ?>
                                                </div>
                                                <div>
                                                    <span class="btn btn-primary btn-file">
                                                        <span class="fileinput-new"><i class="fa fa-camera"></i> Choose Photo</span>
                                                        <span class="fileinput-exists">Ganti</span>
                                                        <input type="file" name="foto"/>
                                                    </span>
                                                    &nbsp;
                                                    <a href="#" data-dismiss="fileinput" class="btn btn-danger fileinput-exists">Remove</a>
                                                    <input type="hidden" name="avatar_lama" value="<?php echo $rows->foto; ?>">
                                                    <p>Max. 1MB</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <button type="submit" class="btn btn-primary submit-reset">Upload Photo</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <strong>Ganti Password</strong>
                                <div class="hr-line-dashed"></div>
                                <form method="post" action="home/update_password" class="form-horizontal form-reset">
                                    <div class="result-reset"></div>
                                    <input type="hidden" value="<?php echo $id_user ?>" name="id_user" >
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Password Sekarang</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password_lama" class="form-control input-dark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Password Baru</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password_baru" class="form-control input-dark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Masukan Lagi Password Baru</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="repassword_baru" class="form-control input-dark">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-sm-offset-3">
                                            <button class="btn btn-primary submit-reset" type="submit">Ganti Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>