<div class="row">
    <div class="col-lg-12">  
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Data Tukar Dinas</h5>
            </div>
            <div class="ibox-content">
                <table id="r_overtime" class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>
                            <th width="10px">No.</th>
                            <th>Nama Pemohon</th>
                            <th>Nama Pengganti</th>
                            <th>Jadwal Pemohon</th>
                            <th>Jadwal Pengganti </th>
                            <th>Tanggal</th>
                            <th>Alasan</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody id="baris">
                        <?php 
                        $perpage = $this->uri->segment(4);
                        $no=1+$perpage;  
                        foreach ($data_tunas as $rows) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $rows->nama_user; ?></td>
                                <td><?php echo $rows->nama_pengganti; ?></td>
                                <td><?php echo $rows->shift_pemohon; ?></td>
                                <td><?php echo $rows->shift_pengganti; ?></td>
                                <td><?php echo $rows->tanggal; ?></td>
                                <td><?php echo $rows->alasan; ?></td>    
                                <td>
                                    <?php if($rows->status=='0'){?>
                                        <p class="btn btn-sm btn-info" disabled><b>Menunggu</b></p>                     
                                    <?php }elseif($rows->status=='1'){ ?>
                                        <p class="btn btn-sm btn-info" disabled><b>Disetujui Pengganti</b></p>                                          
                                    <?php }elseif($rows->status=='3'){ ?>
                                        <p class="btn btn-sm btn-success" disabled><b>Disetujui Pengganti & SPV</b></p> 
                                    <?php }elseif($rows->status=='4'){ ?>
                                        <p class="btn btn-sm btn-danger" disabled><b>Ditolak</b></p>    
                                    <?php }?>
                                </td>    
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>

                    <hr>
                </div>
            </div>
        </div>
    </div>

    <?php foreach ($data_tunas as $rows) { ?>   
        <div class="modal inmodal" id="modal-hapus<?php echo $rows->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <b>Konfirmasi</b>
                    </div>
                    <div class="modal-body">
                        <p>Apakah Anda yakin membatalkan pengajuan anda?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Tidak</button>
                        <a href="<?php echo base_url(); ?>karyawan/delete_tunas/<?php echo $rows->id; ?>" class="btn btn-primary">Ya</a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>