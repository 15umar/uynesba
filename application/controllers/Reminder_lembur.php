<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reminder_lembur extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Model_karyawan','rm');
	}
	public function index()
	{
		
		$cek=$this->db->query("SELECT b.nama_lengkap, a.`insert` AS umar,id_user, due_date FROM 
			(SELECT id_user,`status`, jenis_lembur, `insert`, CURDATE() AS tanggal_sekarang, DATEDIFF(`insert`, CURDATE()) AS due_date,
			CASE WHEN DATEDIFF(`insert`, CURDATE()) <= -7 THEN '1' ELSE '0' END AS show_notif
			FROM tbu_lembur WHERE `status` = '1' OR `status`='2' AND DATEDIFF(`insert`, CURDATE()) <= 0) a
			LEFT JOIN `tbu_user` b ON b.id=a.id_user
			WHERE show_notif = 1");
		if ($cek ->num_rows() < 1) 
		{
			echo '<script>alert("Anda Sedang Libur");history.go(-1) </script>';

		}else{
			
			
			$config = [
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				'protocol'  => 'smtp',
				'smtp_host' => 'mail.must.co.id',
				'smtp_user' => 'support@must.co.id', 
				'smtp_pass'   => 'Must*2020#', 
				'smtp_crypto' => 'ssl',
				'smtp_port'   => 465,
				'crlf'    => "\r\n",
				'newline' => "\r\n",
			];
			$this->load->library('email', $config);

			$subject = 'Reminder Pengajuan Lembur';
			$data['reminder_lembur'] = $this->rm->get_reminderlembur();
			$isi = $this->load->view('karyawan/reminder',$data,true);
			$this->email->to('umarkoto@gmail.com');
			$this->email->from('umarkoto@must.co.id', 'Uynesba');
			$this->email->cc('umarkoto@gmail.com');
			$this->email->subject($subject);
			$this->email->message($isi);
			$result = $this->email->send();	
			if($result){
				// $this->rm->save_lembur($data);
				$this->session->set_flashdata('sukses', 'Data telah ditambah');
				redirect(base_url('karyawan'),'refresh');
			}else{
				echo '<br />';
				echo $this->email->print_debugger();
			}
			exit;
		}
	}
}
