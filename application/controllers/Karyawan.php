<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Karyawan extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('model_karyawan', 'rm');
		$this->load->model('Model_spv');
		$this->load->library('user_agent');
		$this->load->helper('url');
	}

	//dashboard
	public function index()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Selamat Datang";
			$data['datatelat'] = $this->rm->get_telat();
			$data['umarkoto'] = $this->rm->get_umarkoto();
			$data['jadwal'] = $this->rm->get_jadwal();
			$data['member'] = $this->rm->getAllMember();
			$data['jmlcutiapp'] = $this->rm->getAppCuti();
			$data['jmlcuti'] = $this->rm->getJumlahCuti();
			$data['jmlsakitapp'] = $this->rm->getAppSakit();
			$data['jmlsakit'] = $this->rm->getJumlahSakit();
			$data['jmllembur'] = $this->rm->getJumlahLembur();
			$data['jmllemburapp'] = $this->rm->getAppLembur();
			$user = $this->session->userdata('id_user');
			$data['absen'] = $this->rm->get_absen($user);
			$this->load->view('home/vheader', $data);
			$this->load->view('karyawan/vtambah_absen', $data);
			$this->load->view('home/vfooter');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function schedule()
	{ //umar 
		if ($this->session->userdata('level') == '3') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Jadwal Kerja";
			$this->load->view('home/vheader', $data);
			$this->load->view('karyawan/show');
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function absen_masuk()
	{ //umarkoto
		$img = $_POST['image'];
		$folderPath = "upload/";
		$image_parts = explode(";base64,", $img);
		$image_type_aux = explode("image/", $image_parts[0]);
		$image_type = $image_type_aux[1];
		$image_base64 = base64_decode($image_parts[1]);
		$fileName = uniqid() . '.png';
		$file = $folderPath . $fileName;
		file_put_contents($file, $image_base64);
		if ($this->agent->is_browser()) {
			$agent = $this->agent->browser() . ' ' . $this->agent->version();
		} elseif ($this->agent->is_mobile()) {
			$agent = $this->agent->mobile();
		} else {
			$agent = 'Data user gagal di dapatkan';
		}
		$browser = "" . $agent . "";
		$os = "" . $this->agent->platform();
		$ip = "" . $this->input->ip_address();
		$kode_abs = 1;
		$datenow = date("Y-m-d");
		$jam = "";
		$id_user     = $this->session->userdata('id_user');
		$cek = $this->db->query("select id_user, jam from tbu_absensi where id_user='$id_user' and tanggal='$datenow' and kode_abs='1'");
		// $cekjadwal=$this->db->query("SELECT user_id FROM schedule WHERE user_id='$id_user' AND tanggal='$datenow' AND id_shifting='L'");
		// $cekjadwal = $this->db->query("SELECT user_id FROM schedule WHERE user_id='$id_user' AND tanggal=DATE(DATE_ADD(TIMESTAMP(NOW()), INTERVAL 13 HOUR)) AND id_shifting='L'");
		// if ($cekjadwal->num_rows() > 0) {
		// 	echo '<script>alert("Anda Sedang Libur");history.go(-1) </script>';
		// } 
		// elseif 
		// 	if($cek->num_rows() > 0) {
		// 	foreach ($cek->result() as $data) {
		// 		$jam = $data->jam;
		// 	}
		// 	echo '<script>alert("Anda Sudah Absen Pukul ' . $jam . ', Silahkan Absen Pulang");history.go(-1) </script>';
		// 	return false;
		// } 
		// else {
		$data = array(
			'id_user'     => $this->session->userdata('id_user'),
			'browser'   => $browser,
			'os'        => $os,
			'ip'        => $ip,
			'kode_abs'  => $kode_abs,
			'realisasi' => $this->input->post('realisasi'),
			'tanggal'   => $datenow,
			'fotoabsen'   => $fileName,
			'lang' => $this->input->post('lang'),
			'jam'       => date("H:i:s"),
			'insert'	=> $datenow,
		);
		$query = $this->db->insert('tbu_absensi', $data);
		echo '<script>alert("Sukses melakukan absensi, Selamat bekerja dan bekarya");history.go(-1) </script>';
		// }
	}

	public function absen_pulang()
	{ //umarkoto
		$img = $_POST['image'];
		$folderPath = "upload/";
		$image_parts = explode(";base64,", $img);
		$image_type_aux = explode("image/", $image_parts[0]);
		$image_type = $image_type_aux[1];
		$image_base64 = base64_decode($image_parts[1]);
		$fileName = uniqid() . '.png';
		$file = $folderPath . $fileName;
		file_put_contents($file, $image_base64);
		if ($this->agent->is_browser()) {
			$agent = $this->agent->browser() . ' ' . $this->agent->version();
		} elseif ($this->agent->is_mobile()) {
			$agent = $this->agent->mobile();
		} else {
			$agent = 'Data user gagal di dapatkan';
		}
		$browser = "" . $agent . "";
		$os = "" . $this->agent->platform();
		$ip = "" . $this->input->ip_address();
		$kode_abs = 2;
		$datenow = date("Y-m-d");
		$jam = "";
		$id_user     = $this->session->userdata('id_user');
		$cekstatus = $this->db->query("select id_user, jam from tbu_absensi where id_user='$id_user' and jam>TIME(DATE_SUB(TIMESTAMP(NOW()), INTERVAL 14 HOUR)) and tanggal='$datenow' and kode_abs='1'");
		$cek = $this->db->query("select id_user, jam from tbu_absensi where id_user='$id_user' and tanggal='$datenow' and kode_abs='2'");
		// if ($cek->num_rows() > 0) {
		// 	foreach ($cek->result() as $data) {
		// 		$jam = $data->jam;
		// 	}
		// 	echo '<script>alert("Tidak perlu melakukan absen pulang karena sudah melakukan absen pulang ' . $jam . ', Selamat beristirahat dan berjumpa dengan keluarga tercinta");history.go(-1) </script>';
		// 	return false;
		// }
		// elseif ($cekstatus->num_rows() > 0){
		// 	foreach ($cekstatus->result() as $data) {
		// 		$jam=$data->jam;
		// 	}
		// 	echo '<script>alert("Oppps, tidak boleh korupsi waktu. Silahkan absen pulang ketika sudah 8 jam setelah absen masuk");history.go(-1) </script>';
		// 	return false;



		// } 
		// else {
		$data = array(
			'id_user'     => $this->session->userdata('id_user'),
			'browser'   => $browser,
			'os'        => $os,
			'ip'        => $ip,
			'kode_abs'  => $kode_abs,
			'tanggal'   => $datenow,
			'fotoabsen'   => $fileName,
			'pekerjaan' => $this->input->post('pekerjaan'),
			'lang' => $this->input->post('lang'),
			'jam'       => date("H:i:s"),
			'insert'	=> $datenow,
		);
		$query = $this->db->insert('tbu_absensi', $data);
		echo '<script>alert("Sukses melakukan absen pulang, Selamat beristirahat dan berjumpa dengan keluarga tercinta");history.go(-1) </script>';
		// }
	}

	//report
	public function bulanan()
	{ //umarkoto
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Bulanan";
		$this->load->view('home/vheader', $data);
		$data['koreksi'] = $this->rm->get_correction($this->session->userdata('id_user'));
		$this->load->view('karyawan/vbulanan', $data);
		$this->load->view('home/vfooter');
	}

	public function pencarianbulanan()
	{
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Absensi";
		$tglawal = $this->input->get('tglawal');
		$tglakhir = $this->input->get('tglakhir');
		$data['koreksi'] = $this->rm->pencarianbulanan($tglawal, $tglakhir);
		$this->load->view('home/vheader', $data);
		$this->load->view("karyawan/vbulanan", $data);
		$this->load->view('home/vfooter');
	}

	//cuti
	public function cuti()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Cuti";

			$data['sisa'] = $this->rm->get_sisa_cutitahunan();
			$data['melahirkan'] = $this->rm->get_jml_melahirkan();
			$data['ibadah'] = $this->rm->get_jml_ibadah();

			$data['sisamelahirkan'] = $this->rm->get_sisa_cutimelahirkan();
			$data['sisaibadah'] = $this->rm->get_sisa_cutiibadah();
			$data['jeniscuti'] = $this->rm->get_jeniscuti();
			$data['jeniscutikhusus'] = $this->rm->get_jeniscutikhusus();

			$data['sakit'] = $this->rm->get_jml_izin();
			$this->load->view('home/vheader', $data);
			$this->load->view('karyawan/vadd_cuti', $data);
			$this->load->view('home/vfooter');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function get_cuti()
	{
		$data['sisa'] = $this->rm->get_cutiumar();
	}

	public function simpan_cuti()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$datenow = date("Y-m-d");
			$data = array(
				'id_user' 		=> $this->session->userdata('id_user'),
				'nama_lengkap'	=> $this->input->post('nama_lengkap'),
				'jenis_leave'   => $this->input->post('jenis_leave'),
				'jenis_cutikhusus'   => $this->input->post('jenis_cutikhusus'),
				'start_date'	=> $this->input->post('start_date'),
				'end_date'		=> $this->input->post('end_date'),
				'alamat_cuti'	=> $this->input->post('alamat_cuti'),
				'jumlah_leave'	=> $this->input->post('jumlah_leave'),
				'keterangan'	=> $this->input->post('keterangan'),
				'tanggal_input' => $datenow,
			);
			$this->notif($data, 2);
			// $query = $this->db->insert('tbu_cuti',$data);
			// echo '<script>alert("Berhasil input data cuti");history.go(-1) </script>';
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function tukar()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Tukar Jadwal";
			$this->load->view('home/vheader', $data);
			$this->load->view('karyawan/vadd_tukar', $data);
			$this->load->view('home/vfooter');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function ambiljadwal()
	{
		$tanggal = $this->input->post('tanggal');
		$available = $this->rm->ambiljadwal($tanggal);
		echo json_encode($available);
	}

	public function get_available()
	{
		$tanggal = $this->input->post('tanggal');
		$jdwl = $this->input->post('jdwl');
		$available = $this->rm->get_available($tanggal, $jdwl);
		echo json_encode($available);
	}

	public function simpan_tukar()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$shift_pengganti = explode(',', $this->input->post('id_pengganti'))[1];
			$id_pengganti = explode(',', $this->input->post('id_pengganti'))[0];
			$datenow = date("Y-m-d");
			$data = array(
				'id_user' 		=> $this->session->userdata('id_user'),
				'shift_pemohon'		=> $this->input->post('id_shifting') . ' > ' . $shift_pengganti,
				'shift_pengganti'	=> $shift_pengganti . ' > ' . $this->input->post('id_shifting'),
				'id_pengganti'		=> $id_pengganti,
				'tanggal'			=> $this->input->post('tanggal'),
				'alasan'			=> $this->input->post('alasan'),
			);
			// $this->notif($data,2);
			$query = $this->db->insert('tbu_tukarjadwal', $data);
			echo '<script>alert("Berhasil tukar jadwal");history.go(-1) </script>';
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function report_cuti()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Cuti";
		$user = $this->session->userdata('id_user');
		$data['data_report'] = $this->rm->report_leave($user);
		// $data['sisa'] = $this->rm->get_sisa_cutitahunan();
		$data['jeniscuti'] = $this->rm->get_jeniscuti();
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vreport_leave', $data);
		$this->load->view('home/vfooter');
	}

	public function report_tukar()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Tukar Jadwal";
		$user = $this->session->userdata('id_user');
		$data['data_tunas'] = $this->rm->approval_official_exchange($user);
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vreport_tukar', $data);
		$this->load->view('home/vfooter');
	}

	public function delete_tunas($id)
	{ //umar
		if ($this->session->userdata('level') == '3') {
			$this->db->where('id', $id);
			$query = $this->db->delete('tbu_tukarjadwal');
			if ($query) {
				redirect('' . base_url() . 'karyawan/report_tukar');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function review_approval($kode, $id)
	{ //umar
		if ($this->session->userdata('level') == '3') {
			$result = $this->rm->review_approval($kode, $id, $this->session->userdata('id_user'));
			($result) ? ($this->session->set_flashdata('sukses', 'Permintaan berhasil di Approv')) : ($this->session->set_flashdata('gagal', 'Permintaan gagal di Review'));
			echo '<script>alert("Berhasil diterima");history.go(-1) </script>';
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function delete_cuti($id)
	{ //umar
		if ($this->session->userdata('level') == '3') {
			$this->db->where('id', $id);
			$query = $this->db->delete('tbu_cuti');
			if ($query) {
				redirect('' . base_url() . 'karyawan/report_cuti');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function izin()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Izin";
			$this->load->view('home/vheader', $data);
			$data['jeniscuti'] = $this->rm->get_jenisizin();
			$data['sakit'] = $this->rm->get_jml_izin();
			$this->load->view('karyawan/vadd_izin', $data);
			$this->load->view('home/vfooter');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function simpan_izin()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$datenow = date("Y-m-d");
			$data = array(
				'id_user' 		=> $this->session->userdata('id_user'),
				'nama_lengkap'	=> $this->input->post('nama_lengkap'),
				'jenis_leave'   => $this->input->post('jenis_leave'),
				'start_date'	=> $this->input->post('start_date'),
				'end_date'		=> $this->input->post('end_date'),
				'jumlah_leave'	=> $this->input->post('jumlah_leave'),
				'keterangan'	=> $this->input->post('keterangan'),
				'tanggal_input' => $datenow,
			);
			// $this->notif($data,2);
			$query = $this->db->insert('tbu_cuti', $data);
			echo '<script>alert("Berhasil input data izin");history.go(-1) </script>';
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function report_izin()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Izin";
		$user = $this->session->userdata('id_user');
		$data['data_report'] = $this->rm->report_izin($user);
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vreport_izin', $data);
		$this->load->view('home/vfooter');
	}

	public function delete_izin($id)
	{ //umar
		if ($this->session->userdata('level') == '3') {
			$this->db->where('id', $id);
			$query = $this->db->delete('tbu_cuti');
			if ($query) {
				redirect('' . base_url() . 'karyawan/report_izin');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function print_cuti($id)
	{ //umar
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Daftar Permohonan";
		$data['cuti'] = $this->rm->get_cutiId($id);
		$data['tot'] = $this->rm->get_cutitot($data['cuti'][0]->end_date);
		$data['sisa'] = $this->rm->get_cutisisa($data['cuti'][0]->end_date);
		$this->load->view('karyawan/vprint_cuti', $data);
	}

	public function print_lembur($id)
	{ //umar
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Daftar Permohonan";
		$data['lembur'] = $this->rm->get_lemburId($id);
		$this->load->view('karyawan/vprint_lembur', $data);
	}

	//sakit
	public function sakit()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Sakit";

			$data['sakit'] = $this->rm->get_jml_sakit();
			$this->load->view('home/vheader', $data);
			$this->load->view('karyawan/vadd_sakit', $data);
			$this->load->view('home/vfooter');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	function simpan_sakit()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$datenow = date("Y-m-d");
			$id_user = $this->session->userdata('id_user');
			$nama_lengkap = $this->input->post('nama_lengkap');
			$start_date = $this->input->post('start_date');
			$end_date = $this->input->post('end_date');
			$jumlah_leave = $this->input->post('jumlah_leave');
			$keterangan = $this->input->post('keterangan');
			$insert   = $datenow;
			$acak = mt_rand(1000000000, mt_getrandmax());
			$bersih = $_FILES['userfile']['name'];
			// $exts = @split("[/\\.]", $bersih);
			$exts = @explode("[/\\.]", $bersih);
			$n = count($exts) - 1;
			$ext = $exts[$n];
			$nama_fl = $acak . '_' . $id_user . '_' . $insert . '.' . $ext;
			$config["file_name"] = $nama_fl;
			$config['upload_path'] = './upload/sakit/';
			$config['allowed_types'] = 'jpg|jpeg|png|pdf';
			$config['max_size'] = '500000';
			$this->upload->initialize($config);
			if (!$this->upload->do_upload()) {
				echo $this->upload->display_errors();
			} else {
				$data = array(
					'id_user' 		=> $this->session->userdata('id_user'),
					'nama_lengkap'	=> $this->input->post('nama_lengkap'),
					'start_date'	=> $this->input->post('start_date'),
					'end_date'		=> $this->input->post('end_date'),
					'jumlah_leave'	=> $this->input->post('jumlah_leave'),
					'keterangan'	=> $this->input->post('keterangan'),
					'tanggal_input' => $datenow,
				);
				$this->notif($data, 3);
				// $this->rm->jalankan_query_manual("insert into tbu_cuti 
				// 	(id_user,nama_lengkap,start_date,end_date,jumlah_leave,keterangan,nama_file,tanggal_input) 
				// 	values('".$id_user."','".$nama_lengkap."','".$start_date."','".$end_date."','".$jumlah_leave."','".$keterangan."','".$nama_fl."','".$insert."')");	
				// echo '<script>alert("Berhasil input data sakit");history.go(-1) </script>';
			}
		}
	}

	public function report_sakit()
	{	//umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Sakit";
		$user = $this->session->userdata('id_user');
		$data['data_report'] = $this->rm->report_sakit($user);
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vreport_sakit', $data);
		$this->load->view('home/vfooter');
	}

	public function delete_sakit($id)
	{ //umar
		if ($this->session->userdata('level') == '3') {
			$this->db->where('id', $id);
			$query = $this->db->delete('tbu_cuti');
			if ($query) {
				redirect('' . base_url() . 'karyawan/report_sakit');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	//lembur
	public function lembur()
	{ //umarkoto
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Pengajuan Lembur";
		$this->load->view('home/vheader', $data);
		$data['om'] = $this->rm->get_profile();
		$data['jenislembur'] = $this->rm->get_jenislembur();
		$this->load->view('karyawan/vadd_lembur', $data);
		$this->load->view('home/vfooter');
	}

	public function simpan_lembur()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$datenow = date("Y-m-d");
			$data = array(
				'id_user' 			=> $this->session->userdata('id_user'),
				'alasan'			=> $this->input->post('alasan'),
				'jenis_lembur'			=> $this->input->post('jenis_lembur'),
				'type_lembur'			=> $this->input->post('type_lembur'),
				'start_datetime'	=> $this->input->post('start_date') . ' ' . $this->input->post('start_time'),
				'end_datetime'		=> $this->input->post('end_date') . ' ' . $this->input->post('end_time'),
				'insert'   		=> $datenow,
			);
			$this->notif($data, 1);
			// $query = $this->db->insert('tbu_lembur',$data);
			// echo '<script>alert("Berhasil input data lembur");history.go(-1) </script>';
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function report_lembur()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Lembur";
		$user = $this->session->userdata('id_user');
		$data['data_report'] = $this->rm->report_overtime($user);
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vreport_lembur', $data);
		$this->load->view('home/vfooter');
	}

	public function delete_lembur($id)
	{ //umar	
		if ($this->session->userdata('level') == '3') {
			$this->db->where('id', $id);
			$query = $this->db->delete('tbu_lembur');
			if ($query) {
				redirect('' . base_url() . 'karyawan/report_lembur');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function perjalanan_dinas()
	{ //umarkoto
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Pengajuan Dinas";
		// $user = $this->session->userdata('id_user');
		$data['data_report'] = $this->rm->get_dinas();
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vadd_dinas');
		$this->load->view('home/vfooter');
	}

	public function simpan_dinas()
	{ //umarkoto
		if ($this->session->userdata('level') == '3') {
			$datenow = date("Y-m-d");
			$data = array(
				'id_user' => $this->session->userdata('id_user'),
				'tanggal_pergi'		=> $this->input->post('tanggal_pergi'),
				'tanggal_pulang'			=> $this->input->post('tanggal_pulang'),
				'transportasi'			=> $this->input->post('transportasi'),
				'kota_asal'			=> $this->input->post('kota_asal'),
				'tujuan'			=> $this->input->post('tujuan'),
				'pekerjaan'		=> $this->input->post('pekerjaan'),
				'jumlah'		=> $this->input->post('jumlah'),
				'pengajuan'		=> $this->input->post('pengajuan'),
				'referensi'		=> $this->input->post('referensi'),
				'insert'   => $datenow,
			);
			$this->notif($data, 4);
			// $query = $this->db->insert('tbu_perjalanandinas',$data);
			// echo '<script>alert("Berhasil input data perjalanan dinas, silahkan upload bukti tiket di kolom tiket untuk mengetahui Nominal keseluruhan");history.go(-1) </script>';
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function tiket($id)
	{

		$data['title'] = "Uynesba";
		$data['page'] = "Create Tiket";
		$this->load->view('home/vheader', $data);
		$data['cuti'] = $this->rm->get_cuti($id);
		$this->load->view('karyawan/vtiket', $data);
		$this->load->view('home/vfooter');
	}

	public function update_tiket()
	{
		// $upload_data = array('uploads'=>$this->upload->data());
		$datenow = date("Y-m-d");
		$id_user = $this->session->userdata('id_user');
$insert   = $datenow;
$acak = mt_rand(1000000000, mt_getrandmax());
		$config['upload_path'] = './uploads/perjalanan_dinas/';
		$config['allowed_types'] = 'pdf|jpg';
		$config['max_size'] = '2000';
		$bersih = $_FILES['userfile']['name'];
			// $exts = @split("[/\\.]", $bersih);
			$exts = @explode("[/\\.]", $bersih);
			$n = count($exts) - 1;
			
			$dname = explode(".", $_FILES['userfile']['name']);
			$ext = end($dname);
			$nama_fl = $acak . '_' . $id_user . '_' . $insert . '.' . $ext;
			$config["file_name"] = $nama_fl;
		$this->upload->initialize($config);
		if (!$this->upload->do_upload('userfile')) {
			echo $this->upload->display_errors();
		} else {
			$data = array('upload_data' => $this->upload->data()); 
			$pet = $config['upload_path'].$config['file_name']; 
			$data = array(

				'tanggal_pergi'			=> $this->input->post('tanggal_pergi'),
				'tanggal_pulang'				=> $this->input->post('tanggal_pulang'),
				'harga_tiket'				=> $this->input->post('harga_tikett'),
				'nama_file' => $nama_fl,

			);
			$this->db->where('id_dinas', $this->input->post('id_dinas'));
			$query = $this->db->update('tbu_perjalanandinas', $data);
			if ($query) {
				redirect('' . base_url() . 'karyawan/report_perjalanan_dinas');
			}
		}
	}

	public function report_perjalanan_dinas()
	{ //umar	
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Perjalanan Dinas";
		$user = $this->session->userdata('id_user');
		$data['data_report'] = $this->rm->report_perjalanan_dinas($user);
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vreport_perjalanan_dinas', $data);
		$this->load->view('home/vfooter');
	}

	public function delete_dinas($id)
	{ //umar
		if ($this->session->userdata('level') == '3') {
			$this->db->where('id_dinas', $id);
			$query = $this->db->delete('tbu_perjalanandinas');
			if ($query) {
				redirect('' . base_url() . 'karyawan/report_perjalanan_dinas');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function print_dinas($id)
	{ //umar
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Daftar Permohonan";
		$data['lembur'] = $this->rm->get_dinasId($id);
		$this->load->view('karyawan/vprint_dinas', $data);
	}

	public function slip_gaji()
	{ //umar
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Data Slip";
		$user = $this->session->userdata('id_user');
		$data['data_report'] = $this->rm->get_slip($user);
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vslip_gaji', $data);
		$this->load->view('home/vfooter');
	}

	public function print_slip($id)
	{ //umar
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Daftar Permohonan";
		$data['lembur'] = $this->rm->get_slip($id);
		$this->load->view('karyawan/vprint_slip', $data);
	}

	//notifemail
	public function notif($data, $jenis)
	{ //umarkoto

		error_reporting(0);
		$divisi = $this->session->userdata('organisasi');
		$getemail = $this->rm->get_email($divisi, '2');
		$config = [
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'protocol'  => 'smtp',
			'smtp_host' => 'mail.must.co.id',
			'smtp_user' => 'support@must.co.id',
			'smtp_pass'   => '5upPort_2#1',
			'smtp_crypto' => 'ssl',
			'smtp_port'   => 465,
			'crlf'    => "\r\n",
			'newline' => "\r\n",
		];
		$this->load->library('email', $config);

		if ($jenis == 1) {
			$split_start = explode(" ", $data[start_datetime]);
			$split_end = explode(" ", $data[end_datetime]);
			$subject = 'Notifikasi Pengajuan Lembur ' . $this->session->userdata('nama') . '';
			$message = 'Dengan hormat, <br> <br> <br>' .
				'Menginformasikan bahwa :<br><br>
			Nama : ' . $this->session->userdata('nama') . ' <br><br>
			Telah mengajukan form lembur pada tanggal ' . $split_start[0] . ' jam ' . $split_start[1] . '- tanggal' . $split_end[0] . ' jam ' . $split_end[1] . ' wib dengan alasan ' . $data[alasan] . ', mohon izin/ persetujuan.<br> <br>
			Demikian kami sampaikan.<br> <br><br>
			Regards <br><br>
			(' . $this->session->userdata('nama') . ')';
			$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
			<title>' . html_escape($subject) . '</title>
			<style type="text/css">
			body {
				font-family: Arial, Verdana, Helvetica, sans-serif;
				font-size: 16px;
			}
			</style>
			</head>
			<body>
			' . $message . '
			</body>
			</html>';
			foreach ($getemail as $key => $val) {
				$this->email->to($val->email);
			}
			$this->email->from('support@must.co.id', 'Uynesba');
			$this->email->cc($this->session->userdata('email'));
			$this->email->subject($subject);
			$this->email->message($body);
			$result = $this->email->send();
			if ($result) {
				$this->rm->save_lembur($data);
				$this->session->set_flashdata('sukses', 'Data telah ditambah');
				redirect(base_url('karyawan/lembur'), 'refresh');
			} else {
				echo '<br />';
				echo $this->email->print_debugger();
			}
			exit;
		} elseif ($jenis == 2) {
			$subject = 'Notifikasi Pengajuan Cuti ' . $data[nama_lengkap] . '';
			$message = 'Dengan hormat Bpk/ Ibu, <br> <br> <br>' .
				'Menginformasikan bahwa :<br><br>
			Nama : ' . $data[nama_lengkap] . ' <br><br>
			Telah mengajukan cuti pada tanggal ' . $data[start_date] . ' sampai dengan ' . $data[end_date] . ' selama ' . $data[jumlah_leave] . ' hari dengan alasan ' . $data[keterangan] . ', mohon izin/ persetujuan.<br> <br>
			Demikian kami sampaikan.<br> <br><br>
			Regards <br><br>
			(' . $data[nama_lengkap] . ')';
			$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
			<title>' . html_escape($subject) . '</title>
			<style type="text/css">
			body {
				font-family: Arial, Verdana, Helvetica, sans-serif;
				font-size: 16px;
			}
			</style>
			</head>
			<body>
			' . $message . '
			</body>
			</html>';
			foreach ($getemail as $key => $val) {
				$this->email->to($val->email);
			}
			$this->email->from('support@must.co.id', 'Uynesba');
			$this->email->cc($this->session->userdata('email'));
			$this->email->subject($subject);
			$this->email->message($body);
			$result = $this->email->send();
			if ($result) {
				$this->rm->save_cuti($data);
				$this->session->set_flashdata('sukses', 'Data telah ditambah');
				redirect(base_url('karyawan/cuti'), 'refresh');
			} else {
				echo '<br />';
				echo $this->email->print_debugger();
			}
			exit;
		} elseif ($jenis == 3) {
			$subject = 'Notifikasi Pengajuan Sakit ' . $data[nama_lengkap] . '';
			$message = 'Dengan hormat Bpk/ Ibu, <br> <br> <br>' .
				'Menginformasikan bahwa :<br><br>
			Nama : ' . $data[nama_lengkap] . ' <br><br>
			Telah mengajukan sakit pada tanggal ' . $data[start_date] . ' sampai dengan ' . $data[end_date] . ' selama ' . $data[jumlah_leave] . ' hari dengan alasan sakit ' . $data[keterangan] . ', mohon izin/ persetujuan.<br> <br>
			Demikian kami sampaikan.<br> <br><br>
			Regards <br><br>
			(' . $data[nama_lengkap] . ')';
			$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
			<title>' . html_escape($subject) . '</title>
			<style type="text/css">
			body {
				font-family: Arial, Verdana, Helvetica, sans-serif;
				font-size: 16px;
			}
			</style>
			</head>
			<body>
			' . $message . '
			</body>
			</html>';
			foreach ($getemail as $key => $val) {
				$this->email->to($val->email);
			}
			$this->email->from('support@must.co.id', 'Uynesba');
			$this->email->cc($this->session->userdata('email'));
			$this->email->subject($subject);
			$this->email->message($body);
			$result = $this->email->send();
			if ($result) {
				$this->rm->save_cuti($data);
				$this->session->set_flashdata('sukses', 'Data telah ditambah');
				redirect(base_url('karyawan/sakit'), 'refresh');
			} else {
				echo '<br />';
				echo $this->email->print_debugger();
			}
			exit;
		} elseif ($jenis == 4) {
			$subject = 'Notifikasi Pengajuan SPPD ' . $this->session->userdata('nama') . '';
			$message = 'Dengan hormat Bpk/ Ibu, <br> <br> <br>' .
				'Menginformasikan bahwa :<br><br>
			Nama : ' . $this->session->userdata('nama') . ' <br><br>
			Telah mengajukan cuti pada tanggal ' . $data[tanggal_pergi] . ' sampai dengan ' . $data[tanggal_pulang] . ' selama ' . $data[jumlah] . ' hari dengan pekerjaan ' . $data[pekerjaan] . ', mohon izin/ persetujuan.<br> <br>
			Demikian kami sampaikan.<br> <br><br>
			Regards <br><br>
			(' . $this->session->userdata('nama') . ')';
			$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
			<title>' . html_escape($subject) . '</title>
			<style type="text/css">
			body {
				font-family: Arial, Verdana, Helvetica, sans-serif;
				font-size: 16px;
			}
			</style>
			</head>
			<body>
			' . $message . '
			</body>
			</html>';
			foreach ($getemail as $key => $val) {
				$this->email->to($val->email);
			}
			$this->email->from('support@must.co.id', 'Uynesba');
			$this->email->cc($this->session->userdata('email'));
			$this->email->subject($subject);
			$this->email->message($body);
			$result = $this->email->send();
			if ($result) {
				$this->rm->save_sppd($data);
				$this->session->set_flashdata('sukses', 'Data telah ditambah');
				redirect(base_url('karyawan/perjalanan_dinas'), 'refresh');
			} else {
				echo '<br />';
				echo $this->email->print_debugger();
			}
			exit;
		} else {
			$subject = 'Notifikasi Request Koreksi Absen Presisi';
			$message = 'Dengan hormat, <br> <br> <br>' .
				'Menginformasikan bahwa :<br><br>
			' . $this->session->userdata('nama') . ' <br><br>
			Telah mengajukan form koreksi absen pada tanggal ' . $data[presence_date] . ' dengan alasan ' . $data[reason] . ', mohon untuk direview / approve.<br> <br>
			Demikian kami sampaikan, dan terima kasih telah menggunakan aplikasi Presisi.<br> <br><br>
			Regards';
			$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
			<title>' . html_escape($subject) . '</title>
			<style type="text/css">
			body {
				font-family: Arial, Verdana, Helvetica, sans-serif;
				font-size: 16px;
			}
			</style>
			</head>
			<body>
			' . $message . '
			</body>
			</html>';
			foreach ($getemail as $key => $val) {
				$this->email->to($val->email);
			}
			$this->email->from('support@must.co.id');
			$this->email->cc($this->session->userdata('email'));
			$this->email->subject($subject);
			$this->email->message($body);
			$result = $this->email->send();
			if ($result) {
				$result = $this->rm->request_correction($data);
				$this->session->set_flashdata('sukses', 'Data berhasil diinput');
				redirect(base_url('request/correction'), 'refresh');
			} else {
				echo '<br />';
				echo $this->email->print_debugger();
			}
			exit;
		}
	}
}
