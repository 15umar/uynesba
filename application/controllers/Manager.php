<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manager extends CI_Controller 
{
	function __construct(){
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model('Model_spv');
		$this->load->model('Model_manager');
		$this->load->model('model_schedule');
	}

	public function tukar(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Tukar Jadwal";
		$user = $this->session->userdata('id_user');
		$data['data_tunas'] = $this->Model_manager->approval_official_exchange($user);
		$this->load->view('home/vheader',$data);
		$this->load->view('manager/vadd_tukar',$data);
		$this->load->view('home/vfooter');			
	}

	public function izin(){ //umarkoto
		if ($this->session->userdata('level')=='6') 
		{
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Izin";
			$data['data_cuti'] = $this->Model_manager->approval_izin();
			$this->load->view('home/vheader',$data);
			$this->load->view('manager/vizin',$data);
			$this->load->view('home/vfooter');	
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}			
	}

	public function index(){ //umarkoto
		if ($this->session->userdata('level')=='6') 
		{
			$data['datatelat'] = $this->Model_manager->get_telat();
			$data['member'] = $this ->Model_manager->getAllMember();
			$data['jmlcuti'] = $this ->Model_manager->getJumlahCuti();
			$data['jmlcutiapp'] = $this ->Model_manager->getAppCuti();
			$data['jmllembur'] = $this ->Model_manager->getJumlahLembur();
			$data['jmlsakit'] = $this ->Model_manager->getJumlahSakit();
			$data['jmlsakitapp'] = $this ->Model_manager->getAppSakit();
			$data['jmldinas'] = $this ->Model_manager->getJumlahDinas();
			$data['jmldinasapp'] = $this ->Model_manager->getAppDinas();
			// $data['jmlresign'] = $this ->Model_manager->getJumlahResign();
			// $data['jmlresignapp'] = $this ->Model_manager->getAppResign();
			$data['jmllemburapp'] = $this ->Model_manager->getAppLembur();

			$data['tukarjadwal'] = $this ->Model_manager->gettukarjadwal();
			$data['absen'] = $this ->Model_manager->get_absen();
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Selamat Datang";
			$this->load->view('home/vheader',$data);
			$this->load->view('home/vberanda', $data);
			$this->load->view('home/vfooter');
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	

	public function get_overtime(){
		$tglawal = $this->input->post('tglawal');
		$tglakhir = $this->input->post('tglakhir');
		//echo json_encode($tglawal);
		$report = $this->Model_admin->get_overtime($tglawal,$tglakhir);

		echo json_encode($report);
	}

	public function get_excel(){
		$tglawal = $this->input->post('tglawal');
		$tglakhir = $this->input->post('tglakhir');
		//echo json_encode($tglawal);
		$report = $this->Model_admin->get_overtime($tglawal,$tglakhir);
		
		echo json_encode($report);
	}

	public function cuti(){ //umar
		if ($this->session->userdata('level')=='6') 
		{
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Cuti";
			$data['data_cuti'] = $this->Model_manager->approval_leave();
			$this->load->view('home/vheader',$data);
			$this->load->view('manager/vcuti',$data);
			$this->load->view('home/vfooter');	
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}		
	}

	public function reject_approval(){ //umar
		if ($this->session->userdata('level')=='6' or '5') 
		{
			$kode = $this->input->post('kode');
			$id = $this->input->post('id');
			$reason = $this->input->post('reason');			
			$result = $this->Model_manager->reject_approval($kode, $id, $this->session->userdata('id_user'), $reason);
			($result)?($this->session->set_flashdata('sukses', 'Permintaan berhasil di Reject')):($this->session->set_flashdata('gagal', 'Permintaan gagal di Reject'));
			echo '<script>alert("Berhasil ditolak");history.go(-1) </script>';
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}	
	}

	public function review_approval($kode, $id){ //umar
		if ($this->session->userdata('level')=='6') 
		{
			$result = $this->Model_manager->review_approval($kode, $id, $this->session->userdata('id_user'));
			($result)?($this->session->set_flashdata('sukses', 'Permintaan berhasil di Approv')):($this->session->set_flashdata('gagal', 'Permintaan gagal di Review'));
			echo '<script>alert("Berhasil diterima");history.go(-1) </script>';
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}	
	}

	public function sakit(){ //umar
		if ($this->session->userdata('level')=='6') 
		{
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Sakit";
			$data['data_cuti'] = $this->Model_manager->approval_sakit();
			$this->load->view('home/vheader',$data);
			$this->load->view('manager/vsakit',$data);
			$this->load->view('home/vfooter');	
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}			
	}

	public function lembur(){ //umar
		if ($this->session->userdata('level')=='6') 
		{
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Approv Pengajuan Lembur";
			$data['data_lembur'] = $this->Model_manager->approval_overtime();
			$this->load->view('home/vheader',$data);
			$this->load->view('manager/vlembur',$data);
			$this->load->view('home/vfooter');	
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}			
	}

	public function perjalanan_dinas(){ //umar
		if ($this->session->userdata('level')=='6') 
		{
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Approv Pengajuan Perjalanan Dinas";
			$data['data_perjalanan_dinas'] = $this->Model_manager->approval_perjalanan_dinas();
			$this->load->view('home/vheader',$data);
			$this->load->view('manager/vperjalanan_dinas',$data);
			$this->load->view('home/vfooter');			
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}	
	}

	

	public function schedule(){ //umar
		if ($this->session->userdata('level')=='2') 
		{
			$data['title'] 	= "Uynesba";
			$data['page'] 	= "Jadwal Kerja";
			$this->load->view('home/vheader',$data);
			$this->load->view('admin/show');
			$this->load->view('home/vfooter');
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}	
	}
	
	public function create(){ //umar
		if ($this->session->userdata('level')=='2') 
		{
			$data['title'] 	= "Schedule | ABS";
			$data['page'] 	= "Schedule";
			$this->load->view('home/vheader',$data);
			$this->load->view('admin/create');
			$this->load->view('home/vfooter');
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}	
	}
	
	public function show_schedule(){ //umar
		if ($this->session->userdata('level')=='2') 
		{
			$date    =$this->input->post('start').'|'.$this->input->post('end');
			$results =$this->Model_admin->show_schedule($date);
			$sdate   =((substr($this->input->post('start'),0,7)==substr($this->input->post('end'),0,7))?'1':'0');
			$data = array(
				'range' => $this->date_range($this->input->post('start'),$this->input->post('end')),
				'date'	=> $date,
				'sdate'	=> $sdate,
				'schedule'	=> json_encode($results)
			);
			$this->load->view('admin/schedule',$data);
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}	
	}

	public function get_pattern(){ //umar
		if ($this->session->userdata('level')=='2') 
		{
			$data = array(
				'input' => $this->input->post('input'),
				'emp' 	=> $this->input->post('emp'),
				'range' => $this->date_range($this->input->post('start'),$this->input->post('end')),
				'date'	=> $this->input->post('start').'|'.$this->input->post('end')
			);
			$this->load->view('admin/pattern',$data);
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function get_employee(){ //umar
		if ($this->session->userdata('level')=='2') 
		{
			$results=$this->Model_admin->get_employee();
			echo json_encode(array("person" => $results));
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function save(){ //umar
		if ($this->session->userdata('level')=='2') 
		{
			$emp   = $this->input->post('emp');	
			$input = $this->input->post('input');
			$date  = $this->input->post('date');
			$results=$this->Model_admin->save_schedule($emp,$date,$input);
			echo $results;
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	private function date_range($first, $last, $step = '+1 day', $output_format = 'Y/m/d' ) {

		$dates = array();
		$current = strtotime($first);
		$last = strtotime($last);

		while( $current <= $last ) {

			$dates[] = date($output_format, $current);
			$current = strtotime($step, $current);
		}

		return $dates;
	}

	public function import_karyawan(){ //umar

		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Import Karyawan";
		$this->load->view('home/vheader',$data);
		$this->load->view('admin/import_karyawan');
		$this->load->view('home/vfooter');
	}

	public function add_karyawan(){ //umar
		if ($this->session->userdata('level')=='2' or '6') 
		{
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Data Karyawan";
			$data['data_karyawan'] = $this->Model_manager->getmember();
			$this->load->view('home/vheader',$data);
			$this->load->view('spv/add_karyawan',$data);
			$this->load->view('home/vfooter');
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function createdata(){ //umar
		$config['upload_path'] 	 = './upload/excel';
		$config['allowed_types'] = 'xls';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload())
		{
			$data = array('error' => $this->upload->display_errors());
		}
		else
		{
			$data = array('error' => false);
			$upload_data = $this->upload->data();
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$file =  $upload_data['full_path'];
			$this->excel_reader->read($file);
			error_reporting(E_ALL ^ E_NOTICE);
			$data = $this->excel_reader->sheets[0] ;
			$dataexcel = Array();
			for ($i = 2; $i <= $data['numRows']; $i++) 
			{
				if($data['cells'][$i][1] == '') break;
				$dataexcel[$i-2]['email']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][1]);
				$dataexcel[$i-2]['password']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][2]);
				$dataexcel[$i-2]['level']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][3]);
				$dataexcel[$i-2]['organisasi']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][4]);
				$dataexcel[$i-2]['no_induk']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][5]);
				$dataexcel[$i-2]['nama_lengkap']			= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][6]);
				$dataexcel[$i-2]['lokasi']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][7]);
				$dataexcel[$i-2]['unit_kerja']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][8]);
				$dataexcel[$i-2]['tempat_lahir']			= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][9]);
				$dataexcel[$i-2]['tgl_lahir']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][10]);
				$dataexcel[$i-2]['status_perkawinan']		= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][11]);
				$dataexcel[$i-2]['agama']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][12]);
				$dataexcel[$i-2]['jenis_kelamin']			= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][13]);
				$dataexcel[$i-2]['status_pajak']			= (is_null($data['cells'][$i][5]) ? 0 : $data['cells'][$i][14]);
				$dataexcel[$i-2]['golongan_status_bpjs']	= (is_null($data['cells'][$i][5]) ? 0 : $data['cells'][$i][15]);
				$dataexcel[$i-2]['no_ktp']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][16]);
				$dataexcel[$i-2]['alamat']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][17]);
				$dataexcel[$i-2]['no_telepon']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][18]);
				$dataexcel[$i-2]['jabatan']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][19]);
				$dataexcel[$i-2]['no_npwp']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][20]);
				$dataexcel[$i-2]['no_bpjs_ketenagakerjaan']	= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][21]);
				$dataexcel[$i-2]['no_bpjs_kesehatan']		= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][22]);
				$dataexcel[$i-2]['no_rek']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][23]);
				$dataexcel[$i-2]['bank']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][24]);
				$dataexcel[$i-2]['nama_sesuai_rekening']	= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][25]);
			}
			delete_files($upload_data['file_path']);
			$this->Model_admin->tambahcontact($dataexcel);
		}
		redirect(''.base_url().'admin/import_karyawan');
	}
}

