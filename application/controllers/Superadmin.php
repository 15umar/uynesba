<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Superadmin extends CI_Controller 
{
	function __construct(){
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model('Model_superadmin');
		$this->load->model('Model_spv');
		$this->load->model('Model_home');
	}

	public function index(){ //umar
		if ($this->session->userdata('level')=='1') 
		{
			$data['datatelat'] = $this->Model_superadmin->get_telat();
			$data['member'] = $this ->Model_superadmin->getAllMember();
			$data['memberdas'] = $this ->Model_superadmin->getdasMember();
			$data['jmlcuti'] = $this ->Model_superadmin->getJumlahCuti();
			$data['jmlcutiapp'] = $this ->Model_superadmin->getAppCuti();
			$data['jmlsakit'] = $this ->Model_superadmin->getJumlahSakit();
			$data['jmlsakitapp'] = $this ->Model_superadmin->getAppSakit();
			$data['jmllembur'] = $this ->Model_superadmin->getJumlahLembur();
			$data['jmllemburapp'] = $this ->Model_superadmin->getAppLembur();
			$data['absen'] = $this ->Model_superadmin->get_absen();
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Selamat Datang";
			$this->load->view('home/vheader',$data);
			$this->load->view('superadmin/vberanda', $data);
			$this->load->view('home/vfooter');
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function cuti(){	//umar
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Uynesba";
			$data['page'] 	= "Pengajuan Cuti";
			$data['data_cuti'] = $this->Model_superadmin->approval_leave();
			$this->load->view('home/vheader',$data);
			$this->load->view('superadmin/vcuti',$data);
			$this->load->view('home/vfooter');		
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}	
	}

	public function sakit(){ //umar
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Uynesba";
			$data['page'] 	= "Pengajuan Sakit";
			$data['data_cuti'] = $this->Model_superadmin->approval_sakit();
			$this->load->view('home/vheader',$data);
			$this->load->view('superadmin/vsakit',$data);
			$this->load->view('home/vfooter');	
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}			
	}

	public function lembur(){ //umar
		if ($this->session->userdata('level')=='1') 
		{	
			$data['title'] 	= "Uynesba";
			$data['page'] 	= "Approv Pengajuan Lembur";
			$data['data_lembur'] = $this->Model_superadmin->approval_overtime();
			$this->load->view('home/vheader',$data);
			$this->load->view('superadmin/vlembur',$data);
			$this->load->view('home/vfooter');			
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}	
	}

	public function import_karyawan(){ //umar	
		if ($this->session->userdata('level')=='1' or '4') 
		{
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Import Karyawan";
			$this->load->view('home/vheader',$data);
			$this->load->view('superadmin/import_karyawan');
			$this->load->view('home/vfooter');
		}else{
			echo"<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function createdata(){ //simpanimport
		$config['upload_path'] 	 = './upload/excel';
		$config['allowed_types'] = 'xls';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload())
		{
			$data = array('error' => $this->upload->display_errors());
		}
		else
		{
			$data = array('error' => false);
			$upload_data = $this->upload->data();
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$file =  $upload_data['full_path'];
			$this->excel_reader->read($file);
			error_reporting(E_ALL ^ E_NOTICE);
			$data = $this->excel_reader->sheets[0] ;
			$dataexcel = Array();
			for ($i = 2; $i <= $data['numRows']; $i++) 
			{
				if($data['cells'][$i][1] == '') break;
				$dataexcel[$i-2]['email']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][1]);
				$dataexcel[$i-2]['password']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][2]);
				$dataexcel[$i-2]['level']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][3]);
				$dataexcel[$i-2]['organisasi']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][4]);
				$dataexcel[$i-2]['no_induk']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][5]);
				$dataexcel[$i-2]['nama_lengkap']			= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][6]);
				
				$dataexcel[$i-2]['lokasi']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][7]);
				$dataexcel[$i-2]['unit_kerja']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][8]);
				$dataexcel[$i-2]['tempat_lahir']			= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][9]);
				$dataexcel[$i-2]['tgl_lahir']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][10]);
				$dataexcel[$i-2]['status_perkawinan']		= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][11]);
				$dataexcel[$i-2]['agama']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][12]);
				$dataexcel[$i-2]['jenis_kelamin']			= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][13]);
				$dataexcel[$i-2]['status_pajak']			= (is_null($data['cells'][$i][5]) ? 0 : $data['cells'][$i][14]);
				$dataexcel[$i-2]['golongan_status_bpjs']	= (is_null($data['cells'][$i][5]) ? 0 : $data['cells'][$i][15]);
				$dataexcel[$i-2]['no_ktp']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][16]);
				$dataexcel[$i-2]['alamat']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][17]);
				$dataexcel[$i-2]['no_telepon']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][18]);
				$dataexcel[$i-2]['jabatan']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][19]);
				// $dataexcel[$i-2]['no_npwp']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][20]);
				// $dataexcel[$i-2]['no_bpjs_ketenagakerjaan']	= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][21]);
				// $dataexcel[$i-2]['no_bpjs_kesehatan']		= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][22]);
				// $dataexcel[$i-2]['no_rek']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][23]);
				// $dataexcel[$i-2]['bank']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][24]);
				// $dataexcel[$i-2]['nama_sesuai_rekening']	= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][25]);
			}
			delete_files($upload_data['file_path']);
			$this->Model_superadmin->tambahcontact($dataexcel);
		}
		redirect(''.base_url().'superadmin/import_karyawan');
	}

	
	public function review_approval($kode, $id){
		$result = $this->Model_superadmin->review_approval($kode, $id, $this->session->userdata('id_user'));
		($result)?($this->session->set_flashdata('sukses', 'Permintaan berhasil di Review')):($this->session->set_flashdata('gagal', 'Permintaan gagal di Review'));
		$this->notif($kode,$id);
	}

	

	public function premi(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Premi";
		$data['premi'] = $this->Model_superadmin->premi();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/premi',$data);
		$this->load->view('home/vfooter');			
	}

	public function sppd(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "SPPD";
		$data['sppd'] = $this->Model_superadmin->sppd();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/sppd',$data);
		$this->load->view('home/vfooter');			
	}

	public function simpan_sppd()
	{
		$data = array
		(
			'id_transportasi'=> $this->input->post('id_transportasi'),
			'nama_transportasi'=> $this->input->post('nama_transportasi'),
			'biaya_penginapan'=> $this->input->post('biaya_penginapan'),
			'uang_makan'=> $this->input->post('uang_makan'),
			'uang_saku'=> $this->input->post('uang_saku'),
			'angkutan_setempat'=> $this->input->post('angkutan_setempat'),
			'ke_bandara'=> $this->input->post('ke_bandara'),
		);
		$query = $this->db->insert('tbu_kategori_sppd',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function client(){ //umar
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Client";
		$data['client'] = $this->Model_superadmin->client();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/client',$data);
		$this->load->view('home/vfooter');			
	}

	public function simpan_client()
	{
		$data = array
		(
			'id_personal'=> $this->input->post('id_personal'),
			'client'=> $this->input->post('client'),
			'bidang'=> $this->input->post('bidang'),
			'subbid'=> $this->input->post('subbid'),
			'spv'=> $this->input->post('spv'),
			'manajer'=> $this->input->post('manajer'),
		);
		$query = $this->db->insert('organisasi',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function bidang(){ //umar
		$data['title'] 	= "Uynesba V.0.2";
		$data['page'] 	= "Bidang";
		$data['bidang'] = $this->Model_superadmin->bidang();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/bidang',$data);
		$this->load->view('home/vfooter');			
	}

	public function simpan_bidang()
	{
		$data = array
		(
			'id_bidang'=> $this->input->post('id_bidang'),
			'nama'=> $this->input->post('nama')
		);
		$query = $this->db->insert('tbu_bidang',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function subbidang(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Sub Bidang";
		$data['subbid'] = $this->Model_superadmin->subbid();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/subbid',$data);
		$this->load->view('home/vfooter');			
	}

	public function simpan_subbidang(){
		$data = array
		(
			'id_subbid'=> $this->input->post('id_subbid'),
			'nama'=> $this->input->post('nama')
		);
		$query = $this->db->insert('tbu_subbid',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function supervisor(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Supervisor";
		$data['supervisor'] = $this->Model_superadmin->supervisor();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/supervisor',$data);
		$this->load->view('home/vfooter');			
	}

	public function delete_supervisor($id){ //umar
		
		$this->db->where('id',$id);
		$query=$this->db->delete('tbu_supervisor');
		if ($query)
		{	
			redirect(''.base_url().'superadmin/supervisor');
		}
		
	}

	public function simpan_supervisor(){
		$data = array
		(
			'id_supervisor'=> $this->input->post('id_supervisor'),
			'nama'=> $this->input->post('nama')
		);
		$query = $this->db->insert('tbu_supervisor',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function manager(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Manager";
		$data['manager'] = $this->Model_superadmin->manager();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/manager',$data);
		$this->load->view('home/vfooter');			
	}

	public function simpan_manager(){
		$data = array
		(
			'id_manager'=> $this->input->post('id_manager'),
			'nama'=> $this->input->post('nama')
		);
		$query = $this->db->insert('tbu_manager',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function klien(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Client";
		$data['klien'] = $this->Model_superadmin->klien();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/klien',$data);
		$this->load->view('home/vfooter');			
	}

	public function simpan_klien(){
		$data = array
		(
			'id_client'=> $this->input->post('id_client'),
			'nama'=> $this->input->post('nama')
		);
		$query = $this->db->insert('tbu_client',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function organisasi(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Organisasi";
		$data['organisasi'] = $this->Model_superadmin->client();
		$data['client'] = $this->Model_superadmin->klien();
		$data['bidang'] = $this->Model_superadmin->bidang();
		$data['subbid'] = $this->Model_superadmin->subbid();
		$data['supervisor'] = $this->Model_superadmin->supervisor();
		$data['manager'] = $this->Model_superadmin->manager();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/organisasi',$data);
		$this->load->view('home/vfooter');			
	}

	public function simpan_organisasi(){
		$data = array
		(
			'id_personal'=> $this->input->post('id_personal'),
			'client'=> $this->input->post('client'),
			'bidang'=> $this->input->post('bidang'),
			'subbid'=> $this->input->post('subbid'),
			'spv'=> $this->input->post('spv'),
			'manajer'=> $this->input->post('manajer')

		);
		$query = $this->db->insert('organisasi',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function karyawan(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Karyawan";
		$data['karyawan'] = $this->Model_superadmin->karyawan();
		$data['level'] = $this->Model_superadmin->level();
		$data['organisasi'] = $this->Model_superadmin->client();
		$this->load->view('home/vheader',$data);
		$this->load->view('superadmin/karyawan',$data);
		$this->load->view('home/vfooter');			
	}

	public function simpan_karyawan(){
		$data = array
		(
			'no_induk'=> $this->input->post('no_induk'),
			'password'=> md5($this->input->post('password')),
			'nama_lengkap'=> $this->input->post('nama_lengkap'),
			'email'=> $this->input->post('email'),
			'jabatan'=> $this->input->post('jabatan'),
			'tempat_lahir'=> $this->input->post('tempat_lahir'),
			'tgl_lahir'=> $this->input->post('tgl_lahir'),
			'level'=> $this->input->post('level'),
			'organisasi'=> $this->input->post('organisasi'),
			'awal_kontrak'=> $this->input->post('awal_kontrak'),
			'akhir_kontrak'=> $this->input->post('akhir_kontrak')
		);
		$query = $this->db->insert('tbu_user',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function update_karyawan()
	{
		if($this->session->userdata('level')=='1')
		{
			$data=array(
				'no_induk'=> $this->input->post('no_induk'),
				// 'password'=> md5($this->input->post('password')),
				'nama_lengkap'=> $this->input->post('nama_lengkap'),
				'email'=> $this->input->post('email'),
				'jabatan'=> $this->input->post('jabatan'),
			);
			$this->db->where('no_induk',$this->input->post('no_induk'));
			$query=$this->db->update('tbu_user',$data);
			if($query)
			{
				redirect(''.base_url().'superadmin/karyawan');
			}
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function reset_karyawan()
	{
		if($this->session->userdata('level')=='1')
		{
			$data=array(
				'no_induk'=> $this->input->post('no_induk'),
				// 'nama_lengkap'=> $this->input->post('nama_lengkap'),
				'password'=> md5($this->input->post('password')),
			);
			$this->db->where('no_induk',$this->input->post('no_induk'));
			$query=$this->db->update('tbu_user',$data);
			if($query)
			{
				redirect(''.base_url().'superadmin/karyawan');
			}
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	

	
	//closenew
	
	public function cek($id){	
		$query2 = $this->db->query("SELECT * FROM cuti WHERE id = '".$id."' AND status = '3'");
		$row2 = $query2->row();
		if (isset($row2)){
			if($row2->jenis_leave == '1' || $row2->jenis_leave == '3' || $row2->jenis_leave == '4'){
				for($i=0;$i<$row2->jumlah_leave;$i++){
					$tanggal = date("Y-m-d", strtotime("+".$i." days",strtotime($row2->start_date)));
					$this->db->query("UPDATE `schedule` SET id_shifting = 'C' WHERE user_id = '".$row2->id_om."' AND tanggal = '".$tanggal."'");
				}						
			}else if($row2->jenis_leave == '5' || $row2->jenis_leave == '3' || $row2->jenis_leave == '7'){
				for($i=0;$i<$row2->jumlah_leave;$i++){
					$tanggal = date("Y-m-d", strtotime("+".$i." days",strtotime($row2->start_date)));
					$this->db->query("UPDATE `schedule` SET id_shifting = 'SA' WHERE user_id = '".$row2->id_om."' AND tanggal = '".$row2->start_date."'");
				}
			}
		}
	}
	
	public function checkin(){	
		if($this->session->userdata('id_user') != NULL){	
			$kode = $this->input->post('kode');		
			if ($this->agent->is_browser()){
				$agent = $this->agent->browser().' '.$this->agent->version();
			}elseif ($this->agent->is_mobile()){
				$agent = $this->agent->mobile();
			}else{
				$agent = 'Data user gagal di dapatkan';
			}
			$browser = "". $agent . "";
			$os = "" . $this->agent->platform();
			$ip = "" . $this->input->ip_address();
			$kode_abs = 1;
			$id_om     = $this->session->userdata('id_user');
			$cek = $this->Model_home->absen_in($id_om, $browser, $os, $ip, $kode);
			echo json_encode($cek);
		}else{
			$array = array('errSess'=>'1');
			echo json_encode($array);
		}
	}
	
	
	//
	public function profile()
	{
		
		$data['title'] 	= "Profil OM | ABS";
		$data['page'] 	= "Profil OM";
		$this->load->view('home/header',$data);
		$data['om'] = $this->Model_home->get_profile();
		$this->load->view('home/profile',$data);
		$this->load->view('home/footer');

	}

	public function update_profile(){
		/* if($this->session->userdata('level')=='OM'){ */
			$data = array(
				'no_induk' 	   => $this->input->post('no_induk'),
				'nama_lengkap' => $this->input->post('nama'),
				'jabatan'	   => $this->input->post('jabatan'),
				'alamat'	   => $this->input->post('alamat'),
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'tgl_lahir'	   => $this->input->post('tgl_lahir'),
				'jenis_kelamin'=> $this->input->post('jenis_kelamin'),
				'no_telepon'   => $this->input->post('telepon'),
				'email'		   => $this->input->post('email')
			);
			$this->db->where('id',$this->session->userdata('id_user'));
			$query = $this->db->update('employee',$data);
			if ($query) { ?>
				<div class="alert alert-success alert-dismissable">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					Edit Profile <a class="alert-link" href="#">Berhasil.</a>.
				</div>
			<?php }
		/* }else{
			redirect(''.base_url().'login');
		} */
	}
//public function update_profile(){
//		if($this->session->userdata('level')=='OM'){
//			$data = array(
//				'no_induk' 	   => $this->input->post('no_induk'),
//				'nama_lengkap' => $this->input->post('nama'),
//				'alamat'	   => $this->input->post('alamat'),
//				'tempat_lahir' => $this->input->post('tempat_lahir'),
//				'tgl_lahir'	   => $this->input->post('tgl_lahir'),
//				'jenis_kelamin'=> $this->input->post('jenis_kelamin'),
//				'no_telepon'   => $this->input->post('telepon'),
//				'email'		   => $this->input->post('email')
//			);
//			$this->db->where('id_om',$this->input->post('id_user'));
//			$query = $this->db->update('om',$data);
//		if ($query) { 
//				<div class="alert alert-success alert-dismissable">
//					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
//					Edit Profile <a class="alert-link" href="#">Berhasil.</a>.
//					</div>
//			<?php }
//		}else{
//			redirect(''.base_url().'login');
//		}
//	} 
	

	public function changeavatar(){
		if($this->session->userdata('logged_in')==TRUE){
			$config['upload_path'] 		= './uploads/avatar/';
			$config['allowed_types'] 	= 'jpg|png|jpeg';
			$config['max_size'] 		= '5000';
			$config['max_width']  		= '10000';
			$config['max_height']  		= '10000';
			$config['overwrite'] 		= TRUE;
			$this->upload->initialize($config);
			$dataupload = array();
			$user_id  = $this->session->userdata('id_user');
			$avatar_lama = $this->input->post('avatar_lama');
			$rand = random_string('alnum', 15);
			foreach($_FILES as $field => $file){
				if($file['error'] == 0){
					if ($this->upload->do_upload($field)){	
						$data = $dataupload[$field] = $this->upload->data();
						foreach ($dataupload as $key => $value) {
							if(isset($value['file_name'])){
								$data[$key] = $value['file_name'];
								$this->load->library('image_lib');
								$config_crop['image_library'] 	= 'gd2';
								$config_crop['source_image'] 	= './uploads/avatar/'.$data[$key].'';
								$config_crop['maintain_ratio'] 	= FALSE;
								$config_crop['create_thumb'] 	= FALSE;
								$config_crop['new_image'] 		= './uploads/avatar/ava_'.$rand.'.jpg';
								$config_crop['quality'] 		= "60%";
								//Set cropping for y or x axis, depending on image orientation
								if ($value['image_width'] > $value['image_height']){
									$config_crop['width'] = $value['image_height'];
									$config_crop['height'] = $value['image_height'];
									$config_crop['x_axis'] = (($value['image_width'] / 2) - ($config_crop['width'] / 2));
								}else{
									$config_crop['height'] = $value['image_width'];
									$config_crop['width'] = $value['image_width'];
									$config_crop['y_axis'] = (($value['image_height'] / 2) - ($config_crop['height'] / 2));
								}
								$this->image_lib->initialize($config_crop);
								$sukses = $this->image_lib->crop();
								$this->image_lib->clear();
								if($sukses){
									unlink('./uploads/avatar/'.$data[$key].'');
								}
							}else{
								$data[$key] = "";
							}
						} 
					}else{
						$this->form_validation->set_message('_file[]', $this->upload->display_errors());
						echo"<script>alert('Format file upload tidak sesuai. Silahkan ulangi kembali.'); window.history.back();</script>";
						return FALSE;
					}
				}
			}
			$data = array('id_om' => $user_id);
			foreach ($dataupload as $key => $value) {
				if(isset($value['file_name'])){
					$data[$key] = 'ava_'.$rand.'.jpg';
				}else{
					$data[$key] = "";
				}
			}
			$this->db->where('id_om',$user_id);
			$this->db->update('om',$data);
			unlink('./uploads/avatar/'.$avatar_lama.''); 
			redirect(''.base_url().'om/profile');
		}else{
			redirect(''.base_url().'');
		}	
	}

	public function update_password(){
		if ($this->session->userdata('level')=='1'){
			$password_lama = md5($this->input->post('password_lama'));
			$password_baru = $this->input->post('password_baru');
			$repassword_baru = $this->input->post('repassword_baru');
			$id_user  = $this->input->post('id_user');
			$this->form_validation->set_rules('password_lama', 'password lama', 'trim|required');
			$this->form_validation->set_rules('password_baru', 'password baru', 'trim|required');
			$this->form_validation->set_rules('repassword_baru', 'ulangi password baru', 'trim|required|matches[password_baru]');
			if($this->form_validation->run() == FALSE){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}else{
				$password = $this->om_m->get_profile();
				foreach ($password as $rows){
					$pass_lama_db = $rows->password;
				}
				if ($password_lama!=$pass_lama_db){?>
					<div class="alert alert-success alert-dismissable">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						password lama <a class="alert-link" href="#">salah.</a>.
					</div>
					<?php
				}else{
					$data=array(
						'password' => md5($this->input->post('password_baru')),
					);
					$this->db->where('id_om',$this->input->post('id_user'));
					$query = $this->db->update('om',$data);
					?>
					<div class="alert alert-success alert-dismissable">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						Ganti password <a class="alert-link" href="#">Berhasil.</a>.
					</div>
					<?php
				}
			}
		}else{
			redirect(''.base_url().'login');
		}
	}	
//--------------------------------------------------------------------------------------
		//presensi
	public function presensi()
	{
		if ($this->session->userdata('level')=='1')
		{
			$data['title']  = "Welcome | ABS";
			$data['page']   = "Welcome";
			$this->load->view('home/header',$data);
			$this->load->view('om/add_presensi',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function view_presensi()
	{
		if ($this->session->userdata('level')=='1')
		{
			$data['title']  = "Welcome | ABS";
			$data['page']   = "Welcome";
			$data['absensi'] = $this->om_m->get_absensi();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('home/header',$data);
			$this->load->view('om/view_presensi',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	



		//lembur
	public function lembur2()
	{
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Ajukan Lembur | ABS";
			$data['page'] 	= "Lembur";
			$this->load->view('home/header',$data);
			$data['lembur'] = $this->om_m->get_lembur();
			$data['om'] = $this->om_m->get_profile();
			$data['layanan'] = $this->om_m->get_layanan();
			$this->load->view('om/add_lembur',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function view_lembur()
	{
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Ajukan Lembur | ABS";
			$data['page'] 	= "Lembur";
			$this->load->view('home/header',$data);
			$data['lembur'] = $this->om_m->get_lemburrep();
			$data['om'] = $this->om_m->get_profile();
			$data['layanan'] = $this->om_m->get_layanan();
			$this->load->view('om/view_lembur',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function view_sakit()
	{
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Ajukan Lembur | ABS";
			$data['page'] 	= "Lembur";
			$this->load->view('home/header',$data);
			$data['sakit'] = $this->om_m->get_sakit();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/view_sakit',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function app_lembur()
	{
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Approval Lembur | ABS";
			$data['page'] 	= "Lembur";
			$this->load->view('home/header',$data);
			$data['lembur'] = $this->om_m->get_sakit();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/app_lembur',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function cetaklembur($id)
	{
		if ($this->session->userdata('level')=='1')
		{
			$data['title'] 	= "Welcome | ABS";
			$data['page'] 	= "Daftar Permohonan";
			$data['cuti'] = $this->om_m->get_cutiId($id);
			$data['tot'] = $this->om_m->get_cutitot();
			$data['sisa'] = $this->om_m->get_cutisisa();
			$this->load->view('om/printcuti',$data);
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function simpan_lem()
	{
		$data = array
		(
			'id_om' => $this->session->userdata('id_user'),
			'nama_lengkap'=> $this->input->post('nama'),
			'id_layanan'	=> $this->input->post('id_layanan'),
			'alasan'	=> $this->input->post('alasan'),
			'start_time'	=> $this->input->post('start_time'),
			'end_time'	=> $this->input->post('end_time'),
			'tanggal'		=> date('Y-m-d H:i:s'),
			'status' => 'menunggu'
		);
		$query = $this->db->insert('lembur',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function delete_lembur($id_lembur)
	{
		if($this->session->userdata('level')=='1')
		{
			$this->db->where('id_lembur',$id_lembur);
			$query=$this->db->delete('lembur');
			if ($query)
			{	
				redirect(''.base_url().'om/view_lembur');
			}
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}


		//cuti
	

	

	

	public function edit_cuti()
	{
		if($this->session->userdata('level')=="om")
		{
			$data['title']= "Edit Cuti | ABS";
			$data['page']= "Edit Cuti";
			$this->load->view('home/header',$data);
			$data['cuti'] = $this->om_m->get_cuti();
			$this->load->view('om/edit_cuti',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'');
		}
	}

	public function view_cuti()
	{
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Form Cuti | ABS";
			$data['page'] 	= "Cuti";
			$this->load->view('home/header',$data);
			$data['cuti'] = $this->om_m->get_cutirep();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/view_cuti',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function app_cuti()
	{
		if ($this->session->userdata('level')=='operator') 
		{
			$data['title'] 	= "Approval Cuti | ABS";
			$data['page'] 	= "Cuti";
			$this->load->view('home/header',$data);
			$data['cuti'] = $this->om_m->get_cuti();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/app_cuti',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function cetakcuti($id)
	{
		if ($this->session->userdata('level')=='1')
		{
			$data['title'] 	= "Welcome | ABS";
			$data['page'] 	= "Daftar Permohonan";
			$data['cuti'] = $this->om_m->get_cutiId($id);
			$data['tot'] = $this->om_m->get_cutitot();
			$data['sisa'] = $this->om_m->get_cutisisa();
			$this->load->view('om/print_cuti',$data);
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function simpan_cuti()
	{	
		$datenow=date("Y-m-d");
		$data = array
		(
			'id_om' => $this->session->userdata('id_user'),
			'nama_lengkap'=> $this->input->post('nama'),
			'start_date'		=> $this->input->post('start_date'),
			'end_date'		=> $this->input->post('end_date'),
			'jenis_cuti'	=> $this->input->post('jenis_cuti'),
			'jml_cuti'	=> $this->input->post('jml_cuti'),
			'alamat_cuti'	=> $this->input->post('alamat_cuti'),
			'alasan'	=> $this->input->post('alasan'),
			'tanggal'   => $datenow,
			'status' => 'menunggu'
		);
		$query = $this->db->insert('cuti',$data);
		echo '<script>alert("Berhasil input data cuti, silahkan dikoordinasikan agar cuti anda di approv");history.go(-1) </script>';
			// redirect(''.base_url().'om/app_cuti');
	}

	public function update_cuti()
	{
		if($this->session->userdata('level')=='1')
		{
			$data=array(
				'nama_lengkap'			=> $this->input->post('nama_lengkap'),
				'start_date'			=> $this->input->post('start_date'),
				'end_date'				=> $this->input->post('end_date'),
				'jenis_cuti'				=> $this->input->post('jenis_cuti'),

			);
			$this->db->where('id_cuti',$this->input->post('id_cuti'));
			$query=$this->db->update('cuti',$data);
			if($query)
			{
				redirect(''.base_url().'om/view_cuti');
			}
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function delete_cuti($id_cuti)
	{
		if($this->session->userdata('level')=='1')
		{
			$this->db->where('id_cuti',$id_cuti);
			$query=$this->db->delete('cuti');
			if ($query)
			{	
				redirect(''.base_url().'om/view_cuti');
			}
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

		//tukar dinas
	public function tunas()
	{
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Ajukan Tunas | ABS";
			$data['page'] 	= "Tukar Dinas";
			$this->load->view('home/header',$data);
				//$data['shift'] = $this->om_m->get_shift();
			$data['om'] = $this->om_m->get_profile();
			$data['perner'] = $this->om_m->get_perner();
			$this->load->view('om/add_tunas',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function simpan_tunas()
	{
		$data = array
		(
			'id_om' => $this->session->userdata('id_user'),
			'nama_lengkap'=> $this->input->post('nama'),
			'perner'=> $this->input->post('perner'),
			'tanggal'		=> $this->input->post('tanggal'),
			'jam'		=> $this->input->post('jam'),
			'alasan'	=> $this->input->post('alasan'),
			'status' => 'menunggu'
		);
		$query = $this->db->insert('tunas',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function app_tunas()
	{
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Approval TuNas | ABS";
			$data['page'] 	= "Tukar Dinas";
			$this->load->view('home/header',$data);
			$data['tunas'] = $this->om_m->get_tunas();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/app_tunas',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function delete_tunas($id_tunas)
	{
		if($this->session->userdata('level')=='1')
		{
			$this->db->where('id_tunas',$id_tunas);
			$query=$this->db->delete('tunas');
			if ($query)
			{	
				redirect(''.base_url().'om/view_tunas');
			}
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function jadwal()
	{
		if ($this->session->userdata('level')=='1') 
		{
			$data['title'] 	= "Approval Cuti | ABS";
			$data['page'] 	= "Cuti";
			$this->load->view('home/header',$data);
			$data['jadwal'] = $this->om_m->get_jadwal();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/jadwal',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function add_sakit1()
	{
		if ($this->session->userdata('level')=='1')
		{
			$data['title']  = "Welcome | ABS";
			$data['page']   = "Welcome";
			$data['shift'] = $this->om_m->get_shift();
			$data['absensi'] = $this->om_m->get_sakit();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('home/header',$data);
			$this->load->view('om/add_sakit',$data);
			$this->load->view('home/footer');
		}
		else
		{
			redirect(''.base_url().'login');
		}
	}

	public function add_sakit(){
		
		
		$valid 	= $this->form_validation;

		$valid->set_rules('start_sakit','Start Date','required',
			array(	'required'			=> ' Start Date is required'));


		if($valid->run()){
			$upload_data					= array('uploads'=>$this->upload->data());
			$file_name =   $upload_data['file_name'];
			var_dump($_FILES['upload_surat']['name']);
			if(!empty($_FILES['upload_surat']['name'])){

				$config['upload_path']		='./assets/upload/images/';
				$config['allowed_types']	='gif|jpg|png|jpeg';
			$config['max_size']			=5000; //dalam kilobyte
			$config['max_width']		=5000; //dalam pixel
			$config['max_height']		=5000;
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('upload_surat')) {

			//end validasi


				$data['title']  = "Welcome | ABS";
				$data['page']   = "Welcome";
				$data['shift'] = $this->om_m->get_shift();
			//$data['absensi'] = $this->om_m->get_sakit();
				$data['om'] = $this->om_m->get_profile();
				$this->load->view('home/header',$data);
				$this->load->view('om/add_sakit',$data);
				$this->load->view('home/footer');
		//apabila ga error akan masuk database
			}else{
			//proses manipulasi gambar
				$upload_data					= array('uploads'=>$this->upload->data());
		//gambar asli disimpan di folder assets/upload/image
		//lalu gambar asli di copy untuk versi mini(thumbs) ke folder assets/upload/image/thumbs
				$config['image_library']		= 'gd2';
				$config['source_image']			= './assets/upload/images/'.$upload_data['uploads']['file_name'];
				$config['new_image']			= './assets/upload/images/thumbs/'.$upload_data['uploads']['file_name'];
				$config['create_thumb']   	= TRUE;
				$config['quality']       	= "100%";
				$config['maintain_ratio']   = TRUE;
		$config['width']       		= 360; // Pixel
		$config['height']       	= 360; // Pixel
		$config['x_axis']       	= 0;
		$config['y_axis']       	= 0;
		$config['thumb_marker']   	= '';

		$this->load->library('image_lib',$config);
		$this->image_lib->resize();

		$i = $this->input;
		$data = array(	'id_om'			=> $this->session->userdata('id_user'),
			'nama_lengkap'	=> $this->input->post('nama'),
			'start_sakit'	=> $this->input->post('start_sakit'),
			'end_sakit'		=> $this->input->post('end_sakit'),
			'alasan'		=> $this->input->post('alasan'),
			'status'		=> '1',
			'upload_surat'	=> $upload_data['uploads']['file_name'],
		);
		$query = $this->db->insert('sakit',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
		redirect(base_url('om'),'refresh');
	}}else{
		$i = $this->input;
		$data = array(	'id_om'			=> $this->session->userdata('id_user'),
			'nama_lengkap'	=> $this->input->post('nama'),
			'start_sakit'	=> $this->input->post('start_sakit'),
			'end_sakit'		=> $this->input->post('end_sakit'),
			'alasan'		=> $this->input->post('alasan'),
			'status'		=> '1',
		);
		$query = $this->db->insert('sakit',$data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
		redirect(base_url('om'),'refresh');
	}}
		//end masuk database
	$data['title']  = "Welcome | ABS";
	$data['page']   = "Welcome";
			//$data['shift'] = $this->om_m->get_shift();
	$data['absensi'] = $this->om_m->get_sakit();
	$data['om'] = $this->om_m->get_profile();
	$this->load->view('home/header',$data);
	$this->load->view('om/add_sakit1',$data);
	$this->load->view('home/footer');
}

		// 
//tambah
public function tambah()
{
	if($this->session->userdata('logged_in')==TRUE)
	{
		$config['upload_path'] 		= './uploads/avatar/';
		$config['allowed_types'] 	= 'jpg|png|jpeg';
		$config['max_size'] 		= '5000';
		$config['max_width']  		= '10000';
		$config['max_height']  		= '10000';
		$config['overwrite'] 		= TRUE;
		$this->upload->initialize($config);
		$dataupload = array();
		$user_id  = $this->session->userdata('id_user');
				//$avatar_lama = $this->input->post('avatar_lama');
		$rand = random_string('alnum', 15);
		foreach($_FILES as $field => $file)
		{
			if($file['error'] == 0)
			{
				if ($this->upload->do_upload($field))
				{	
					$data = $dataupload[$field] = $this->upload->data();
							//var_dump($data);

					foreach ($dataupload as $key => $value) {
						if(isset($value['file_name']))
						{
							$data[$key] = $value['file_name'];
							$this->load->library('image_lib');
							$config_crop['image_library'] 	= 'gd2';
							$config_crop['source_image'] 	= './uploads/avatar/'.$data[$key].'';
							$config_crop['maintain_ratio'] 	= FALSE;
							$config_crop['create_thumb'] 	= FALSE;
							$config_crop['new_image'] 		= './uploads/avatar/ava_'.$rand.'.jpg';
							$config_crop['quality'] 		= "60%";
								//Set cropping for y or x axis, depending on image orientation
							if ($value['image_width'] > $value['image_height']) 
							{
								$config_crop['width'] = $value['image_height'];
								$config_crop['height'] = $value['image_height'];
								$config_crop['x_axis'] = (($value['image_width'] / 2) - ($config_crop['width'] / 2));
							}
							else 
							{
								$config_crop['height'] = $value['image_width'];
								$config_crop['width'] = $value['image_width'];
								$config_crop['y_axis'] = (($value['image_height'] / 2) - ($config_crop['height'] / 2));
							}

							$this->image_lib->initialize($config_crop);
							$sukses = $this->image_lib->crop();
							$this->image_lib->clear();
							if($sukses)
							{
								unlink('./uploads/avatar/'.$data[$key].'');
							}
						}
						else
						{
							$data[$key] = "";
						}
					} 
				}
				else
				{
					$this->form_validation->set_message('_file[]', $this->upload->display_errors());
					echo"<script>alert('Format file upload tidak sesuai. Silahkan ulangi kembali.'); window.history.back();</script>";
					return FALSE;
				}
			}
		}
		$data1 = array(	'id_om'			=> $this->session->userdata('id_user'),
			'nama_lengkap'	=> $this->input->post('nama'),
			'start_sakit'	=> $this->input->post('start_sakit'),
			'end_sakit'		=> $this->input->post('end_sakit'),
			'alasan'		=> $this->input->post('alasan'),
			'jenis_sakit'	=> $this->input->post('jenis_sakit'),
			'status'		=> '1',
			'upload_surat'	=> $data['file_name'],
		);
		$query = $this->db->insert('sakit',$data1);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
		redirect(base_url('om'),'refresh');
	}
	else
	{
		redirect(''.base_url().'');
	}	
}

public function correction(){
	$data['title'] 	= "Presence Correction | Presisi";
	$data['page'] 	= "Presence Correction";
	$this->load->view('home/header',$data);
	$data['koreksi'] = $this->rm->get_correction($this->session->userdata('id_user'));
	$this->load->view('request/add_correction',$data);
	$this->load->view('home/footer');
}

}

