<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cek_notif extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Model_karyawan','rm');
	}
	public function index()
	{
		
		$cek=$this->db->query("SELECT nama_lengkap, awal_kontrak FROM
			(SELECT
			no_induk,
			awal_kontrak,
			nama_lengkap,
			akhir_kontrak,
			akhir_kontrak - INTERVAL 7 DAY AS day_reminder,
			CURDATE() AS tanggal_sekarang,
			DATEDIFF(akhir_kontrak, CURDATE()) AS due_date,
			active AS status_asli,
			CASE
			WHEN DATEDIFF(akhir_kontrak, CURDATE()) <= 7 THEN '1' ELSE '0' END AS show_notif 
			FROM tbu_user WHERE active != '0' AND DATEDIFF(akhir_kontrak, CURDATE()) >= 0) a
			WHERE show_notif = 1");
		if ($cek ->num_rows() < 1) 
		{
			echo '<script>alert("Anda Sedang Libur");history.go(-1) </script>';

		}else{
			
			
			$config = [
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				'protocol'  => 'smtp',
				'smtp_host' => 'mail.must.co.id',
				'smtp_user' => 'support@must.co.id', 
				'smtp_pass'   => 'Must*2020#', 
				'smtp_crypto' => 'ssl',
				'smtp_port'   => 465,
				'crlf'    => "\r\n",
				'newline' => "\r\n",
			];
			$this->load->library('email', $config);

			$subject = 'Notifikasi Pengajuan Umar';
			$data['statuskontrak'] = $this->rm->get_kontrak();
			$isi = $this->load->view('karyawan/kontrak',$data,true);
			$this->email->to('umarkoto@gmail.com');
			$this->email->from('umarkoto@must.co.id', 'Uynesba');
			$this->email->cc('umarkoto@gmail.com');
			$this->email->subject($subject);
			$this->email->message($isi);
			$result = $this->email->send();	
			if($result){
				// $this->rm->save_lembur($data);
				$this->session->set_flashdata('sukses', 'Data telah ditambah');
				redirect(base_url('karyawan'),'refresh');
			}else{
				echo '<br />';
				echo $this->email->print_debugger();
			}
			exit;
		}
	}
}
