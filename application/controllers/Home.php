<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model('Model_home');
		$this->load->model('Model_spv');
	}

	public function profile()
	{ //umar
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Profile";
		$this->load->view('home/vheader', $data);
		$data['om'] = $this->Model_home->get_profile();
		$this->load->view('home/vprofile', $data);
		$this->load->view('home/vfooter');
	}

	public function update_profile()
	{ //umar
		$data = array(
			'no_induk' 	   => $this->input->post('no_induk'),
			'nama_lengkap' => $this->input->post('nama'),
			'jabatan'	   => $this->input->post('jabatan'),
			'alamat'	   => $this->input->post('alamat'),
			'agama'	   => $this->input->post('agama'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'tgl_lahir'	   => $this->input->post('tgl_lahir'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'no_telepon'   => $this->input->post('telepon'),
			'email'		   => $this->input->post('email'),
			'status_perkawinan'	   => $this->input->post('status_perkawinan'),
			'no_ktp'	   => $this->input->post('no_ktp'),
			'pendidikan_terakhir'	   => $this->input->post('pendidikan_terakhir'),
			'nama_sesuai_rekening'	   => $this->input->post('nama_sesuai_rekening'),
			'no_npwp'	   => $this->input->post('no_npwp'),
			'no_rek'	   => $this->input->post('no_rek'),
			'no_bpjs_ketenagakerjaan'	   => $this->input->post('no_bpjs_ketenagakerjaan'),
			'bank'	   => $this->input->post('bank'),
			'no_bpjs_kesehatan'	   => $this->input->post('no_bpjs_kesehatan'),
			'jurusan'	   => $this->input->post('jurusan')
		);
		$this->db->where('id', $this->session->userdata('id_user'));
		$query = $this->db->update('tbu_user', $data);
		if ($query) { ?>
			<div class="alert alert-success alert-dismissable">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				Edit Profile <a class="alert-link" href="#">Berhasil.</a>.
			</div>
		<?php }
	}

	public function changeavatar()
	{ //umar
		if ($this->session->userdata('logged_in') == TRUE) {
			$config['upload_path'] 		= './uploads/avatar/';
			$config['allowed_types'] 	= 'jpg|png|jpeg';
			$config['max_size'] 		= '50000';
			$config['max_width']  		= '100000';
			$config['max_height']  		= '100000';
			$config['overwrite'] 		= TRUE;
			$this->upload->initialize($config);
			$dataupload = array();
			$user_id  = $this->session->userdata('id_user');
			$avatar_lama = $this->input->post('avatar_lama');
			$rand = random_string('alnum', 15);
			foreach ($_FILES as $field => $file) {
				if ($file['error'] == 0) {
					if ($this->upload->do_upload($field)) {
						$data = $dataupload[$field] = $this->upload->data();
						foreach ($dataupload as $key => $value) {
							if (isset($value['file_name'])) {
								$data[$key] = $value['file_name'];
								$this->load->library('image_lib');
								$config_crop['image_library'] 	= 'gd2';
								$config_crop['source_image'] 	= './uploads/avatar/' . $data[$key] . '';
								$config_crop['maintain_ratio'] 	= FALSE;
								$config_crop['create_thumb'] 	= FALSE;
								$config_crop['new_image'] 		= './uploads/avatar/ava_' . $rand . '.jpg';
								$config_crop['quality'] 		= "60%";
								if ($value['image_width'] > $value['image_height']) {
									$config_crop['width'] = $value['image_height'];
									$config_crop['height'] = $value['image_height'];
									$config_crop['x_axis'] = (($value['image_width'] / 2) - ($config_crop['width'] / 2));
								} else {
									$config_crop['height'] = $value['image_width'];
									$config_crop['width'] = $value['image_width'];
									$config_crop['y_axis'] = (($value['image_height'] / 2) - ($config_crop['height'] / 2));
								}
								$this->image_lib->initialize($config_crop);
								$sukses = $this->image_lib->crop();
								$this->image_lib->clear();
								if ($sukses) {
									unlink('./uploads/avatar/' . $data[$key] . '');
								}
							} else {
								$data[$key] = "";
							}
						}
					} else {
						$this->form_validation->set_message('_file[]', $this->upload->display_errors());
						echo "<script>alert('Format file upload tidak sesuai. Silahkan ulangi kembali.'); window.history.back();</script>";
						return FALSE;
					}
				}
			}
			$data = array('id' => $user_id);
			foreach ($dataupload as $key => $value) {
				if (isset($value['file_name'])) {
					$data[$key] = 'ava_' . $rand . '.jpg';
				} else {
					$data[$key] = "";
				}
			}
			$this->db->where('id', $user_id);
			$this->db->update('tbu_user', $data);
			unlink('./uploads/avatar/' . $avatar_lama . '');
			redirect('' . base_url() . 'home/profile');
		} else {
			redirect('' . base_url() . '');
		}
	}

	public function update_password()
	{ //umar
		$password_lama = md5($this->input->post('password_lama'));
		$password_baru = $this->input->post('password_baru');
		$repassword_baru = $this->input->post('repassword_baru');
		$id_user  = $this->input->post('id_user');
		$this->form_validation->set_rules('password_lama', 'password lama', 'trim|required');
		$this->form_validation->set_rules('password_baru', 'password baru', 'trim|required');
		$this->form_validation->set_rules('repassword_baru', 'ulangi password baru', 'trim|required|matches[password_baru]');
		if ($this->form_validation->run() == FALSE) { ?>
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo validation_errors(); ?>
			</div>
			<?php
		} else {
			$password = $this->Model_home->get_profile();
			foreach ($password as $rows) {
				$pass_lama_db = $rows->password;
			}
			if ($password_lama != $pass_lama_db) { ?>
				<div class="alert alert-success alert-dismissable">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					password lama <a class="alert-link" href="#">salah.</a>.
				</div>
			<?php
			} else {
				$data = array(
					'password' => md5($this->input->post('password_baru')),
				);
				$this->db->where('id', $this->input->post('id_user'));
				$query = $this->db->update('tbu_user', $data);
			?>
				<div class="alert alert-success alert-dismissable">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					Ganti password <a class="alert-link" href="#">Berhasil.</a>.
				</div>
<?php
			}
		}
	}

	public function index()
	{
		$data['datatelat'] = $this->Model_home->get_telat();
		$data['notifcuti'] = $this->Model_home->get_cuti();
		$data['member'] = $this->Model_home->getAllMember();
		$data['jmlcuti'] = $this->Model_home->getJumlahCuti();
		$data['jmltunas'] = $this->Model_home->getJumlahTunas();
		$data['jmllembur'] = $this->Model_home->getJumlahLembur();
		$data['title'] 	= "Welcome | Presisi";
		$data['page'] 	= "Welcome";
		$this->load->view('home/header', $data);
		$this->load->view('home/body', $data);
		$this->load->view('home/footer');
	}

	public function cek($id)
	{
		$query2 = $this->db->query("SELECT * FROM cuti WHERE id = '" . $id . "' AND status = '3'");
		$row2 = $query2->row();
		if (isset($row2)) {
			if ($row2->jenis_leave == '1' || $row2->jenis_leave == '3' || $row2->jenis_leave == '4') {
				for ($i = 0; $i < $row2->jumlah_leave; $i++) {
					$tanggal = date("Y-m-d", strtotime("+" . $i . " days", strtotime($row2->start_date)));
					$this->db->query("UPDATE `schedule` SET id_shifting = 'C' WHERE user_id = '" . $row2->id_om . "' AND tanggal = '" . $tanggal . "'");
				}
			} else if ($row2->jenis_leave == '5' || $row2->jenis_leave == '3' || $row2->jenis_leave == '7') {
				for ($i = 0; $i < $row2->jumlah_leave; $i++) {
					$tanggal = date("Y-m-d", strtotime("+" . $i . " days", strtotime($row2->start_date)));
					$this->db->query("UPDATE `schedule` SET id_shifting = 'SA' WHERE user_id = '" . $row2->id_om . "' AND tanggal = '" . $row2->start_date . "'");
				}
			}
		}
	}

	public function checkin()
	{
		if ($this->session->userdata('id_user') != NULL) {
			$kode = $this->input->post('kode');
			if ($this->agent->is_browser()) {
				$agent = $this->agent->browser() . ' ' . $this->agent->version();
			} elseif ($this->agent->is_mobile()) {
				$agent = $this->agent->mobile();
			} else {
				$agent = 'Data user gagal di dapatkan';
			}
			$browser = "" . $agent . "";
			$os = "" . $this->agent->platform();
			$ip = "" . $this->input->ip_address();
			$kode_abs = 1;
			$id_om     = $this->session->userdata('id_user');
			$cek = $this->Model_home->absen_in($id_om, $browser, $os, $ip, $kode);
			echo json_encode($cek);
		} else {
			$array = array('errSess' => '1');
			echo json_encode($array);
		}
	}


	//new

	//closenew
	//--------------------------------------------------------------------------------------
	//presensi
	public function presensi()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title']  = "Welcome | ABS";
			$data['page']   = "Welcome";
			$this->load->view('home/header', $data);
			$this->load->view('om/add_presensi', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function view_presensi()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title']  = "Welcome | ABS";
			$data['page']   = "Welcome";
			$data['absensi'] = $this->om_m->get_absensi();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('home/header', $data);
			$this->load->view('om/view_presensi', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}





	//lembur
	public function lembur()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Ajukan Lembur | ABS";
			$data['page'] 	= "Lembur";
			$this->load->view('home/header', $data);
			$data['lembur'] = $this->om_m->get_lembur();
			$data['om'] = $this->om_m->get_profile();
			$data['layanan'] = $this->om_m->get_layanan();
			$this->load->view('om/add_lembur', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function view_lembur()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Ajukan Lembur | ABS";
			$data['page'] 	= "Lembur";
			$this->load->view('home/header', $data);
			$data['lembur'] = $this->om_m->get_lemburrep();
			$data['om'] = $this->om_m->get_profile();
			$data['layanan'] = $this->om_m->get_layanan();
			$this->load->view('om/view_lembur', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function view_sakit()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Ajukan Lembur | ABS";
			$data['page'] 	= "Lembur";
			$this->load->view('home/header', $data);
			$data['sakit'] = $this->om_m->get_sakit();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/view_sakit', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function app_lembur()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Approval Lembur | ABS";
			$data['page'] 	= "Lembur";
			$this->load->view('home/header', $data);
			$data['lembur'] = $this->om_m->get_sakit();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/app_lembur', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function cetaklembur($id)
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Welcome | ABS";
			$data['page'] 	= "Daftar Permohonan";
			$data['cuti'] = $this->om_m->get_cutiId($id);
			$data['tot'] = $this->om_m->get_cutitot();
			$data['sisa'] = $this->om_m->get_cutisisa();
			$this->load->view('om/printcuti', $data);
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function simpan_lem()
	{
		$data = array(
			'id_om' => $this->session->userdata('id_user'),
			'nama_lengkap' => $this->input->post('nama'),
			'id_layanan'	=> $this->input->post('id_layanan'),
			'alasan'	=> $this->input->post('alasan'),
			'start_time'	=> $this->input->post('start_time'),
			'end_time'	=> $this->input->post('end_time'),
			'tanggal'		=> date('Y-m-d H:i:s'),
			'status' => 'menunggu'
		);
		$query = $this->db->insert('lembur', $data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function delete_lembur($id_lembur)
	{
		if ($this->session->userdata('level') == '1') {
			$this->db->where('id_lembur', $id_lembur);
			$query = $this->db->delete('lembur');
			if ($query) {
				redirect('' . base_url() . 'om/view_lembur');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}


	//cuti
	public function cuti()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Ajukan Cuti | ABS";
			$data['page'] 	= "Cuti";
			$this->load->view('home/header', $data);
			$data['cuti'] = $this->om_m->get_cuti();
			$data['om'] = $this->om_m->get_profile();
			$data['jeniscuti'] = $this->om_m->get_jeniscuti();
			$data['sisa'] = $this->om_m->get_cutisisa();
			$this->load->view('om/add_cuti', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function edit_cuti()
	{
		if ($this->session->userdata('level') == "om") {
			$data['title'] = "Edit Cuti | ABS";
			$data['page'] = "Edit Cuti";
			$this->load->view('home/header', $data);
			$data['cuti'] = $this->om_m->get_cuti();
			$this->load->view('om/edit_cuti', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . '');
		}
	}

	public function view_cuti()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Form Cuti | ABS";
			$data['page'] 	= "Cuti";
			$this->load->view('home/header', $data);
			$data['cuti'] = $this->om_m->get_cutirep();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/view_cuti', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function app_cuti()
	{
		if ($this->session->userdata('level') == 'operator') {
			$data['title'] 	= "Approval Cuti | ABS";
			$data['page'] 	= "Cuti";
			$this->load->view('home/header', $data);
			$data['cuti'] = $this->om_m->get_cuti();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/app_cuti', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function cetakcuti($id)
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Welcome | ABS";
			$data['page'] 	= "Daftar Permohonan";
			$data['cuti'] = $this->om_m->get_cutiId($id);
			$data['tot'] = $this->om_m->get_cutitot();
			$data['sisa'] = $this->om_m->get_cutisisa();
			$this->load->view('om/print_cuti', $data);
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function simpan_cuti()
	{
		$datenow = date("Y-m-d");
		$data = array(
			'id_om' => $this->session->userdata('id_user'),
			'nama_lengkap' => $this->input->post('nama'),
			'start_date'		=> $this->input->post('start_date'),
			'end_date'		=> $this->input->post('end_date'),
			'jenis_cuti'	=> $this->input->post('jenis_cuti'),
			'jml_cuti'	=> $this->input->post('jml_cuti'),
			'alamat_cuti'	=> $this->input->post('alamat_cuti'),
			'alasan'	=> $this->input->post('alasan'),
			'tanggal'   => $datenow,
			'status' => 'menunggu'
		);
		$query = $this->db->insert('cuti', $data);
		echo '<script>alert("Berhasil input data cuti, silahkan dikoordinasikan agar cuti anda di approv");history.go(-1) </script>';
		// redirect(''.base_url().'om/app_cuti');
	}

	public function update_cuti()
	{
		if ($this->session->userdata('level') == '1') {
			$data = array(
				'nama_lengkap'			=> $this->input->post('nama_lengkap'),
				'start_date'			=> $this->input->post('start_date'),
				'end_date'				=> $this->input->post('end_date'),
				'jenis_cuti'				=> $this->input->post('jenis_cuti'),

			);
			$this->db->where('id_cuti', $this->input->post('id_cuti'));
			$query = $this->db->update('cuti', $data);
			if ($query) {
				redirect('' . base_url() . 'om/view_cuti');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function delete_cuti($id_cuti)
	{
		if ($this->session->userdata('level') == '1') {
			$this->db->where('id_cuti', $id_cuti);
			$query = $this->db->delete('cuti');
			if ($query) {
				redirect('' . base_url() . 'om/view_cuti');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	//tukar dinas
	public function tunas()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Ajukan Tunas | ABS";
			$data['page'] 	= "Tukar Dinas";
			$this->load->view('home/header', $data);
			//$data['shift'] = $this->om_m->get_shift();
			$data['om'] = $this->om_m->get_profile();
			$data['perner'] = $this->om_m->get_perner();
			$this->load->view('om/add_tunas', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function simpan_tunas()
	{
		$data = array(
			'id_om' => $this->session->userdata('id_user'),
			'nama_lengkap' => $this->input->post('nama'),
			'perner' => $this->input->post('perner'),
			'tanggal'		=> $this->input->post('tanggal'),
			'jam'		=> $this->input->post('jam'),
			'alasan'	=> $this->input->post('alasan'),
			'status' => 'menunggu'
		);
		$query = $this->db->insert('tunas', $data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function app_tunas()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Approval TuNas | ABS";
			$data['page'] 	= "Tukar Dinas";
			$this->load->view('home/header', $data);
			$data['tunas'] = $this->om_m->get_tunas();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/app_tunas', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function delete_tunas($id_tunas)
	{
		if ($this->session->userdata('level') == '1') {
			$this->db->where('id_tunas', $id_tunas);
			$query = $this->db->delete('tunas');
			if ($query) {
				redirect('' . base_url() . 'om/view_tunas');
			}
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function jadwal()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title'] 	= "Approval Cuti | ABS";
			$data['page'] 	= "Cuti";
			$this->load->view('home/header', $data);
			$data['jadwal'] = $this->om_m->get_jadwal();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('om/jadwal', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function add_sakit1()
	{
		if ($this->session->userdata('level') == '1') {
			$data['title']  = "Welcome | ABS";
			$data['page']   = "Welcome";
			$data['shift'] = $this->om_m->get_shift();
			$data['absensi'] = $this->om_m->get_sakit();
			$data['om'] = $this->om_m->get_profile();
			$this->load->view('home/header', $data);
			$this->load->view('om/add_sakit', $data);
			$this->load->view('home/footer');
		} else {
			redirect('' . base_url() . 'login');
		}
	}

	public function add_sakit()
	{


		$valid 	= $this->form_validation;

		$valid->set_rules(
			'start_sakit',
			'Start Date',
			'required',
			array('required'			=> ' Start Date is required')
		);


		if ($valid->run()) {
			$upload_data					= array('uploads' => $this->upload->data());
			$file_name =   $upload_data['file_name'];
			var_dump($_FILES['upload_surat']['name']);
			if (!empty($_FILES['upload_surat']['name'])) {

				$config['upload_path']		= './assets/upload/images/';
				$config['allowed_types']	= 'gif|jpg|png|jpeg';
				$config['max_size']			= 5000; //dalam kilobyte
				$config['max_width']		= 5000; //dalam pixel
				$config['max_height']		= 5000;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('upload_surat')) {

					//end validasi


					$data['title']  = "Welcome | ABS";
					$data['page']   = "Welcome";
					$data['shift'] = $this->om_m->get_shift();
					//$data['absensi'] = $this->om_m->get_sakit();
					$data['om'] = $this->om_m->get_profile();
					$this->load->view('home/header', $data);
					$this->load->view('om/add_sakit', $data);
					$this->load->view('home/footer');
					//apabila ga error akan masuk database
				} else {
					//proses manipulasi gambar
					$upload_data					= array('uploads' => $this->upload->data());
					//gambar asli disimpan di folder assets/upload/image
					//lalu gambar asli di copy untuk versi mini(thumbs) ke folder assets/upload/image/thumbs
					$config['image_library']		= 'gd2';
					$config['source_image']			= './assets/upload/images/' . $upload_data['uploads']['file_name'];
					$config['new_image']			= './assets/upload/images/thumbs/' . $upload_data['uploads']['file_name'];
					$config['create_thumb']   	= TRUE;
					$config['quality']       	= "100%";
					$config['maintain_ratio']   = TRUE;
					$config['width']       		= 360; // Pixel
					$config['height']       	= 360; // Pixel
					$config['x_axis']       	= 0;
					$config['y_axis']       	= 0;
					$config['thumb_marker']   	= '';

					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$i = $this->input;
					$data = array(
						'id_om'			=> $this->session->userdata('id_user'),
						'nama_lengkap'	=> $this->input->post('nama'),
						'start_sakit'	=> $this->input->post('start_sakit'),
						'end_sakit'		=> $this->input->post('end_sakit'),
						'alasan'		=> $this->input->post('alasan'),
						'status'		=> '1',
						'upload_surat'	=> $upload_data['uploads']['file_name'],
					);
					$query = $this->db->insert('sakit', $data);
					echo '<script>alert("Berhasil input data");history.go(-1) </script>';
					redirect(base_url('om'), 'refresh');
				}
			} else {
				$i = $this->input;
				$data = array(
					'id_om'			=> $this->session->userdata('id_user'),
					'nama_lengkap'	=> $this->input->post('nama'),
					'start_sakit'	=> $this->input->post('start_sakit'),
					'end_sakit'		=> $this->input->post('end_sakit'),
					'alasan'		=> $this->input->post('alasan'),
					'status'		=> '1',
				);
				$query = $this->db->insert('sakit', $data);
				echo '<script>alert("Berhasil input data");history.go(-1) </script>';
				redirect(base_url('om'), 'refresh');
			}
		}
		//end masuk database
		$data['title']  = "Welcome | ABS";
		$data['page']   = "Welcome";
		//$data['shift'] = $this->om_m->get_shift();
		$data['absensi'] = $this->om_m->get_sakit();
		$data['om'] = $this->om_m->get_profile();
		$this->load->view('home/header', $data);
		$this->load->view('om/add_sakit1', $data);
		$this->load->view('home/footer');
	}

	// 
	//tambah
	public function tambah()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			$config['upload_path'] 		= './uploads/avatar/';
			$config['allowed_types'] 	= 'jpg|png|jpeg';
			$config['max_size'] 		= '5000';
			$config['max_width']  		= '10000';
			$config['max_height']  		= '10000';
			$config['overwrite'] 		= TRUE;
			$this->upload->initialize($config);
			$dataupload = array();
			$user_id  = $this->session->userdata('id_user');
			//$avatar_lama = $this->input->post('avatar_lama');
			$rand = random_string('alnum', 15);
			foreach ($_FILES as $field => $file) {
				if ($file['error'] == 0) {
					if ($this->upload->do_upload($field)) {
						$data = $dataupload[$field] = $this->upload->data();
						//var_dump($data);

						foreach ($dataupload as $key => $value) {
							if (isset($value['file_name'])) {
								$data[$key] = $value['file_name'];
								$this->load->library('image_lib');
								$config_crop['image_library'] 	= 'gd2';
								$config_crop['source_image'] 	= './uploads/avatar/' . $data[$key] . '';
								$config_crop['maintain_ratio'] 	= FALSE;
								$config_crop['create_thumb'] 	= FALSE;
								$config_crop['new_image'] 		= './uploads/avatar/ava_' . $rand . '.jpg';
								$config_crop['quality'] 		= "60%";
								//Set cropping for y or x axis, depending on image orientation
								if ($value['image_width'] > $value['image_height']) {
									$config_crop['width'] = $value['image_height'];
									$config_crop['height'] = $value['image_height'];
									$config_crop['x_axis'] = (($value['image_width'] / 2) - ($config_crop['width'] / 2));
								} else {
									$config_crop['height'] = $value['image_width'];
									$config_crop['width'] = $value['image_width'];
									$config_crop['y_axis'] = (($value['image_height'] / 2) - ($config_crop['height'] / 2));
								}

								$this->image_lib->initialize($config_crop);
								$sukses = $this->image_lib->crop();
								$this->image_lib->clear();
								if ($sukses) {
									unlink('./uploads/avatar/' . $data[$key] . '');
								}
							} else {
								$data[$key] = "";
							}
						}
					} else {
						$this->form_validation->set_message('_file[]', $this->upload->display_errors());
						echo "<script>alert('Format file upload tidak sesuai. Silahkan ulangi kembali.'); window.history.back();</script>";
						return FALSE;
					}
				}
			}
			$data1 = array(
				'id_om'			=> $this->session->userdata('id_user'),
				'nama_lengkap'	=> $this->input->post('nama'),
				'start_sakit'	=> $this->input->post('start_sakit'),
				'end_sakit'		=> $this->input->post('end_sakit'),
				'alasan'		=> $this->input->post('alasan'),
				'jenis_sakit'	=> $this->input->post('jenis_sakit'),
				'status'		=> '1',
				'upload_surat'	=> $data['file_name'],
			);
			$query = $this->db->insert('sakit', $data1);
			echo '<script>alert("Berhasil input data");history.go(-1) </script>';
			redirect(base_url('om'), 'refresh');
		} else {
			redirect('' . base_url() . '');
		}
	}
}
