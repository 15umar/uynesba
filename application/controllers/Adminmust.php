<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adminmust extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model('Model_superadmin');
		$this->load->model('Model_adminmust');
		$this->load->model('Model_spv');
	}

	//new
	public function index()
	{ //beranda
		$data['datatelat'] = $this->Model_superadmin->get_telat();
		$data['member'] = $this->Model_superadmin->getAllMember();
		$data['memberdas'] = $this->Model_superadmin->getdasMember();
		$data['jmlcuti'] = $this->Model_superadmin->getJumlahCuti();
		$data['jmlcutiapp'] = $this->Model_superadmin->getAppCuti();
		$data['jmlsakit'] = $this->Model_superadmin->getJumlahSakit();
		$data['jmlsakitapp'] = $this->Model_superadmin->getAppSakit();
		$data['jmllembur'] = $this->Model_superadmin->getJumlahLembur();
		$data['jmllemburapp'] = $this->Model_superadmin->getAppLembur();
		$data['absen'] = $this->Model_superadmin->get_absen();
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Selamat Datang";
		$this->load->view('home/vheader', $data);
		$this->load->view('superadmin/vberanda', $data);
		$this->load->view('home/vfooter');
	}

	public function tukar()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Tukar Jadwal";
		$user = $this->session->userdata('id_user');
		$data['data_tunas'] = $this->Model_adminmust->approval_official_exchange($user);
		$this->load->view('home/vheader', $data);
		$this->load->view('superadmin/vadd_tukar', $data);
		$this->load->view('home/vfooter');
	}

	public function karyawan()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Karyawan";
		$data['karyawan'] = $this->Model_adminmust->karyawan();
		$data['level'] = $this->Model_superadmin->level();
		$data['organisasi'] = $this->Model_superadmin->client();
		$this->load->view('home/vheader', $data);
		$this->load->view('superadmin/karyawan', $data);
		$this->load->view('home/vfooter');
	}

	public function add_karyawan()
	{ //umar
		if ($this->session->userdata('level') == '4') {
			$data['title'] 	= "Uynesba";
			$data['page'] 	= "Data Karyawan";
			$data['data_karyawan'] = $this->Model_adminmust->getmember();
			$this->load->view('home/vheader', $data);
			$this->load->view('adminmust/add_karyawan', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function harian()
	{ //umar
		$data['datatelat'] = $this->Model_adminmust->get_telat2();
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Selamat Datang";
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vharian', $data);
		$this->load->view('home/vfooter');
	}

	public function pagu()
	{ //umar
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Pagu";
		$data['pagu'] = $this->Model_adminmust->pagu();
		$data['bidang'] = $this->Model_adminmust->bidang();
		$this->load->view('home/vheader', $data);
		$this->load->view('adminmust/pagu', $data);
		$this->load->view('home/vfooter');
	}



	public function simpan_pagu()
	{
		$data = array(
			'id_organisasi' => $this->input->post('id_organisasi'),
			'biaya_jasa' => $this->input->post('biaya_jasa'),
			'biaya_non_po' => $this->input->post('biaya_non_po'),
			'awal_kontrak' => $this->input->post('awal_kontrak'),
			'akhir_kontrak' => $this->input->post('akhir_kontrak')
		);
		$query = $this->db->insert('tbu_pagu', $data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function jasa()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Bauk Jasa";
		$data['jasa'] = $this->Model_adminmust->jasa();
		$data['user'] = $this->Model_adminmust->user();
		// $data['bidang'] = $this->Model_adminmust->bidang();
		$this->load->view('home/vheader', $data);
		$this->load->view('adminmust/jasa', $data);
		$this->load->view('home/vfooter');
	}

	public function simpan_jasa()
	{
		$data = array(
			'id_user' => $this->input->post('id_user'),
			'qty_po' => $this->input->post('qty_po'),
			'qty_real' => $this->input->post('qty_real'),
			'hk_po' => $this->input->post('hk_po'),
			'hk_real' => $this->input->post('hk_real'),
			'harga_satuan' => $this->input->post('harga_satuan'),
			'jumlah_harga' => $this->input->post('jumlah_harga')
		);
		$query = $this->db->insert('tbu_jasa', $data);
		echo '<script>alert("Berhasil input data");history.go(-1) </script>';
	}

	public function nonpo()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "BIaya Non PO";
		$data['nonpo'] = $this->Model_adminmust->nonpo();
		// $data['data_cuti'] = $this->Model_adminmust->rekap();
		// $data['bidang'] = $this->Model_adminmust->bidang();
		$this->load->view('home/vheader', $data);
		$this->load->view('adminmust/nonpo', $data);
		$this->load->view('home/vfooter');
	}

	public function bulanan()
	{
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Bulanan";
		$this->load->view('home/vheader', $data);
		$data['koreksi'] = $this->Model_adminmust->get_correction();
		$this->load->view('adminmust/vbulanan', $data);
		$this->load->view('home/vfooter');
	}

	public function pencarianbulanan()
	{
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Absensi";
		$tglawal = $this->input->get('tglawal');
		$tglakhir = $this->input->get('tglakhir');
		$data['koreksi'] = $this->Model_adminmust->pencarianbulanan($tglawal, $tglakhir);
		$this->load->view('home/vheader', $data);
		$this->load->view("adminmust/vbulanan", $data);
		$this->load->view('home/vfooter');
	}

	public function slip()
	{ //import_karyawan	
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Import Karyawan";
		$this->load->view('home/vheader', $data);
		$this->load->view('adminmust/import_slip');
		$this->load->view('home/vfooter');
	}

	public function createdata()
	{ //simpanimport
		$config['upload_path'] 	 = './upload/excel';
		$config['allowed_types'] = 'xls';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (!$this->upload->do_upload()) {
			$data = array('error' => $this->upload->display_errors());
		} else {
			$data = array('error' => false);
			$upload_data = $this->upload->data();
			$this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');
			$file =  $upload_data['full_path'];
			$this->excel_reader->read($file);
			error_reporting(E_ALL ^ E_NOTICE);
			$data = $this->excel_reader->sheets[0];
			$dataexcel = array();
			for ($i = 2; $i <= $data['numRows']; $i++) {
				if ($data['cells'][$i][1] == '') break;
				$dataexcel[$i - 2]['nik']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][1]);
				$dataexcel[$i - 2]['bulan']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][2]);
				$dataexcel[$i - 2]['tahun']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][3]);
				$dataexcel[$i - 2]['gapok']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][4]);
				$dataexcel[$i - 2]['tunjangan']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][5]);
				$dataexcel[$i - 2]['u_shift']			= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][6]);
				$dataexcel[$i - 2]['u_lembur']			= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][7]);
				$dataexcel[$i - 2]['thr']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][8]);
				$dataexcel[$i - 2]['u_cuti']		= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][9]);
				$dataexcel[$i - 2]['reward']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][10]);
				$dataexcel[$i - 2]['koreksi']			= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][11]);
				$dataexcel[$i - 2]['total1']			= (is_null($data['cells'][$i][5]) ? 0 : $data['cells'][$i][12]);
				$dataexcel[$i - 2]['bpjs_tk']	= (is_null($data['cells'][$i][5]) ? 0 : $data['cells'][$i][13]);
				$dataexcel[$i - 2]['bpjs_kes']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][14]);
				$dataexcel[$i - 2]['jp']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][15]);
				$dataexcel[$i - 2]['pph21']				= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][16]);
				$dataexcel[$i - 2]['total2']					= (is_null($data['cells'][$i][1]) ? 0 : $data['cells'][$i][17]);
			}
			delete_files($upload_data['file_path']);
			$this->Model_adminmust->tambahcontact($dataexcel);
		}
		redirect('' . base_url() . 'adminmust/slip');
	}

	public function cuti()
	{ //umar
		if ($this->session->userdata('level') == '4') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Cuti";
			$data['data_cuti'] = $this->Model_adminmust->approval_leave();
			$this->load->view('home/vheader', $data);
			$this->load->view('superadmin/vcuti', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function izin()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Izin";
		$data['data_report'] = $this->Model_adminmust->report_izin();
		$this->load->view('home/vheader', $data);
		$this->load->view('superadmin/v_izin', $data);
		$this->load->view('home/vfooter');
	}

	public function sakit()
	{ //umar
		if ($this->session->userdata('level') == '4') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Sakit";
			$data['data_cuti'] = $this->Model_adminmust->approval_sakit();
			$this->load->view('home/vheader', $data);
			$this->load->view('superadmin/vsakit', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function rekap()
	{ //umar
		if ($this->session->userdata('level') == '4') {
			$data['title'] 	= "Uynesba";
			$data['page'] 	= "Rekap";
			$data['data_cuti'] = $this->Model_adminmust->rekap();
			$this->load->view('home/vheader', $data);
			$this->load->view('adminmust/vrekap', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function export_excel()
	{ //umar
		if ($this->session->userdata('level') == '4') {
			$data['title'] 	= "Uynesba";
			$data['page'] 	= "Rekap";
			$data['data_cuti'] = $this->Model_adminmust->rekap();
			// $this->load->view('home/vheader',$data);
			$this->load->view('adminmust/vdownloadrekap', $data);
			// $this->load->view('home/vfooter');	
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function lembur()
	{ //umar
		if ($this->session->userdata('level') == '4') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Approv Pengajuan Lembur";
			$data['data_lembur'] = $this->Model_adminmust->approval_overtime();
			$this->load->view('home/vheader', $data);
			$this->load->view('superadmin/vlembur', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function perjalanan_dinas()
	{ //umar
		if ($this->session->userdata('level') == '4' or '1') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Approv Pengajuan Perjalanan Dinas";
			$data['data_perjalanan_dinas'] = $this->Model_adminmust->approval_perjalanan_dinas();
			$this->load->view('home/vheader', $data);
			$this->load->view('superadmin/vperjalanan_dinas', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}
}
