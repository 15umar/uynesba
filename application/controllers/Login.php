<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_login');
	}

	public function index()
	{
		$this->load->view('home/vlogin');
	}

	public function auth()
	{ //umarkoto
		$username = $this->input->post("username");
		$password = md5($this->input->post("password"));
		$query = $this->Model_login->check_login($username, $password);
		if ($query and $this->session->userdata('level') == "1") { ?>
			<script>
				window.location.href = "<?php echo base_url(); ?>superadmin"
			</script>
		<?php } elseif ($query and $this->session->userdata('level') == '2') { ?>
			<script>
				window.location.href = "<?php echo base_url(); ?>spv"
			</script>
		<?php } elseif ($query and $this->session->userdata('level') == '3') { ?>
			<script>
				window.location.href = "<?php echo base_url(); ?>karyawan"
			</script>
		<?php } elseif ($query and $this->session->userdata('level') == '4') { ?>
			<script>
				window.location.href = "<?php echo base_url(); ?>adminmust"
			</script>
		<?php } elseif ($query and $this->session->userdata('level') == '5') { ?>
			<script>
				window.location.href = "<?php echo base_url(); ?>sdm"
			</script>
		<?php } elseif ($query and $this->session->userdata('level') == '6') { ?>
			<script>
				window.location.href = "<?php echo base_url(); ?>manager"
			</script>
		<?php } else { ?>
			<div class="alert alert-danger alert-dismissable">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				<script>
					alert('Gagal login: Cek username, password!');
					history.go(-1);
				</script>
			</div>
<?php }
	}
}
