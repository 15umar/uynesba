<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reminder_sppd extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Model_karyawan','rm');
	}
	public function index()
	{
		
		$cek=$this->db->query("SELECT b.nama_lengkap, a.`tanggal_pergi` AS umar,id_user, due_date FROM 
			(SELECT id_user,`status`, `tanggal_pergi`, CURDATE() AS tanggal_sekarang, DATEDIFF(`tanggal_pergi`, CURDATE()) AS due_date,
			CASE WHEN DATEDIFF(`tanggal_pergi`, CURDATE()) <= -7 THEN '1' ELSE '0' END AS show_notif
			FROM `tbu_perjalanandinas` WHERE `status` = '1' OR `status`='2' AND DATEDIFF(`tanggal_pergi`, CURDATE()) <= 0) a
			LEFT JOIN `tbu_user` b ON b.id=a.id_user
			WHERE show_notif = 1");
		if ($cek ->num_rows() < 1) 
		{
			echo '<script>alert("Anda Sedang Libur");history.go(-1) </script>';

		}else{
			
			
			$config = [
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				'protocol'  => 'smtp',
				'smtp_host' => 'mail.must.co.id',
				'smtp_user' => 'support@must.co.id', 
				'smtp_pass'   => 'Must*2020#', 
				'smtp_crypto' => 'ssl',
				'smtp_port'   => 465,
				'crlf'    => "\r\n",
				'newline' => "\r\n",
			];
			$this->load->library('email', $config);

			$subject = 'Reminder Pengajuan Perjalanan Dinas';
			$data['reminder_sppd'] = $this->rm->get_remindersppd();
			$isi = $this->load->view('karyawan/reminder_sppd',$data,true);
			$this->email->to('umarkoto@gmail.com');
			$this->email->from('umarkoto@must.co.id', 'Uynesba');
			$this->email->cc('umarkoto@gmail.com');
			$this->email->subject($subject);
			$this->email->message($isi);
			$result = $this->email->send();	
			if($result){
				// $this->rm->save_lembur($data);
				$this->session->set_flashdata('sukses', 'Data telah ditambah');
				redirect(base_url('karyawan'),'refresh');
			}else{
				echo '<br />';
				echo $this->email->print_debugger();
			}
			exit;
		}
	}
}
