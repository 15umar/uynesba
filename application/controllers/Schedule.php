<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('model_schedule');
		$this->load->model('Model_spv');
	}
	
	public function index(){ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Jadwal Kerja";
		$this->load->view('home/vheader',$data);
		$this->load->view('schedule/show');
		$this->load->view('home/vfooter');
	}
	
	public function create(){
		$data['title'] 	= "Schedule | ABS";
		$data['page'] 	= "Schedule";
		$this->load->view('home/vheader',$data);
		$this->load->view('schedule/create');
		$this->load->view('home/vfooter');
	}
	
	public function show_schedule(){
		$date    =$this->input->post('start').'|'.$this->input->post('end');
		$results =$this->model_schedule->show_schedule($date);
		
		$sdate   =((substr($this->input->post('start'),0,7)==substr($this->input->post('end'),0,7))?'1':'0');
		$data = array(
				'range' => $this->date_range($this->input->post('start'),$this->input->post('end')),
				'date'	=> $date,
				'sdate'	=> $sdate,
				'schedule'	=> json_encode($results)
			);
		$this->load->view('schedule/schedule',$data);
		// $this->load->view('home/vfooter');
	}
	
	public function get_employee(){
		$results=$this->model_schedule->get_employee();
		
		echo json_encode(array("person" => $results));
	}
	
	public function get_pattern(){
	$data = array(
				'input' => $this->input->post('input'),
				'emp' 	=> $this->input->post('emp'),
				'range' => $this->date_range($this->input->post('start'),$this->input->post('end')),
				'date'	=> $this->input->post('start').'|'.$this->input->post('end')
			);
	$this->load->view('schedule/pattern',$data);
	}
	
	public function save(){
	$emp   = $this->input->post('emp');	
	$input = $this->input->post('input');
	$date  = $this->input->post('date');
	
	$results=$this->model_schedule->save_schedule($emp,$date,$input);
	
	echo $results;
	}
	
	private function date_range($first, $last, $step = '+1 day', $output_format = 'Y/m/d' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
	}

}

/* End of file Schedule.php */
/* Location: ./application/controllers/Schedule.php */
// gugun