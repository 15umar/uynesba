<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function index(){ //umar
		$data2=array(
			'id_user' 		=> "",
			'nama' 			=> "",
			'email' 		=> "",
			'alamat' 		=> "",
			'no_telepon' 	=> "",
			'level' 		=> "",
			'foto' 			=> "",
			'logged_in'		=> FALSE
		);
		$this->session->sess_destroy();
		redirect("".base_url()."");
	}
}