<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sdm extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model('Model_superadmin');
		$this->load->model('Model_adminmust');
		$this->load->model('Model_sdm');
		$this->load->model('Model_spv');
		$this->load->model('model_schedule');
	}

	//new

	public function index()
	{ //beranda
		$data['datatelat'] = $this->Model_superadmin->get_telat();
		$data['member'] = $this->Model_superadmin->getAllMember();
		$data['memberdas'] = $this->Model_superadmin->getdasMember();
		$data['jmlcuti'] = $this->Model_superadmin->getJumlahCuti();
		$data['jmlcutiapp'] = $this->Model_superadmin->getAppCuti();
		$data['jmlsakit'] = $this->Model_superadmin->getJumlahSakit();
		$data['jmlsakitapp'] = $this->Model_superadmin->getAppSakit();
		$data['jmllembur'] = $this->Model_superadmin->getJumlahLembur();
		$data['jmllemburapp'] = $this->Model_superadmin->getAppLembur();

		$data['tukarjadwal'] = $this->Model_superadmin->gettukarjadwal();
		$data['absen'] = $this->Model_superadmin->get_absen();
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Selamat Datang";
		$this->load->view('home/vheader', $data);
		$this->load->view('sdm/vberanda', $data);
		$this->load->view('home/vfooter');
	}

	public function pencarianbauk()
	{
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Bauk Jasa";
		$tglawal = $this->input->get('tglawal');
		$tglakhir = $this->input->get('tglakhir');
		// $tanggal2=$this->input->get('tanggal');
		// $end_date=$this->input->get('end_date');
		// $data['aplikasi'] = $this->Model_user->get_aplikasi();
		$data['nonpo'] = $this->Model_sdm->pencarian_d($tglawal, $tglakhir);
		$this->load->view('home/vheader', $data);
		$this->load->view("sdm/non_po", $data);
		$this->load->view('home/vfooter');
	}

	public function pencarianpremi()
	{
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Bauk Jasa";
		$tglawal = $this->input->get('tglawal');
		$tglakhir = $this->input->get('tglakhir');
		// $tanggal2=$this->input->get('tanggal');
		// $end_date=$this->input->get('end_date');
		// $data['aplikasi'] = $this->Model_user->get_aplikasi();
		$data['nonpo'] = $this->Model_adminmust->pencarianpremi($tglawal, $tglakhir);
		$this->load->view('home/vheader', $data);
		$this->load->view("sdm/premi", $data);
		$this->load->view('home/vfooter');
	}

	public function pencarianlembur()
	{
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Bauk Jasa";
		$tglawal = $this->input->get('tglawal');
		$tglakhir = $this->input->get('tglakhir');
		// $tanggal2=$this->input->get('tanggal');
		// $end_date=$this->input->get('end_date');
		// $data['aplikasi'] = $this->Model_user->get_aplikasi();
		$data['nonpo'] = $this->Model_adminmust->pencarianlembur($tglawal, $tglakhir);
		$this->load->view('home/vheader', $data);
		$this->load->view("sdm/bauklembur", $data);
		$this->load->view('home/vfooter');
	}

	public function pencarianjasa()
	{

		// $data['jasa'] = $this->Model_adminmust->jasa();
		// $data['user'] = $this->Model_adminmust->user();
		// $data['ogjasa'] = $this->Model_adminmust->ogjasa();


		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Bauk Jasa";
		$organisasi = $this->input->get('organisasi');
		// $data['user'] = $this->Model_adminmust->user();
		$data['ogjasa'] = $this->Model_adminmust->ogjasa();
		$data['jasa'] = $this->Model_sdm->pencarian_j($organisasi);
		$this->load->view('home/vheader', $data);
		$this->load->view("sdm/jasa", $data);
		$this->load->view('home/vfooter');
	}



	public function baukpo()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Bauk Jasa";
		$data['jasa'] = $this->Model_adminmust->jasa();
		$data['user'] = $this->Model_adminmust->user();
		$data['ogjasa'] = $this->Model_adminmust->ogjasa();
		// $data['bidang'] = $this->Model_adminmust->bidang();
		$this->load->view('home/vheader', $data);
		$this->load->view('sdm/jasa', $data);
		$this->load->view('home/vfooter');
	}

	public function pagu()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Pagu";
		$data['pagu'] = $this->Model_adminmust->pagu();
		$data['bidang'] = $this->Model_adminmust->bidang();
		$this->load->view('home/vheader', $data);
		$this->load->view('adminmust/pagu', $data);
		$this->load->view('home/vfooter');
	}

	public function nonpo()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "BIaya Non PO";
		$data['nonpo'] = $this->Model_adminmust->nonpo();
		// $data['data_cuti'] = $this->Model_adminmust->rekap();
		// $data['bidang'] = $this->Model_adminmust->bidang();
		$this->load->view('home/vheader', $data);
		$this->load->view('sdm/non_po', $data);
		$this->load->view('home/vfooter');
	}

	// test
	public function baukpremi()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "BIaya Non PO";
		$data['nonpo'] = $this->Model_adminmust->premi();
		$this->load->view('home/vheader', $data);
		$this->load->view('sdm/premi', $data);
		$this->load->view('home/vfooter');
	}

	public function bauknonpo()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "BIaya Non PO";
		$data['nonpo'] = $this->Model_adminmust->bauknonpo();
		$this->load->view('home/vheader', $data);
		$this->load->view('sdm/bauknonpo', $data);
		$this->load->view('home/vfooter');
	}

	public function bauklembur()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "BIaya Non PO";
		$data['nonpo'] = $this->Model_adminmust->bauklembur();
		$this->load->view('home/vheader', $data);
		$this->load->view('sdm/bauklembur', $data);
		$this->load->view('home/vfooter');
	}

	public function bauksppd()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "BIaya Non PO";
		$data['nonpo'] = $this->Model_adminmust->bauksppd();
		$this->load->view('home/vheader', $data);
		$this->load->view('sdm/bauksppd', $data);
		$this->load->view('home/vfooter');
	}
	// tutuptest

	public function harian()
	{ //umar
		$data['datatelat'] = $this->Model_adminmust->get_telat2();
		$data['title'] 	= "Uynesba";
		$data['page'] 	= "Selamat Datang";
		$this->load->view('home/vheader', $data);
		$this->load->view('karyawan/vharian', $data);
		$this->load->view('home/vfooter');
	}

	public function bulanan()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Bulanan";
		$this->load->view('home/vheader', $data);
		$data['koreksi'] = $this->Model_adminmust->get_correction();
		$this->load->view('sdm/vbulanan', $data);
		$this->load->view('home/vfooter');
	}

	public function cuti()
	{ //umar
		if ($this->session->userdata('level') == '5') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Cuti";
			$data['data_cuti'] = $this->Model_sdm->approval_leave();
			$this->load->view('home/vheader', $data);
			$this->load->view('sdm/vcuti', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function sakit()
	{ //umar
		if ($this->session->userdata('level') == '5') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Sakit";
			$data['data_cuti'] = $this->Model_adminmust->approval_sakit();
			$this->load->view('home/vheader', $data);
			$this->load->view('manager/vsakit', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function lembur()
	{ //umar
		if ($this->session->userdata('level') == '5') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Approv Pengajuan Lembur";
			$data['data_lembur'] = $this->Model_sdm->approval_overtime();
			$this->load->view('home/vheader', $data);
			$this->load->view('sdm/vlembur', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function izin()
	{ //umarkoto
		if ($this->session->userdata('level') == '5') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Pengajuan Izin";
			$data['data_cuti'] = $this->Model_sdm->approval_izin();
			$this->load->view('home/vheader', $data);
			$this->load->view('sdm/vizin', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function tukar()
	{ //umar
		$data['title'] 	= "Uynesba V.2.0";
		$data['page'] 	= "Data Tukar Jadwal";
		$user = $this->session->userdata('id_user');
		$data['data_tunas'] = $this->Model_sdm->approval_official_exchange($user);
		$this->load->view('home/vheader', $data);
		$this->load->view('manager/vadd_tukar', $data);
		$this->load->view('home/vfooter');
	}

	public function perjalanan_dinas()
	{ //umar
		if ($this->session->userdata('level') == '5') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Approv Pengajuan Perjalanan Dinas";
			$data['data_perjalanan_dinas'] = $this->Model_sdm->approval_perjalanan_dinas();
			$this->load->view('home/vheader', $data);
			$this->load->view('sdm/vperjalanan_dinas', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function schedule()
	{ //umar 
		if ($this->session->userdata('level') == '5') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Jadwal Kerja";
			$this->load->view('home/vheader', $data);
			$this->load->view('sdm/show');
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function show_schedule()
	{ //umar
		if ($this->session->userdata('level') == '5') {
			$date    = $this->input->post('start') . '|' . $this->input->post('end');
			$results = $this->Model_sdm->show_schedule($date);
			$sdate   = ((substr($this->input->post('start'), 0, 7) == substr($this->input->post('end'), 0, 7)) ? '1' : '0');
			$data = array(
				'range' => $this->date_range($this->input->post('start'), $this->input->post('end')),
				'date'	=> $date,
				'sdate'	=> $sdate,
				'schedule'	=> json_encode($results)
			);
			// $this->load->view('home/vheader');
			$this->load->view('sdm/schedule', $data);
			// $this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	private function date_range($first, $last, $step = '+1 day', $output_format = 'Y/m/d')
	{

		$dates = array();
		$current = strtotime($first);
		$last = strtotime($last);

		while ($current <= $last) {

			$dates[] = date($output_format, $current);
			$current = strtotime($step, $current);
		}

		return $dates;
	}

	public function add_karyawan()
	{ //umar
		if ($this->session->userdata('level') == '5') {
			$data['title'] 	= "Uynesba V.2.0";
			$data['page'] 	= "Data Karyawan";
			$data['data_karyawan'] = $this->Model_sdm->getmember();
			$this->load->view('home/vheader', $data);
			$this->load->view('sdm/add_karyawan', $data);
			$this->load->view('home/vfooter');
		} else {
			echo "<script>alert('Mau ngapain?'); window.history.back();</script>";
		}
	}

	public function detailkaryawan($id)
	{ //umar
		$data['title'] 	= "Uynesba";
		$this->load->view('sdm/detail', $data);
	}
}
