<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_manager extends CI_Model {

	function __constuct(){
		parent::__constuct();  
		loader::database();  
	}

	public function approval_official_exchange($user){
		$divisi = $this->session->userdata('manajer');
		
		$q="SELECT A.id, A.id_user, B.`nama_lengkap` AS nama_user, A.`id_pengganti`, C.`nama_lengkap` AS nama_pengganti, A.`shift_pemohon`, A.`shift_pengganti`, A.`tanggal`, A.`alasan`, A.`status`  
		FROM `tbu_tukarjadwal` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_user C ON A.`id_pengganti`=C.`id` 
		WHERE (B.`manajer`='".$divisi."') ORDER BY A.`status`, A.`tanggal` ASC";
		
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function approval_izin(){ //umar
		$divisi = $this->session->userdata('organisasi');
		$q="SELECT A.`id`, B.id id_user, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave IN ('4','5','6','7','8','9','10') AND start_date BETWEEN '2019-11-01' AND '2020-11-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE B.`organisasi`='".$divisi."' AND A.`jenis_leave` IN ('4','5','6','7','8','9','10') ORDER BY A.`status`, A.`id` ASC";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function get_telat(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT C.nama_lengkap, B.lang, B.id_abs, B.os, B.ip, A.id_shifting,  B.jam jam_masuk, D.jam jam_pulang, B.ketepatan FROM `schedule` A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1') B ON A.`user_id` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2') D ON A.`user_id` = D.`id_user` AND A.`tanggal` = D.`tanggal`
		LEFT JOIN tbu_user C ON A.user_id = C.id
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) AND A.id_shifting  IN ('P','S','M','PO') AND A.tanggal = DATE(DATE_ADD(timestamp(now()), INTERVAL 12 HOUR)) AND C.active = '1' AND C.`manajer`= '".$divisi."' AND level='3' 
		ORDER BY B.`id_abs` ASC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function approval_overtime(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT A.`type_lembur`,A.`id`, B.id id_user, B.`nama_lengkap`, A.`start_datetime`, C.`name` jenis_lembur, A.`end_datetime`, A.`alasan`, A.`status` 
		FROM `tbu_lembur` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id`  
		LEFT JOIN tbu_jenislembur C ON C.`id`=A.`jenis_lembur`
		WHERE B.manajer='".$divisi."'
		ORDER BY A.`id` desc";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function approval_leave(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT B.`manajer`, A.`id`, B.id id_user, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'12') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (14 - SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' AND start_date BETWEEN '2021-01-01' AND '2021-12-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` 
		WHERE B.manajer='".$divisi."' AND A.`jenis_leave` IN ('1','2','3','12') ORDER BY A.`status`, A.`id` DESC";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function getmember(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT A.active,A.no_induk,A.nama_lengkap,A.email,A.jabatan,B.spv FROM tbu_user A LEFT JOIN organisasi B ON B.id_personal = A.organisasi WHERE A.`manajer`='".$divisi."' AND A.level='3' ORDER BY A.`nama_lengkap` ASC";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}
	
	public function approval_sakit(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT A.`id`, B.id id_user, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='11' AND start_date BETWEEN '2019-11-01' AND '2020-11-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE B.`manajer`='".$divisi."' AND A.`jenis_leave` = '11' ORDER BY A.`status`, A.`id` desc";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function getAllMember(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT id jumlah FROM tbu_user WHERE `manajer`='".$divisi."' AND `active`='1' AND level='3' ";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppCuti(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT A.id jumlah FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE A.`status`='1' AND A.`jenis_leave`='1' AND B.`manajer`='".$divisi."' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahLembur(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT A.id FROM tbu_lembur A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE A.`status`='3' AND B.`manajer`='".$divisi."' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahSakit(){ //umarkoto
		$divisi = $this->session->userdata('manajer');
		$q="SELECT * FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE A.`jenis_leave`='11' and A.`status`='3' AND B.`manajer`='".$divisi."'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppSakit(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT * FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE jenis_leave='2' AND B.`manajer`='".$divisi."'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function gettukarjadwal(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT * FROM tbu_tukarjadwal A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE status='0' AND B.`manajer`='".$divisi."'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahDinas(){ //umarkoto
		$divisi = $this->session->userdata('manajer');
		$q="SELECT * FROM tbu_perjalanandinas A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE B.`manajer`='".$divisi."'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppDinas(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT * FROM tbu_perjalanandinas A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE B.`manajer`='".$divisi."'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahResign(){ //umarkoto
		$divisi = $this->session->userdata('manajer');
		$q="SELECT * FROM tbu_resign A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE B.`manajer`='".$divisi."'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppResign(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT * FROM tbu_resign A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE B.`manajer`='".$divisi."'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppLembur(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT * FROM tbu_lembur A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE B.`manajer`='".$divisi."' and A.status in ('1','2')";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function get_absen(){ //umar


		$divisi = $this->session->userdata('manajer');
		$q="SELECT 
		SUM(CASE WHEN kode_abs = '1' THEN 1 END) AS masuk,
		SUM(CASE WHEN kode_abs = '2' THEN 1 END) AS pulang
		FROM tbu_absensi LEFT JOIN tbu_user ON id_user=id WHERE manajer='".$divisi."';";
		$query = $this->db->query($q);
		return $query->result();


		// $q="SELECT 
		// SUM(CASE WHEN kode_abs = '1' THEN 1 END) AS masuk,
		// SUM(CASE WHEN kode_abs = '2' THEN 1 END) AS pulang
		// FROM tbu_absensi;";
		// $query = $this->db->query($q);
		// return $query->result();
	}


	public function getJumlahCuti(){ //umar
		$divisi = $this->session->userdata('manajer');
		$q="SELECT A.id jumlah FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE A.`status`='3' AND A.`jenis_leave` in ('1','2','3','12') AND B.`manajer`='".$divisi."' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	

	public function review_approval($kode, $id, $user_id){ //umar		
		if($kode == '1'){	
			$q="UPDATE `tbu_lembur` SET `status` = '3', `approved_by` = '".$user_id."', `approved_date` = NOW() WHERE id = '".$id."'";			
		}else if($kode == '2'){
			$q="UPDATE `tbu_perjalanandinas` SET `status` = '3', `approved_by` = '".$user_id."', `approved_date` = NOW() WHERE id_dinas = '".$id."'";
		}else if($kode == '3'){
			$q="UPDATE `tbu_cuti` SET `status` = '3', `reviewed_by` = '".$user_id."', `reviewed_date` = NOW() WHERE id = '".$id."'";
		}
		$query = $this->db->query($q);
		if($query){
			if($kode == '2'){//LEAVE				
				$query2 = $this->db->query("SELECT * FROM tbu_perjalanandinas WHERE id_dinas = '".$id."' AND status = '3'");
				$row2 = $query2->row();
				if (isset($row2)){
					for($i=0;$i<$row2->jumlah;$i++){
						$tanggal = date("Y-m-d", strtotime("+".$i." days",strtotime($row2->tanggal_pergi)));
						$this->db->query("UPDATE `schedule` SET id_shifting = 'T' WHERE user_id = '".$row2->id_user."' AND tanggal = '".$tanggal."'");				
					}
				}else{
					return false;
				}
			}else if($kode == '4'){//CORRECTION				
				$query2 = $this->db->query("SELECT * FROM correction WHERE id = '".$id."' AND status = '3'");
				$row2 = $query2->row();
				if (isset($row2)){
					if($row2->id_abs == null){
						$this->db->query("INSERT INTO absensi (id_om, tanggal, jam, kode_abs, browser, os, ip, ketepatan, keterangan) VALUES ('".$row2->id_om."','".$row2->presence_date."','".$row2->correction_time."','".$row2->presence_code."','SERVER','SERVER','10.14.151.51','1','CORRECTION')");
					}else{
						$this->db->query("UPDATE `absensi` SET jam = '".$row2->correction_time."', ketepatan = '1', keterangan = 'CORRECTION' WHERE id_abs = '".$row2->id_abs."' AND tanggal = '".$row2->presence_date."'");
					}
				}else{
					return false;
				}
			}
			if($kode == '1'){
				return true;
			}else{
				if($query2){
					return true;
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
		$this->db->close();
	}	

	public function reject_approval($kode, $id, $user_id, $reason){ //umar	
		if($kode == '1'){		
			$q="UPDATE `tbu_lembur` SET `status` = '4', `rejected_by` = '".$user_id."', `rejected_reason` = '".$reason."', `rejected_date` = NOW() WHERE id = '".$id."'";		
		}else if($kode == '2'){
			$q="UPDATE `tbu_perjalanandinas` SET `status` = '4' WHERE id_dinas = '".$id."'";
		}else if($kode == '3'){
			$q="UPDATE `tbu_cuti` SET `status` = '4', `rejected_by` = '".$user_id."', `rejected_reason` = '".$reason."', `rejected_date` = NOW() WHERE id = '".$id."'";
		}
		$query = $this->db->query($q);
		if($query){
			return true;
		}else{
			return false;
		}
		$this->db->close();
	}

	public function approval_perjalanan_dinas(){ //umar
		$divisi = $this->session->userdata('organisasi');
		$q="

		SELECT A.*, B.*, (A.`jumlah`-1)*C.`penginapan` AS penginapan, A.`jumlah`*C.`biaya_makan` AS makan, A.`jumlah`*C.`uang_saku` AS uangsaku, A.`jumlah`*C.`angkutan_setempat` AS angkutan, (A.`jumlah`-1)*C.`ke_bandara` AS tobandara, 
		(((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+(A.`harga_tiket`)) AS total,
		(((A.`jumlah`)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)) AS totall,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` + ((A.`jumlah`)*C.`biaya_makan`) +
		((A.`jumlah`)*C.`uang_saku`) + ((A.`jumlah`)*C.`angkutan_setempat`) + (IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) +
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) + (A.`harga_tiket`) AS totalsppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN `tbu_user` B ON A.`id_user`=B.`id`
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		WHERE B.`organisasi`='".$divisi."' 
		ORDER BY A.`id_dinas` desc";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}
}