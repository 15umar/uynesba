<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_Model {

	public function check_login($username,$password){ //umar

		$this->db->where("(no_induk='$username' OR email='$username')");
		$this->db->where('password',$password);
		$query2=$this->db->get('tbu_user');		

		if($query2->num_rows()>0){
			foreach ($query2->result() as $rows){
				$data=array(
					'id_user' 		=> $rows->id,
					'nama' 			=> $rows->nama_lengkap,
					'organisasi'	=> $rows->organisasi,
					'manajer'	=> $rows->manajer,
					'email' 		=> $rows->email,
					'alamat' 		=> $rows->alamat,
					'no_telepon' 	=> $rows->no_telepon,
					'level' 		=> $rows->level,
					'foto' 			=> $rows->foto,
					'logged_in'		=> TRUE
				);	
			}
			$this->session->set_userdata($data);
			return true;
		}
	}
	
	public function getBiodata($id_user){ //umar
		$this->db->where('e.id',$id_user);
		$this->db->join('organisasi as b','b.id_personal=e.organisasi');
		$this->db->join('level as c','c.id=e.level');
		$query = $this->db->get('tbu_user as e');

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}

	public function cek_user($data) {
		$query = $this->db->get_where('tbl_personal', $data);
		return $query;
	}
}
?>
