<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_spv extends CI_Model
{

	function __constuct()
	{
		parent::__constuct();
		loader::database();
	}

	public function getAllMember()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT id jumlah FROM tbu_user WHERE `organisasi`='" . $divisi . "' AND `active`='1' AND level='3' ";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function approval_official_exchange($user)
	{
		$divisi = $this->session->userdata('organisasi');

		$q = "SELECT A.id, A.id_user, B.`nama_lengkap` AS nama_user, A.`id_pengganti`, C.`nama_lengkap` AS nama_pengganti, A.`shift_pemohon`, A.`shift_pengganti`, A.`tanggal`, A.`alasan`, A.`status`  
		FROM `tbu_tukarjadwal` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_user C ON A.`id_pengganti`=C.`id` 
		WHERE (B.`organisasi`='" . $divisi . "') ORDER BY A.`status`, A.`tanggal` ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function getmember()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT A.active,A.no_induk,A.nama_lengkap,A.email,A.jabatan,B.spv FROM tbu_user A LEFT JOIN organisasi B ON B.id_personal = A.organisasi WHERE A.`organisasi`='" . $divisi . "' AND A.level='3' ORDER BY A.`nama_lengkap` ASC";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function get_overtime($tglawal, $tglakhir)
	{
		$q = "SELECT A.`id`, B.id id_user, B.`nama_lengkap`, A.`start_datetime`, A.`end_datetime`, A.`alasan`, A.`status` 
		FROM `tbu_lembur` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id`  
		WHERE A.start_datetime BETWEEN '" . $tglawal . "' AND '" . $tglakhir . "' and A.status='3'
		ORDER BY A.`status`, A.`start_datetime` ASC";


		// 			$q="SELECT * FROM tbu_lembur 
		// WHERE start_datetime BETWEEN '".$tglawal."' AND '".$tglakhir."'";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
		} else {
			return $q;
		}
		$this->db->close();
	}

	public function get_telat()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT C.nama_lengkap, B.lang, B.id_abs, B.os, B.ip, A.id_shifting,  B.jam jam_masuk, D.jam jam_pulang, B.ketepatan FROM `schedule` A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1') B ON A.`user_id` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2') D ON A.`user_id` = D.`id_user` AND A.`tanggal` = D.`tanggal`
		LEFT JOIN tbu_user C ON A.user_id = C.id
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) AND A.id_shifting  IN ('P','S','M','PO') AND A.tanggal = DATE(DATE_ADD(timestamp(now()), INTERVAL 12 HOUR)) AND C.active = '1' AND C.`organisasi`= '" . $divisi . "' AND level='3' 
		ORDER BY B.`id_abs` ASC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_correction222()
	{
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT DISTINCT A.lang,E.tanggal,A.tanggal, D.nama_lengkap, D.lokasi, A.os, A.ip, E.id_shifting,  B.jam jam_masuk, C.jam jam_pulang,B.fotoabsen masuk, C.fotoabsen keluar FROM tbu_absensi A
		LEFT JOIN (SELECT  fotoabsen,kode_abs, jam, id_user, tanggal FROM tbu_absensi WHERE kode_abs = '1'  ) B ON A.`id_user` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT  fotoabsen,kode_abs, jam, id_user, tanggal FROM tbu_absensi WHERE kode_abs = '2'  ) C ON A.`id_user` = C.`id_user` AND A.`tanggal` = C.`tanggal`
		LEFT JOIN tbu_user D ON A.id_user = D.id
		LEFT JOIN (SELECT  tanggal,id_shifting, user_id FROM `schedule` E) E ON E.user_id=A.id_user AND E.tanggal=A.tanggal
		WHERE D.`organisasi`= '" . $divisi . "' AND   A.Tanggal = DATE(DATE_ADD(timestamp(now()), INTERVAL 11 HOUR))
		GROUP BY B.fotoabsen
		ORDER BY A.tanggal DESC, B.jam DESC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_correction()
	{
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT DISTINCT A.tanggal, A.lang, D.nama_lengkap, D.lokasi, A.os, A.insert, A.ip, C.`pekerjaan`,  B.jam jam_masuk, C.jam jam_pulang,B.fotoabsen masuk, C.fotoabsen keluar FROM tbu_absensi A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1'  GROUP BY id_abs) B ON A.`id_user` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2'  GROUP BY id_abs) C ON A.`id_user` = C.`id_user` AND A.`tanggal` = C.`tanggal`
		LEFT JOIN tbu_user D ON A.id_user = D.id
		WHERE D.`organisasi`= '" . $divisi . "' AND (B.kode_abs = '1' OR B.kode_abs IS NULL) AND A.insert = curdate()
		GROUP BY B.fotoabsen
		ORDER BY A.tanggal DESC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function pencarianbulanan($tglawal, $tglakhir)
	{ //umarkoto
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT DISTINCT E.tanggal,A.tanggal, D.nama_lengkap, D.lokasi, A.os, A.ip, E.id_shifting,  B.jam jam_masuk, C.jam jam_pulang,B.fotoabsen masuk, C.fotoabsen keluar FROM tbu_absensi A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1'  GROUP BY id_abs) B ON A.`id_user` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2'  GROUP BY id_abs) C ON A.`id_user` = C.`id_user` AND A.`tanggal` = C.`tanggal`
		LEFT JOIN tbu_user D ON A.id_user = D.id
		LEFT JOIN (SELECT * FROM `schedule`  GROUP BY id ) E ON E.user_id=A.id_user AND E.tanggal=A.tanggal
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) 
		AND A.Tanggal BETWEEN '" . $tglawal . "' AND '" . $tglakhir . "' AND D.`organisasi`= '" . $divisi . "'
		GROUP BY B.fotoabsen
		ORDER BY A.tanggal DESC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function show_schedule($date)
	{ //umar
		$xdate = explode('|', $date);
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT a.user_id AS nip, b.nama_lengkap,a.id_shifting,tanggal FROM `schedule` a, `tbu_user` b WHERE a.user_id=b.id and b.organisasi='" . $divisi . "' AND tanggal between '" . $xdate[0] . "' and '" . $xdate[1] . "' order by 2 asc";
		$query = $this->db->query($q);

		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function get_employee($param = NULL)
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$query = $this->db->query("SELECT id AS NIP, nama_lengkap AS NAMA,jabatan AS JOB_TITLE FROM `tbu_user` WHERE active='1' AND level='3' and shift='2' and organisasi='" . $divisi . "'  $param order by 2 asc");
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function save_schedule($emp, $date, $input)
	{ //umar
		$xemp  = explode(',', $emp);
		$xdate = explode("|", $date);
		foreach ($xemp as $r) {
			$r = explode("|", $r);
			$del = "delete from `schedule` where user_id='" . $r[1] . "' and tanggal between '" . $xdate[0] . "' and '" . $xdate[1] . "';";
			$this->db->query($del);
		}
		$this->db->close();
		echo $input;
		$items = explode(",", $input);
		$q = "INSERT INTO `schedule` (tanggal,user_id,id_shifting,keterangan) VALUES";
		foreach ($items as $item) {
			$exitem = explode("_", $item);
			$q .= "(STR_TO_DATE('" . $exitem[1] . "','%Y%m%d'),'" . $exitem[2] . "','" . $exitem[3] . "','1'),";
		}
		$query = rtrim($q, ',');
		$query = $this->db->query($query);
		if ($query) {
			return $query;
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	function tambahcontact($dataarray)
	{ //umar
		for ($i = 0; $i < count($dataarray); $i++) {
			$data = array(
				'email' 					=> $dataarray[$i]['email'],
				'password'    				=> md5($dataarray[$i]['password']),
				'level'    					=> $dataarray[$i]['level'],
				'organisasi'				=> $dataarray[$i]['organisasi'],
				'no_induk'					=> $dataarray[$i]['no_induk'],
				'nama_lengkap'  			=> $dataarray[$i]['nama_lengkap'],
				'lokasi'  					=> $dataarray[$i]['lokasi'],
				'unit_kerja' 				=> $dataarray[$i]['unit_kerja'],
				'tempat_lahir' 				=> $dataarray[$i]['tempat_lahir'],
				'tgl_lahir' 				=> $dataarray[$i]['tgl_lahir'],
				'status_perkawinan' 		=> $dataarray[$i]['status_perkawinan'],
				'agama' 					=> $dataarray[$i]['agama'],
				'jenis_kelamin'   			=> $dataarray[$i]['jenis_kelamin'],
				'status_pajak'   			=> $dataarray[$i]['status_pajak'],
				'golongan_status_bpjs'   	=> $dataarray[$i]['golongan_status_bpjs'],
				'no_ktp' 					=> $dataarray[$i]['no_ktp'],
				'alamat' 					=> $dataarray[$i]['alamat'],
				'no_telepon' 				=> $dataarray[$i]['no_telepon'],
				'jabatan' 					=> $dataarray[$i]['jabatan'],
				'no_npwp' 					=> $dataarray[$i]['no_npwp'],
				'no_bpjs_ketenagakerjaan'	=> $dataarray[$i]['no_bpjs_ketenagakerjaan'],
				'no_bpjs_kesehatan' 		=> $dataarray[$i]['no_bpjs_kesehatan'],
				'no_rek' 					=> $dataarray[$i]['no_rek'],
				'bank' 						=> $dataarray[$i]['bank'],
				'nama_sesuai_rekening' 		=> $dataarray[$i]['nama_sesuai_rekening'],
				'foto' 						=> $dataarray[$i]['foto'],
				'active' 					=> $dataarray[$i]['active'],
			);
			$this->db->insert('tbu_user', $data);
		}
	}

	public function reject_approval($kode, $id, $user_id, $reason)
	{ //umar	
		if ($kode == '1') {
			$q = "UPDATE `tbu_lembur` SET `status` = '4', `rejected_by` = '" . $user_id . "', `rejected_reason` = '" . $reason . "', `rejected_date` = NOW() WHERE id = '" . $id . "'";
		} else if ($kode == '2') {
			$q = "UPDATE `tbu_perjalanandinas` SET `status` = '4' WHERE id_dinas = '" . $id . "'";
		} else if ($kode == '3') {
			$q = "UPDATE `tbu_cuti` SET `status` = '4', `rejected_by` = '" . $user_id . "', `rejected_reason` = '" . $reason . "', `rejected_date` = NOW() WHERE id = '" . $id . "'";
		}
		$query = $this->db->query($q);
		if ($query) {
			return true;
		} else {
			return false;
		}
		$this->db->close();
	}

	public function getJumlahCuti()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT A.id jumlah FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE A.`status`='3' AND A.`jenis_leave` IN ('1','2','3','12') AND B.`organisasi`='" . $divisi . "' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahLembur()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT A.id FROM tbu_lembur A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE A.`status`='3' AND B.`organisasi`='" . $divisi . "' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppCuti()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT A.id jumlah FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE A.`status`='1' AND A.`jenis_leave`='1' AND B.`organisasi`='" . $divisi . "' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahSakit()
	{ //umarkoto
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE A.`jenis_leave`='11' and status='3' AND B.`organisasi`='" . $divisi . "'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppSakit()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE jenis_leave='11' and status='1' AND B.`organisasi`='" . $divisi . "'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function gettukarjadwal()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_tukarjadwal A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE status in ('1','0') AND B.`organisasi`='" . $divisi . "'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getnotiflembur()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_lembur A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE B.`organisasi`='" . $divisi . "' and A.`status`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getnotifjadwal()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_tukarjadwal A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE status in ('1','0') AND B.`organisasi`='" . $divisi . "'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getnotifsakit()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE jenis_leave='11' and status='1' AND B.`organisasi`='" . $divisi . "'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getnotifcuti()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT A.id jumlah FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE A.`status`='1' AND A.`jenis_leave`='1' AND B.`organisasi`='" . $divisi . "' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getnotifdinas()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_cuti A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE jenis_leave='11' and status='1' AND B.`organisasi`='" . $divisi . "'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppLembur()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_lembur A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` WHERE B.`organisasi`='" . $divisi . "' and A.`status`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function get_absen()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT 
		SUM(CASE WHEN a.kode_abs = '1' THEN 1 END) AS masuk,
		SUM(CASE WHEN a.kode_abs = '2' THEN 1 END) AS pulang
		FROM tbu_absensi a
		LEFT JOIN tbu_user b ON a.`id_user`=b.`id`
		WHERE b.`organisasi`='" . $divisi . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function approval_leave()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT A.`id`, B.id id_user, B.`nama_lengkap`, C.`name` jenis_cuti, A.`jenis_leave`,A.`jenis_cutikhusus`, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'12') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (12 - SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='1' AND start_date BETWEEN '2021-01-01' AND '2021-12-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE B.`organisasi`='" . $divisi . "' AND A.`jenis_leave` IN ('1','2','3','12') 
		ORDER BY A.`id` DESC";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function approval_sakit()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT A.`id`, B.id id_user, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='11' AND start_date BETWEEN '2021-01-01' AND '2021-12-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE B.`organisasi`='" . $divisi . "' AND A.`jenis_leave` = '11' 
		ORDER BY A.`id` desc";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function approval_izin()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT A.`id`, B.id id_user, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave IN ('4','5','6','7','8','9','10') AND start_date BETWEEN '2019-11-01' AND '2020-11-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE B.`organisasi`='" . $divisi . "' AND A.`jenis_leave` IN ('4','5','6','7','8','9','10') ORDER BY A.`status`, A.`id` ASC";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function approval_overtime()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT A.`type_lembur`, A.`id`, B.id id_user, B.`nama_lengkap`, A.`start_datetime`, C.`name` jenis_lembur, A.`end_datetime`, A.`alasan`, A.`status` 
		FROM `tbu_lembur` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id`  
		LEFT JOIN tbu_jenislembur C ON C.`id`=A.`jenis_lembur`
		WHERE B.organisasi='" . $divisi . "'
		ORDER BY A.`id` desc";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function approval_perjalanan_dinas()
	{ //umar
		$divisi = $this->session->userdata('organisasi');
		$q = "

		SELECT A.*, B.*, (A.`jumlah`-1)*C.`penginapan` AS penginapan, A.`jumlah`*C.`biaya_makan` AS makan, A.`jumlah`*C.`uang_saku` AS uangsaku, A.`jumlah`*C.`angkutan_setempat` AS angkutan, (A.`jumlah`-1)*C.`ke_bandara` AS tobandara, 
		(((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+(A.`harga_tiket`)) AS total,
		(((A.`jumlah`)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)) AS totall,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` + ((A.`jumlah`)*C.`biaya_makan`) +
		((A.`jumlah`)*C.`uang_saku`) + ((A.`jumlah`)*C.`angkutan_setempat`) + (IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) +
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) + (A.`harga_tiket`) AS totalsppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN `tbu_user` B ON A.`id_user`=B.`id`
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		WHERE B.organisasi='" . $divisi . "' 
		ORDER BY A.`id_dinas` desc";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function review_approval($kode, $id, $user_id)
	{
		if ($kode == '1') {
			$q = "UPDATE `tbu_lembur` SET `status` = '2', `reviewed_by` = '" . $user_id . "', `reviewed_date` = NOW() WHERE id = '" . $id . "'";
		} else if ($kode == '2') {
			$q = "UPDATE `tbu_perjalanandinas` SET `status` = '2', `reviewed_by` = '" . $user_id . "', `reviewed_date` = NOW() WHERE id_dinas = '" . $id . "'";
		} else if ($kode == '3') {
			$q = "UPDATE `tbu_cuti` SET `status` = '3', `reviewed_by` = '" . $user_id . "', `reviewed_date` = NOW() WHERE id = '" . $id . "'";
		} else if ($kode == '4') {
			$q = "UPDATE `tbu_tukarjadwal` SET `status` = '3' WHERE id = '" . $id . "'";
		}
		$query = $this->db->query($q);

		if ($query) {
			if ($kode == '3') { //LEAVE				
				$query2 = $this->db->query("SELECT * FROM tbu_cuti WHERE id = '" . $id . "' AND status = '3'");
				$row2 = $query2->row();
				if (isset($row2)) {
					if ($row2->jenis_leave == '1' || $row2->jenis_leave == '2' || $row2->jenis_leave == '3') {
						for ($i = 0; $i < $row2->jumlah_leave; $i++) {
							$tanggal = date("Y-m-d", strtotime("+" . $i . " days", strtotime($row2->start_date)));
							$this->db->query("UPDATE `schedule` SET id_shifting = 'C' WHERE user_id = '" . $row2->id_user . "' AND tanggal = '" . $tanggal . "'");
						}
					} else if ($row2->jenis_leave == '4' || $row2->jenis_leave == '5' || $row2->jenis_leave == '6' || $row2->jenis_leave == '7' || $row2->jenis_leave == '8' || $row2->jenis_leave == '9' || $row2->jenis_leave == '10') {
						for ($i = 0; $i < $row2->jumlah_leave; $i++) {
							$tanggal = date("Y-m-d", strtotime("+" . $i . " days", strtotime($row2->start_date)));
							$this->db->query("UPDATE `schedule` SET id_shifting = 'I' WHERE user_id = '" . $row2->id_user . "' AND tanggal = '" . $tanggal . "'");
						}
					} else if ($row2->jenis_leave == '11') {
						for ($i = 0; $i < $row2->jumlah_leave; $i++) {
							$tanggal = date("Y-m-d", strtotime("+" . $i . " days", strtotime($row2->start_date)));
							$this->db->query("UPDATE `schedule` SET id_shifting = 'SK' WHERE user_id = '" . $row2->id_user . "' AND tanggal = '" . $tanggal . "'");
						}
					}
				} else {
					return false;
				}
			} else if ($kode == '4') { //CORRECTION				
				$query2 = $this->db->query("SELECT * FROM tbu_tukarjadwal WHERE id = '" . $id . "' AND status = '3'");
				$row2 = $query2->row();
				if (isset($row2)) {
					$shiftpemohon = explode(">", str_replace(' ', '', $row2->shift_pemohon));
					$shiftpengganti = explode(">", str_replace(' ', '', $row2->shift_pengganti));

					$this->db->query("UPDATE `schedule` SET id_shifting = '" . $shiftpemohon[1] . "' WHERE user_id = '" . $row2->id_user . "' AND tanggal = '" . $row2->tanggal . "'");

					$this->db->query("UPDATE `schedule` SET id_shifting = '" . $shiftpengganti[1] . "' WHERE user_id = '" . $row2->id_pengganti . "' AND tanggal = '" . $row2->tanggal . "'");
				} else {
					return false;
				}
			}
			if ($kode == '2') {
				return true;
			} else {
				if ($kode == '1') {
					return true;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
		$this->db->close();
	}
}
