<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_schedule extends CI_Model
{

	function __constuct()
	{
		parent::__constuct();  // Call the Model constructor 
		loader::database();    // Connect to current database setting.
	}

	public function get_employee($param = NULL)
	{
		$query = $this->db->query("SELECT id AS NIP, nama_lengkap AS NAMA,organisasi AS JOB_TITLE FROM `tbu_user` WHERE active='1' and level='3' and shift='2'  $param order by 2 asc");
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function show_schedule($date)
	{
		$xdate = explode('|', $date);
		$q = "SELECT a.user_id AS nip, b.nama_lengkap,a.id_shifting,tanggal FROM `schedule` a, `tbu_user` b WHERE a.user_id=b.id AND tanggal between '" . $xdate[0] . "' and '" . $xdate[1] . "' order by 2 asc";
		$query = $this->db->query($q);

		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function save_schedule($emp, $date, $input)
	{
		$xemp  = explode(',', $emp);
		$xdate = explode("|", $date);

		foreach ($xemp as $r) {
			$r = explode("|", $r);
			$del = "delete from `schedule` where user_id='" . $r[1] . "' and tanggal between '" . $xdate[0] . "' and '" . $xdate[1] . "';";
			$this->db->query($del);
		}
		$this->db->close();
		echo $input;
		$items = explode(",", $input);
		$q = "INSERT INTO `schedule` (tanggal,user_id,id_shifting,keterangan) VALUES";
		foreach ($items as $item) {
			$exitem = explode("_", $item);
			$q .= "(STR_TO_DATE('" . $exitem[1] . "','%Y%m%d'),'" . $exitem[2] . "','" . $exitem[3] . "','1'),";
		}

		$query = rtrim($q, ',');
		$query = $this->db->query($query);
		if ($query) {
			return $query;
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}
}
