<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_home extends CI_Model {

	public function get_telat(){
		$divisi = $this->session->userdata('organisasi');
		$q="SELECT C.nama_lengkap, C.lokasi, B.os, B.ip, A.id_shifting, B.jam, B.ketepatan FROM `schedule` A
		LEFT JOIN `absensi` B ON A.user_id = B.id_om AND A.`tanggal` = B.tanggal
		LEFT JOIN employee C ON A.user_id = C.id
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) AND A.id_shifting  IN ('P','S','M','PO') AND A.tanggal = CURRENT_DATE() AND C.active = '1' AND C.`organisasi`= '".$divisi."'
		ORDER BY ketepatan ASC, jam DESC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jadwal($user_id){
		$q="SELECT * FROM SCHEDULE A LEFT JOIN kode_absen B ON A.`id_shifting` = B.kode WHERE A.user_id = '".$user_id."' AND A.tanggal = CURRENT_DATE() AND (CASE WHEN B.kode = 'PO' THEN B.active = '1' AND (CASE WHEN DAYNAME(NOW()) = 'Friday' THEN B.tipe = 2 ELSE B.tipe = 1 END) ELSE TRUE END)";
		$query = $this->db->query($q);
		return $query->result();
	}
	public function get_cuti(){
		$divisi = $this->session->userdata('organisasi');
		$q="SELECT a.* FROM CUTI a LEFT JOIN employee b ON A.`id_om`=b.id 
		WHERE b.`organisasi`='".$divisi."' AND STATUS = '3' AND (start_date BETWEEN CURDATE() AND DATE_ADD(CURRENT_DATE, INTERVAL 2 DAY) OR end_date BETWEEN CURDATE() AND DATE_ADD(CURRENT_DATE, INTERVAL 1 DAY)) AND STATUS ='3' ";
		$query = $this->db->query($q);
		return $query->result();
	}
	
	public function absen_in($id_om, $browser, $os, $ip, $kode){
		$q="call proc_presensi('".$id_om."', '".$browser."', '".$os."', '".$ip."', '".$kode."')";
		$query = $this->db->query($q);
		return $query->result();
	}
	
	public function getAllMember(){
		$divisi = $this->session->userdata('organisasi');
		$q="SELECT id jumlah FROM employee WHERE `organisasi`='".$divisi."' AND `active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}
	
	public function getJumlahCuti(){
		$divisi = $this->session->userdata('organisasi');
		$q="SELECT A.id jumlah FROM cuti A LEFT JOIN employee B ON A.`id_om`=B.`id` WHERE A.`status`='3' AND B.`organisasi`='".$divisi."' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}
	
	public function getJumlahTunas(){
		$divisi = $this->session->userdata('organisasi');
		$q="SELECT A.id FROM tunas A LEFT JOIN employee B ON A.`id_user`=B.`id` WHERE A.`status`='3' AND B.`organisasi`='".$divisi."' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}
	
	public function getJumlahLembur(){
		$divisi = $this->session->userdata('organisasi');
		$q="SELECT A.id FROM lembur A LEFT JOIN employee B ON A.`id_om`=B.`id` WHERE A.`status`='3' AND B.`organisasi`='".$divisi."' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	//profile
	public function get_profile(){ //umar
		$this->db->join('organisasi', 'tbu_user.organisasi=organisasi.id_personal');
		$this->db->where('id',$this->session->userdata('id_user'));
		$query = $this->db->get('tbu_user');
		return $query->result();
	}
}