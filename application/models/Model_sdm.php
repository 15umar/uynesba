<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_sdm extends CI_Model {

	function __constuct(){
		parent::__constuct();  
		loader::database();  
	}

	public function approval_overtime(){
		$q="SELECT A.`reviewed_date` as umar, A.`approved_date`, A.`id`, B.id id_user, B.`nama_lengkap`, A.`start_datetime`, C.`name` jenis_lembur, A.`end_datetime`, A.`alasan`, A.`status`
		FROM `tbu_lembur` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id`  
		LEFT JOIN tbu_jenislembur C ON C.`id`=A.`jenis_lembur`
		ORDER BY A.`id` desc";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function approval_perjalanan_dinas(){
		$q="SELECT A.`reviewed_date` as umar, A.`approved_date`, A.insert,A.`id_dinas`,A.`pengajuan`,A.`referensi`,A.`tanggal_pergi`,A.`tanggal_pulang`,A.`jumlah`,A.`tujuan`,A.`status`, B.*, (A.`jumlah`-1)*C.`penginapan` AS penginapan, A.`jumlah`*C.`biaya_makan` AS makan, A.`jumlah`*C.`uang_saku` AS uangsaku, A.`jumlah`*C.`angkutan_setempat` AS angkutan, (A.`jumlah`-1)*C.`ke_bandara` AS tobandara, 
		(((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+(A.`harga_tiket`)) AS total,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` + ((A.`jumlah`)*C.`biaya_makan`) +
		((A.`jumlah`)*C.`uang_saku`) + ((A.`jumlah`)*C.`angkutan_setempat`) + (IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) +
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) + (A.`harga_tiket`) AS totalsppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN `tbu_user` B ON A.`id_user`=B.`id`
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		ORDER BY A.`id_dinas` desc";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}


	public function pencarian_j($organisasi){ 
		$q="select * from tbu_user a left join organisasi b on a.organisasi=b.id_personal WHERE a.level='3' and a.organisasi='$organisasi'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function pencarian_d($tglawal, $tglakhir){ //umar
		$q="SELECT a.nama_lengkap, e.bidang, e.subbid, a.jabatan, 
		c.harikerja, c.harilibur,c.haribesar, c.totallembur,
		COUNT(IF(b.`id_shifting`='P', b.`id_shifting`, NULL)) AS 'p',
		COUNT(IF(b.`id_shifting`='S', b.`id_shifting`, NULL)) AS 's',
		COUNT(IF(b.`id_shifting`='M', b.`id_shifting`, NULL)) AS 'm',
		COUNT(IF(b.`id_shifting`='P', b.`id_shifting`, NULL)) * 17000 + COUNT(IF(b.`id_shifting`='S', b.`id_shifting`, NULL)) * 20000 + COUNT(IF(b.`id_shifting`='M', b.`id_shifting`, NULL)) * 23000 AS premishift,
		d.bpenginapan, d.uangmakan,d.uangsaku,d.angkutan,d.bandara,d.terminal, d.sppd ,d.tiket, d.totalsppd
		FROM `tbu_user` a

		LEFT JOIN (SELECT id_user, 
		SUM(IF(jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) AS harikerja,
		SUM(IF(jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) AS harilibur,
		SUM(IF(jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) AS haribesar,  
		SUM(IF(jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) * 15000 +
		SUM(IF(jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) * 25000 +
		SUM(IF(jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) * 30000 AS totallembur 
		FROM `tbu_lembur`  
		GROUP BY ID_USER ) c ON c.id_user = a.id

		LEFT JOIN (SELECT A.id_user,
		SUM((A.`jumlah`-1)*C.`penginapan`) AS bpenginapan,
		SUM((A.`jumlah`)*C.`biaya_makan`) AS uangmakan,
		SUM((A.`jumlah`)*C.`uang_saku`) AS uangsaku,
		SUM((A.`jumlah`)*C.`angkutan_setempat`) AS angkutan,
		SUM((A.`jumlah`-1)*C.`ke_bandara`) AS bandara,
		SUM((A.`jumlah`-1)*C.`ke_terminal`) AS terminal,
		SUM((((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+((A.`jumlah`-1)*C.`ke_terminal`))) AS sppd,
		SUM(A.`harga_tiket`) AS tiket,
		SUM((((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+((A.`jumlah`-1)*C.`ke_terminal`)+(A.`harga_tiket`))) AS totalsppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		GROUP BY id_user) d ON d.id_user = a.id

		LEFT JOIN `schedule` b ON b.user_id = a.id
		LEFT JOIN `organisasi` e ON e.id_personal = a.organisasi
		WHERE b.`tanggal` BETWEEN '".$tglawal."' AND '".$tglakhir."'
		GROUP BY a. id
		ORDER BY a.nama_lengkap";
		
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function approval_leave(){ //umar
		$q="SELECT A.`id`, A.`status`, B.id id_user, B.`no_induk`, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, 
		A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'12') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status`,
		E.`bidang`, E.`subbid`
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN organisasi E ON E.`id_personal` = B.`organisasi`
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (12 - SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='1' AND start_date BETWEEN '2021-01-01' AND '2021-12-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` = '1' or A.`jenis_leave` = '2' or A.`jenis_leave` = '3' or A.`jenis_leave` = '12' 
		ORDER BY A.`id` desc";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	

	public function approval_izin(){
		$q="SELECT A.`id`, B.id id_user, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave IN ('4','5','6','7','8','9','10') AND start_date BETWEEN '2019-11-01' AND '2020-11-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` IN ('4','5','6','7','8','9','10') ORDER BY A.`status`, A.`id` ASC";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function approval_official_exchange($user){
		$q="SELECT A.id, A.id_user, B.`nama_lengkap` AS nama_user, A.`id_pengganti`, C.`nama_lengkap` AS nama_pengganti, A.`shift_pemohon`, A.`shift_pengganti`, A.`tanggal`, A.`alasan`, A.`status`  
		FROM `tbu_tukarjadwal` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_user C ON A.`id_pengganti`=C.`id` 
		ORDER BY A.`id` desc";
		
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function show_schedule($date){ //umar
		$xdate=explode('|',$date);
		$q="SELECT a.user_id AS nip, b.nama_lengkap,a.id_shifting,tanggal FROM `schedule` a, `tbu_user` b WHERE a.user_id=b.id and tanggal between '".$xdate[0]."' and '".$xdate[1]."' order by 2 asc";
		$query = $this->db->query($q);
		
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function getmember(){
		$q="SELECT * FROM tbu_user LEFT JOIN organisasi ON id_personal = organisasi WHERE level='3' ORDER BY `nama_lengkap` ASC";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}


}