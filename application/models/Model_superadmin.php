<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_superadmin extends CI_Model {

	//new
	//
	public function review_approval($kode, $id, $user_id){				
		if($kode == '1'){//OVERTIME	
			$q="UPDATE `lembur` SET `status` = '3', `reviewed_by` = '".$user_id."', `reviewed_date` = NOW() WHERE id = '".$id."'";			
		}else if($kode == '2'){//OFFICIAL EXCHANGE
			$q="UPDATE `tunas` SET `status` = '3', `reviewed_by` = '".$user_id."', `reviewed_date` = NOW() WHERE id = '".$id."'";
		}else if($kode == '3'){//LEAVE
			$q="UPDATE `cuti` SET `status` = '3', `reviewed_by` = '".$user_id."', `reviewed_date` = NOW() WHERE id = '".$id."'";
		}
		$query = $this->db->query($q);
		if($query){
			return true;
		}else{
			return false;
		}
		$this->db->close();
	}

	function tambahcontact($dataarray){ //
		for($i=0; $i<count($dataarray); $i++)
		{
			$data = array(
				'email' 					=> $dataarray[$i]['email'],
				'password'    				=> md5($dataarray[$i]['password']),
				'level'    					=> $dataarray[$i]['level'],	
				'organisasi'				=> $dataarray[$i]['organisasi'],
				'no_induk'					=> $dataarray[$i]['no_induk'],
				'nama_lengkap'  			=> $dataarray[$i]['nama_lengkap'],
				'lokasi'  					=> $dataarray[$i]['lokasi'],
				'unit_kerja' 				=> $dataarray[$i]['unit_kerja'],
				'tempat_lahir' 				=> $dataarray[$i]['tempat_lahir'],
				'tgl_lahir' 				=> $dataarray[$i]['tgl_lahir'],
				'status_perkawinan' 		=> $dataarray[$i]['status_perkawinan'],
				'agama' 					=> $dataarray[$i]['agama'],
				'jenis_kelamin'   			=> $dataarray[$i]['jenis_kelamin'],
				'status_pajak'   			=> $dataarray[$i]['status_pajak'],
				'golongan_status_bpjs'   	=> $dataarray[$i]['golongan_status_bpjs'],
				'no_ktp' 					=> $dataarray[$i]['no_ktp'],
				'alamat' 					=> $dataarray[$i]['alamat'],
				'no_telepon' 				=> $dataarray[$i]['no_telepon'],
				'jabatan' 					=> $dataarray[$i]['jabatan'],
				// 'no_npwp' 					=> $dataarray[$i]['no_npwp'],
				// 'no_bpjs_ketenagakerjaan'	=> $dataarray[$i]['no_bpjs_ketenagakerjaan'],
				// 'no_bpjs_kesehatan' 		=> $dataarray[$i]['no_bpjs_kesehatan'],
				// 'no_rek' 					=> $dataarray[$i]['no_rek'],
				// 'bank' 						=> $dataarray[$i]['bank'],
				// 'nama_sesuai_rekening' 		=> $dataarray[$i]['nama_sesuai_rekening'],
			);
			$this->db->insert('tbu_user',$data);
		}
	}
	
	public function reject_approval($kode, $id, $user_id, $reason){				
		if($kode == '1'){//OVERTIME		
			$q="UPDATE `lembur` SET `status` = '2', `rejected_by` = '".$user_id."', `rejected_reason` = '".$reason."', `rejected_date` = NOW() WHERE id = '".$id."'";		
		}else if($kode == '2'){//OFFICIAL EXCHANGE
			$q="UPDATE `tunas` SET `status` = '2', `rejected_by` = '".$user_id."', `rejected_reason` = '".$reason."', `rejected_date` = NOW() WHERE id = '".$id."'";
		}else if($kode == '3'){//
			$q="UPDATE `cuti` SET `status` = '2', `rejected_by` = '".$user_id."', `rejected_reason` = '".$reason."', `rejected_date` = NOW() WHERE id = '".$id."'";
		}else if($kode == '4'){//
			$q="UPDATE `correction` SET `status` = '2', `rejected_by` = '".$user_id."', `rejected_reason` = '".$reason."', `rejected_date` = NOW() WHERE id = '".$id."'";
		}
		$query = $this->db->query($q);
		if($query){
			return true;
		}else{
			return false;
		}
		$this->db->close();
	}

	public function get_telat(){ //umar
		// $divisi = $this->session->userdata('organisasi');
		$q="SELECT C.nama_lengkap, B.lang, B.id_abs, B.os, B.ip, A.id_shifting,  B.jam jam_masuk, D.jam jam_pulang, B.ketepatan FROM `schedule` A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1') B ON A.`user_id` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2') D ON A.`user_id` = D.`id_user` AND A.`tanggal` = D.`tanggal`
		LEFT JOIN tbu_user C ON A.user_id = C.id
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) AND A.id_shifting  IN ('P','S','M','OH') AND A.tanggal = DATE(DATE_ADD(timestamp(now()), INTERVAL 12 HOUR)) AND C.active = '1' AND level='3' 
		ORDER BY B.`id_abs` ASC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_absen(){ //umar
		$q="SELECT 
		SUM(CASE WHEN kode_abs = '1' THEN 1 END) AS masuk,
		SUM(CASE WHEN kode_abs = '2' THEN 1 END) AS pulang
		FROM tbu_absensi;";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function getdasMember(){ //umar
		$q="SELECT 
		SUM(CASE WHEN organisasi = '1' THEN 1 END) AS must,
		SUM(CASE WHEN organisasi = 'ICON001' THEN 1 END) AS icon,
		SUM(CASE WHEN organisasi = 'ICON002' THEN 1 END) AS smart
		FROM tbu_user;";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_cuti(){
		$divisi = $this->session->userdata('organisasi');
		$q="SELECT a.* FROM `cuti` a 
		LEFT JOIN `employee` b ON a.id_user=b.id 
		WHERE b.organisasi='".$divisi."' AND STATUS = '3' AND (start_date BETWEEN CURDATE() AND DATE_ADD(CURRENT_DATE, INTERVAL 2 DAY) OR end_date BETWEEN CURDATE() AND DATE_ADD(CURRENT_DATE, INTERVAL 1 DAY)) AND STATUS ='3'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function getAllMember(){ //umar
		$q="SELECT id jumlah FROM tbu_user WHERE `active`='1' and level='3'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahCuti(){ //umar
		$q="SELECT * FROM tbu_cuti WHERE jenis_leave in ('1','2','3','12') AND status='3'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppCuti(){ //umar
		$q="SELECT * FROM tbu_cuti WHERE jenis_leave='1' AND status='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahSakit(){ //umar
		$q="SELECT * FROM tbu_cuti WHERE jenis_leave='11' AND status='3'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppSakit(){ //umar
		$q="SELECT * FROM tbu_cuti WHERE jenis_leave='11' AND status='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahLembur(){ //umar
		$q="SELECT * FROM tbu_lembur WHERE status='3'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppLembur(){ //umar
		$q="SELECT * FROM tbu_lembur WHERE status='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function gettukarjadwal(){ //umar
		$q="SELECT * FROM tbu_tukarjadwal WHERE status='0'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function get_jeniscuti(){
		$q="select * from leave_types WHERE tipe_ = '1'";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function client(){ //
		$q="select * from organisasi order by id_p desc";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function premi(){ //
		$q="select * from kode_absen";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function sppd(){ //
		$q="select * from tbu_kategori_sppd";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function organisasi(){ //
		$q="select * from organisasi";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function bidang(){ //
		$q="select * from tbu_bidang";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function subbid(){ //
		$q="select * from tbu_subbid";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function supervisor(){ //
		$q="select * from tbu_supervisor";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function manager(){ //
		$q="select * from tbu_manager";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function klien(){ //
		$q="select * from tbu_client";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function level(){ //
		$q="select * from level";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function karyawan(){ //
		$q="select * from tbu_user a
		left join organisasi b on a.organisasi=b.id_personal
		left join level c on c.id=a.level
		order by a.id desc";
		$query=$this->db->query($q); 
		return $query->result();
	}

	public function add_organisasi($data){ //
		$this->db->insert('organisasi', $data);
	}

	
	//closenew


	public function get_jadwal($user_id){
		$q="SELECT * FROM SCHEDULE A LEFT JOIN kode_absen B ON A.`id_shifting` = B.kode WHERE A.user_id = '".$user_id."' AND A.tanggal = CURRENT_DATE() AND (CASE WHEN B.kode = 'PO' THEN B.active = '1' AND (CASE WHEN DAYNAME(NOW()) = 'Friday' THEN B.tipe = 2 ELSE B.tipe = 1 END) ELSE TRUE END)";
		$query = $this->db->query($q);
		return $query->result();
	}


	public function absen_in($id_om, $browser, $os, $ip, $kode){
		$q="call proc_presensi('".$id_om."', '".$browser."', '".$os."', '".$ip."', '".$kode."')";
		$query = $this->db->query($q);
		return $query->result();
	}





	public function getJumlahTunas(){
		$divisi = $this->session->userdata('organisasi');
		$q="SELECT A.id FROM tunas A LEFT JOIN employee B ON A.`id_user`=B.`id` WHERE A.`status`='3' AND B.`organisasi`='".$divisi."' AND B.`active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	//new
	//
	public function approval_leave(){ //umar
		$q="SELECT A.`id`, B.id id_user, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'12') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (12 - SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti 
		WHERE STATUS = '3' and jenis_leave='1' AND start_date BETWEEN '2019-11-01' AND '2020-11-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` = '1' ORDER BY  A.`status`, A.`id` desc";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function approval_sakit(){ //umar
		$q="SELECT A.`id` AS id_user, B.id, B.`nama_lengkap`, C.`name` jenis_cuti, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.`keterangan`, A.`status`, A.`reviewed_by`, A.`approved_by`, A.`rejected_by`, A.`rejected_reason` 
		FROM `tbu_cuti` A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='2' AND start_date BETWEEN '2019-11-01' AND '2020-11-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` = 2  ORDER BY A.`status`, A.`tanggal_input` ASC";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

	public function approval_overtime(){
		$q="SELECT A.`id`, B.id id_user, B.`nama_lengkap`, A.`start_datetime`, A.`end_datetime`, A.`alasan`, A.`status` 
		FROM `tbu_lembur` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id`  
		ORDER BY A.`status`, A.`start_datetime` ASC";
		$query = $this->db->query($q);
		if($query){
			return $query->result();
			$this->db->close();
		}else{
			return "Error has occurred";
		}
	}

}