<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_adminmust extends CI_Model
{

	function tambahcontact($dataarray)
	{ //
		for ($i = 0; $i < count($dataarray); $i++) {
			$data = array(
				'nik' 					=> $dataarray[$i]['nik'],
				'bulan'    				=> $dataarray[$i]['bulan'],
				'tahun'    					=> $dataarray[$i]['tahun'],
				'gapok'				=> $dataarray[$i]['gapok'],
				'tunjangan'					=> $dataarray[$i]['tunjangan'],
				'u_shift'  			=> $dataarray[$i]['u_shift'],
				'u_lembur' 				=> $dataarray[$i]['u_lembur'],
				'thr' 				=> $dataarray[$i]['thr'],
				'u_cuti' 		=> $dataarray[$i]['u_cuti'],
				'reward' 					=> $dataarray[$i]['reward'],
				'koreksi'   			=> $dataarray[$i]['koreksi'],
				'total1'   			=> $dataarray[$i]['total1'],
				'bpjs_tk'   	=> $dataarray[$i]['bpjs_tk'],
				'bpjs_kes' 					=> $dataarray[$i]['bpjs_kes'],
				'jp' 					=> $dataarray[$i]['jp'],
				'pph21' 				=> $dataarray[$i]['pph21'],
				'total2' 					=> $dataarray[$i]['total2'],
			);
			$this->db->insert('slip', $data);
		}
	}

	public function karyawan()
	{ //
		$q = "select * from tbu_user a
		left join organisasi b on a.organisasi=b.id_personal
		left join level c on c.id=a.level
		left join tbu_manager d on d.id_manager=a.manajer
		where a.level='3' and a.active='1'
		order by b.spv asc, a.nama_lengkap asc";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function approval_official_exchange($user)
	{
		$divisi = $this->session->userdata('organisasi');

		$q = "SELECT A.id, A.id_user, B.`nama_lengkap` AS nama_user, A.`id_pengganti`, C.`nama_lengkap` AS nama_pengganti, A.`shift_pemohon`, A.`shift_pengganti`, A.`tanggal`, A.`alasan`, A.`status`  
		FROM `tbu_tukarjadwal` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_user C ON A.`id_pengganti`=C.`id` 
		ORDER BY A.`status`, A.`tanggal` ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function report_izin()
	{ //umar
		$q = "SELECT A.`id` AS id_user, B.id, B.`nama_lengkap`, C.`name` jenis_cuti, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.`keterangan`, A.`status`, A.`reviewed_by`, A.`approved_by`, A.`rejected_by`, A.`rejected_reason` 
		FROM `tbu_cuti` A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='4' AND start_date BETWEEN '2019-11-01' AND '2020-11-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` = 4 ORDER BY A.`status`, A.`tanggal_input` ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function nonpo2()
	{ //umar
		$q = "SELECT A.`id`, B.id id_user, B.`nama_lengkap`, A.`start_datetime`,jenis_lembur, A.`end_datetime`, A.`alasan`, A.`status`, A.`reviewed_by`, A.`approved_by`, A.`rejected_by`, A.`rejected_reason` 
		FROM `tbu_lembur` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		-- LEFT JOIN tbu_jenislembur C ON C.`id`=A.`jenis_lembur`
		WHERE A.`status` > 0 ORDER BY A.`status`, A.`start_datetime` ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function premi()
	{ //umar
		$bln = date("m");
		$q = "SELECT a.`no_induk`, a.`nama_lengkap`,d.`nama`, b.`bidang`, b.`subbid`, a.`jabatan`, c.`day1`, c.`day2`, c.`day3`, c.`day4`, c.`day5`, c.`day6`,
		c.`day7`, c.`day8`, c.`day9`, c.`day10`, c.`day11`, c.`day12`, c.`day13`, c.`day14`, c.`day15`, c.`day16`, c.`day17`, c.`day18`,
		c.`day19`, c.`day20`, c.`day21`, c.`day22`, c.`day23`, c.`day24`, c.`day25`, c.`day26`, c.`day27`, c.`day28`, c.`day29`, c.`day30`, c.`day31`,
		c.`p`, c.`s`, c.`m`, c.`premishift`
		FROM `tbu_user` a
		LEFT JOIN `organisasi` b ON b.id_personal=a.organisasi
		LEFT JOIN `tbu_shift` d ON d.id=a.shift
		LEFT JOIN (
		SELECT user_id, 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 1, `id_shifting`, NULL)) AS 'day1', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 2, `id_shifting`, NULL)) AS 'day2',
		GROUP_CONCAT(IF(DAY(`tanggal`) = 3, `id_shifting`, NULL)) AS 'day3', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 4, `id_shifting`, NULL)) AS 'day4', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 5, `id_shifting`, NULL)) AS 'day5', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 6, `id_shifting`, NULL)) AS 'day6', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 7, `id_shifting`, NULL)) AS 'day7', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 8, `id_shifting`, NULL)) AS 'day8', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 9, `id_shifting`, NULL)) AS 'day9', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 10, `id_shifting`, NULL)) AS 'day10',
		GROUP_CONCAT(IF(DAY(`tanggal`) = 11, `id_shifting`, NULL)) AS 'day11', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 12, `id_shifting`, NULL)) AS 'day12', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 13, `id_shifting`, NULL)) AS 'day13', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 14, `id_shifting`, NULL)) AS 'day14', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 15, `id_shifting`, NULL)) AS 'day15', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 16, `id_shifting`, NULL)) AS 'day16', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 17, `id_shifting`, NULL)) AS 'day17', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 18, `id_shifting`, NULL)) AS 'day18', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 19, `id_shifting`, NULL)) AS 'day19', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 20, `id_shifting`, NULL)) AS 'day20', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 21, `id_shifting`, NULL)) AS 'day21', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 22, `id_shifting`, NULL)) AS 'day22', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 23, `id_shifting`, NULL)) AS 'day23', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 24, `id_shifting`, NULL)) AS 'day24', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 25, `id_shifting`, NULL)) AS 'day25', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 26, `id_shifting`, NULL)) AS 'day26', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 27, `id_shifting`, NULL)) AS 'day27', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 28, `id_shifting`, NULL)) AS 'day28', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 29, `id_shifting`, NULL)) AS 'day29', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 30, `id_shifting`, NULL)) AS 'day30',  
		GROUP_CONCAT(IF(DAY(`tanggal`) = 31, `id_shifting`, NULL)) AS 'day31' ,
		COUNT(IF(`id_shifting`='P' OR `id_shifting`='M1', `id_shifting`, NULL)) AS 'p',
		COUNT(IF(`id_shifting`='S' OR `id_shifting`='M2', `id_shifting`, NULL)) AS 's',
		COUNT(IF(`id_shifting`='M' OR `id_shifting`='M3', `id_shifting`, NULL)) AS 'm',
		COUNT(IF(`id_shifting`='P' OR `id_shifting`='M1', `id_shifting`, NULL)) * 17000 + COUNT(IF(`id_shifting`='S' OR `id_shifting`='M2', `id_shifting`, NULL)) * 20000 + 
		COUNT(IF(`id_shifting`='M' OR `id_shifting`='M3', `id_shifting`, NULL)) * 23000 AS premishift
		FROM `schedule` 
		WHERE MONTH(`tanggal`) = '" . $bln . "'
		GROUP BY user_id
		) c ON c.user_id=a.id
		WHERE a.`level` = '3' AND a.`active` = '1'
		GROUP BY a.`id`
		ORDER BY a.`organisasi` ASC, a.`nama_lengkap` ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function bauknonpo()
	{ //umar
		$bln = date("m");
		$q = "SELECT a.id, a.`no_induk`, a.`nama_lengkap`,d.`nama`, b.`bidang`, b.`subbid`, a.`jabatan`, c.`p`, c.`s`, c.`m`, 
		c.`premishift`, r.harikerja, r.harilibur, r.haribesar, r.totallembur, r.tanggal_lembur, u.totalsppd,
		COALESCE(c.`premishift`,0)+ COALESCE(r.`totallembur`,0)+ COALESCE(u.`totalsppd`,0) AS totalall
		FROM `tbu_user` a
		LEFT JOIN `organisasi` b ON b.id_personal=a.organisasi
		LEFT JOIN `tbu_shift` d ON d.id=a.shift
		LEFT JOIN (
		SELECT id_user, 
		COUNT(IF(`realisasi`='P' , `realisasi`, NULL)) AS 'p',
		COUNT(IF(`realisasi`='S' , `realisasi`, NULL)) AS 's',
		COUNT(IF(`realisasi`='M' , `realisasi`, NULL)) AS 'm',
		COUNT(IF(`realisasi`='P' , `realisasi`, NULL)) * 17000 + COUNT(IF(`realisasi`='S' OR `realisasi`='M2', `realisasi`, NULL)) * 20000 + 
		COUNT(IF(`realisasi`='M' , `realisasi`, NULL)) * 23000 AS premishift
		FROM `tbu_absensi` 
		WHERE kode_abs='1' and MONTH(`tanggal`) = '" . $bln . "' GROUP BY id_user
		) c ON c.id_user=a.id
		LEFT JOIN (SELECT a.id_user, b.no_induk,b.nama_lengkap,c.`bidang`,c.subbid, b.`jabatan`, CONCAT_WS(' s/d ',a.start_datetime, a.end_datetime) AS tanggal_lembur,
		SUM(IF(a.jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) AS harikerja,
		SUM(IF(a.jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) AS harilibur,
		SUM(IF(a.jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) AS haribesar,  
		SUM(IF(a.jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) * 15000 +
		SUM(IF(a.jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) * 25000 +
		SUM(IF(a.jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) * 30000 AS totallembur
		FROM `tbu_lembur` a
		LEFT JOIN tbu_user b ON b.id=a.id_user
		LEFT JOIN organisasi c ON c.id_personal=b.organisasi
		WHERE b.level='3' AND a.status='3' and MONTH(`insert`) = '" . $bln . "' GROUP BY a.id_user) r ON r.id_user=a.id
		LEFT JOIN (SELECT A.id_user,B.no_induk, B.nama_lengkap,D.`bidang`,D.subbid, B.`jabatan`, CONCAT_WS(' s/d ',tanggal_pergi, tanggal_pulang) AS TANGGAL, jumlah, tujuan, pengajuan, transportasi,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` AS bpenginapan,
		((A.`jumlah`)*C.`biaya_makan`) AS uangmakan,
		((A.`jumlah`)*C.`uang_saku`) AS uangsaku,
		((A.`jumlah`)*C.`angkutan_setempat`) AS angkutan,
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) AS bandara,
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) AS terminal,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` + ((A.`jumlah`)*C.`biaya_makan`) +
		((A.`jumlah`)*C.`uang_saku`) + ((A.`jumlah`)*C.`angkutan_setempat`) + (IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) +
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) AS nominalsppd,
		(A.`harga_tiket`) AS tiket,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` + ((A.`jumlah`)*C.`biaya_makan`) +
		((A.`jumlah`)*C.`uang_saku`) + ((A.`jumlah`)*C.`angkutan_setempat`) + (IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) +
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) + (A.`harga_tiket`) AS totalsppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN tbu_user B ON B.id=A.id_user
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		LEFT JOIN organisasi D ON D.id_personal=B.organisasi
		WHERE B.level='3' and MONTH(`insert`) = '" . $bln . "'
		GROUP BY A.id_dinas
		ORDER BY D.bidang ASC, B.nama_lengkap ASC) u ON u.id_user=a.id
		WHERE a.`level` = '3' AND a.`active` = '1' 
		GROUP BY a.`id`
		ORDER BY a.`organisasi` ASC, a.`nama_lengkap` ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function pencarianpremi($tglawal, $tglakhir)
	{ //umar
		// $bln = date("m");
		$q = "SELECT a.`no_induk`, a.`nama_lengkap`,d.`nama`, b.`bidang`, b.`subbid`, a.`jabatan`, c.`day1`, c.`day2`, c.`day3`, c.`day4`, c.`day5`, c.`day6`,
		c.`day7`, c.`day8`, c.`day9`, c.`day10`, c.`day11`, c.`day12`, c.`day13`, c.`day14`, c.`day15`, c.`day16`, c.`day17`, c.`day18`,
		c.`day19`, c.`day20`, c.`day21`, c.`day22`, c.`day23`, c.`day24`, c.`day25`, c.`day26`, c.`day27`, c.`day28`, c.`day29`, c.`day30`, c.`day31`,
		c.`p`, c.`s`, c.`m`, c.`premishift`
		FROM `tbu_user` a
		LEFT JOIN `organisasi` b ON b.id_personal=a.organisasi
		LEFT JOIN `tbu_shift` d ON d.id=a.shift
		LEFT JOIN (
		SELECT user_id, 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 1, `id_shifting`, NULL)) AS 'day1', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 2, `id_shifting`, NULL)) AS 'day2',
		GROUP_CONCAT(IF(DAY(`tanggal`) = 3, `id_shifting`, NULL)) AS 'day3', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 4, `id_shifting`, NULL)) AS 'day4', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 5, `id_shifting`, NULL)) AS 'day5', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 6, `id_shifting`, NULL)) AS 'day6', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 7, `id_shifting`, NULL)) AS 'day7', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 8, `id_shifting`, NULL)) AS 'day8', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 9, `id_shifting`, NULL)) AS 'day9', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 10, `id_shifting`, NULL)) AS 'day10',
		GROUP_CONCAT(IF(DAY(`tanggal`) = 11, `id_shifting`, NULL)) AS 'day11', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 12, `id_shifting`, NULL)) AS 'day12', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 13, `id_shifting`, NULL)) AS 'day13', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 14, `id_shifting`, NULL)) AS 'day14', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 15, `id_shifting`, NULL)) AS 'day15', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 16, `id_shifting`, NULL)) AS 'day16', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 17, `id_shifting`, NULL)) AS 'day17', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 18, `id_shifting`, NULL)) AS 'day18', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 19, `id_shifting`, NULL)) AS 'day19', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 20, `id_shifting`, NULL)) AS 'day20', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 21, `id_shifting`, NULL)) AS 'day21', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 22, `id_shifting`, NULL)) AS 'day22', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 23, `id_shifting`, NULL)) AS 'day23', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 24, `id_shifting`, NULL)) AS 'day24', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 25, `id_shifting`, NULL)) AS 'day25', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 26, `id_shifting`, NULL)) AS 'day26', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 27, `id_shifting`, NULL)) AS 'day27', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 28, `id_shifting`, NULL)) AS 'day28', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 29, `id_shifting`, NULL)) AS 'day29', 
		GROUP_CONCAT(IF(DAY(`tanggal`) = 30, `id_shifting`, NULL)) AS 'day30',  
		GROUP_CONCAT(IF(DAY(`tanggal`) = 31, `id_shifting`, NULL)) AS 'day31' ,
		COUNT(IF(`id_shifting`='P' OR `id_shifting`='M1', `id_shifting`, NULL)) AS 'p',
		COUNT(IF(`id_shifting`='S' OR `id_shifting`='M2', `id_shifting`, NULL)) AS 's',
		COUNT(IF(`id_shifting`='M' OR `id_shifting`='M3', `id_shifting`, NULL)) AS 'm',
		COUNT(IF(`id_shifting`='P' OR `id_shifting`='M1', `id_shifting`, NULL)) * 17000 + COUNT(IF(`id_shifting`='S' OR `id_shifting`='M2', `id_shifting`, NULL)) * 20000 + 
		COUNT(IF(`id_shifting`='M' OR `id_shifting`='M3', `id_shifting`, NULL)) * 23000 AS premishift
		FROM `schedule` 
		WHERE `tanggal` BETWEEN '" . $tglawal . "' AND '" . $tglakhir . "' 
		GROUP BY user_id
		) c ON c.user_id=a.id
		WHERE a.`level` = '3' AND a.`active` = '1'
		GROUP BY a.`id`
		ORDER BY a.`organisasi` ASC, a.`nama_lengkap` ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function premi2()
	{ //umar
		$bln = date("m");
		$q = "SELECT b.no_induk,b.`nama_lengkap`,c.`bidang`,c.subbid, b.`jabatan`,
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 1, a.`id_shifting`, NULL)) AS 'day1', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 2, a.`id_shifting`, NULL)) AS 'day2', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 3, a.`id_shifting`, NULL)) AS 'day3', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 4, a.`id_shifting`, NULL)) AS 'day4', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 5, a.`id_shifting`, NULL)) AS 'day5', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 6, a.`id_shifting`, NULL)) AS 'day6', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 7, a.`id_shifting`, NULL)) AS 'day7', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 8, a.`id_shifting`, NULL)) AS 'day8', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 9, a.`id_shifting`, NULL)) AS 'day9', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 10, a.`id_shifting`, NULL)) AS 'day10',
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 11, a.`id_shifting`, NULL)) AS 'day11', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 12, a.`id_shifting`, NULL)) AS 'day12', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 13, a.`id_shifting`, NULL)) AS 'day13', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 14, a.`id_shifting`, NULL)) AS 'day14', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 15, a.`id_shifting`, NULL)) AS 'day15', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 16, a.`id_shifting`, NULL)) AS 'day16', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 17, a.`id_shifting`, NULL)) AS 'day17', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 18, a.`id_shifting`, NULL)) AS 'day18', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 19, a.`id_shifting`, NULL)) AS 'day19', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 20, a.`id_shifting`, NULL)) AS 'day20', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 21, a.`id_shifting`, NULL)) AS 'day21', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 22, a.`id_shifting`, NULL)) AS 'day22', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 23, a.`id_shifting`, NULL)) AS 'day23', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 24, a.`id_shifting`, NULL)) AS 'day24', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 25, a.`id_shifting`, NULL)) AS 'day25', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 26, a.`id_shifting`, NULL)) AS 'day26', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 27, a.`id_shifting`, NULL)) AS 'day27', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 28, a.`id_shifting`, NULL)) AS 'day28', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 29, a.`id_shifting`, NULL)) AS 'day29', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 30, a.`id_shifting`, NULL)) AS 'day30',  
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 31, a.`id_shifting`, NULL)) AS 'day31',
		COUNT(IF(a.`id_shifting`='P', a.`id_shifting`, NULL)) AS 'p',
		COUNT(IF(a.`id_shifting`='S', a.`id_shifting`, NULL)) AS 's',
		COUNT(IF(a.`id_shifting`='M', a.`id_shifting`, NULL)) AS 'm',
		COUNT(IF(a.`id_shifting`='P', a.`id_shifting`, NULL)) * 17000 + COUNT(IF(a.`id_shifting`='S', a.`id_shifting`, NULL)) * 20000 + 
		COUNT(IF(a.`id_shifting`='M', a.`id_shifting`, NULL)) * 23000 AS premishift
		FROM `schedule` a
		LEFT JOIN tbu_user b ON b.id=a.user_id
		LEFT JOIN organisasi c ON c.id_personal=b.organisasi
		WHERE b.level='3' and MONTH(a.`tanggal`) = '" . $bln . "' 
		GROUP BY a.`user_id`
		ORDER BY c.bidang ASC, b.nama_lengkap ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function pencarianpremi2($tglawal, $tglakhir)
	{ //umar
		// $bln = date("m");
		$q = "SELECT b.no_induk,b.`nama_lengkap`,c.`bidang`,c.subbid, b.`jabatan`,
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 1, a.`id_shifting`, NULL)) AS 'day1', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 2, a.`id_shifting`, NULL)) AS 'day2', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 3, a.`id_shifting`, NULL)) AS 'day3', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 4, a.`id_shifting`, NULL)) AS 'day4', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 5, a.`id_shifting`, NULL)) AS 'day5', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 6, a.`id_shifting`, NULL)) AS 'day6', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 7, a.`id_shifting`, NULL)) AS 'day7', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 8, a.`id_shifting`, NULL)) AS 'day8', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 9, a.`id_shifting`, NULL)) AS 'day9', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 10, a.`id_shifting`, NULL)) AS 'day10',
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 11, a.`id_shifting`, NULL)) AS 'day11', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 12, a.`id_shifting`, NULL)) AS 'day12', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 13, a.`id_shifting`, NULL)) AS 'day13', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 14, a.`id_shifting`, NULL)) AS 'day14', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 15, a.`id_shifting`, NULL)) AS 'day15', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 16, a.`id_shifting`, NULL)) AS 'day16', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 17, a.`id_shifting`, NULL)) AS 'day17', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 18, a.`id_shifting`, NULL)) AS 'day18', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 19, a.`id_shifting`, NULL)) AS 'day19', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 20, a.`id_shifting`, NULL)) AS 'day20', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 21, a.`id_shifting`, NULL)) AS 'day21', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 22, a.`id_shifting`, NULL)) AS 'day22', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 23, a.`id_shifting`, NULL)) AS 'day23', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 24, a.`id_shifting`, NULL)) AS 'day24', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 25, a.`id_shifting`, NULL)) AS 'day25', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 26, a.`id_shifting`, NULL)) AS 'day26', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 27, a.`id_shifting`, NULL)) AS 'day27', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 28, a.`id_shifting`, NULL)) AS 'day28', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 29, a.`id_shifting`, NULL)) AS 'day29', 
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 30, a.`id_shifting`, NULL)) AS 'day30',  
		GROUP_CONCAT(IF(DAY(a.`tanggal`) = 31, a.`id_shifting`, NULL)) AS 'day31',
		COUNT(IF(a.`id_shifting`='P', a.`id_shifting`, NULL)) AS 'p',
		COUNT(IF(a.`id_shifting`='S', a.`id_shifting`, NULL)) AS 's',
		COUNT(IF(a.`id_shifting`='M', a.`id_shifting`, NULL)) AS 'm',
		COUNT(IF(a.`id_shifting`='P', a.`id_shifting`, NULL)) * 17000 + COUNT(IF(a.`id_shifting`='S', a.`id_shifting`, NULL)) * 20000 + 
		COUNT(IF(a.`id_shifting`='M', a.`id_shifting`, NULL)) * 23000 AS premishift
		FROM `schedule` a
		LEFT JOIN tbu_user b ON b.id=a.user_id
		LEFT JOIN organisasi c ON c.id_personal=b.organisasi
		WHERE b.level='3' and a.`tanggal` BETWEEN '" . $tglawal . "' AND '" . $tglakhir . "' 
		GROUP BY a.`user_id`
		ORDER BY c.bidang ASC, b.nama_lengkap ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function pencarianlembur($tglawal, $tglakhir)
	{ //umar
		$bln = date("m");
		$q = "SELECT DATE_ADD(a.`reviewed_date`, INTERVAL 11 HOUR) as umar, b.no_induk,b.nama_lengkap,c.`bidang`,c.subbid, b.`jabatan`, CONCAT_WS(' s/d ',a.start_datetime, a.end_datetime) AS tanggal_lembur,
		(IF(a.jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) AS harikerja,
		(IF(a.jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) AS harilibur,
		(IF(a.jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) AS haribesar,  
		(IF(a.jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) * 15000 +
		(IF(a.jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) * 25000 +
		(IF(a.jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) * 30000 AS totallembur 
		FROM `tbu_lembur` a
		LEFT JOIN tbu_user b ON b.id=a.id_user
		LEFT JOIN organisasi c ON c.id_personal=b.organisasi
		WHERE b.level='3' and a.status='3' and a.`insert` BETWEEN '" . $tglawal . "' AND '" . $tglakhir . "' 
		GROUP BY a.`id`
		ORDER BY c.bidang ASC, b.nama_lengkap ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function bauklembur()
	{ //umar
		$bln = date("m");
		$q = "SELECT DATE_ADD(a.`reviewed_date`, INTERVAL 11 HOUR) as umar, b.no_induk,b.nama_lengkap,c.`bidang`,c.subbid, b.`jabatan`, CONCAT_WS(' s/d ',a.start_datetime, a.end_datetime) AS tanggal_lembur,
		(IF(a.jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) AS harikerja,
		(IF(a.jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) AS harilibur,
		(IF(a.jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) AS haribesar,  
		(IF(a.jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) * 15000 +
		(IF(a.jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) * 25000 +
		(IF(a.jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(a.`end_datetime`, a.`start_datetime`), '%h')), 0)) * 30000 AS totallembur 
		FROM `tbu_lembur` a
		LEFT JOIN tbu_user b ON b.id=a.id_user
		LEFT JOIN organisasi c ON c.id_personal=b.organisasi
		WHERE b.level='3' and a.status='3' and MONTH(a.`insert`) = '" . $bln . "' 
		GROUP BY a.`id`
		ORDER BY c.bidang ASC, b.nama_lengkap ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function bauksppd()
	{ //umar
		$bln = date("m");
		$q = "SELECT DATE_ADD(A.`reviewed_date`, INTERVAL 11 HOUR) as umar,B.no_induk, B.nama_lengkap,D.`bidang`,D.subbid, B.`jabatan`, CONCAT_WS(' s/d ',tanggal_pergi, tanggal_pulang) AS TANGGAL, jumlah, tujuan, pengajuan, transportasi,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` AS bpenginapan,
		((A.`jumlah`)*C.`biaya_makan`) AS uangmakan,
		((A.`jumlah`)*C.`uang_saku`) AS uangsaku,
		((A.`jumlah`)*C.`angkutan_setempat`) AS angkutan,
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) AS bandara,
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) AS terminal,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` + ((A.`jumlah`)*C.`biaya_makan`) +
		((A.`jumlah`)*C.`uang_saku`) + ((A.`jumlah`)*C.`angkutan_setempat`) + (IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) +
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) AS nominalsppd,
		(A.`harga_tiket`) AS tiket,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` + ((A.`jumlah`)*C.`biaya_makan`) +
		((A.`jumlah`)*C.`uang_saku`) + ((A.`jumlah`)*C.`angkutan_setempat`) + (IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) +
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) + (A.`harga_tiket`) AS totalsppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN tbu_user B ON B.id=A.id_user
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		LEFT JOIN organisasi D ON D.id_personal=B.organisasi
		WHERE B.level='3' and MONTH(A.`insert`) = '" . $bln . "' 
		GROUP BY A.id_dinas
		ORDER BY D.bidang ASC, B.nama_lengkap ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function nonpo()
	{ //umar
		$bln = date("m");
		$q = "SELECT a.nama_lengkap, e.bidang, e.subbid, a.jabatan, 
		c.harikerja, c.harilibur,c.haribesar, c.totallembur,
		COUNT(IF(b.`id_shifting`='P', b.`id_shifting`, NULL)) AS 'p',
		COUNT(IF(b.`id_shifting`='S', b.`id_shifting`, NULL)) AS 's',
		COUNT(IF(b.`id_shifting`='M', b.`id_shifting`, NULL)) AS 'm',
		COUNT(IF(b.`id_shifting`='P', b.`id_shifting`, NULL)) * 17000 + COUNT(IF(b.`id_shifting`='S', b.`id_shifting`, NULL)) * 20000 + COUNT(IF(b.`id_shifting`='M', b.`id_shifting`, NULL)) * 23000 AS premishift,
		d.bpenginapan, d.uangmakan,d.uangsaku,d.angkutan,d.bandara,d.terminal, d.sppd ,d.tiket, d.totalsppd
		FROM `tbu_user` a

		LEFT JOIN (SELECT id_user, 
		SUM(IF(jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) AS harikerja,
		SUM(IF(jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) AS harilibur,
		SUM(IF(jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) AS haribesar,  
		SUM(IF(jenis_lembur='1', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) * 15000 +
		SUM(IF(jenis_lembur='2', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) * 25000 +
		SUM(IF(jenis_lembur='3', (TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h')), 0)) * 30000 AS totallembur 
		FROM `tbu_lembur`  
		GROUP BY ID_USER ) c ON c.id_user = a.id

		LEFT JOIN (SELECT A.id_user,
		SUM((A.`jumlah`-1)*C.`penginapan`) AS bpenginapan,
		SUM((A.`jumlah`)*C.`biaya_makan`) AS uangmakan,
		SUM((A.`jumlah`)*C.`uang_saku`) AS uangsaku,
		SUM((A.`jumlah`)*C.`angkutan_setempat`) AS angkutan,
		SUM((A.`jumlah`-1)*C.`ke_bandara`) AS bandara,
		SUM((A.`jumlah`-1)*C.`ke_terminal`) AS terminal,
		SUM((((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+((A.`jumlah`-1)*C.`ke_terminal`))) AS sppd,
		SUM(A.`harga_tiket`) AS tiket,
		SUM((((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+((A.`jumlah`-1)*C.`ke_terminal`)+(A.`harga_tiket`))) AS totalsppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		GROUP BY id_user) d ON d.id_user = a.id

		LEFT JOIN `schedule` b ON b.user_id = a.id
		LEFT JOIN `organisasi` e ON e.id_personal = a.organisasi
		where MONTH(b.`tanggal`) = '" . $bln . "' 
		GROUP BY a. id
		ORDER BY a.nama_lengkap";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function pagu2()
	{ //
		$q = "select * from tbu_pagu a left join tbu_bidang b on b.id_bidang=a.id_organisasi";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function jasa2()
	{ //
		$q = "select * from tbu_jasa a left join tbu_user b on b.id=a.id_user left join organisasi c on c.id_personal=b.organisasi";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function jasa()
	{ //
		$q = "SELECT a.no_induk, a.`nama_lengkap`, b.`bidang`, a.`jabatan`, a.`qty_po`, a.`qty_real`, a.`hk_po`,a.`hk_real`,
		c.`hargasatuan`*(a.`hk_real`/a.`hk_po`) AS hargaorang,
		c.`fee`*(a.`hk_real`/a.`hk_po`) AS fee,
		(c.`hargasatuan`*(a.`hk_real`/a.`hk_po`)+c.`fee`*(a.`hk_real`/a.`hk_po`)) AS satuanjasa
		FROM `tbu_user` a
		LEFT JOIN `organisasi` b ON b.id_personal=a.organisasi
		LEFT JOIN `tbu_perhitungan` c ON c.id_jabatan=a.jabatan
		WHERE a.level = '3' AND a.active = '1'
		ORDER BY b.bidang desc, a.jabatan desc";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function pagu()
	{ //
		$q = "SELECT e.biaya_jasa,e.biaya_non_po, f. bidang,
		COUNT(IF(b.`id_shifting`='P', b.`id_shifting`, NULL)) * 17000 + COUNT(IF(b.`id_shifting`='S', b.`id_shifting`, NULL)) * 20000 + COUNT(IF(b.`id_shifting`='M', b.`id_shifting`, NULL)) * 23000 AS premishift, c.lembur, d.sppd, g.baukjasa
		FROM `tbu_user` a
		LEFT JOIN (SELECT id_user, SUM(TIME_FORMAT(TIMEDIFF(`end_datetime`, `start_datetime`), '%h'))*15000 AS lembur FROM `tbu_lembur`  GROUP BY ID_USER ) c ON c.id_user = a.id
		LEFT JOIN (SELECT id_user, SUM(`jumlah_harga`) AS baukjasa FROM `tbu_jasa`  GROUP BY ID_USER ) g ON g.id_user = a.id
		LEFT JOIN (SELECT A.id_user, 
		SUM((((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+(A.`harga_tiket`))) AS sppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		GROUP BY id_user) d ON d.id_user = a.id
		LEFT JOIN `schedule` b ON b.user_id = a.id
		LEFT JOIN `tbu_pagu` e ON e.id_organisasi = a.organisasi
		LEFT JOIN `organisasi` f ON f.id_personal = a.organisasi
		WHERE b.`tanggal` BETWEEN '2020-03-01' AND '2020-04-30'
		GROUP BY a. organisasi
		ORDER BY a.nama_lengkap";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function bidang()
	{ //
		$q = "select * from tbu_bidang";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function user()
	{ //
		$q = "select * from tbu_user";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function ogjasa()
	{ //umar
		$q = "SELECT * FROM tbu_user a LEFT JOIN organisasi b ON b.id_personal = a.organisasi where a.level='3' group by b.bidang";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function getmember()
	{ //umar
		$q = "SELECT * FROM tbu_user LEFT JOIN organisasi ON id_personal = organisasi WHERE level='3' ORDER BY `nama_lengkap` ASC";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function get_telat2()
	{ //umar
		$q = "SELECT C.nama_lengkap, B.lang, B.id_abs, B.os, B.ip, A.id_shifting,  B.jam jam_masuk, D.jam jam_pulang, B.ketepatan FROM `schedule` A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1') B ON A.`user_id` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2') D ON A.`user_id` = D.`id_user` AND A.`tanggal` = D.`tanggal`
		LEFT JOIN tbu_user C ON A.user_id = C.id
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) AND A.id_shifting  IN ('P','S','M','PO') AND A.tanggal = CURRENT_DATE() AND C.active = '1' AND level='3' 
		ORDER BY B.`id_abs` ASC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_correctionrevisi()
	{
		$q = "SELECT DISTINCT E.tanggal,A.tanggal, D.nama_lengkap, D.lokasi, A.os, E.id_shifting,  B.jam jam_masuk, C.jam jam_pulang,B.fotoabsen masuk, C.fotoabsen keluar FROM tbu_absensi A
		LEFT JOIN (SELECT  fotoabsen, kode_abs, jam, id_user, tanggal FROM tbu_absensi WHERE kode_abs = '1'  ) B ON A.`id_user` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT  fotoabsen, kode_abs, jam, id_user, tanggal FROM tbu_absensi WHERE kode_abs = '2'  ) C ON A.`id_user` = C.`id_user` AND A.`tanggal` = C.`tanggal`
		LEFT JOIN tbu_user D ON A.id_user = D.id
		LEFT JOIN (SELECT  tanggal,id_shifting, user_id FROM `schedule` E) E ON E.user_id=A.id_user AND E.tanggal=A.tanggal
		WHERE  A.Tanggal BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()),INTERVAL 1 DAY),INTERVAL -1 MONTH) 
		AND DATE_ADD(TIMESTAMP(NOW()), INTERVAL 7 HOUR)
		GROUP BY A.id_abs
		ORDER BY A.tanggal DESC, B.jam DESC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_correction()
	{
		// $bln = date("Y,m,d");
		$q = "SELECT DISTINCT A.tanggal, D.nama_lengkap, D.lokasi, A.os, A.insert, A.ip, C.`pekerjaan`,  B.jam jam_masuk, C.jam jam_pulang,B.fotoabsen masuk, C.fotoabsen keluar FROM tbu_absensi A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1'  GROUP BY id_abs) B ON A.`id_user` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2'  GROUP BY id_abs) C ON A.`id_user` = C.`id_user` AND A.`tanggal` = C.`tanggal`
		LEFT JOIN tbu_user D ON A.id_user = D.id
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) AND A.insert BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()),INTERVAL 1 DAY),INTERVAL -1 MONTH) 
		AND DATE_ADD(TIMESTAMP(NOW()), INTERVAL 7 HOUR) 
		GROUP BY B.fotoabsen
		ORDER BY A.tanggal DESC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function pencarianbulanan($tglawal, $tglakhir)
	{ //umarkoto
		$q = "SELECT DISTINCT A.tanggal, D.nama_lengkap, D.lokasi, A.os, A.ip, B.jam jam_masuk, C.jam jam_pulang,B.fotoabsen masuk, C.fotoabsen keluar FROM tbu_absensi A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1'  GROUP BY id_abs) B ON A.`id_user` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2'  GROUP BY id_abs) C ON A.`id_user` = C.`id_user` AND A.`tanggal` = C.`tanggal`
		LEFT JOIN tbu_user D ON A.id_user = D.id
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) 
		AND A.Tanggal BETWEEN '" . $tglawal . "' AND '" . $tglakhir . "'
		GROUP BY B.fotoabsen
		ORDER BY A.tanggal DESC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function approval_leave()
	{ //umar
		$q = "SELECT A.`id`, A.`status`, B.id id_user, B.`no_induk`, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'12') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (14 - SUM(jumlah_leave)) sisa_cuti FROM tbu_cuti WHERE STATUS = '3' AND start_date BETWEEN '2018-09-01' AND '2019-08-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` = '1' or A.`jenis_leave` = '2' or A.`jenis_leave` = '3' or A.`jenis_leave` = '12' ORDER BY A.`status` ASC";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function approval_sakit()
	{ //umar
		$q = "SELECT A.`id`, B.id id_user, B.`nama_lengkap`, C.`name` jenis_leave, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.alamat_cuti, A.`keterangan`, A.`status` 
		FROM `tbu_cuti` A 
		LEFT JOIN tbu_user B ON A.`id_user`= B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='2' AND start_date BETWEEN '2021-01-01' AND '2021-12-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` = '11' 
		ORDER BY A.`id` desc";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function rekap()
	{ //umar
		$q = "SELECT B.`nama_lengkap`,
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 1, A.`id_shifting`, NULL)) AS 'day1', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 2, A.`id_shifting`, NULL)) AS 'day2', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 3, A.`id_shifting`, NULL)) AS 'day3', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 4, A.`id_shifting`, NULL)) AS 'day4', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 5, A.`id_shifting`, NULL)) AS 'day5', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 6, A.`id_shifting`, NULL)) AS 'day6', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 7, A.`id_shifting`, NULL)) AS 'day7', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 8, A.`id_shifting`, NULL)) AS 'day8', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 9, A.`id_shifting`, NULL)) AS 'day9', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 10, A.`id_shifting`, NULL)) AS 'day10',
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 11, A.`id_shifting`, NULL)) AS 'day11', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 12, A.`id_shifting`, NULL)) AS 'day12', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 13, A.`id_shifting`, NULL)) AS 'day13', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 14, A.`id_shifting`, NULL)) AS 'day14', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 15, A.`id_shifting`, NULL)) AS 'day15', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 16, A.`id_shifting`, NULL)) AS 'day16', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 17, A.`id_shifting`, NULL)) AS 'day17', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 18, A.`id_shifting`, NULL)) AS 'day18', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 19, A.`id_shifting`, NULL)) AS 'day19', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 20, A.`id_shifting`, NULL)) AS 'day20', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 21, A.`id_shifting`, NULL)) AS 'day21', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 22, A.`id_shifting`, NULL)) AS 'day22', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 23, A.`id_shifting`, NULL)) AS 'day23', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 24, A.`id_shifting`, NULL)) AS 'day24', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 25, A.`id_shifting`, NULL)) AS 'day25', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 26, A.`id_shifting`, NULL)) AS 'day26', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 27, A.`id_shifting`, NULL)) AS 'day27', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 28, A.`id_shifting`, NULL)) AS 'day28', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 29, A.`id_shifting`, NULL)) AS 'day29', 
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 30, A.`id_shifting`, NULL)) AS 'day30',  
		GROUP_CONCAT(IF(DAY(A.`tanggal`) = 31, A.`id_shifting`, NULL)) AS 'day31', 
		COUNT(IF(A.`id_shifting` IN ('P','S','M'), A.`id_shifting`, NULL)) AS 'H',
		COUNT(IF(A.`id_shifting`='P', A.`id_shifting`, NULL)) AS 'P',
		COUNT(IF(A.`id_shifting`='S', A.`id_shifting`, NULL)) AS 'S',
		COUNT(IF(A.`id_shifting`='M', A.`id_shifting`, NULL)) AS 'M',
		COUNT(IF(A.`id_shifting`='L', A.`id_shifting`, NULL)) AS 'L',

		COUNT(IF(A.`id_shifting`='I', A.`id_shifting`, NULL)) AS 'I',
		COUNT(IF(A.`id_shifting`='SK', A.`id_shifting`, NULL)) AS 'SK',
		COUNT(IF(A.`id_shifting`='C', A.`id_shifting`, NULL)) AS 'C',
		COUNT(IF(A.`id_shifting`='A', A.`id_shifting`, NULL)) AS 'A',
		COUNT(IF(A.`id_shifting`='T', A.`id_shifting`, NULL)) AS 'T',
		
		COUNT(IF(A.`id_shifting`='P', A.`id_shifting`, NULL)) * 17000 AS 'TP',
		COUNT(IF(A.`id_shifting`='S', A.`id_shifting`, NULL)) * 20000 AS 'TS',
		COUNT(IF(A.`id_shifting`='M', A.`id_shifting`, NULL)) * 23000 AS 'TM',
		COUNT(IF(A.`id_shifting`='P', A.`id_shifting`, NULL)) * 17000 + COUNT(IF(A.`id_shifting`='S', A.`id_shifting`, NULL)) * 20000 + COUNT(IF(A.`id_shifting`='M', A.`id_shifting`, NULL)) * 23000 AS 'jumlah'
		FROM `schedule` A
		LEFT JOIN `tbu_user` B ON A.user_id = B.id
		WHERE A.`tanggal` BETWEEN '2020-02-01' AND '2020-02-29'
		GROUP BY A.user_id
		ORDER BY B.nama_lengkap";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function approval_overtime()
	{ //umar
		$q = "SELECT A.`id`, B.id id_user, B.`nama_lengkap`, A.`start_datetime`, A.`end_datetime`, A.`alasan`, A.`status`, A.`reviewed_date` 
		FROM `tbu_lembur` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id`  
		ORDER BY A.`status` ASC";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function approval_perjalanan_dinas()
	{ //umar
		$q = "

		SELECT A.*, B.*, (A.`jumlah`-1)*C.`penginapan` AS penginapan, A.`jumlah`*C.`biaya_makan` AS makan, A.`jumlah`*C.`uang_saku` AS uangsaku, A.`jumlah`*C.`angkutan_setempat` AS angkutan, (A.`jumlah`-1)*C.`ke_bandara` AS tobandara, 
		(((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+(A.`harga_tiket`)) AS total
		FROM `tbu_perjalanandinas` A
		LEFT JOIN `tbu_user` B ON A.`id_user`=B.`id`
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		ORDER BY A.`status` ASC";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}
}
