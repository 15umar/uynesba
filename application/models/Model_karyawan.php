<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_karyawan extends CI_Model
{

	public function get_telat()
	{ //umarkoto
		$q = "SELECT D.nama_lengkap, D.lokasi, B.os, B.ip, A.id_shifting, A.user_id, A.tanggal, B.jam jam_masuk, C.jam jam_pulang, B.ketepatan FROM `schedule` A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1') B ON A.`user_id` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2') C ON A.`user_id` = C.`id_user` AND A.`tanggal` = C.`tanggal`
		LEFT JOIN tbu_user D ON A.user_id = D.id
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) AND B.tanggal = DATE(DATE_ADD(timestamp(now()), INTERVAL 7 HOUR)) AND D.active = '1' AND user_id='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_correction($user)
	{ //umarkoto
		$q = "SELECT DISTINCT  E.tanggal,A.tanggal, C.pekerjaan, D.nama_lengkap, D.lokasi, A.os, A.ip, E.id_shifting,  B.jam jam_masuk, C.jam jam_pulang,B.fotoabsen masuk, C.fotoabsen keluar FROM tbu_absensi A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1'  GROUP BY id_abs) B ON A.`id_user` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2'  GROUP BY id_abs) C ON A.`id_user` = C.`id_user` AND A.`tanggal` = C.`tanggal`
		LEFT JOIN tbu_user D ON A.id_user = D.id
		LEFT JOIN (SELECT * FROM `schedule`  GROUP BY id ) E ON E.user_id=A.id_user AND E.tanggal=A.tanggal
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) 
		AND A.Tanggal BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()),INTERVAL 1 DAY),INTERVAL -1 MONTH) 
		AND DATE_ADD(TIMESTAMP(NOW()), INTERVAL 11 HOUR) AND A.id_user='" . $user . "'
		GROUP BY B.fotoabsen
		ORDER BY A.tanggal DESC";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function pencarianbulanan($tglawal, $tglakhir)
	{ //umarkoto
		$q = "SELECT DISTINCT A.pekerjaan,E.tanggal,A.tanggal, D.nama_lengkap, D.lokasi, A.os, A.ip, E.id_shifting,  B.jam jam_masuk, C.jam jam_pulang,B.fotoabsen masuk, C.fotoabsen keluar FROM tbu_absensi A
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1'  GROUP BY id_abs) B ON A.`id_user` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
		LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2'  GROUP BY id_abs) C ON A.`id_user` = C.`id_user` AND A.`tanggal` = C.`tanggal`
		LEFT JOIN tbu_user D ON A.id_user = D.id
		LEFT JOIN (SELECT * FROM `schedule`  GROUP BY id ) E ON E.user_id=A.id_user AND E.tanggal=A.tanggal
		WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) 
		AND A.Tanggal BETWEEN '" . $tglawal . "' AND '" . $tglakhir . "' AND A.id_user='" . $this->session->userdata('id_user') . "'
		GROUP BY B.fotoabsen
		ORDER BY A.tanggal DESC";
		$query = $this->db->query($q);
		return $query->result();
	}




	// public function get_telat(){ //umarkoto
	// 	$q="SELECT D.nama_lengkap, D.lokasi, A.os, A.ip, E.id_shifting, A.tanggal, B.jam jam_masuk, C.jam jam_pulang FROM tbu_absensi A
	// 	LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '1') B ON A.`id_user` = B.`id_user` AND A.`tanggal` = B.`tanggal` 
	// 	LEFT JOIN (SELECT * FROM tbu_absensi WHERE kode_abs = '2') C ON A.`id_user` = C.`id_user` AND A.`tanggal` = C.`tanggal`
	// 	LEFT JOIN tbu_user D ON A.id_user = D.id
	// 	LEFT JOIN (SELECT * FROM `schedule` WHERE tanggal = DATE(DATE_ADD(TIMESTAMP(NOW()), INTERVAL 7 HOUR))) E ON E.user_id=A.id_user
	// 	WHERE (B.kode_abs = '1' OR B.kode_abs IS NULL) AND B.tanggal = DATE(DATE_ADD(TIMESTAMP(NOW()), INTERVAL 7 HOUR)) AND D.active = '1' 
	// 	AND A.id_user='";
	// 	$query = $this->db->query($q);
	// 	return $query->result();
	// }

	public function get_umarkoto()
	{ //umarkoto
		$q = "SELECT a.realisasi, c.user_id,c.tanggal,a.tanggal,b.nama_lengkap, c.id_shifting, a.kode_abs,
		IF(a.kode_abs='1', a.jam, 0) AS masuk,
		IF(a.kode_abs='2', a.jam, 0) AS keluar

		FROM `tbu_absensi` a
		LEFT JOIN `tbu_user` b ON b.id=a.id_user
		LEFT JOIN (SELECT * FROM `schedule` WHERE tanggal = DATE(DATE_ADD(TIMESTAMP(NOW()), INTERVAL 11 HOUR))) c ON c.user_id=a.id_user
		WHERE a.tanggal = curdate() AND id_user='" . $this->session->userdata('id_user') . "'
		GROUP BY a.id_abs";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jadwal()
	{ //umarkoto
		$q = "SELECT * FROM `schedule` a
		LEFT JOIN `kode_absen` b ON b.kode=a.id_shifting
		WHERE a.user_id='" . $this->session->userdata('id_user') . "' AND a.tanggal = curdate()";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function getAllMember()
	{ //umarkoto
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT id jumlah FROM tbu_user WHERE organisasi='" . $divisi . "' and level='3' and `active`='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function ambiljadwal($tanggal)
	{
		$q = "SELECT a.user_id AS nip, b.nama_lengkap,a.id_shifting,tanggal FROM `schedule` a, `tbu_user` b 
		WHERE a.user_id=b.id AND a.user_id='" . $this->session->userdata('id_user') . "' and tanggal ='" . $tanggal . "' ";
		$query = $this->db->query($q);
		return $query->result();
	}

	function get_lemburId($id)
	{ //umar
		$q = "SELECT * FROM tbu_lembur a 
		LEFT JOIN tbu_user b ON b.id=a.id_user
		LEFT JOIN level d ON d.id=b.level 
		LEFT JOIN organisasi e ON e.id_personal=b.organisasi 
		WHERE a.id = '" . $id . "'";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
		} else {
			return $q;
		}
		$this->db->close();
	}

	// function get_cutiId($id){ //umar
	// 	$q = "SELECT *, c.name as jeniscuti FROM tbu_cuti a 
	// 	LEFT JOIN tbu_user b ON b.id=a.id_user
	// 	LEFT JOIN tbu_jeniscuti c ON c.id=a.jenis_leave
	// 	LEFT JOIN level d ON d.id=b.level 
	// 	LEFT JOIN organisasi e ON e.id_personal=b.organisasi 
	// 	WHERE a.id = '".$id."'";
	// 	$query = $this->db->query($q);
	// 	if($query){
	// 		return $query->result();
	// 	}else{
	// 		return $q;
	// 	}
	// 	$this->db->close();
	// }

	public function get_available($tanggal, $jdwl)
	{
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT a.user_id AS nip, b.nama_lengkap,a.id_shifting,tanggal FROM `schedule` a, `tbu_user` b 
		WHERE b.`organisasi`='" . $divisi . "' and a.user_id=b.id AND tanggal ='" . $tanggal . "' and a.id_shifting !='" . $jdwl . "' order by a.id_shifting ";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_slip($user)
	{ //umar
		$q = "SELECT * FROM `tbu_slip` A LEFT JOIN tbu_user B ON A.`nik`=B.`no_induk` WHERE B.`id` = '" . $user . "'";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function getAppCuti()
	{ //umarkoto
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_cuti a LEFT JOIN tbu_user b  ON a.`id_user`=b.`id`  WHERE organisasi='" . $divisi . "' AND jenis_leave in ('1','2','3','12') AND status='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahCuti()
	{ //umarkoto
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_cuti a LEFT JOIN tbu_user b  ON a.`id_user`=b.`id` WHERE organisasi='" . $divisi . "' and jenis_leave in ('1','2','3','12') AND status='3'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppSakit()
	{ //umarkoto
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_cuti a LEFT JOIN tbu_user b  ON a.`id_user`=b.`id` WHERE organisasi='" . $divisi . "' and jenis_leave='11' AND status='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahSakit()
	{ //umarkoto
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_cuti a LEFT JOIN tbu_user b  ON a.`id_user`=b.`id` WHERE organisasi='" . $divisi . "' and jenis_leave='11' AND status='3'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getJumlahLembur()
	{ //umarkoto
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_lembur a LEFT JOIN tbu_user b  ON a.`id_user`=b.`id` WHERE organisasi='" . $divisi . "' and status='3'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function getAppLembur()
	{ //umarkoto
		$divisi = $this->session->userdata('organisasi');
		$q = "SELECT * FROM tbu_lembur a LEFT JOIN tbu_user b  ON a.`id_user`=b.`id` WHERE organisasi='" . $divisi . "' and status='1'";
		$query = $this->db->query($q);
		return $query->num_rows();
	}

	public function get_absen($user)
	{ //umarkoto
		// $divisi = $this->session->userdata('organisasi');
		$q = "SELECT 
		SUM(CASE WHEN kode_abs = '1' THEN 1 END) AS masuk,
		SUM(CASE WHEN kode_abs = '2' THEN 1 END) AS pulang
		FROM tbu_absensi LEFT JOIN tbu_user ON id_user=id WHERE `id_user` = '" . $user . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function approval_official_exchange($user)
	{
		$divisi = $this->session->userdata('organisasi');

		$q = "SELECT A.id, A.id_user, B.`nama_lengkap` AS nama_user, A.`id_pengganti`, C.`nama_lengkap` AS nama_pengganti, A.`shift_pemohon`, A.`shift_pengganti`, A.`tanggal`, A.`alasan`, A.`status`  
		FROM `tbu_tukarjadwal` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_user C ON A.`id_pengganti`=C.`id` 
		WHERE (id_user = '" . $user . "' OR id_pengganti = '" . $user . "') ORDER BY A.`status`, A.`tanggal` ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function review_approval($kode, $id, $user_id)
	{
		if ($kode == '1') { //OVERTIME	
			$q = "UPDATE `lembur` SET `status` = '2', `reviewed_by` = '" . $user_id . "', `reviewed_date` = NOW() WHERE id = '" . $id . "'";
		} else if ($kode == '2') { //OFFICIAL EXCHANGE
			$q = "UPDATE `tbu_tukarjadwal` SET `status` = '1' WHERE id = '" . $id . "'";
		} else if ($kode == '3') { //LEAVE
			$q = "UPDATE `cuti` SET `status` = '2', `reviewed_by` = '" . $user_id . "', `reviewed_date` = NOW() WHERE id = '" . $id . "'";
		}
		$query = $this->db->query($q);
		if ($query) {
			return true;
		} else {
			return false;
		}
		$this->db->close();
	}





	public function get_cutiumar()
	{ //umarkoto
		$q = "SELECT 12-SUM(jumlah_leave) as sisa_cutitahunan FROM tbu_cuti WHERE status='3' and jenis_leave='1' and start_date BETWEEN '2019-11-01' AND '2020-11-30' and id_user='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_dinas()
	{ //umarkoto
		$q = "SELECT * FROM tbu_perjalanandinas WHERE referensi='1' and status IN ('3') and id_user='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}



	public function get_sisa_cutiibadah()
	{ //umarkoto
		$q = "SELECT 3-SUM(jumlah_leave) as sisa_cutiibadah FROM tbu_cuti WHERE status='3' and jenis_leave='3' and start_date BETWEEN '2019-11-01' AND '2020-11-30' and id_user='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jeniscuti()
	{ //umarkoto
		$q = "select * from tbu_jeniscuti WHERE tipe_ = '1' AND status = '1'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jeniscutikhusus()
	{ //umarkoto
		$q = "select * from tbu_jeniscuti WHERE tipe_ = '2' AND status = '1'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jenislembur()
	{ //umarkoto
		$q = "select * from tbu_jenislembur WHERE status = '1'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jenisizin()
	{ //umarkoto
		$q = "select * from tbu_jeniscuti WHERE tipe_ = '2' AND status = '1'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_kontrak()
	{ //umarkoto
		$q = "SELECT * FROM
		(SELECT
		no_induk,
		awal_kontrak,
		nama_lengkap,
		akhir_kontrak,
		akhir_kontrak - INTERVAL 7 DAY AS day_reminder,
		CURDATE() AS tanggal_sekarang,
		DATEDIFF(akhir_kontrak, CURDATE()) AS due_date,
		active AS status_asli,
		CASE
		WHEN DATEDIFF(akhir_kontrak, CURDATE()) <= 7 THEN '1' ELSE '0' END AS show_notif 
		FROM tbu_user WHERE active != '0' AND DATEDIFF(akhir_kontrak, CURDATE()) >= 0) a
		WHERE show_notif = 1";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_reminderlembur()
	{ //umarkoto
		$q = "SELECT b.nama_lengkap, a.`insert` AS umar,id_user, due_date FROM 
		(SELECT id_user,`status`, jenis_lembur, `insert`, CURDATE() AS tanggal_sekarang, DATEDIFF(`insert`, CURDATE()) AS due_date,
		CASE WHEN DATEDIFF(`insert`, CURDATE()) <= -7 THEN '1' ELSE '0' END AS show_notif
		FROM tbu_lembur WHERE `status` = '1' OR `status`='2' AND DATEDIFF(`insert`, CURDATE()) <= 0) a
		LEFT JOIN `tbu_user` b ON b.id=a.id_user
		WHERE show_notif = 1";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_remindersppd()
	{ //umarkoto
		$q = "SELECT b.nama_lengkap, a.`tanggal_pergi` AS umar,id_user, due_date FROM 
		(SELECT id_user,`status`, `tanggal_pergi`, CURDATE() AS tanggal_sekarang, DATEDIFF(`tanggal_pergi`, CURDATE()) AS due_date,
		CASE WHEN DATEDIFF(`tanggal_pergi`, CURDATE()) <= -7 THEN '1' ELSE '0' END AS show_notif
		FROM `tbu_perjalanandinas` WHERE `status` = '1' OR `status`='2' AND DATEDIFF(`tanggal_pergi`, CURDATE()) <= 0) a
		LEFT JOIN `tbu_user` b ON b.id=a.id_user
		WHERE show_notif = 1";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jml_sakit()
	{ //umarkoto
		$q = "SELECT SUM(jumlah_leave) AS jml_sakit FROM tbu_cuti WHERE STATUS='3' AND jenis_leave='11' AND start_date BETWEEN '2020-01-01' AND '2020-12-31' AND id_user='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jml_melahirkan()
	{ //umarkoto
		$q = "SELECT SUM(jumlah_leave) AS jml_melahirkan FROM tbu_cuti WHERE STATUS='3' AND jenis_leave='2' AND start_date BETWEEN '2020-01-01' AND '2020-12-31' AND id_user='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jml_ibadah()
	{ //umarkoto
		$q = "SELECT SUM(jumlah_leave) AS jml_ibadah FROM tbu_cuti WHERE STATUS='3' AND jenis_leave='3' AND start_date BETWEEN '2020-01-01' AND '2020-12-31' AND id_user='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_jml_izin()
	{ //umarkoto
		$q = "SELECT SUM(jumlah_leave) AS jml_sakit FROM tbu_cuti WHERE STATUS='3' AND jenis_leave in('12') AND start_date BETWEEN '2019-11-01' AND '2020-11-30' AND id_user='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	function jalankan_query_manual($datainput)
	{ //umarkoto
		$q = $this->db->query($datainput);
	}

	public function save_cuti($data)
	{ //umarkoto
		$this->db->insert('tbu_cuti', $data);
	}

	public function get_email($divisi, $level)
	{ //umarkoto
		$q = "select email from tbu_user WHERE organisasi = '" . $divisi . "' and level='" . $level . "' AND active='1' ";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_profile()
	{ //umarkoto
		$this->db->where('id', $this->session->userdata('id_user'));
		$query = $this->db->get('tbu_user');
		return $query->result();
	}

	public function save_lembur($data)
	{ //umarkoto
		$this->db->insert('tbu_lembur', $data);
	}

	public function insert($data)
	{
		$this->db->insert('tbu_perjalanandinas', $data);

		return ($this->db->affected_rows() > 0) ? true : false;
	}

	public function save_sppd($data)
	{ //umarkoto
		$this->db->insert('tbu_perjalanandinas', $data);
	}

	public function report_leave($user)
	{ //umar
		$q = "SELECT A.`id` AS id_user, A.`jenis_leave`,A.`jenis_cutikhusus`, B.id, B.`nama_lengkap`, C.`name` jenis_cuti, A.`tanggal_input`, A.`start_date`, A.`alamat_cuti`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'12') sisa_cuti, A.`keterangan`, A.`status`, A.`reviewed_by`, A.`approved_by`, A.`rejected_by`, A.`rejected_reason` 
		FROM `tbu_cuti` A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (12 - SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='1' AND start_date BETWEEN '2021-01-01' AND '2021-12-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` IN ('1','2','3','12') AND B.`id` = '" . $user . "' 
		ORDER BY A.`id` DESC";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function get_sisa_cutitahunan()
	{ //umarkoto
		$q = "SELECT 12-SUM(jumlah_leave) as sisa_cutitahunan FROM tbu_cuti WHERE status='3' and jenis_leave='1' and start_date BETWEEN '2021-01-01' AND '2021-12-30' and id_user='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function get_sisa_cutimelahirkan()
	{ //umarkoto
		$q = "SELECT 92-SUM(jumlah_leave) as sisa_cutimelahirkan FROM tbu_cuti WHERE status='3' and jenis_leave='2' and start_date BETWEEN '2020-01-01' AND '2020-12-30' and id_user='" . $this->session->userdata('id_user') . "'";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function report_sakit($user)
	{ //umar
		$q = "SELECT A.`id` AS id_user, B.id, B.`nama_lengkap`, C.`name` jenis_cuti, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.`keterangan`, A.`status`, A.`reviewed_by`, A.`approved_by`, A.`rejected_by`, A.`rejected_reason` 
		FROM `tbu_cuti` A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='11' AND start_date BETWEEN '2019-11-01' AND '2020-11-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` = 11 AND B.`id` = '" . $user . "' ORDER BY A.`id` desc";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function report_izin($user)
	{ //umar
		$q = "SELECT A.`id` AS id_user, B.id, B.`nama_lengkap`, C.`name` jenis_cuti, A.`tanggal_input`, A.`start_date`, A.`end_date`, A.jumlah_leave, IFNULL(D.sisa_cuti,'0') sisa_cuti, A.`keterangan`, A.`status`, A.`reviewed_by`, A.`approved_by`, A.`rejected_by`, A.`rejected_reason` 
		FROM `tbu_cuti` A LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_jeniscuti C ON A.`jenis_leave` = C.`id` 
		LEFT JOIN (SELECT id_user, (SUM(jumlah_leave)) sisa_cuti 
		FROM tbu_cuti WHERE STATUS = '3' and jenis_leave='4' AND start_date BETWEEN '2019-11-01' AND '2020-11-30' 
		GROUP BY id_user) D ON A.`id_user` = D.`id_user` WHERE A.`jenis_leave` not in ('1','2','3','11') AND B.`id` = '" . $user . "' ORDER BY A.`status`, A.`tanggal_input` ASC";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function report_overtime($user)
	{ //umar
		$q = "SELECT A.`id`, B.id id_user, B.`nama_lengkap`, A.`start_datetime`, C.`name` jenis_lembur, A.`end_datetime`, A.`alasan`, A.`status`, A.`reviewed_by`, A.`approved_by`, A.`rejected_by`, A.`rejected_reason` 
		FROM `tbu_lembur` A 
		LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		LEFT JOIN tbu_jenislembur C ON C.`id`=A.`jenis_lembur`
		WHERE A.`status` > 0 AND B.`id` = '" . $user . "' ORDER BY A.`id` desc";

		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function report_perjalanan_dinas($user)
	{ //umar
		$q = "SELECT A.*, B.*, (A.`jumlah`-1)*C.`penginapan` AS penginapan, A.`jumlah`*C.`biaya_makan` AS makan, A.`jumlah`*C.`uang_saku` AS uangsaku, A.`jumlah`*C.`angkutan_setempat` AS angkutan, (A.`jumlah`-1)*C.`ke_bandara` AS tobandara, 
		(((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+(A.`harga_tiket`)) AS total,
		(((A.`jumlah`)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)) AS totall,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` + ((A.`jumlah`)*C.`biaya_makan`) +
		((A.`jumlah`)*C.`uang_saku`) + ((A.`jumlah`)*C.`angkutan_setempat`) + (IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) +
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) + (A.`harga_tiket`) AS totalsppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN `tbu_user` B ON A.`id_user`=B.`id`
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi`
		WHERE A.`status` > 0 AND B.`id` = '" . $user . "'
		ORDER BY A.`id_dinas` desc";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
			$this->db->close();
		} else {
			return "Error has occurred";
		}
	}

	public function get_cuti($id)
	{ //umar
		$this->db->order_by('tbu_perjalanandinas.id_dinas', 'DESC');
		$this->db->join('tbu_user', 'tbu_user.id=tbu_perjalanandinas.id_user');
		$this->db->join('tbu_kategori_sppd', 'tbu_kategori_sppd.id_transportasi=tbu_perjalanandinas.transportasi');
		$this->db->where('id_dinas', $id);
		$query = $this->db->get('tbu_perjalanandinas');
		return $query->result();

		// $q="SELECT * FROM `tbu_perjalanandinas` A 
		// LEFT JOIN tbu_user B ON A.`id_user`=B.`id` 
		// LEFT JOIN tbu_kategori_sppd C ON C.`id_transportasi`=A.`transportasi`
		// WHERE A.`id_dinas`, $id";
		// $query = $this->db->query($q);
		// if($query){
		// 	return $query->result();
		// 	$this->db->close();
		// }else{
		// 	return "Error has occurred";
		// }
	}

	function get_cutiId($id)
	{ //umar
		$q = "SELECT *, c.name as jeniscuti FROM tbu_cuti a 
		LEFT JOIN tbu_user b ON b.id=a.id_user
		LEFT JOIN tbu_jeniscuti c ON c.id=a.jenis_leave
		LEFT JOIN level d ON d.id=b.level 
		LEFT JOIN organisasi e ON e.id_personal=b.organisasi 
		WHERE a.id = '" . $id . "'";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
		} else {
			return $q;
		}
		$this->db->close();
	}

	function get_cutitot($date)
	{ //umar
		$q = "SELECT SUM((end_date-start_date)+1) AS total FROM tbu_cuti WHERE id_user ='" . $this->session->userdata('id_user') . "' AND STATUS='3' AND end_date BETWEEN '2018-09-01' AND '" . $date . "' AND jenis_leave IN ('1')";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
		} else {
			return $q;
		}
		$this->db->close();
	}

	public function get_cutisisa($date)
	{ //umar
		$q = "SELECT 12-SUM((end_date-start_date)+1) AS total FROM tbu_cuti WHERE id_user ='" . $this->session->userdata('id_user') . "' AND STATUS='3' AND end_date BETWEEN '2018-09-01' AND '" . $date . "' AND jenis_leave IN ('1')";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
		} else {
			return $q;
		}
		$this->db->close();
	}

	function get_dinasId($id)
	{ //umar
		$q = "SELECT C.`ke_terminal`, C.`id_transportasi`, C.`nama_transportasi`, A.*, B.*, (A.`jumlah`-1)*C.`penginapan` AS totalpenginapan,A.`jumlah`*C.`penginapan` AS totalpenginapann,C.`penginapan`, A.`jumlah`*C.`biaya_makan` AS makan, C.`biaya_makan`, A.`jumlah`*C.`uang_saku` AS uangsaku, C.`uang_saku`, A.`jumlah`*C.`angkutan_setempat` AS angkutan, C.`angkutan_setempat`, 
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) AS tobandara,
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) AS toterminal, 
		C.`ke_bandara`,
		(((A.`jumlah`-1)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)+((A.`jumlah`-1)*C.`ke_bandara`)+(A.`harga_tiket`)) AS total,
		(((A.`jumlah`)*C.`penginapan`)+(A.`jumlah`*C.`biaya_makan`)+(A.`jumlah`*C.`uang_saku`)+(A.`jumlah`*C.`angkutan_setempat`)) AS totall,
		(IF(A.jumlah='1', A.jumlah, 0)) * C.`penginapan` + (IF(A.jumlah>'1', (A.jumlah - 1), 0)) * C.`penginapan` + ((A.`jumlah`)*C.`biaya_makan`) +
		((A.`jumlah`)*C.`uang_saku`) + ((A.`jumlah`)*C.`angkutan_setempat`) + (IF(A.pengajuan='baru', 1, 0)) * C.`ke_bandara` + (IF(A.pengajuan='perpanjangan', 0, 0)) +
		(IF(A.pengajuan='baru', 1, 0)) * C.`ke_terminal` + (IF(A.pengajuan='perpanjangan', 0, 0)) + (A.`harga_tiket`) AS totalsppd
		FROM `tbu_perjalanandinas` A
		LEFT JOIN `tbu_user` B ON A.`id_user`=B.`id`
		LEFT JOIN `tbu_kategori_sppd` C ON C.`id_transportasi`=A.`transportasi` 
		WHERE A.`id_dinas` = '" . $id . "'";
		$query = $this->db->query($q);
		if ($query) {
			return $query->result();
		} else {
			return $q;
		}
		$this->db->close();
	}
}
